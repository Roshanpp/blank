from django.core.files.base import ContentFile
import base64
import six
import imghdr
import base64
from PIL import Image
from io import BytesIO



def decode_base64_file(data, filename,file_type):
    # Check if this is a base64 string
    print("outside")
    if isinstance(data, six.string_types):
        print("inside")
        # Check if the base64 string is in the "data:" format
        # if 'data:' in data and ';base64,' in data:
        #     # Break out the header from the base64 content
        #     header, data = data.split(';base64,')

        # Try to decode the file. Return validation error if it fails.
        try:
            decoded_file  = convertToJpeg(Image.open(BytesIO(base64.b64decode(data))).convert('RGB'),file_type)
        except TypeError:
            TypeError('invalid_image')

        # Generate file name:
        file_name = str(filename)
        # Get the file name extension:
        file_extension = get_file_extension(file_name, decoded_file)

        complete_file_name = "%s.%s" % (file_name, file_type, )

        return ContentFile(decoded_file, name=complete_file_name)


def get_file_extension(file_name, decoded_file):
    extension = imghdr.what(file_name, decoded_file)
    extension = "jpg" if extension == "jpeg" else extension

    return extension

def convertToJpeg(im,file_type):
    with BytesIO() as f:
        if file_type=='jpg':
            file_type='jpeg'
        im.save(f, format=file_type.upper())
        return f.getvalue()