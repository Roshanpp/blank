from django.apps import AppConfig


class DmsloaderConfig(AppConfig):
    name = 'DMSloader'
