from django.shortcuts import render,reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.core.paginator import Paginator

from DMSapp.models import FileUpload
import base64
from .decode_base64_file import decode_base64_file, get_file_extension,convertToJpeg
from django.conf import settings
from django.core.files.base import ContentFile
import os
from datetime import datetime
from .models import Workbooks
from django.http import HttpResponse
from PyPDF2 import PdfFileWriter, PdfFileReader
import io
import mammoth
from bs4 import BeautifulSoup

from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter
import openpyxl
from django.http import JsonResponse
from django.core import serializers
from django.contrib.auth.decorators import login_required
import json
from django.template.loader import render_to_string
from django.db.models import Q
from DMSapp.pagination import paginate
from django.contrib import messages

@login_required(login_url='/login/')
@csrf_exempt
def user_file(request, id):
    user_last_login = request.user.last_login
    files = FileUpload.objects.filter(id=id)
    type=files[0].file_type
    try:
        if type in ["png","jpg","jpeg","gif"]:  #Image Editor
            context = {'files': files,'last_login':user_last_login}
            return render(request, 'image_editor.html',context)

        elif type=='pdf':    #Pdf Editor
            existing_pdf = PdfFileReader(open(settings.MEDIA_ROOT + '/' + str(files[0].File1), 'rb'), strict=False)
            total_page=existing_pdf.numPages
            context0 =  {'files': files,'total_page':total_page,'last_login':user_last_login}
            return render(request, 'pdf_editor.html',context0)

        elif type=='xlsx':  #Excel Editor
            context01 = {'files': files,'last_login':user_last_login}
            return render(request, 'excel_editor.html', context01)

        elif type in ["docx"]:   #Document Editor
            file = FileUpload.objects.get(id=id)
            path = file.File1
            file_path = os.path.join(settings.MEDIA_ROOT, str(path))

            with open(file_path, "rb") as docx_file:
                result = mammoth.convert_to_html(docx_file)
                html_data = result.value  # The generated HTML
            context02 =  {'file': file, 'html_data': html_data}
            return render(request, 'doc_editor.html', context02)

        elif type in ["txt"]:    #Text Editor
            file = FileUpload.objects.get(id=id)
            path = file.File1
            file_path = os.path.join(settings.MEDIA_ROOT, str(path))
            contents = open(file_path, "r")
            dir_path = os.path.dirname(file_path)
            with open(dir_path + '/temp.html', "w") as e:
                for lines in contents.readlines():
                    e.write("<p>" + lines + "</p>")
            with open(dir_path + '/temp.html', "r") as e:
                html_code = e.read()
            os.remove(dir_path + '/temp.html')
            contents.close()
            args01  = {'file': file, 'html_code': html_code}
            return render(request, 'text_editor.html',args01)
        else:
            return HttpResponse('invalid file type')
    except:
        print("IN EXCEPTION")
        messages.error(request, 'File is Encrypted')
        return HttpResponseRedirect(reverse('DMSloader:user_file_info'))


@csrf_exempt
def saveAs(request):
    if request.method == 'POST':
        try:
            image_data = request.POST['canvas']
            image_data=image_data.replace('data:image/png;base64,','')
            filename  = request.POST['filename']
            file_type = request.POST['type']
            uploader = request.POST['uploader']
            file=decode_base64_file(image_data, filename,file_type)
            filename = filename  +"."+file_type

            p = FileUpload(Filename=filename, File1=file, Uploader =uploader,
                           file_type=file_type,  user=request.user)
            p.save()
            return HttpResponse('Success')
        except FileUpload.DoesNotExist:
            return HttpResponse('Failure')
    else:
        return HttpResponse('post did not reach')


@csrf_exempt
def save(request):

    if request.method == 'POST':
        try:

            image_data = request.POST['canvas']
            image_data=image_data.replace('data:image/png;base64,','')
            file_id  = request.POST['imageId']
            File = FileUpload.objects.get(id=file_id)
            now=datetime.now()

            File.edited_at=now
            filename=os.path.splitext(File.Filename)[0]
            file_type = File.file_type
            file=decode_base64_file(image_data, filename,file_type)
            os.remove(settings.MEDIA_ROOT + '/' + str(File.File1) )
            File.File1 = file
            File.save()
            return HttpResponse('Success')
        except FileUpload.DoesNotExist:
            return HttpResponse('Failure')
    else:
        return HttpResponse('post did not reach')


@csrf_exempt
def user_file_upload(request):
    if request.method == 'POST':
        try:
            filename = request.POST['save_fname']
            data = request.POST['save_cdata']
            image_data = request.POST['save_image']

            image_split_data = image_data.split("base64,")[1]
            image_decoded = base64.b64decode(image_split_data)
            p = FileUpload(Filename=filename, File1=decode_base64_file(image_data, filename),
                           file_type=get_file_extension(filename, image_decoded),  user=request.user)
            p.save()
            return HttpResponse('Success')
        except FileUpload.DoesNotExist:
            return HttpResponse('Failure')
    else:
        return HttpResponse('post did not reach')


@login_required(login_url='/login/')
@csrf_exempt
def user_file_info(request):
    # if request.user.is_superuser:
        # files = FileUpload.objects.all().order_by("-pk")
    # else:
    files = FileUpload.objects.filter(user_id=request.user,deleted='0').order_by("-pk")

    if request.GET.get('searchVal'):
        searchfor=request.GET.get('searchVal')
        print(searchfor)
        if searchfor!='0' and searchfor!='' and searchfor!='NULL' and searchfor is not None:
            if request.user.is_superuser:
                files = FileUpload.objects.filter(Q(Filename__icontains = searchfor) | Q(Uploader__icontains = searchfor),deleted='0').order_by("-pk")
            else:
                files = FileUpload.objects.filter(Q(Filename__icontains = searchfor) | Q(Uploader__icontains = searchfor),user_id=request.user,deleted='0').order_by("-pk")

        elif searchfor=='0':

            if request.user.is_superuser:
                files = FileUpload.objects.filter(deleted='0').order_by("-pk")
            else:
                files = FileUpload.history.filter(user_id=request.user,deleted='0').order_by("-pk")
        qs_json = serializers.serialize('json', files)
        qs_json=json.loads(qs_json)
        page = request.GET.get('page', 1)
        #paginator = Paginator(qs_json, 5)

        count=0

        for i in qs_json:

            count+=1
            i['fields']['srNo']=str(count)
            if i['fields']['uploaded_at']:
                dateup = i['fields']['uploaded_at']
                dt = datetime.strptime(dateup, "%Y-%m-%dT%H:%M:%S.%f")
                i['fields']['uploaded_at'] = dt
            else:
                i['fields']['uploaded_at'] = ''
            if i['fields']['edited_at']:
                date_edit = i['fields']['edited_at']
                dt1 = datetime.strptime(date_edit, "%Y-%m-%dT%H:%M:%S.%f")
                i['fields']['edited_at'] = dt1
            else:
                i['fields']['edited_at'] = ''

        qs_json = paginate(request,qs_json,page)
        return edit_search_list(request, qs_json, 'user_file_info.html')
    elif request.GET.get('searchFOR'):
        searchfor=request.GET.get('searchFOR')
        if request.user.is_superuser:
            files = FileUpload.objects.filter(Q(Filename__icontains = searchfor) | Q(Uploader__icontains = searchfor),deleted='0').order_by("-pk")
        else:
            files = FileUpload.objects.filter(Q(Filename__icontains = searchfor) | Q(Uploader__icontains = searchfor),user_id=request.user,deleted='0').order_by("-pk")
        qs_json = serializers.serialize('json', files)
        qs_json=json.loads(qs_json)


        page = request.GET.get('page')
        #paginator = Paginator(qs_json, 5)
        count=0

        for i in qs_json:

            count+=1
            i['fields']['srNo']=str(count)
            if i['fields']['uploaded_at']:
                dateup = i['fields']['uploaded_at']
                dt = datetime.strptime(dateup, "%Y-%m-%dT%H:%M:%S.%f")
                i['fields']['uploaded_at'] = dt
            else:
                i['fields']['uploaded_at'] = ''
            if i['fields']['edited_at']:
                date_edit = i['fields']['edited_at']
                dt1 = datetime.strptime(date_edit, "%Y-%m-%dT%H:%M:%S.%f")
                i['fields']['edited_at'] = dt1
            else:
                i['fields']['edited_at'] = ''



        qs_json = paginate(request,qs_json,page)
        return edit_search_list(request, qs_json, 'user_file_info.html')

    else:
        qs_json = serializers.serialize('json', files)
        qs_json = json.loads(qs_json)
        page = request.GET.get('page', 1)
        #paginator = Paginator(qs_json, 5)

        count = 0
        for i in qs_json:
            count += 1
            i['fields']['srNo'] = str(count)
            if i['fields']['uploaded_at']:
                dateup = i['fields']['uploaded_at']
                dt = datetime.strptime(dateup, "%Y-%m-%dT%H:%M:%S.%f")
                i['fields']['uploaded_at'] = dt
            else:
                i['fields']['uploaded_at'] = ''
            if i['fields']['edited_at']:
                date_edit = i['fields']['edited_at']
                dt1 = datetime.strptime(date_edit, "%Y-%m-%dT%H:%M:%S.%f")
                i['fields']['edited_at'] = dt1
            else:
                i['fields']['edited_at'] = ''

        qs_json = paginate(request,qs_json,page)

        context03 = {'files': qs_json}
        return render(request, 'user_file_info.html',context03)


def edit_search_list(request, files, template_name):
    data = dict()
    if request.method == 'GET':
        data['form_is_valid'] = True
        context = {'files': files}
        data['html_book_list'] = render_to_string('loader_files/user_edit_files.html',context, request=request)

        data['page_div'] = render_to_string('files/pagination.html',context, request=request)
    data['html_form'] = render_to_string(template_name, request=request)
    return JsonResponse(data)


@login_required(login_url='/login/')
def editor1(request):
    return render(request, 'test.html')


def config(request):
    return render(request, 'config.html')


def watermarks(request):
    if request.method == 'POST':
        try:

            file_id = request.POST['url']
            stamptype=request.POST['stamptype']
            region= request.POST['region']

            allPages= request.POST['allPages']

            File = FileUpload.objects.get(id=file_id)
            now=datetime.now()
            File.edited_at=now
            File.save()
            packet = io.BytesIO()
            import decimal

            can = canvas.Canvas(packet, pagesize=letter)
            existing_pdf = PdfFileReader(open(settings.MEDIA_ROOT + '/' + str(File.File1), 'rb'),strict=False)
            cord1= existing_pdf.getPage(0).mediaBox.upperLeft
            cord2 = existing_pdf.getPage(0).mediaBox.upperRight
            cord3 = existing_pdf.getPage(0).mediaBox.lowerLeft
            cord4 = existing_pdf.getPage(0).mediaBox.lowerRight

            x1=int(cord1[0])
            y1=int(cord1[1])
            x2 = int(cord2[0])
            y2 = int(cord2[1])
            x3 = int(cord3[0])
            y3 = int(cord3[1])
            x4 = int(cord4[0])
            y4 = int(cord4[1])

            print(x1, y1)

            if(stamptype=="pending"):
                image_url= settings.STATIC_DIR + '/assets/images/stickers/animals/2.png'
                # can.backColor = "#FFFF00"
                can.setFillColorRGB(1, 0, 0)
            elif stamptype=="approved":
                image_url = settings.STATIC_DIR + '/assets/images/stickers/animals/1.png'
                can.setFillColorRGB(0, 1, 0)
            elif stamptype=="rejected":
                image_url = settings.STATIC_DIR + '/assets/images/stickers/animals/3.png'
                can.setFillColorRGB(1, 0, 0)
              # choose your font colour

            can.setFont("Times-Bold",8)
            now = datetime.now()  # for date
            if region=="Top Left":
                can.drawImage(image_url, x1, y1-60, width=100, height=60, mask='auto')
                #can.drawString(x1+10, y1 - 50, now.strftime(" [%d-%m-%Y] [%H:%M]"))
            elif region=="Top Right":
                can.drawImage(image_url, x2-110, y1-60, width=100, height=60, mask='auto')
                #can.drawString(x2-100, y1 - 35, now.strftime(" [%d-%m-%Y] [%H:%M]"))
            elif region=="Top Center":
                can.drawImage(image_url, x2/2-50, y2-60, width=100, height=60, mask='auto')
            #    can.drawString(x2/2-40 , y2 - 35, now.strftime(" [%d-%m-%Y] [%H:%M]"))
            elif region=="Center":
                print("type of x2 ", x2 ,type(x2))
                can.drawImage(image_url, (x2/2)-40, y2/2-40, width=100, height=65, mask='auto')
                #can.drawString(x2/2-40 , y2/2 - 35, now.strftime(" [%d-%m-%Y] [%H:%M]"))
            elif region=="Bottom Left":
                can.drawImage(image_url, 0, 0, width=100, height=60, mask='auto')
                #can.drawString(10, 5, now.strftime(" [%d-%m-%Y] [%H:%M]"))
            elif region=="Bottom Right":
                can.drawImage(image_url, x4-105, y4,  mask='auto')
                #can.drawString(x4-100, y4+5, now.strftime(" [%d-%m-%Y] [%H:%M]"))
            elif region=="Bottom Center":
                can.drawImage(image_url, x4/2-50, y4, width=100, height=60, mask='auto')
                #can.drawString((x4/2)-60 , y4+5, now.strftime(" [%d-%m-%Y] [%H:%M]"))
            can.save()
            packet.seek(0)
            new_pdf = PdfFileReader(packet)
            output = PdfFileWriter()

            if allPages=="true":

                for page in range(existing_pdf.numPages):
                    page = existing_pdf.getPage(page)
                    page.mergePage(new_pdf.getPage(0))
                    output.addPage(page)

                os.remove(settings.MEDIA_ROOT + '/' + str(File.File1))
                outputStream = open(settings.MEDIA_ROOT + '/' + str(File.File1), "wb")
                output.write(outputStream)
                outputStream.close()
            else:
                print("futt")
                PageRange = request.POST['PageRange']
                print("PageRange",PageRange)
                PageRange = list(map(int, PageRange.split("-")))
                star_page=PageRange[0]-1
                end_page=PageRange[1]
                for page in range(0,star_page):
                    page = existing_pdf.getPage(page)
                    # page.mergePage(new_pdf.getPage(0))
                    output.addPage(page)
                for page in range(star_page,end_page):
                    page = existing_pdf.getPage(page)
                    page.mergePage(new_pdf.getPage(0))
                    output.addPage(page)
                for page in range(end_page,existing_pdf.numPages):
                    page = existing_pdf.getPage(page)
                    # page.mergePage(new_pdf.getPage(0))
                    output.addPage(page)
                # finally, write "output" to a real file
                print(File.File1)
                os.remove(settings.MEDIA_ROOT + '/' + str(File.File1))
                outputStream = open(settings.MEDIA_ROOT + '/' + str(File.File1), "wb")
                output.write(outputStream)
                outputStream.close()

            return HttpResponse('Success')
        except FileUpload.DoesNotExist:
            return HttpResponse('Failure')
    else:
        return HttpResponse('post did not reach')



@csrf_exempt
def validate_username1(request):
    import json
    filename = request.POST.get('filename')
    blob = request.FILES['blob']

    print(filename)
    print(blob)
    # d = json.loads(data)
    # print(d['version'], "sheetCount: " ,d['sheetCount'])
    # data = {
    #     'is_taken': data}
    return JsonResponse({'success': True})


def save_as_editor(request, file_id):
    if request.method == 'POST':
        file = FileUpload.objects.get(pk=file_id)
        Filename = str(file.File1).split('/')[-1]
        print(Filename)
        file_path = os.path.join(settings.MEDIA_ROOT, str(file.File1))

        try:
            editor_data = request.FILES['data']

            file = FileUpload.objects.get(pk=file_id)
            os.remove(os.path.join(settings.MEDIA_ROOT, str(file_path)))
            content_object = ContentFile(editor_data.read())
            content_object.content_type = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'
            file.edited_at = datetime.now()
            file.File1.save(Filename, content_object ,save=True)

            return JsonResponse({'Success': True})
        except FileUpload.DoesNotExist:
            return JsonResponse({'Success': False})
    else:
        return HttpResponse('post did not reach')


@csrf_exempt
def save_text_from_editor(request, file_id):
    if request.method == 'POST':
        file = FileUpload.objects.get(pk=file_id)
        file_path = os.path.join(settings.MEDIA_ROOT, str(file.File1))

        try:
            editor_data = request.POST['editor_data']
            os.remove(os.path.join(settings.MEDIA_ROOT, str(file_path)))

            soup = BeautifulSoup(editor_data, 'lxml')
            body = soup.body

            if body is None:
                return None
            for tag in body.select('script'):
                tag.decompose()
            for tag in body.select('style'):
                tag.decompose()

            text = body.get_text()
            final_text = text.lstrip()

            with open(os.path.join(settings.MEDIA_ROOT, str(file_path)), "w") as e:
                e.write(final_text)

            file.edited_at = datetime.now()
            return JsonResponse({'Success': True})
        except FileUpload.DoesNotExist:
            return JsonResponse({'Success': False})

    else:
        return HttpResponse('post did not reach')

