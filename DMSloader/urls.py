from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from django.urls import path

from . import views

urlpatterns = [
    # url(r'^$', views.user_file, name='file_loader'),
    path('file_loader/<int:id>/', views.user_file, name='file_loader'),
    url(r'user_file_uploaded/', views.user_file_upload, name='user_file_uploaded'),
    url(r'user_file_info/', views.user_file_info, name='user_file_info'),
     url(r'editor1/', views.editor1, name='editor1'),
    url(r'config/', views.config, name='config'),
    path('saveAs/', views.saveAs, name='saveAs'),
    path('save/', views.save, name='save'),
    path('watermarks/', views.watermarks, name='watermarks'),

    url(r'^validate_username1/$', views.validate_username1, name='validate_username1'),

    path('save_as_editor/<int:file_id>', views.save_as_editor, name='save_as_editor'),
    path('save_text_from_editor/<int:file_id>', views.save_text_from_editor, name='save_text_from_editor'),

]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
