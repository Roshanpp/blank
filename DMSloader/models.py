from django.db import models


class Files(models.Model):
    name = models.CharField(max_length=20)
    image = models.TextField()
    canvas_image = models.TextField()

    def __unicode__(self):
        return self.name

# Create your models here.

class Workbooks(models.Model):
    workbook_name = models.CharField(max_length=30)
    sheet_name = models.CharField(max_length=30)
    data = models.TextField()