from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from DMSapp import views as DMSapp_views
from DMSContact import views as DMSContact_views
from DMSpages import views as DMSpages_views
from DMSaccount import views as DMSaccount_views
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls.i18n import i18n_patterns

from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from ajax_select import urls as ajax_select_urls

admin.site.site_header = "DMS Admin"
admin.site.site_title = "DMS Admin Portal"
admin.site.index_title = "Welcome to Document Management System!!"

urlpatterns = [
    # path('home/<DMS>', DMSapp_views.home, name='home'),
    url(r'^$', DMSapp_views.home, name='home'),
    path('admin/', admin.site.urls),
    path('admin1', DMSaccount_views.admin, name='admin'),
    path('home/<DMS>', DMSapp_views.home, name='home'),
    path('home/', DMSapp_views.home, name='home'),
    url(r'^ajax_select/', include(ajax_select_urls)),
    path('signup/', DMSaccount_views.signup, name='signup'),
    path('login/', DMSaccount_views.login1, name='user_login'),
    path('password/', DMSaccount_views.change_password, name='change_password'),
    path('user_files/<int:id>/', DMSapp_views.user_files, name='user_files'),
    url(r'^', include(('DMSapp.urls', 'DMSapp'), namespace='DMS')),
    url(r'^', include(('DMSpages.urls', 'DMSpages'), namespace='DMSpages')),
    url(r'^', include(('DMSaccount.urls', 'DMSaccount'), namespace='DMSaccount')),
    url(r'^', include(('DMSloader.urls', 'DMSloader'), namespace='DMSloader')),
    url(r'^', include(('twofactor.urls', 'twofactor'), namespace='two_factor')),
    url(r'^', include(('WorkflowManagement.urls', 'WorkflowManagement'), namespace='WorkflowManagement')),

    path('root_files/<int:pk>/', DMSapp_views.root_files, name='root_files'),

    # for OTP
    path('sendOTP/<id>/', DMSaccount_views.send_otp, name='send_otp'),
    path('resendOTP/<id>/<mobile_no>/', DMSaccount_views.resend_otp, name='resendOTP'),
    path('enterOTP/<id>/<mobile_no>/', DMSaccount_views.enter_otp, name='enterOTP'),
    path('verifyOTP/<id>/<mobile_no>/', DMSaccount_views.verify_otp, name='verifyOTP'),
    # path('detectorAndTrainer/(?P<id>\d+)/',DMSaccount_views.detectorAndTrainer, name='detectorAndTrainer'),
    path('contact/', DMSContact_views.contact, name='contact'),
    path('load_pages/', DMSpages_views.home, name='load_pages'),
    url(r'^ckeditor/', include('ckeditor_uploader.urls')),
    url('i18n/', include('django.conf.urls.i18n')),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += staticfiles_urlpatterns()
urlpatterns += [url(r'^captcha/', include('captcha.urls')), ]


urlpatterns += [url(r'^ckeditor/', include('ckeditor_uploader.urls')), ]
