import ldap
from django_auth_ldap.config import LDAPSearch, GroupOfNamesType

# Baseline configuration.



SERVER_URI = "ldap://192.168.71.10:389"

AUTH_LDAP_BIND_AS_AUTHENTICATING_USER=True
LDAP_BIND_DN = "7255@ccbl.com"
LDAP_BIND_PASSWORD = "Vish2424@"
LDAP_USER_SEARCH = LDAPSearch(
    "ou=ccbl_users,dc=ccbl,dc=com", ldap.SCOPE_SUBTREE, "(sAMAccountName=%(user)s)"
)

AUTH_LDAP_GROUP_SEARCH = LDAPSearch(
    "ou=ccbl_users,dc=ccbl,dc=com",
    ldap.SCOPE_SUBTREE,
    "(objectClass=top)",
 )
AUTH_LDAP_GROUP_TYPE = GroupOfNamesType(name_attr="cn")
