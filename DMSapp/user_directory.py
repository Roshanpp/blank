from datetime import datetime


def user_directory_identity(instance, filename):
    date = datetime.now()

    ext = filename.split('.')[-1]
    filename = str(instance.customer_name) + '_1.' + ext
    return 'customer_details/{0}/{1}/{2}/'.format(date.strftime('%d-%m-%Y'), 'ACS_DMS_' + str(instance.customer_name),
                                                  filename)


def user_directory_pan(instance, filename):
    date = datetime.now()
    ext = filename.split('.')[-1]
    filename = str(instance.customer_name) + '_2.' + ext
    return 'customer_details/{0}/{1}/{2}/'.format(date.strftime('%d-%m-%Y'), 'ACS_DMS_' + str(instance.customer_name),
                                                  filename)


def user_directory_photo(instance, filename):
    date = datetime.now()
    ext = filename.split('.')[-1]
    filename = str(instance.customer_name) + '_3.' + ext
    return 'customer_details/{0}/{1}/{2}/'.format(date.strftime('%d-%m-%Y'), 'ACS_DMS_' + str(instance.customer_name),
                                                  filename)


def user_directory_signature(instance, filename):
    date = datetime.now()
    ext = filename.split('.')[-1]
    filename = str(instance.customer_name) + '_4.' + ext
    return 'customer_details/{0}/{1}/{2}/'.format(date.strftime('%d-%m-%Y'), 'ACS_DMS_' + str(instance.customer_name),
                                                  filename)
