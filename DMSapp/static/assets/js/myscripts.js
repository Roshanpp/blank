function saveAsFunction() {
  swal.withForm({
    title: 'Save Document ',
    text: 'Are You Sure About This',
    showCancelButton: true,
    confirmButtonColor: '#DD6B55',
    confirmButtonText: 'Save !',
    closeOnConfirm: true,
    formFields: [
      { id: 'filename', placeholder: 'Filename',required:true },
      { id: 'uploader', placeholder: 'uploader' ,required:true },

      { id: 'select',
        type: 'select',
        options: [
          {value: 'png', text: 'png'},
          {value: 'jpeg', text: 'jpeg'},
          {value: 'jpg', text: 'jpg'},
          {value: 'pdf', text: 'pdf'},

        ]}
    ]
  }, function (isConfirm) {

          console.log(this.swalForm)
          var canvas = document.getElementById('canvas');
          var dataURL = canvas.toDataURL();
          var filename= document.getElementById("filename").value;
          var type= document.getElementById("select").value;
          console.log(type);
          var uploader= document.getElementById("uploader").value;

            if(isConfirm){
                  $.ajax({
                             type: "POST",
                             url: "/saveAs/",
                             data: { canvas: dataURL ,
                                     filename:filename,
                                     uploader:uploader,
                                     type:type,
                                     csrfmiddlewaretoken: '{{ csrf_token }}'},
                          success: function (data) {
                                  swal("Success!", "Your file is Saved.", "success");
                             }
                             });
                   }
         else {
                swal("Cancelled", "Request is Cancelled!", "error");
            }


  })
}



function saveFunction() {

      var canvas = document.getElementById('canvas');
      var dataURL = canvas.toDataURL();
      var imageId= document.getElementById("imageId").value;
      var url = '/save/';
      var csrfmiddlewaretoken = '{{ csrf_token }}';
      var sharing = SaveImage( url, dataURL, imageId, csrfmiddlewaretoken );
      return false;

     }



function SaveImage(url, dataURL, imageId, csrfmiddlewaretoken){
    swal({
          title: "Save Document",
          text: "Are You Sure About This!!",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-warning",
          confirmButtonText: "Yes, Save it!",
          cancelButtonText: "No, cancel plx!",
          closeOnConfirm: false,
          closeOnCancel: false
        },
        function(isConfirm){
            if(isConfirm){

                $.ajax({
                 type: "POST",
                 url: "/save/",
                 data: { canvas: dataURL ,
                         imageId:imageId,
                         csrfmiddlewaretoken: csrfmiddlewaretoken},
              success: function (data) {

                    swal("Success!", "Your file is Saved.", "success");

                    }
             });
             }
     else {
            swal("Cancelled", "Request is Cancelled!", "error");
        }


      }
)}
