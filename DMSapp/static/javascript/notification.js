
     $('.deletebtn').on('click', function (){

    var url = $(this).attr('data-cities-url')

    $.ajax({
            url: url,
            success: function (data) {
              if (data) {
                $("#notification_list tbody").html(data.html_book_list);
                $(".mypage").html(data.page_div);
              }
            }
          });
    })



//Mark Read

$(function() {
        $('.btn-notification').on('click', function(e) {

            var self = $(this);
            var url = $(this).attr('data-cities-url');

            $.getJSON(url, function(result) {
                if (result.success) {
                    window.location.reload();
                    $('.fa-envelope', self).toggleClass('fa-envelope-open');
                }
            });
            return false;
        });
    });


//Encryption

$(function() {
    $('.btn-encrypt').on('click', function(e) {
          var id = $(this);  
           var url = $(this).attr('data-url');
           var data = $(this).attr('data-form-data'); 
           var encrypt = Encryption(e, url, data, id);
            return false;
    });

 });


function Encryption(e, url, data, id){
    if(data == 'True'){
    var title = "Decrypt Document"
     }
    else {
       var title = "Encrypt Document"
    } 
      swal({
          title: title,
          text: "Are You Sure About This!!",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-warning",
          confirmButtonText: "Yes.!!",
          cancelButtonText: "No, cancel!",
          closeOnConfirm: false,
          closeOnCancel: false
        },
        function(isConfirm){
            if(isConfirm){
            e.preventDefault();
                    $.getJSON(url, function(result) {
                        if(result.is_not_encrypted){
                           swal("Decrypted!", result.is_not_encrypted , "success");
                           $(id).removeClass('btn-danger');  
                           $(id).addClass('btn-primary'); 
                           }
                        if (result.is_encrypted){
                            swal("Encrypted!", result.is_encrypted, "success");
                          $(id).removeClass('btn-primary'); 
                          $(id).addClass('btn-danger');   
                         }
                        if (result.not_authenticated){
                            swal("Not logged In!", result.not_authenticated, "warning");
                        }
                    });
            }
            else{
            swal("Cancelled", "Request is Cancelled!", "error");
            }
        }
)}
