
    $(function () {

    $(".js-upload-photos").click(function () {
      $("#fileupload").click();
    });

    $("#fileupload").fileupload({
      dataType: 'json',

      done: function (e, data) {
	 if (data.result.is_valid) {
            $('th.btn-doc').show();
          $("#bulkUpload tbody").prepend(
            "<tr><td><ol><li><a href='" + data.result.url + "'>" + data.result.name + "</li></ol></a></td></tr>"
          )
        }
      else{
          swal("Error!", "File Format is not Supported..!", "warning");
        }
      }

    });

  });
