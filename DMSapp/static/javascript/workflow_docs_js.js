//     File for changes in workflow docs

        if($('input[class^=abc]:checked').length == 0){
            $('#btn_version').prop('disabled', true);
        }

        $('#tbody_id').on('change', 'input[type=checkbox]', function() {

               if ($('#selectAll_1').is(':checked')) {
                    $('#selectAll_1').prop("checked", true)
               }


                if($('#tbody_id input:checked').length >= 2){
                    $('#btn_download').prop('disabled', true);
                    $('#btn_delete').prop('disabled', true);
                     $('#btn_version').prop('disabled', true); 
                    $('#select_option').prop('disabled', true);
                }
                else {
                     $('#btn_download').prop('disabled', false);
                     $('#btn_delete').prop('disabled', false);
                     $('#select_option').prop('disabled', false);
                     $('#btn_version').prop('disabled', false);

                    }
        });


         $('#selectAll_1').change(function() {
            if ($('#selectAll_1').is(':checked')){
                $('#btn_download').prop('disabled', false);
                $('#btn_version').prop('disabled', true);
                $('#btn_delete').prop('disabled', true);
                $('#select_option').prop('disabled', true);
            }
            else{

                $('#btn_version').prop('disabled', true);
                $('#btn_delete').prop('disabled', false);
                $('#select_option').prop('disabled', false);
            }

         });


        // For One Selection

        $('#btn_download').on('click', function(e) {

            var selectedOption = $('.select_option').children("option:selected").val();
            alert(selectedOption);
            console.log(selectedOption);

            var self = $(this);
            var value = $(this).attr('data-attr-value');
            url = "confirm_workflow_docs"

            message = ""
            $("#tbody_id input[type=checkbox]:checked").each(function () {
               var row = $(this).closest("tr")[0];
            //   message = row.cells[1].innerHTML;
               message = $(row.cells[1]).find(".view_document").text();
             });
            
            console.log(message, "message")

            if (message != "") {
                    data_url = "download_workflow_docs/" + value + '/' + message + '?selectedOption=' + selectedOption;

                     $.getJSON(url, function(result) {
                    if (result){
                         location.href = data_url;
                     }
                    else {
                        alert("User is not authenticated")
                    }
                });

            }

            return false;
        });



       // For select All button

       $('#btn_download').on('click', function(e) {
          if($("#selectAll_1").is(":checked")){

            var self = $(this);
            var value = $(this).attr('data-attr-value');
            url = "confirm_workflow_docs"
            data_url = "download_workflow_docs/" + value;
            $.getJSON(url, function(result) {
                if (result){
                     location.href = data_url;
                 }
                else {
                    alert("User is not authenticated")
                }
            });
            return false;
           }
        });
