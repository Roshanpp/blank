$(function() {
    $('.btn_download_externally').on('click', function(e) {
            var self = $(this);
            var url = $(this).attr('data-cities-url');
            var file_id = $(this).attr('name');
            data_url = "/download_externally/requested_file";
            $.getJSON(url, function(result) {
                if (result.success) {
                    downloadFunction(result.pass, data_url, file_id);
                }
                else{
                    location.href = data_url + "/" + file_id;
                }
            });
            return false;
        });
    });


function downloadFunction(pass, data_url, file_id){
    swal({
        title: "Download Document",
        text: "Please Enter Your Password",
        type: "input",
        inputType: "password",
        closeOnConfirm: false,
        showCancelButton: true,
        inputPlaceholder: "Password",
    },function (inputValue){

       $.ajax({
               type: 'GET',
               url: '/check_valid_password/',
               data: {
               'original_password': pass,
               'inputVal': inputValue
               },

               success:  function (data) {

                if (data['is_matched'] == true){

                    location.href = data_url + "/" + file_id;
                    swal.close();
                }
                else if (data['is_matched'] == false ){

                    swal("Error!", 'Wrong Password', "warning");
                    return false;
                }
                else if (data['is_matched'] == 'No data' ){

                    swal.close();
                    return false;
                }

               }

              });

      }
   )}



// function downloadFunction(pass, data_url, file_id){
    // swal({
        // title: "Download Doc",
        // text: "Please Enter Your Password",
        // type: "input",
        // inputType: "password",
        // showCancelButton: true
    // },function (inputValue){
		// alert(pass, inputValue)
        // if (inputValue == pass){
            // location.href = data_url + "/" + file_id;
        // }
        // else{
        // swal('error', 'Wrong password entered', 'error')
        // //swal.showInputError("You need to write something!");
        // }
      // }
   // )}
