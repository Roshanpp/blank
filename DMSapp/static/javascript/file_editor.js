window.onload = function() {
      var canvas = document.getElementById("myCanvas");
      var ctx = canvas.getContext("2d");
      var img = document.getElementById("MyImage");
      ctx.drawImage(img, 0, 0, canvas.width, canvas.height,);

      img.addEventListener('contextmenu', function (e) {
      var dataURL = canvas.toDataURL('image/png');
      img.src = dataURL;
    });

    var angleInDegrees=0;

    $("#clockwise").click(function(){
        angleInDegrees+=90;
        drawRotated(angleInDegrees);
    });

    $("#counterclockwise").click(function(){
        angleInDegrees-=90;
        drawRotated(angleInDegrees);
    });

    function drawRotated(degrees){
        ctx.clearRect(0, 0, canvas.width,canvas.height);
        ctx.save();
        ctx.translate(canvas.width/2,canvas.height/2);
        ctx.rotate(degrees*Math.PI/180);
        ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
        ctx.restore();
    }

    var button1 = document.getElementById('btn-download');
    button1.addEventListener('click', function (e) {
        var dataURL = canvas.toDataURL('image/png');
        button1.href = dataURL;
    });

    var lastX;
    var lastY;
    var strokeColor = "red";
    var strokeWidth = 2;
    var mouseX;
    var mouseY;
    var canvasOffset = $("#myCanvas").offset();
    var offsetX = canvasOffset.left;
    var offsetY = canvasOffset.top;
    var isMouseDown = false;
    var brushSize = 8 ;
    var brushColor = "#ff0000";
    ctx.lineJoin = "round";

    var points = [];

    function handleMouseDown(e) {
        mouseX = parseInt(e.clientX - offsetX);
        mouseY = parseInt(e.clientY - offsetY);

        // Put your mousedown stuff here
        ctx.beginPath();
        if (ctx.lineWidth != brushSize) {
            ctx.lineWidth = brushSize;
        }
        if (ctx.strokeStyle != brushColor) {
            ctx.strokeStyle = brushColor;
        }
        ctx.moveTo(mouseX, mouseY);
        points.push({
            x: mouseX,
            y: mouseY,
            size: brushSize,
            color: brushColor,
            mode: "begin"
        });
        lastX = mouseX;
        lastY = mouseY;
        isMouseDown = true;
    }

    function handleMouseUp(e) {
      mouseX = parseInt(e.clientX - offsetX);
      mouseY = parseInt(e.clientY - offsetY);

      // Put your mouseup stuff here
      isMouseDown = false;
      points.push({
            x: mouseX,
            y: mouseY,
            size: brushSize,
            color: brushColor,
            mode: "end"
        });
    }

    function handleMouseMove(e) {
        mouseX = parseInt(e.clientX - offsetX);
        mouseY = parseInt(e.clientY - offsetY);

        // Put your mousemove stuff here
        if (isMouseDown) {
            ctx.lineTo(mouseX, mouseY);
            ctx.stroke();
            lastX = mouseX;
            lastY = mouseY;
            // command pattern stuff
            points.push({
                x: mouseX,
                y: mouseY,
                size: brushSize,
                color: brushColor,
                mode: "draw"
            });
        }
    }


    $("#myCanvas").mousedown(function (e) {
        handleMouseDown(e);
    });
    $("#myCanvas").mousemove(function (e) {
        handleMouseMove(e);
    });
    $("#myCanvas").mouseup(function (e) {
        handleMouseUp(e);
    });

    function redrawAll() {
    if (points.length == 0) {
        return;
    }
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.save()
        var img = document.getElementById("MyImage");
        ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
        for (var i = 0; i < points.length; i++) {
            var pt = points[i];
            var begin = false;
            if (ctx.lineWidth != pt.size) {
                ctx.lineWidth = pt.size;
                begin = true;
            }
            if (ctx.strokeStyle != pt.color) {
                ctx.strokeStyle = pt.color;
                begin = true;
            }
            if (pt.mode == "begin" || begin) {
                ctx.beginPath();
                ctx.moveTo(pt.x, pt.y);
            }
            ctx.lineTo(pt.x, pt.y);
            if (pt.mode == "end" || (i == points.length - 1)) {
                ctx.stroke();
            }
        }
        ctx.stroke();
    }
    function undoLast() {
        points.pop();
        redrawAll();
    }
//    $("#brush2").click(function () {
//        brushSize = 2;
//    });
    $("#brushRed").click(function () {
        brushColor = "#ff0000";
    });
    $("#brushBlue").click(function () {
        brushColor = "#0000ff";
    });

    $('#myText').on('input', function(){
        ctx.font = "20px Verdana";
        ctx.drawImage($('canvas').get(0), 0, 0);
        ctx.fillStyle = "black";
        ctx.fillText($(this).val(), 10, 50);
    });

    var interval;
    $("#undo").mousedown(function () {
        interval = setInterval(undoLast, 50);
    }).mouseup(function () {
        clearInterval(interval);
    });



};
