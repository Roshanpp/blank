
//Download Document Scripts

$(function() {
    $('.btn_download').on('click', function(e) {
            var self = $(this);
            var file_id = $(this).attr('name');
            var url = $(this).attr('data-cities-url');
            data_url = "/download/requested_file";
            $.getJSON(url, function(result) {
                if (result.success) {
                    downloadFunction(result.pass, data_url, file_id);
                }
                else{
                    location.href = data_url + "/" + file_id;
                }
            });
            return false;
        });
    });

function downloadFunction(pass, data_url, file_id){
    swal({
        title: "Download Document",
        text: "Please Enter Your Password",
        type: "input",
        inputType: "password",
        closeOnConfirm: false,
        showCancelButton: true,
        inputPlaceholder: "Password",
    },function (inputValue){

       $.ajax({
               type: 'GET',
               url: '/check_valid_password/',
               data: {
               'original_password': pass,
               'inputVal': inputValue
               },

               success:  function (data) {

                if (data['is_matched'] == true){

                    location.href = data_url + "/" + file_id;
                    swal.close();
                }
                else if (data['is_matched'] == false ){

                    swal("Error!", 'Wrong Password', "warning");
                    return false;
                }
                else if (data['is_matched'] == 'No data' ){

                    swal.close();
                    return false;
                }

               }

              });

      }
   )}


//View Document Scripts

$(function(){
    $('.btn_link_view').on('click', function(e){
        var self = $(this);
        var path_url = $(this).attr('href');
        var pass = $(this).attr('about')
        var url = $(this).attr('data-cities-url');
        $.getJSON(url, function(result) {
                if (result.success) {
                   viewFunction(result.pass, path_url);
                }
                else{
                   location.href = path_url ;
                }
            });
            return false;

    });
});



$(function(){
    $('.btn_view').on('click', function(e){
        var self = $(this);
        var path_url = $(this).attr('name');
        var pass = $(this).attr('about')
        var url = $(this).attr('data-cities-url');
        $.getJSON(url, function(result) {
                if (result.success) {
                   viewFunction(result.pass, path_url);
                }
                else{
                   location.href = path_url ;
                }
            });
            return false;

    });
});

function viewFunction(pass, url){
    swal({
        title: "View Document",
        text: "Please Enter Your Password",
        type: "input",
        inputType: "password",
        showCancelButton: true,
        closeOnConfirm: false,
        inputPlaceholder: "Password",
    },function (inputValue){
        $.ajax({
               type: 'GET',
               url: '/check_valid_password/',
               data: {
               'original_password': pass,
               'inputVal': inputValue
               },

               success:  function (data) {

                if (data['is_matched'] == true){

                    location.href = url;
                    swal.close();
                }
                else if (data['is_matched'] == false ){

                    swal("Error!", 'Wrong Password', "warning");
                    return false;
                }
                else if (data['is_matched'] == 'No data' ){

                    swal.close();
                    return false;
                }

               }

              });

      }
   )}

//Share Document Scripts

$(function() {
       $('.btn-share').on('click', function(e) {
       var url = $(this).attr('data-cities-url');
       var protected = $(this).attr('name');
       var id = $(this).attr('about')
       if (protected == 'True'){

            var url = '/share_locked_file/'+id+'/'
            $.ajax({
               url: url,

               dataType: 'json',
               beforeSend: function () {
                 $("#modal-book").modal("show");
               },
               success: function (data) {

                 $("#modal-book .modal-content").html(data.html_form);

               }
             });
             return false;
       }
       else{

             var isValid = isConfirm(e, url);

            return false;
       }
     });
  });


function isConfirm(e, url) {
   swal({
         title: "Share Document",
         text: "You are requesting to share your document with other People!",
         type: "warning",
         showCancelButton: true,
         confirmButtonClass: "btn-warning",
         confirmButtonText: "Yes, Share it!",
         cancelButtonText: "No, cancel!",
         closeOnConfirm: false,
         closeOnCancel: false
       },
       function(isConfirm){
           if(isConfirm){
               var frm = $('#post-id');
               e.preventDefault();
               $.ajax({
                   type: frm.attr('method'),
                   url: frm.attr('action'),
                   data: frm.serialize(),

                   success:  function (data) {
                       swal("Shared!", "Your file has been requested for Sharing.", "success");
                   }

                  });
           }
           else{
           swal("Cancelled", "Your file is safe :)", "error");
           }
       }
)}


//Stop Sharing


$(function() {
    $('.btn-sharing').on('click', function(e) {
            console.log("TRTTRE"); 
            var url = $(this).attr('href');
            var sharing = stopSharing(e, url);
            return false;
    });

 });


function stopSharing(e, url){
    swal({
          title: "Share Document",
          text: "Are You Sure About This!!",
          type: "warning",
          showCancelButton: true,
          confirmButtonClass: "btn-warning",
          confirmButtonText: "Yes, Stop Sharing it!",
          cancelButtonText: "No, cancel!",
          closeOnConfirm: false,
          closeOnCancel: false
        },
        function(isConfirm){
            if(isConfirm){
            e.preventDefault();
                    $.getJSON(url, function(result) {
                        if (result.success) {
                        swal("Success!", "Your file is stopped being shared.", "success");
                        }
                    });
            }
            else{
            swal("Cancelled", "Request is Cancelled!", "error");
            }
        }
)}




//Protecting Doc



$(function() {
  $('.btn-protect').on("click", function (){
     $.ajax({
       url: $(this).attr("data-url"),
       dataType: 'json',
       beforeSend: function () {
         $("#modal-book").modal("show");
       },
       success: function (data) {
         $("#modal-book .modal-content").html(data.html_form);
       }
     });

  });
});



$(function() {
    $('.form-class').on('submit', function(e) {
           var frm = $(this)
          var id = $(this).attr('id')
         $.ajax({
           url: $(this).attr("action"),
           data: frm.serialize(),
           dataType: 'json',
           type : 'post',
           success: function (data) {
             if(data.is_not_protected){
               swal("Unlocked!", data.is_not_protected , "success");
               $('.protect_' + id).removeClass('fa-lock');
               $('.protect_' + id).addClass('fa-unlock');
              }
             if (data.protected){

                swal("Locked!", data.protected, "success");
                $('.protect_' + id).addClass('fa-lock');
               $('.protect_' + id).removeClass('fa-unlock');
            }
            if(data.wrong_password){
                swal("Error!", data.wrong_password, "warning");
            }
           }
         });
         return false;
    });

});


$('.js-restore-file').on('click', function(e) {

            var currentRow=$(this).closest("tr");

            var src = $(this).find('img').attr('src');
            var main_url = window.location.href;
            var url_sub=main_url.split('/')[3]
            if (url_sub === 'archive_invoice_files'){
                var fileName=currentRow.find("td:eq(1)").text();
            }else if (url_sub === 'archive_files'){
                var fileName=currentRow.find("td:eq(2)").text();
            }

            var url = $(this).attr('data-url');
            var name = $(this).attr('name');


            var sharing = isConfirmRestore(e, url, fileName, src);
            return false;
    });


function isConfirmRestore(e, url, fileName, src) {
   swal({
         title: "Restore File",
         text:  "Do you want to restore file "+fileName+" ?",
         imageUrl: src,
         showCancelButton: true,
         confirmButtonClass: "btn-warning",
         confirmButtonText: "Yes, Restore it!",
         cancelButtonText: "No, cancel!",
         closeOnConfirm: false,
         closeOnCancel: false
       },
       function(isConfirmRestore){
           if(isConfirmRestore){


               e.preventDefault();
               $.ajax({

                   url: url,


                   success:  function () {
                       swal("Restored!", "Your file has been restored Successfully.", "success");
                   }

                  });
           }
           else{
           swal("Cancelled","Request is Cancelled!!", "error");
           }
       }
)}
