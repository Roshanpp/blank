$('#btnExportToExcel').on('click', function(){
  $(".table").table2excel({
    exclude: ".noExl",
    name: "Excel Document Name",
    filename: "Downloaded_files.xls",
    fileext: ".xls",
    exclude_img: true,
    exclude_links: true,
    exclude_inputs: true
  });
});


$('#btnExportToPdf').on('click', function(){
  html2canvas(document.getElementById('book-table'), {
    onrendered: function (canvas) {
        var data = canvas.toDataURL();
        var docDefinition = {
            content: [{
                image: data,
                width: 500
            }]
        };
        pdfMake.createPdf(docDefinition).download("Downloaded_files.pdf");
    }
    });
});
