$('.reports_class').on('click', function(e) {
  var id = $(this).attr('id');
  var url = $(this).attr('data-url');

  $.ajax({
    url: url,
    data: {
      'id': id
    },
    dataType: 'json',
    success: function (data) {
        if (data){

           var myConfig = {
              "type":"ring3d",
              "title":{
                "text":"Document Report"
              },
              "legend":{
                // "x":"75%",
                // "y":"50%",
                "border-width":1,
                "border-color":"gray",
                "border-radius":"5px",
                "header":{
                  "text":"Info",
                  "font-family":"Georgia",
                  "font-size":12,
                  "font-color":"#3333cc",
                  "font-weight":"normal"
                }},
              "series":[
                {"values":[data.total_docs], "text": 'Total Documents',},
                {"values":[data.shared_docs], "text": 'Shared Document', "detached":true},
                {"values":[data.fav_docs], "text": 'Favorite Document',},
                {"values":[data.proc_docs], "text": 'Protected Document',},
              ]
            };

           zingchart.render({
                id : 'user_info',
                data : myConfig,
                height: 380,
                width: "100%"
            });
        }
        else{
            alert('Something went wrong');
        }
    }
  });

});
