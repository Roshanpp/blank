// username exist
  $("#id_username").keyup(function(){

      var username = $(this).val().trim();

      if(username!='')
      {
      $.ajax({
    url: "/validate_username/",
    cache:'false',
    dataType: 'json',
    type:"GET",
    data: {
          'username': username
        },
    success: function(data){
     if (data.is_taken){
                    $("#uname_response").html("<span class='not-exists'> Already in exist.</span>");
                }else{
                    $("#uname_response").html("<span class='exists'>Available.</span>");
                }
    }

  });
  }else {
    $("#uname_response").html("<span class='exists'></span>");}


    });

// check email is exist
$("#id_email").keyup(function(){

          var email = $(this).val().trim();

          if(email!='')
          {
          $.ajax({
        url: "/validate_email/",
        cache:'false',
        dataType: 'json',
        type:"GET",
        data: {
              'email': email
            },
        success: function(data){
         if (data.is_taken){
                        $("#email_response").html("<span class='not-exists'> Already in exist.</span>");
                    }else{
                        $("#email_response").html("<span class='exists'>Available.</span>");
                    }
        }

      });
      }else {
        $("#email_response").html("<span class='exists'></span>");}


        });

// password equality check
function checkPasswordMatch() {

            var password = $("#id_password1").val();
            var confirmPassword = $("#id_password2").val();
           if(confirmPassword !='')
           {
                if (password != confirmPassword)
                 {
                     $("#password_response").html("<span class='not-exists'> Passwords do not match!.</span>");
                }else{
                               $("#password_response").html("<span class='exists'>Passwords match.</span>");
                           }
        }else {
                $("#password_response").html("<span class='exists'></span>");
        }
}
$(document).ready(function () {
           $("#id_password2").keyup(checkPasswordMatch);
            $("#id_password1").keyup(checkPasswordMatch);
        });


//password validation

$("#id_password1").keyup(function(){

    var password1 = $(this).val().trim();
    if(password1!='')
    {
  $.ajax({
  url: "/validate_pass/",
  cache:'false',
  dataType: 'json',
  type:"post",
  data: {
        'password1': password1,
        csrfmiddlewaretoken: '{{ csrf_token }}'
      },
  success: function(data){

      if(password1.length<8){
        $("#pass_length").html("<span class='not-exists'> &#10005; Be at least 8 characters.</span>");
      }
  else{
        $("#pass_length").html("");
      }
      if(data.is_number){
 $("#pass_number").html("");
      }
        else{
        $("#pass_number").html("<span class='not-exists'> &#10005;  Include at least one number.</span>");
      }
      if(data.is_upper_lower){
      $("#pass_char").html("");
	}
        else{
        $("#pass_char").html("<span class='not-exists'> &#10005; Include both lower and upper case character.</span>");
      }
   if (data.is_common){
                  $("#pass_commom").html("<span class='not-exists'> &#10005; Very comman password.</span>");
              }
   else{
                  $("#pass_commom").html("");
              }
  }

});
}else {
 $("#pass_length").html("");
  $("#pass_number").html("");
  $("#pass_char").html("");
  $("#pass_commom").html("");}


  });

