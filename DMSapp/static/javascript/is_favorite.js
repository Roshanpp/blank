//$.ajax({
//    url : "{% url 'ajax_is_favorite'%}",
//    data: {
//        'csrfmiddlewaretoken': $('input[name="csrfmiddlewaretoken"]').val(),
//        'ProjectProfileID': 'here you get the id',
//        'isFavorite':true //or false
//    },
//    type : 'POST',
//    dataType : 'json',
//    success : function(json) {
//        if(json.success === true){
//            // do somthing
//        }else{
//            // do somthing
//        }
//    }
//});



$(function () {

  /* Functions */

  var loadForm = function () {
    var btn = $(this);
    $.ajax({
      url: btn.attr("data-url"),
      type: 'get',
      dataType: 'json',
      beforeSend: function () {
        $("#modal-book").modal("show");
      },
      success: function (data) {
        $("#modal-book .modal-content").html(data.html_form);
      }
    });
  };


  var saveForm = function () {
    var form = $(this);
    $.ajax({
      url: form.attr("action"),
      data: form.serialize(),
      type: form.attr("method"),
      dataType: 'json',
      success: function (data) {
        if (data.form_is_valid) {
          $("#book-table tbody").html(data.html_book_list);
          $("#modal-book").modal("hide");
        }
        else {
          $("#modal-book .modal-content").html(data.html_form);
        }
      }
    });
    return false;
  };



    /* Binding */

//  // Create book
//  $(".js-create-book").click(loadForm);
//  $("#modal-book").on("submit", ".js-book-create-form", saveForm);

  // Update book
  $("#result_list").on("click", ".js-favorite", loadForm);
  $("#modal-table").on("submit", ".js-favorite-form", saveForm);

});