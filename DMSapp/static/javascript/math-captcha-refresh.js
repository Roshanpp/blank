
var total;
function getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
       return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}
function getRandom(){return Math.ceil(Math.random()* 10);}
function getRandom_operator(){
         var ops=['+','-','*','/'];
         var opindex = getRandomInt(1,5) //good that your rnum2 cannot be zero
         var operator = ops[opindex-1];
         return operator
}
function createSum(){
        var operator=getRandom_operator()
	      var randomNum1 = getRandom(),randomNum2 = getRandom();
         if (operator==="+"){
                    	total =randomNum1 + randomNum2;
                      $( "#question" ).text( randomNum1 + " + " + randomNum2 + " =" );
                      $( "#sum" ).val( randomNum1 + randomNum2);
                      $("#ans").val('');
                      checkInput();
                    }
        else if (operator==="-"){
                        total =randomNum1 - randomNum2;
                        $( "#question" ).text( randomNum1 + " - " + randomNum2 + " =" );
                        $( "#sum" ).val( randomNum1 - randomNum2);
                        $("#ans").val('');
                        checkInput();
                                      }
       else if (operator==="*"){
                        	total =randomNum1 * randomNum2;
                          $( "#question" ).text( randomNum1 + " * " + randomNum2 + " =" );
                          $( "#sum" ).val( randomNum1 * randomNum2);
                         $("#ans").val('');
                          checkInput();
                         }
       else {
                        	total =(randomNum1 / randomNum2).toFixed(1);
                          $( "#question" ).text( randomNum1 + " / " + randomNum2 + " =" );
                          $( "#sum" ).val(( randomNum1 / randomNum2).toFixed(1))
                         $("#ans").val('');
                         checkInput();
                                  }

}

function checkInput(){
	   	var input = $("#ans").val(),
    	slideSpeed = 200,
      hasInput = !!input,
      input=String(Number(input).toFixed(1))
      valid = hasInput && input == total;
     $('#message').toggle(!hasInput);
     $('button[type=submit]').prop('disabled', !valid);
     $('#success').toggle(valid);
     $('#fail').toggle(hasInput && !valid);

}

$(document).ready(function(){
	//create initial sum
	createSum();
	// On "reset button" click, generate new random sum
	$('#reset').click(createSum);
	// On user input, check value
	$( "#ans" ).keyup(checkInput);
});
