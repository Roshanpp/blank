from django.apps import AppConfig


class DmsappConfig(AppConfig):
    name = 'DMSapp'
