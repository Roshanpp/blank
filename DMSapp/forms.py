from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.core import validators
from django.core.validators import FileExtensionValidator
from django.contrib.auth import get_user_model
from django.utils.translation import ugettext_lazy as _

from .validators import validate_file_extension, pdf_file_validation
from .models import FileUpload, Project, SharedDocument, Notes
from .choices import THEME_CHOICES

User = get_user_model()

alphanumeric = validators.RegexValidator(r'^[0-9a-zA-Z ]*$', 'Only alphanumeric characters are allowed.')


class FileUploadForm(forms.ModelForm):
	CHOICES = (
                ('', _('--Select--')), 
		('Documents', _('Documents')),
		('Invoice', _('Invoice')),
		('HR Documents', _('HR Documents'))
	)

	Uploader = forms.CharField(label=_('Uploaded By'),required=True, validators=[alphanumeric], widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':_('Uploaded By')}))
	File1 = forms.FileField(label=_('File'),widget=forms.FileInput(attrs={'id': 'yz',  'class': 'for_word'}))
	category = forms.ChoiceField(label=_('Category'), choices=CHOICES, widget=forms.Select(attrs={'class': 'form-control'}))

	class Meta:
		model = FileUpload
		fields = ['Uploader', 'category', 'File1']


class Dashboard(forms.ModelForm):
	class Meta:
		model = User
		fields = ('username', )


class ProjectForm(forms.ModelForm):
	class Meta:
		model = Project
		fields = ['name']


class FavoriteForm(forms.ModelForm):
	class Meta:
		model = FileUpload
		fields = ['is_favorite']


class SharedDocumentForm(forms.ModelForm):
	class Meta:
		model = SharedDocument
		fields = ('status', 'Comment')
		widgets = {
			'Comment': forms.Textarea(attrs={'class':'form-control','rows': 4, 'cols': 20}),
			'status' : forms.Select(attrs={'class':'form-group'}),
		}


class SharedDocumentRequestForm(forms.ModelForm):

	requested_user = forms.ModelChoiceField(queryset=User.objects.all(), empty_label="--Select--")

	class Meta:
		model = SharedDocument
		fields = ('requested_user', )


class ChoiceForm(forms.Form):
	CHOICES= (
		('0', _('All')),
		('1', _('Date')),
		('2', _('File Type'))

		)
	Sort_by = forms.ChoiceField(label=_('Sort By'),choices=CHOICES, widget=forms.Select(attrs={
			'class': 'choicy',
			'id':'changeId',
		})
		)


class NewChoiceForm(forms.Form):
	CHOICES= (
		('0', _('All')),
		('1', _('Date'))

		)
	Sort_by = forms.ChoiceField(label=_('Sort By'),choices=CHOICES, widget=forms.Select(attrs={
			'class': 'choicy',
			'id':'changeId',
		})
		)


class NoteForm(forms.ModelForm):
	title = forms.CharField(label=_('Title'), max_length=20, validators=[alphanumeric], widget=forms.TextInput(attrs={'class': 'col-xs-4  col-form-label'}))
	note = forms.CharField(label=_('Note') , max_length=150, widget=forms.TextInput(attrs={'class': 'col-xs-4  col-form-label'}))

	class Meta:
		model = Notes
		fields = ('title', 'note', )


class BulkFileUploadForm(forms.ModelForm):
	File1 = forms.FileField(widget=forms.FileInput(attrs={'id': 'yz'}))

	class Meta:
		model = FileUpload
		fields = ['File1']


class SharedDocumentExternally(forms.Form):
	name = forms.CharField(max_length=20, widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Name', 'autocomplete':'off'}))
	# text = forms.TextInput(attrs={'rows': 4, 'cols': 20})
	email = forms.EmailField(widget=forms.EmailInput(attrs={'class':'form-control', 'placeholder':'Email', 'autocomplete':'off'}))


class MisQueryForm(forms.Form):
	table_request = (
		('', '--Select--'),
		('User', 'User Data'),
		('FileUpload', 'Uploaded Documents'),
		('Workflow_Management', 'Workflow Management'),
	)
	query = forms.CharField(label=_('Query'), widget=forms.Textarea(attrs={'rows': 8, 'cols': 50}))


class ThemeManagementForm(forms.Form):
	theme = forms.ChoiceField(choices=THEME_CHOICES, label=_('Theme'), widget=forms.Select(), required=False)



