from django.shortcuts import render, redirect, HttpResponseRedirect, get_object_or_404, render_to_response
from django.http import HttpResponse
from django.urls import reverse
from django.contrib.auth import login, authenticate, logout
from django.views.decorators.csrf import csrf_protect, csrf_exempt
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.decorators import user_passes_test
from django.http import JsonResponse
from django.template.loader import render_to_string
from django.core.paginator import Paginator,PageNotAnInteger, EmptyPage
from django.core.exceptions import ValidationError
from django.conf import settings
from django.urls import reverse_lazy
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.template import RequestContext
from urllib import request
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.base import View
from django.core.files.base import ContentFile
from django.contrib.sites.shortcuts import get_current_site
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes, force_text
from django.core.mail import EmailMessage, send_mail
from django.contrib.auth.hashers import check_password
from django.db.utils import OperationalError, DatabaseError

from django.db import connection
import os
import mimetypes
import datetime
import calendar
from datetime import datetime,timedelta
import time
import json
from django.db.models import Q
from .choices import Theme_1, Theme_2, Theme_3, Theme_4, Default
from .forms import FileUploadForm, Dashboard, ProjectForm, \
                    ChoiceForm, SharedDocumentForm, SharedDocumentRequestForm, \
                    NewChoiceForm, NoteForm, BulkFileUploadForm, \
                    SharedDocumentExternally, MisQueryForm, ThemeManagementForm

from .models import FileUpload, Project, Download, SharedDocument, \
                    ArchiveFiles, Notification, Notes, \
                    NotificationForAccount

from WorkflowManagement.models import UserDataWorkflowManagement

from DMSaccount.models import Profile,UserSetting
from field_history.models import FieldHistory
from .namers import option_value
from DMSaccount.tokens import File_encrypt_token
from .crypt import Cryptographer
from django.core import serializers
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.hashers import make_password
import PyPDF2
import openpyxl
import pandas as pd
from bs4 import BeautifulSoup
from shutil import copyfile,move
import xlrd
from .pagination import paginate

User = get_user_model()


def get_file_size(value):
    return os.path.getsize(value)


@csrf_protect
def index(request):
    return render(request, 'index.html')


@csrf_exempt
@login_required
def home(request, DMS=None):
    #user = User.objects.get(id=request.user.id)
    user = UserSetting.objects.filter(user__username=request.user)
    if user:
        for i in user:
            theme_color = i.theme
        selected_theme = Default
        if theme_color == 'Theme_1':
            selected_theme = Theme_1
        if theme_color == 'Theme_2':
            selected_theme = Theme_2
        if theme_color == 'Theme_3':
            selected_theme = Theme_3
        if theme_color == 'Theme_4':
            selected_theme = Theme_4
    else:
        selected_theme = Default

    

    # theme_color = ast.literal_eval(theme_color1)

    bg_color = selected_theme['bg_color']
    profile_title = selected_theme['profile_title']

    profile_content = selected_theme['profile_content']
    header_color = selected_theme['header_color']
    footer_color = selected_theme['footer_color']

    profile = Profile.objects.get(username=request.user)
    user_last_login = request.user.last_login
    if request.user.is_superuser:
        count = Notification.objects.filter(unread=True, type='Share File').count()
        count2 = Notification.objects.filter(unread=True, type='notification changed', sender_id=request.user.id).count()
        count3 = Notification.objects.filter(unread=True, type='notification changed', receiver_id=request.user.id).count()

        drafted_count = NotificationForAccount.objects.filter(unread=True, type='Drafted',
                                                                receiver_id=request.user.id).count()

        approved_count = NotificationForAccount.objects.filter(unread=True, type='Approved',
                                                                receiver_id=request.user.id).count()
        pending_count = NotificationForAccount.objects.filter(unread=True, type='Pending',
                                                                receiver_id=request.user.id).count()
        rejected_count = NotificationForAccount.objects.filter(unread=True, type='Rejected',
                                                                receiver_id=request.user.id).count()

        total = count + count2 + count3 + drafted_count + approved_count + \
                    pending_count + rejected_count

    elif request.user.is_maker:

        count2 = Notification.objects.filter(unread=True, type='notification changed',
                                             sender_id=request.user.id).count()
        count3 = Notification.objects.filter(unread=True, type='notification changed',
                                             receiver_id=request.user.id).count()

        approved_count = NotificationForAccount.objects.filter(unread=True, type='Approved',
                                                                receiver_id=request.user.id).count()
        pending_count = NotificationForAccount.objects.filter(unread=True, type='Pending',
                                                                receiver_id=request.user.id).count()
        rejected_count = NotificationForAccount.objects.filter(unread=True, type='Rejected',
                                                                receiver_id=request.user.id).count()

        total = count2 + count3 + approved_count + pending_count + rejected_count

    elif request.user.is_checker:
        #count = Notification.objects.filter(unread=True, type='Share File').count()
        count2 = Notification.objects.filter(unread=True, type='notification changed',
                                             sender_id=request.user.id).count()
        count3 = Notification.objects.filter(unread=True, type='notification changed',
                                             receiver_id=request.user.id).count()

        drafted_count = NotificationForAccount.objects.filter(unread=True, type='Drafted',
                                                                receiver_id=request.user.id).count()
        total =  count2 + count3 + drafted_count

    else:
        count5 = Notification.objects.filter(unread=True, type='notification changed', sender_id=request.user.id).count()
        count6 = Notification.objects.filter(unread=True, type='notification changed', receiver_id=request.user.id).count()


        total = count5 + count6
    if DMS:
        speaker = True
        args = {'profile': profile, 'last_login': user_last_login, 'total_count': total,
                  'speaker':speaker, 'color': bg_color, 'profile_title': profile_title,
                   'profile_content': profile_content, 'header_color': header_color,
                   'footer_color': footer_color}
        return render(request, 'home.html',args)
    else:
        args2 = {'profile': profile, 'last_login': user_last_login, 'total_count': total,
                 'profile_title': profile_title,'profile_content': profile_content, 'color': bg_color,
                  'header_color': header_color, 'footer_color': footer_color }
        return render(request, 'home.html',args2) 


'''To upload the files'''

@login_required(login_url='login/')
def document_upload(request):
    return render(request, 'document_upload.html')


@login_required(login_url='/login/')
def fileupload(request):
    if request.method == 'POST':
        form = FileUploadForm(request.POST, request.FILES)
        if form.is_valid():
            file = form.save(commit=False)
            filename = form.cleaned_data['File1']
            category = form.cleaned_data['category']
            ext = str(filename).split('.')[-1]
            fullName = (str(filename).split('.')[0]).lower()
            if category == 'Invoice':
                isnotValid = validate_pdf(filename)
                if isnotValid:
                    messages.error(request, "Invoice's supports .pdf extension")

                else:
                    file.category = 'Invoice'
                    file.file_type = ext
                    file.user = request.user
                    file.Filename = filename
                    file.save()
                    messages.success(request, 'File Uploaded Successfully.!!')
            elif category == 'HR Documents':
                file.category = 'HR Documents'
                file.file_type = ext
                file.user = request.user
                file.Filename = filename
                file.save()
                messages.success(request, 'File Uploaded Successfully.!!')


            elif fullName.__contains__('invoice') or fullName.__contains__('bill'):
                isnotValid = validate_pdf(filename)
                if isnotValid:
                    file.category = category
                    file.file_type = ext
                    file.user = request.user
                    file.Filename = filename
                    file.save()
                    messages.success(request, 'File Uploaded Successfully.!!')

                else:
                    file.category = 'Invoice'
                    file.file_type = ext
                    file.user = request.user
                    file.Filename = filename
                    file.save()
                    messages.success(request, 'File Uploaded Successfully.!!')
            elif fullName.__contains__('resume') or fullName.__contains__('profile'):
                file.category = 'HR Documents'
                file.file_type = ext
                file.user = request.user
                file.Filename = filename
                file.save()
                messages.success(request, 'File Uploaded Successfully.!!')
            else:
                file.category = 'Documents'
                file.file_type = ext
                file.user = request.user
                file.Filename = filename
                file.save()
                messages.success(request, 'File Uploaded Successfully.!!')

            request.session.cycle_key()
            return HttpResponseRedirect(reverse('DMS:fileupload'))
    else:
        form = FileUploadForm()
        #y, z = form.fields["Uploader"], form.fields["File1"]
        #y.label, z.label = "Uploaded By", "File"
    args = {'form': form, 'type': 'basic'}
    return render(request, 'fileupload.html',args) 


def validate_pdf(value):
    import os
    from django.core.exceptions import ValidationError
    ext = os.path.splitext(value.name)[1]
    valid_extensions = ['.pdf']
    if not ext.lower() in valid_extensions:
        return True


@csrf_exempt
def validate_filename(request):
    file = request.GET.get('file', None)
    filename2 = str(file).split('\\')[-1]
    fileDetails = FileUpload.objects.filter(Filename__iexact=filename2, user_id=request.user.id)
    if fileDetails:
        for f in fileDetails:
            Uploader = f.Uploader
            File_type = f.file_type
            Filename = str(f.Filename)
            is_protected = str(f.is_protected)
            FilePath = f.File1
            uploaded_at = f.uploaded_at
            id = str(f.id)
        dt = datetime.strptime(str(uploaded_at), '%Y-%m-%d %H:%M:%S.%f').strftime('%d-%m-%Y %H:%M:%S')
        data = {
            'is_taken': FileUpload.objects.filter(Filename__iexact=filename2, user_id=request.user.id).exists(),
            'uploaded_at':dt,
            'Filename':Filename,
            'File_type':File_type,
            'FilePath':str(FilePath),
            'Uploader':Uploader,
            'is_protected':is_protected,
            'id': id


        }
    else:
        data = {
            'is_taken': FileUpload.objects.filter(Filename__iexact=filename2, user_id=request.user.id).exists(),

        }
    print(data)

    return JsonResponse(data)


@csrf_exempt
def replace_file(request):

    if request.method == 'POST' and request.is_ajax:
        fileName = request.FILES['File1']
        Uploader = request.POST.get('Uploader')

        File2 = get_object_or_404(FileUpload, Filename=fileName, user_id=request.user.id)
        if File2:
            path = FileUpload.objects.filter(Filename = fileName, user_id=request.user.id)

            if path:
                for p in path:
                    fPath = p.File1
                    uploaded_at = p.uploaded_at
                    category = p.category
            form = FileUploadForm(data=request.POST, files=request.FILES, instance=File2)

            if form.is_valid():
                os.remove(os.path.join(settings.MEDIA_ROOT, str(fPath)))
                file = form.save(commit=False)
                file.modified_at = datetime.now()
                file.uploaded_at = uploaded_at
                file.Uploader = Uploader
                file.category = category
                file.save()
                messages.success(request, 'File Updated Successfully.!!')

                return HttpResponseRedirect(reverse('DMS:fileupload'))


'''To View the files'''

@login_required(login_url='/login/')
def dashboard_view(request):
    files = FileUpload.objects.all()
    args = {'files': files}
    return render(request, 'dashboard.html',args)


@csrf_exempt
def sort_user_list(request, files, type, template_name):
    import json

    data = dict()
    if request.method == 'GET':

        data['form_is_valid'] = True
        args = { 'files': files, 'type': type }
        data['html_book_list'] = render_to_string('files/user_new_list.html',args, request=request) 

        args2 = {'files': files}
        data['page_div'] = render_to_string('files/pagination.html', args, request=request)

    context = {'type': type}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


@login_required(login_url='/login/')
def user_files(request, id, type):
    files = FileUpload.objects.filter(user_id=id, category=type,deleted='0').order_by("-pk")
    choiceForm = ChoiceForm()

    if files:

        if request.user.is_superuser:
            info = User.objects.all()
            qs_json = serializers.serialize('json', info)
            qs_json=json.loads(qs_json)

            page = request.GET.get('page')
            #paginator = Paginator(qs_json, 5)
            #page_records=paginator.count

            count=0

            for i in qs_json:
                count+=1
                i['fields']['srNo']=str(count)
                
            qs_json = paginate(request,qs_json,page)

            
            if request.GET.get('searchVal'):
                searchfor=request.GET.get('searchVal')

                if searchfor!='0' and searchfor!='' and searchfor!='NULL' and searchfor is not None:
                    info = User.objects.filter(Q(first_name__icontains = searchfor) | Q(last_name__icontains = searchfor) | Q(username__icontains = searchfor))

                elif searchfor=='0':
                    info = User.objects.all()

                qs_json = serializers.serialize('json', info)
                qs_json=json.loads(qs_json)
                page = request.GET.get('page', 1)
                #paginator = Paginator(qs_json, 5)
                #page_records=paginator.count

                count=0
                for i in qs_json:
                    count+=1
                    i['fields']['srNo']=str(count)

                qs_json = paginate(request, qs_json, page)
                return sort_user_list(request, qs_json, type, 'userlist.html')

            elif request.GET.get('searchFOR'):
                searchfor=request.GET.get('searchFOR')
                info = User.objects.filter(Q(first_name__icontains = searchfor) | Q(last_name__icontains = searchfor) | Q(username__icontains = searchfor))

                qs_json = serializers.serialize('json', info)
                qs_json=json.loads(qs_json)


                page = request.GET.get('page')
                #paginator = Paginator(qs_json, 5)
                #page_records=paginator.count

                count=0
                for i in qs_json:
                    count+=1
                    i['fields']['srNo']=str(count)

                qs_json = paginate(request, qs_json, page)
                return sort_user_list(request, qs_json, type, 'userlist.html')
            context =  {'files': qs_json, 'type': type}
            return render(request, 'userlist.html',context)

        else:

            if request.GET.get('optionValue'):

                option = request.GET.get('optionValue')
                print(option)
                if option == '1':
                    
                    fDate = request.GET.get('fDate')
                    tDate = request.GET.get('tDate')
                    if fDate == '':
                        fDate = tDate
                    if tDate == '':
                        tDate = fDate
                    from_date = datetime.strptime(fDate, '%d-%m-%Y').strftime('%Y-%m-%d %H:%M:%S')

                    to_date = datetime.strptime(tDate, '%d-%m-%Y')

                    toDate = calendar.timegm(to_date.timetuple())

                    advancedToDate = int(toDate) + 86400

                    finalToDate = datetime.utcfromtimestamp(advancedToDate).strftime('%Y-%m-%d %H:%M:%S')

                    files = FileUpload.objects.filter(user_id=id, category=type, uploaded_at__range=(from_date, finalToDate),deleted='0').order_by("-pk")
                    if files:
                        qs_json = serializers.serialize('json', files)
                        qs_json=json.loads(qs_json)
                        page = request.GET.get('page', 1)
                        #paginator = Paginator(qs_json, 5)
                        
                        count=0
                        for i in qs_json:
                            count+=1
                            size = get_file_size((settings.MEDIA_DIR + '/' + i['fields']['File1']))
                            i['fields']['srNo']=str(count)
                            i['fields']['File1']='/media/'+i['fields']['File1']
                            dateup=i['fields']['uploaded_at']
                            dt= datetime.strptime(dateup, "%Y-%m-%dT%H:%M:%S.%f")
                            i['fields']['File_size']=str(size)

                            i['fields']['uploaded_at']=dt
                        qs_json = paginate(request, qs_json, page)
                        
                    else:
                        qs_json = None

                    return sort_list(request, choiceForm, qs_json, type, 'user_files.html')
                elif option == '0':
                    files = FileUpload.objects.filter(user_id=id, category=type,deleted='0').order_by("-pk")
                    qs_json = serializers.serialize('json', files)
                    qs_json=json.loads(qs_json)
                    page = request.GET.get('page')
                    paginator = Paginator(qs_json, 5)

                    page_range=paginator.page_range

                    count=0
                    for i in qs_json:
                        count+=1
                        size = get_file_size((settings.MEDIA_DIR + '/' + i['fields']['File1']))
                        i['fields']['File_size']=str(size)
                        i['fields']['srNo']=str(count)
                        i['fields']['File1']='/media/'+i['fields']['File1']
                        dateup=i['fields']['uploaded_at']
                        dt= datetime.strptime(dateup, "%Y-%m-%dT%H:%M:%S.%f")

                        i['fields']['uploaded_at']=dt
                        i['page_range']=page_range
                    qs_json = paginate(request, qs_json, page)

                    
                    if request.GET.get('fDate'):
                        fDate = request.GET.get('fDate')
                        tDate = request.GET.get('tDate')
                    if request.GET.get('newfromDate'):
                        fDate = request.GET.get('newfromDate')
                        tDate = request.GET.get('newtoDate')

                    if fDate!='0' or tDate!='0':

                        if tDate == '':
                            tDate = fDate
                        elif fDate == '':
                            fDate = tDate
                        from_date = datetime.strptime(fDate, '%d-%m-%Y').strftime('%Y-%m-%d %H:%M:%S')
                        to_date = datetime.strptime(tDate, '%d-%m-%Y')
                        toDate = calendar.timegm(to_date.timetuple())
                        advancedToDate = int(toDate) + 86400
                        finalToDate = datetime.utcfromtimestamp(advancedToDate).strftime('%Y-%m-%d %H:%M:%S')
                        files = FileUpload.objects.filter(user_id=id, category=type, uploaded_at__range=(from_date, finalToDate),deleted='0').order_by("-pk")
                        qs_json = serializers.serialize('json', files)
                        qs_json=json.loads(qs_json)
                        page = request.GET.get('page')
                        paginator = Paginator(qs_json, 5)

                        page_range=paginator.page_range

                        count=0
                        for i in qs_json:
                            count+=1
                            size = get_file_size((settings.MEDIA_DIR + '/' + i['fields']['File1']))
                            i['fields']['File_size']=str(size)
                            i['fields']['srNo']=str(count)
                            i['fields']['File1']='/media/'+i['fields']['File1']
                            dateup=i['fields']['uploaded_at']
                            dt= datetime.strptime(dateup, "%Y-%m-%dT%H:%M:%S.%f")

                            i['fields']['uploaded_at']=dt
                            i['page_range']=page_range


                        qs_json = paginate(request, qs_json, page)

                        return sort_list(request, choiceForm, qs_json, type, 'user_files.html')
                    else:
                        return sort_list(request, choiceForm, qs_json, type, 'user_files.html')

                elif option == '2':
                    newOption = request.GET.get('newoption')
                    starred='0'
                    data = option_value(request, newOption, id,starred)

                    if newOption == '10':
                        my_list = serializers.serialize('json', data)
                        my_list=json.loads(my_list)
                        count=0
                        for i in my_list:
                            count+=1
                            size = get_file_size((settings.MEDIA_DIR + '/' + i['fields']['File1']))
                            i['fields']['File_size']=str(size)
                            i['fields']['srNo']=str(count)
                            i['fields']['File1']='/media/'+i['fields']['File1']
                            dateup=i['fields']['uploaded_at']
                            dt= datetime.strptime(dateup, "%Y-%m-%dT%H:%M:%S.%f")

                            i['fields']['uploaded_at']=dt
                    else:
                        my_list=[]
                        count=0
                        for i in data:
                            # print((settings.MEDIA_DIR + '/' + str(i['File1'])))
                            size = get_file_size((settings.MEDIA_DIR + '/' + str(i['File1'])))
                            print(size)
                            i['File_size']=str(size)
                            i['File1']='/media/'+str(i['File1'])

                            my_list.append({'fields':i,'pk':i['id']})
                        for m in my_list:
                            count += 1
                            m['fields']['srNo'] = str(count)
                    page = request.GET.get('page', 1)
                    #paginator = Paginator(my_list, 5)
                    qs_json = paginate(request, my_list, page)

                    
                    return sort_list(request, choiceForm, qs_json, type, 'user_files.html')
            else:
                
                qs_json = serializers.serialize('json', files)
                qs_json=json.loads(qs_json)

                page = request.GET.get('page', 1)
                #paginator = Paginator(qs_json, 5)
                #page_records=paginator.count

                count=0
                for i in qs_json:
                    count+=1
                    size = get_file_size((settings.MEDIA_DIR + '/' + i['fields']['File1']))
                    i['fields']['srNo']=str(count)
                    i['fields']['File1']='/media/'+i['fields']['File1']
                    dateup=i['fields']['uploaded_at']
                    dt= datetime.strptime(dateup, "%Y-%m-%dT%H:%M:%S.%f")
                    i['fields']['File_size']=str(size)

                    i['fields']['uploaded_at']=dt

                qs_json = paginate(request, qs_json, page)
                context2 = {'files': qs_json, 'choiceForm': choiceForm, 'type': type}
                return render(request, 'user_files.html', context2)
    else:
        print("files nahi  hai")
        if request.user.is_superuser:
            print("AA")
            info = User.objects.all()
            qs_json = serializers.serialize('json', info)
            qs_json=json.loads(qs_json)

            page = request.GET.get('page')
            #paginator = Paginator(qs_json, 5)
            
            count=0

            for i in qs_json:
                count+=1
                i['fields']['srNo']=str(count)


            qs_json = paginate(request,qs_json,page)
            if request.GET.get('searchVal'):
                searchfor=request.GET.get('searchVal')

                if searchfor!='0' and searchfor!='' and searchfor!='NULL' and searchfor is not None:
                    info = User.objects.filter(Q(first_name__icontains = searchfor) | Q(last_name__icontains = searchfor) | Q(username__icontains = searchfor))

                elif searchfor=='0':
                    info = User.objects.all()

                qs_json = serializers.serialize('json', info)
                qs_json=json.loads(qs_json)
                page = request.GET.get('page', 1)
                #paginator = Paginator(qs_json, 5)
                #page_records=paginator.count

                count=0
                for i in qs_json:
                    count+=1
                    i['fields']['srNo']=str(count)

                qs_json = paginate(request, qs_json, page)
                return sort_user_list(request, qs_json, type, 'userlist.html')

            elif request.GET.get('searchFOR'):
                searchfor=request.GET.get('searchFOR')
                info = User.objects.filter(Q(first_name__icontains = searchfor) | Q(last_name__icontains = searchfor) | Q(username__icontains = searchfor))

                qs_json = serializers.serialize('json', info)
                qs_json=json.loads(qs_json)


                page = request.GET.get('page')
                #paginator = Paginator(qs_json, 5)
                #page_records=paginator.count

                count=0
                for i in qs_json:
                    count+=1
                    i['fields']['srNo']=str(count)

                qs_json = paginate(request, qs_json, page)
                return sort_user_list(request, qs_json, type, 'userlist.html')
            context3 = {'files': qs_json, 'type': type}
            return render(request, 'userlist.html', context3)
        else:
            context5 = {'files': None, 'choiceForm': choiceForm, 'type': type}             
            return render(request, 'user_files.html', context5)


# def user_file_types(request, id, request_data):
#
#     files = FileUpload.objects.filter(user_id=id)
#     for f in files:
#         username = f.user.username
#     if files:
#         for f in files:
#             user = f.user
#         if user.is_superuser:
#             info = User.objects.all()
#             return render(request,'userlist.html',{'info':info})
#
#         else:
#             if request.method =='POST':
#                 option = request.POST.get('filtervalue')
#                 if option == '1':
#                     from_date = datetime.strptime(request.POST.get('fromDate'),'%d-%m-%Y').strftime('%Y-%m-%d %H:%M:%S')
#
#                     to_date = datetime.strptime(request.POST.get('toDate'),'%d-%m-%Y')
#
#                     toDate=calendar.timegm(to_date.timetuple())
#
#                     advancedToDate=int(toDate)+86400
#
#                     finalToDate=datetime.utcfromtimestamp(advancedToDate).strftime('%Y-%m-%d %H:%M:%S')
#
#                     files = FileUpload.objects.filter(user_id=id,uploaded_at__range=(from_date, finalToDate))
#
#                     return render(request, 'user_files.html', {'files': files})
#                 elif option == '0':
#                     return render(request, 'user_files.html', {'files': files})
#
#             elif request.method == 'GET':
#
#                 option = request_data
#                 if option == 0:
#
#                     files = FileUpload.objects.filter(user_id=id)
#                     return render(request, 'user_files.html', {'files': files,'id':id,'username':username})
#                 elif option==1:
#
#                     files = FileUpload.objects.filter(user_id=id)
#                     return render(request, 'user_files.html', {'files': files})
#                 elif option==2:
#
#                     files = FileUpload.objects.filter(user_id=id)
#                     return render(request, 'user_files.html', {'files': files})
#     else:
#         return render(request, 'user_files.html', {'files': None})


@login_required(login_url='/login/')
def root_files(request, pk, type, username):

    files = FileUpload.objects.filter(user_id=pk, category=type,deleted='0').order_by("-pk")
    qs_json = serializers.serialize('json', files)
    qs_json=json.loads(qs_json)

    page = request.GET.get('page', 1)
    #paginator = Paginator(qs_json, 5)
    
    count=0
    my_json=[]
    for i in qs_json:
        count+=1
        size = get_file_size((settings.MEDIA_DIR + '/' + i['fields']['File1']))
        i['fields']['File_size']=str(size)
        i['fields']['srNo']=str(count)
        i['fields']['File1']='/media/'+i['fields']['File1']
        dateup=i['fields']['uploaded_at']
        dt= datetime.strptime(dateup, "%Y-%m-%dT%H:%M:%S.%f")

        i['fields']['uploaded_at']=dt
    for i in qs_json:
        my_json.append(i['fields'])
    
    qs_json = paginate(request, qs_json, page)
    choiceForm = ChoiceForm()

    if files:
        if request.GET.get('optionValue'):

            option = request.GET.get('optionValue')
            if option == '1':
                fDate = request.GET.get('fDate')
                tDate = request.GET.get('tDate')
                if fDate == '':
                    fDate = tDate
                if tDate == '':
                    tDate = fDate
                from_date = datetime.strptime(fDate,'%d-%m-%Y').strftime('%Y-%m-%d %H:%M:%S')

                to_date = datetime.strptime(tDate,'%d-%m-%Y')

                toDate = calendar.timegm(to_date.timetuple())

                advancedToDate = int(toDate)+86400

                finalToDate = datetime.utcfromtimestamp(advancedToDate).strftime('%Y-%m-%d %H:%M:%S')

                files = FileUpload.objects.filter(user_id=pk, category=type, uploaded_at__range=(from_date, finalToDate),deleted='0').order_by("-pk")
                qs_json = serializers.serialize('json', files)
                qs_json=json.loads(qs_json)
                count=0
                for i in qs_json:
                    count+=1
                    size = get_file_size((settings.MEDIA_DIR + '/' + i['fields']['File1']))
                    i['fields']['File_size']=str(size)
                    i['fields']['srNo']=str(count)
                    i['fields']['File1']='/media/'+i['fields']['File1']
                    dateup=i['fields']['uploaded_at']
                    dt= datetime.strptime(dateup, "%Y-%m-%dT%H:%M:%S.%f")

                    i['fields']['uploaded_at']=dt

                #paginator = Paginator(qs_json, 5)

                qs_json = paginate(request, qs_json, page)

                return sort_list(request, choiceForm, qs_json, type, 'user_files.html')

            elif option=='0':
                files = FileUpload.objects.filter(user_id=pk, category=type,deleted='0').order_by("-pk")
                qs_json = serializers.serialize('json', files)
                qs_json=json.loads(qs_json)
                page = request.GET.get('page')
                paginator = Paginator(qs_json, 5)

                page_range=paginator.page_range

                count=0
                for i in qs_json:
                    count+=1
                    size = get_file_size((settings.MEDIA_DIR + '/' + i['fields']['File1']))
                    i['fields']['File_size']=str(size)
                    i['fields']['srNo']=str(count)
                    i['fields']['File1']='/media/'+i['fields']['File1']
                    dateup=i['fields']['uploaded_at']
                    dt= datetime.strptime(dateup, "%Y-%m-%dT%H:%M:%S.%f")

                    i['fields']['uploaded_at']=dt
                    i['page_range']=page_range

                qs_json = paginate(request, qs_json, page)
                
                if request.GET.get('fDate'):
                    fDate = request.GET.get('fDate')
                    tDate = request.GET.get('tDate')
                if request.GET.get('newfromDate'):
                    fDate = request.GET.get('newfromDate')
                    tDate = request.GET.get('newtoDate')

                if fDate!='0' or tDate!='0':

                    if tDate == '':
                        tDate = fDate
                    elif fDate == '':
                        fDate = tDate
                    from_date = datetime.strptime(fDate,'%d-%m-%Y').strftime('%Y-%m-%d %H:%M:%S')
                    to_date = datetime.strptime(tDate,'%d-%m-%Y')
                    toDate = calendar.timegm(to_date.timetuple())
                    advancedToDate = int(toDate)+86400
                    finalToDate = datetime.utcfromtimestamp(advancedToDate).strftime('%Y-%m-%d %H:%M:%S')
                    qs_json = serializers.serialize('json', files)
                    qs_json=json.loads(qs_json)
                    page = request.GET.get('page')
                    paginator = Paginator(qs_json, 5)

                    page_range=paginator.page_range

                    count=0
                    for i in qs_json:
                        count+=1
                        size = get_file_size((settings.MEDIA_DIR + '/' + i['fields']['File1']))
                        i['fields']['File_size']=str(size)
                        i['fields']['srNo']=str(count)
                        i['fields']['File1']='/media/'+i['fields']['File1']
                        dateup=i['fields']['uploaded_at']
                        dt= datetime.strptime(dateup, "%Y-%m-%dT%H:%M:%S.%f")

                        i['fields']['uploaded_at']=dt
                        i['page_range']=page_range
                    qs_json = paginate(request, qs_json, page)
                    
                    files = FileUpload.objects.filter(user_id=pk, category=type, uploaded_at__range=(from_date, finalToDate),deleted='0').order_by("-pk")

                    return sort_list(request, choiceForm, qs_json, type, 'user_files.html')
                else:
                    return sort_list(request, choiceForm, qs_json, type, 'user_files.html')
            elif option == '2':
                newOption = request.GET.get('newoption')
                starred='0'
                data = option_value(request, newOption, pk, starred)
                if newOption == '10':
                    my_list = serializers.serialize('json', data)
                    my_list=json.loads(my_list)
                    count=0
                    for i in my_list:
                        count+=1
                        size = get_file_size((settings.MEDIA_DIR + '/' + i['fields']['File1']))
                        i['fields']['File_size']=str(size)
                        i['fields']['srNo']=str(count)
                        i['fields']['File1']='/media/'+i['fields']['File1']
                        dateup=i['fields']['uploaded_at']
                        dt= datetime.strptime(dateup, "%Y-%m-%dT%H:%M:%S.%f")

                        i['fields']['uploaded_at']=dt
                else:
                    my_list=[]
                    count=0
                    for i in data:
                        count+=1
                        size = get_file_size((settings.MEDIA_DIR + '/' + str(i['File1'])))
                        i['File_size']=str(size)
                        i['srNo']=str(count)
                        i['File1']='/media/'+str(i['File1'])
                        my_list.append({'fields':i,'pk':i['id']})

                #paginator = Paginator(my_list, 5)

                my_list = paginate(request, my_list, page)
                return sort_list(request, choiceForm, my_list, type, 'user_files.html')

        args = {'files': qs_json, 'id': pk, 'username': username,'choiceForm': choiceForm, 'type': type}
        return render(request, 'user_files.html',args)
    else:
        context = {'files': None, 'id': pk, 'choiceForm': choiceForm, 'type': type, 'username': username}
        return render(request, 'user_files.html', context )


@csrf_exempt
def sort_list(request, form, files, type, template_name):
    data = dict()
    if request.method == 'GET':
        data['form_is_valid'] = True

        args = {'files': files, 'type': type}
        data['html_book_list'] = render_to_string('files/user_files_table.html',args, request=request)

        context = { 'files': files }
        data['page_div'] = render_to_string('files/pagination.html', context, request=request)

    context = {'form': form,'type': type}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


@login_required(login_url='/login/')
def total_document(request):
    # users = User.objects.all()
    if request.user.is_superuser:
        posts = User.objects.all()
        if request.GET.get('searchVal'):
            searchfor=request.GET.get('searchVal')
            
            if searchfor!='0' and searchfor!='' and searchfor!='NULL' and searchfor is not None:
                posts = User.objects.filter(Q(username__icontains = searchfor))
            elif searchfor=='0':
                
                posts = User.objects.all()

        qs_json = serializers.serialize('json', posts)
        qs_json=json.loads(qs_json)


        page = request.GET.get('page')
        #paginator = Paginator(qs_json, 5)
        
        count=0
        for i in qs_json:
            count+=1
            i['fields']['srNo']=str(count)
            i['fields']['file_count']=FileUpload.objects.filter(user_id=i['pk'],deleted='0').count()

        qs_json = paginate(request,qs_json,page)
        if request.GET.get('searchVal'):
            return total_document_list(request, qs_json, 'files/total_documents_by_users.html')
        else:
            context2 = {'files': qs_json}
            return render(request, 'files/total_documents_by_users.html',context2)

    else:
        posts = FileUpload.objects.filter(user_id=request.user.pk,deleted='0').count()

        context4 = {'posts': posts}
        return render(request, 'files/total_documents_by_users.html', context4 )


def total_document_list(request, files, template_name):
    data = dict()
    if request.method == 'GET':


        data['form_is_valid'] = True
        args = {'files': files}
        data['html_book_list'] = render_to_string('files/documents.html',args, request=request)

        args2 = {'files': files}
        data['page_div'] = render_to_string('files/pagination.html',args2,    request=request)


    data['html_form'] = render_to_string(template_name, request=request)
    return JsonResponse(data)


"""Setting favorite docs"""


def is_favorite(request, file_id):
    file = get_object_or_404(FileUpload, pk=file_id)
    try:
        if file.is_favorite:
            file.is_favorite = False
            file.save()
            return JsonResponse({'not_fav': True})
        else:
            file.is_favorite = True
            file.save()
            return JsonResponse({'fav': True})
    except (KeyError, FileUpload.DoesNotExist):
        return JsonResponse({'success': False})
    else:
        return JsonResponse({'success': True})


@login_required(login_url='/login/')
def starred_docs(request):
    data = []
    files = FileUpload.objects.filter(user__username=request.user,deleted='0').order_by("-pk")
    for file in files:
        if file.is_favorite:

            data.append(file)
    qs_json = serializers.serialize('json', data)
    qs_json=json.loads(qs_json)
    page = request.GET.get('page', 1)
    #paginator = Paginator(qs_json, 5)

    count=0
    choiceForm = ChoiceForm()
    for i in qs_json:
        count+=1
        size = get_file_size((settings.MEDIA_DIR + '/' + i['fields']['File1']))

        i['fields']['srNo']=str(count)
        i['fields']['File1']='/media/'+i['fields']['File1']
        dateup=i['fields']['uploaded_at']
        dt= datetime.strptime(dateup, "%Y-%m-%dT%H:%M:%S.%f")
        i['fields']['File_size']=str(size)

        i['fields']['uploaded_at']=dt

    qs_json = paginate(request,qs_json,page)
    if request.GET.get('optionValue'):
        option = request.GET.get('optionValue')


        if option == '0':
            
            if request.GET.get('fDate'):
                
                fDate = request.GET.get('fDate')
                tDate = request.GET.get('tDate')
            if request.GET.get('newfromDate'):
                fDate = request.GET.get('newfromDate')
                tDate = request.GET.get('newtoDate')

            if fDate!='0' or tDate!='0':

                if tDate == '':
                    tDate = fDate
                elif fDate == '':
                    fDate = tDate
                from_date = datetime.strptime(fDate, '%d-%m-%Y').strftime('%Y-%m-%d %H:%M:%S')
                to_date = datetime.strptime(tDate, '%d-%m-%Y')
                toDate = calendar.timegm(to_date.timetuple())
                advancedToDate = int(toDate) + 86400
                finalToDate = datetime.utcfromtimestamp(advancedToDate).strftime('%Y-%m-%d %H:%M:%S')
                
                files = FileUpload.objects.filter(user_id=request.user.pk, uploaded_at__range=(from_date, finalToDate),deleted='0').order_by("-pk")
                qs_json = serializers.serialize('json', files)
                qs_json=json.loads(qs_json)
                page = request.GET.get('page')
                
                paginator = Paginator(qs_json, 5)

                page_range=paginator.page_range

                count=0
                for i in qs_json:
                    count+=1
                    i['fields']['srNo']=str(count)
                    i['fields']['File1']='/media/'+i['fields']['File1']
                    dateup=i['fields']['uploaded_at']
                    dt= datetime.strptime(dateup, "%Y-%m-%dT%H:%M:%S.%f")

                    i['fields']['uploaded_at']=dt
                    i['page_range']=page_range

                qs_json = paginate(request,qs_json,page)

                return sort_starred_list(request, choiceForm, qs_json, type, 'starred_documents.html')
            else:
                return sort_starred_list(request, choiceForm, qs_json, type, 'starred_documents.html')

        elif option == '1':
            fDate = request.GET.get('fDate')
            tDate = request.GET.get('tDate')
            if fDate == '':
                fDate = tDate
            if tDate == '':
                tDate = fDate
            from_date = datetime.strptime(fDate, '%d-%m-%Y').strftime('%Y-%m-%d %H:%M:%S')

            to_date = datetime.strptime(tDate, '%d-%m-%Y')

            toDate = calendar.timegm(to_date.timetuple())

            advancedToDate = int(toDate) + 86400

            finalToDate = datetime.utcfromtimestamp(advancedToDate).strftime('%Y-%m-%d %H:%M:%S')

            files = FileUpload.objects.filter(user_id=request.user.pk, is_favorite='1', uploaded_at__range=(from_date, finalToDate),deleted='0').order_by("-pk")
            qs_json = serializers.serialize('json', files)
            qs_json=json.loads(qs_json)
            count=0
            for i in qs_json:
                count+=1
                i['fields']['srNo']=str(count)
                i['fields']['File1']='/media/'+i['fields']['File1']
                dateup=i['fields']['uploaded_at']
                dt= datetime.strptime(dateup, "%Y-%m-%dT%H:%M:%S.%f")

                i['fields']['uploaded_at']=dt
            
            page = request.GET.get('page', 1)
            #paginator = Paginator(qs_json, 5)
            qs_json = paginate(request,qs_json,page)

            return sort_starred_list(request, choiceForm, qs_json, type, 'starred_documents.html')
        elif option == '2':
            newOption = request.GET.get('newoption')
            
            starred='1'
            data = option_value(request, newOption, request.user.pk,starred)
            if newOption == '10':
                my_list = serializers.serialize('json', data)
                my_list=json.loads(my_list)
                count=0
                for i in my_list:
                    count+=1
                    i['fields']['srNo']=str(count)
                    i['fields']['File1']='/media/'+i['fields']['File1']
                    dateup=i['fields']['uploaded_at']
                    dt= datetime.strptime(dateup, "%Y-%m-%dT%H:%M:%S.%f")

                    i['fields']['uploaded_at']=dt
            else:
                my_list=[]
                count=0
                for i in data:
                    count+=1
                    size = get_file_size((settings.MEDIA_DIR + '/' + str(i['File1'])))
                    i['File_size']=str(size)
                    i['srNo']=str(count)
                    i['File1']='/media/'+str(i['File1'])
                    my_list.append({'fields':i,'pk':i['id']})
            #paginator = Paginator(my_list, 5)

            my_list = paginate(request,my_list,page)

            return sort_list(request, choiceForm, my_list, type, 'user_files.html')
    context6 = {'files': qs_json,'choiceForm':choiceForm}
    return render(request, 'starred_documents.html', context6 )


@login_required(login_url='/login/')
@csrf_exempt
def sort_starred_list(request, form, files,type, template_name):
    import json

    data = dict()
    if request.method == 'GET':
        print("JJJJJKGK")

        data['form_is_valid'] = True
        context5 = {'files': files}
        data['html_book_list'] = render_to_string('files/user_files_table.html',context5,request=request)

        data['page_div'] = render_to_string('files/pagination.html',context5, request=request)

    context = {'form':form}

    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


'''Downloading docs'''


def download_protection(request, id, type):
    if type == 'user_files':
        requested_file = get_object_or_404(FileUpload, pk=id)
    elif type == 'archive_files':
        requested_file = get_object_or_404(ArchiveFiles, pk=id) 
    if requested_file.is_protected:
        protected_pass = requested_file.protected
        return JsonResponse({'success': True, 'pass': protected_pass})
    else:
        print("Not protected")
        return JsonResponse({'success': False})


@login_required(login_url='/login/')
def download_history(request, id):
    download_file = get_object_or_404(FileUpload, pk=id)
    data = HttpResponse(download_file.File1, content_type='text/plain')
    filename = download_file.Filename
    ext = download_file.file_type
    file_name = filename
    data['Content-Disposition'] = 'attachment; filename=%s' % file_name
    ip = request.client_ip
    if download_file:
        p = Download(user=request.user, download=file_name, download_at=datetime.now(), ip_address=ip)
        p.save()
    return data


def download_externally(request, id):
    download_file = get_object_or_404(FileUpload, pk=id)
    data = HttpResponse(download_file.File1, content_type='text/plain')
    filename = download_file.Filename
    ext = download_file.file_type
    file_name = filename + '.' + ext
    data['Content-Disposition'] = 'attachment; filename=%s' % file_name
    return data


def download_files(request):
    form = NewChoiceForm()
    download_files = Download.objects.filter(user__username=request.user).order_by('-download_at')
    qs_json = serializers.serialize('json', download_files)
    qs_json=json.loads(qs_json)

    page = request.GET.get('page')
    #paginator = Paginator(qs_json, 5)
    #page_records=paginator.count

    count=0

    for i in qs_json:
        count+=1
        i['fields']['srNo']=str(count)
        i['fields']['username']=request.user
        dateup=i['fields']['download_at']
        dt= datetime.strptime(dateup, "%Y-%m-%dT%H:%M:%S.%f")
        i['fields']['download_at']=dt
        if i['fields']['ip_address']=='' or i['fields']['ip_address']=='NULL' or i['fields']['ip_address'] is None:
            i['fields']['ip_address']=''


    qs_json = paginate(request,qs_json,page)
    if request.GET.get('optionValue'):

        option = request.GET.get('optionValue')

        if option == '1':
            fDate = request.GET.get('fDate')
            tDate = request.GET.get('tDate')
            if fDate == '':
                fDate = tDate
            if tDate == '':
                tDate = fDate
            from_date = datetime.strptime(fDate, '%d-%m-%Y').strftime('%Y-%m-%d %H:%M:%S')

            to_date = datetime.strptime(tDate, '%d-%m-%Y')

            toDate = calendar.timegm(to_date.timetuple())

            advancedToDate = int(toDate) + 86400

            finalToDate = datetime.utcfromtimestamp(advancedToDate).strftime('%Y-%m-%d %H:%M:%S')
            download_files = Download.objects.filter(user__username=request.user, download_at__range=(from_date, finalToDate)).order_by('-download_at')

            qs_json = serializers.serialize('json', download_files)
            qs_json=json.loads(qs_json)
            page = request.GET.get('page', 1)
            #paginator = Paginator(qs_json, 5)
            form = NewChoiceForm()
            count=0
            for i in qs_json:
                count+=1
                i['fields']['srNo']=str(count)
                i['fields']['username']=request.user
                dateup=i['fields']['download_at']
                dt= datetime.strptime(dateup, "%Y-%m-%dT%H:%M:%S.%f")
                i['fields']['download_at']=dt
                if i['fields']['ip_address']=='' or i['fields']['ip_address']=='NULL' or i['fields']['ip_address'] is None:
                    i['fields']['ip_address']=''
            qs_json = paginate(request,qs_json,page)
            return download_list(request, form, qs_json, 'files/downloaded_files.html')

        elif option == '0':
            download_files = Download.objects.filter(user__username=request.user).order_by('-download_at')

            if request.GET.get('fDate'):
                fDate = request.GET.get('fDate')
                tDate = request.GET.get('tDate')
            if request.GET.get('newfromDate'):
                fDate = request.GET.get('newfromDate')
                tDate = request.GET.get('newtoDate')

            if fDate=='0' or tDate=='0':
                qs_json = serializers.serialize('json', download_files)
                qs_json=json.loads(qs_json)
                page = request.GET.get('page', 1)
                #paginator = Paginator(qs_json, 5)
                form = NewChoiceForm()
                count=0
                for i in qs_json:
                    count+=1
                    i['fields']['srNo']=str(count)
                    i['fields']['username']=request.user
                    dateup=i['fields']['download_at']
                    dt= datetime.strptime(dateup, "%Y-%m-%dT%H:%M:%S.%f")
                    i['fields']['download_at']=dt
                    if i['fields']['ip_address']=='' or i['fields']['ip_address']=='NULL' or i['fields']['ip_address'] is None:
                        i['fields']['ip_address']=''

                qs_json = paginate(request,qs_json,page)
                return download_list(request, form, qs_json, 'files/downloaded_files.html')
    elif request.GET.get('searchVal'):
        searchfor=request.GET.get('searchVal')

        if searchfor!='0' and searchfor!='' and searchfor!='NULL' and searchfor is not None:
            download_files = Download.objects.filter(Q(download__icontains = searchfor) | Q(user__username__icontains = searchfor),user__username=request.user).order_by('-download_at')


        elif searchfor=='0':
            download_files = Download.objects.filter(user__username=request.user).order_by('-download_at')
        qs_json = serializers.serialize('json', download_files)
        qs_json=json.loads(qs_json)

        page = request.GET.get('page', 1)
        #paginator = Paginator(qs_json, 5)
        #page_records=paginator.count

        count=0

        for i in qs_json:
            count+=1
            i['fields']['srNo']=str(count)
            i['fields']['username']=request.user
            dateup=i['fields']['download_at']
            dt= datetime.strptime(dateup, "%Y-%m-%dT%H:%M:%S.%f")
            i['fields']['download_at']=dt
            if i['fields']['ip_address']=='' or i['fields']['ip_address']=='NULL' or i['fields']['ip_address'] is None:
                i['fields']['ip_address']=''

        qs_json = paginate(request,qs_json,page)
        return download_list(request, form, qs_json, 'files/downloaded_files.html')

    elif request.GET.get('searchFOR'):
        searchfor=request.GET.get('searchFOR')

        download_files = Download.objects.filter(Q(download__icontains = searchfor) | Q(user__username__icontains = searchfor),user__username=request.user).order_by('-download_at')

        qs_json = serializers.serialize('json', download_files)
        qs_json=json.loads(qs_json)


        page = request.GET.get('page')
        #paginator = Paginator(qs_json, 5)
        #page_records=paginator.count

        count=0
        for i in qs_json:
            count+=1
            i['fields']['srNo']=str(count)
            i['fields']['username']=request.user
            dateup=i['fields']['download_at']
            dt= datetime.strptime(dateup, "%Y-%m-%dT%H:%M:%S.%f")
            i['fields']['download_at']=dt
            if i['fields']['ip_address']=='' or i['fields']['ip_address']=='NULL' or i['fields']['ip_address'] is None:
                i['fields']['ip_address']=''


        qs_json = paginate(request,qs_json,page)
        return download_list(request, form, qs_json, 'files/downloaded_files.html')
    args = {'files': qs_json,'NewChoiceForm':form}
    return render(request, 'files/downloaded_files.html',args)


'''To delete the Files'''


@csrf_exempt
def download_list(request,form,files,template_name):
    import json

    data = dict()
    if request.method == 'GET':
        
        data['form_is_valid'] = True
        args2 = { 'files': files }
        data['html_book_list'] = render_to_string('files/download_file_list.html',args2, request=request)

        data['page_div'] = render_to_string('files/pagination.html', args2, request=request)

    context = {'NewChoiceForm': form}

    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


'''To delete the Files'''
def archived_directory_path(instance, filename):
    if filename:
        ans = filename.split('.')[-1]
    date=datetime.now()
    return 'ArchivedFiles/{0}/{1}/{2}'.format(date.strftime('%d-%m-%Y'),
    instance.user.first_name+' '+instance.user.last_name, ans)

def delete_files_files(request, id, type):
    starred = '0'
    files = None
    file = get_object_or_404(FileUpload, id=id)
    file_Id = file.id
    fileName = str(file.File1).split('/')[-1]
    coded_data_path = settings.MEDIA_ROOT + '/' + str(file.File1)
    data = dict()
    if request.GET.get('delete'):
        url = request.GET.get('url_sub')
        if url == 'user_files':
            starred = '0'
        elif url == 'starred_documents':
            starred = '1'
        userID = ''
        if request.user.is_superuser:
            fuserId = FileUpload.objects.filter(id=id,deleted='0')
            for u in fuserId:
                userID = str(u.user_id)
        else:
            userID = request.user.pk
        # file.delete()
        path = archived_directory_path(file, fileName)
        new_path = settings.MEDIA_ROOT + '/' + str(archived_directory_path(file, fileName))
        if not os.path.isdir(os.path.join(settings.MEDIA_ROOT + '/' + path + '/')):
            print('path not exists')
            try:
                os.makedirs(os.path.join(settings.MEDIA_ROOT + '/' + path + '/'))
            except OSError as exc:  # Guard against race condition
                print("path does not exists")
        if coded_data_path:
            move(coded_data_path, new_path)
        FileUpload.objects.filter(pk=file_Id).update(deleted='1')
        a = ArchiveFiles.objects.create(file_id=file, file_type=file.file_type, Uploader=file.Uploader,
                                        Filename=file.Filename, File1=path + '/' + fileName,
                                        user=request.user, is_favorite=file.is_favorite, is_shared=file.is_shared,
                                        protected=file.protected, is_protected=file.is_protected,
                                        is_encrypted=file.is_encrypted, category=file.category)
        a.save()
        if starred == '0':
            files = FileUpload.objects.filter(user_id=userID, category=type, deleted='0').order_by("-pk")
        elif starred == '1':
            files = FileUpload.objects.filter(user_id=userID, is_favorite=starred, deleted='0').order_by("-pk")

        qs_json = serializers.serialize('json', files)
        qs_json = json.loads(qs_json)

        page = request.GET.get('page', 1)
        #paginator = Paginator(qs_json, 5)
        #page_records=paginator.count
        count = 0
        for i in qs_json:
            count+=1
            i['fields']['srNo']=str(count)
            i['fields']['File1']='/media/'+i['fields']['File1']
            dateup=i['fields']['uploaded_at']
            dt= datetime.strptime(dateup, "%Y-%m-%dT%H:%M:%S.%f")

            i['fields']['uploaded_at'] = dt

        qs_json = paginate(request,qs_json,page)
        if request.GET.get('fDate') or request.GET.get('tDate'):
            fDate = request.GET.get('fDate')
            tDate = request.GET.get('tDate')
            if fDate == '':
                fDate = tDate
            if tDate == '':
                tDate = fDate
            from_date = datetime.strptime(fDate, '%d-%m-%Y').strftime('%Y-%m-%d %H:%M:%S')

            to_date = datetime.strptime(tDate, '%d-%m-%Y')

            toDate = calendar.timegm(to_date.timetuple())

            advancedToDate = int(toDate) + 86400

            finalToDate = datetime.utcfromtimestamp(advancedToDate).strftime('%Y-%m-%d %H:%M:%S')
            if starred == '0':
                files = FileUpload.objects.filter(user_id=userID, category=type,deleted = '0',
                                                  uploaded_at__range=(from_date, finalToDate)).order_by("-pk")
            elif starred == '1':
                files = FileUpload.objects.filter(user_id=userID, category=type,deleted = '0',
                                                  is_favorite=starred,uploaded_at__range=(from_date, finalToDate)).order_by("-pk")
            qs_json = serializers.serialize('json', files)
            qs_json=json.loads(qs_json)

            page = request.GET.get('page', 1)
            #paginator = Paginator(qs_json, 5)
            #page_records=paginator.count

            count = 0

            for i in qs_json:
                count+=1
                i['fields']['srNo']=str(count)
                i['fields']['File1']='/media/'+i['fields']['File1']
                dateup=i['fields']['uploaded_at']
                dt= datetime.strptime(dateup, "%Y-%m-%dT%H:%M:%S.%f")

                i['fields']['uploaded_at']=dt

            qs_json = paginate(request,qs_json,page)

            return deletefilefunction(request, qs_json, 'user_files.html')
        elif request.GET.get('newfromDate') or request.GET.get('newtoDate'):

            newOption=request.GET.get('newOption')

            data = option_value(request, newOption, userID,starred)
            
            if newOption == '10':
                
                my_list = serializers.serialize('json', data)
                my_list=json.loads(my_list)
                count = 0
                for i in my_list:
                    count += 1
                    i['fields']['srNo'] = str(count)
                    i['fields']['File1'] = '/media/'+i['fields']['File1']
                    dateup = i['fields']['uploaded_at']
                    dt = datetime.strptime(dateup, "%Y-%m-%dT%H:%M:%S.%f")

                    i['fields']['uploaded_at'] = dt
            else:

                my_list = []
                count = 0
                for i in data:
                    count += 1
                    size = get_file_size((settings.MEDIA_DIR + '/' + str(i['File1'])))
                    i['File_size'] = str(size)
                    i['srNo'] = str(count)
                    i['File1'] = '/media/'+str(i['File1'])
                    my_list.append({'fields': i, 'pk': i['id']})
            page = request.GET.get('page', 1)
            #paginator = Paginator(my_list, 5)

            qs_json = paginate(request,my_list,page)

            return deletefilefunction(request, my_list,type, 'user_files.html')
        elif request.GET.get('searchFOR'):
            
            searchfor = request.GET.get('searchFOR')
            filter_res = ''
            if searchfor != '':
                if starred == '0':
                    filter_res = FileUpload.objects.filter(Q(Filename__icontains = searchfor) | Q(Uploader__icontains = searchfor),user_id=userID, category=type,deleted='0').order_by("-pk")
                elif starred == '1':
                    filter_res = FileUpload.objects.filter(
                        Q(Filename__icontains=searchfor) | Q(Uploader__icontains=searchfor), user_id=userID,
                        category=type, is_favorite=starred,deleted='0').order_by("-pk")
            else:
                if starred == '0':
                    filter_res = FileUpload.objects.filter(user_id=userID, category=type,deleted='0').order_by("-pk")
                elif starred == '1':

                    filter_res = FileUpload.objects.filter(user_id=userID, category=type, is_favorite=starred,deleted='0').order_by("-pk")
            qs_json = serializers.serialize('json', filter_res)
            qs_json=json.loads(qs_json)

            page = request.GET.get('page', 1)
            #paginator = Paginator(qs_json, 5)
            #page_records = paginator.count

            count=0

            for i in qs_json:
                count += 1
                i['fields']['srNo']=str(count)
                i['fields']['File1'] = '/media/'+i['fields']['File1']
                dateup = i['fields']['uploaded_at']
                dt = datetime.strptime(dateup, "%Y-%m-%dT%H:%M:%S.%f")

                i['fields']['uploaded_at'] = dt

            qs_json = paginate(request,qs_json,page)

        elif request.GET.get('meta_search_text'):
            res_id = []
            searchfor = request.GET.get('meta_search_text')
            
            filter_res = FileUpload.objects.filter(user_id=request.user.pk,deleted='0').order_by("-pk")
            for h in filter_res:

                if h.file_type == 'pdf':

                    pdf_pages = []
                    object = PyPDF2.PdfFileReader(settings.MEDIA_ROOT + '/' + str(h.File1))
                    NumPages = object.getNumPages()

                    for i in range(0, NumPages):

                        PageObj = object.getPage(i)
                        Text = PageObj.extractText()
                        Text = Text.lower()
                        searchfor = searchfor.lower()
                        if (searchfor in Text) or (searchfor.replace(" ", "") in Text):
                            pdf_pages.append(str(i))

                    if pdf_pages != []:
                        res_id.append(h.pk)


                elif h.file_type == 'xlsx' or h.file_type == 'xls' or h.file_type == 'XLS' or h.file_type == 'csv':
                    if h.file_type == 'xls' or h.file_type == 'XLS':

                        file = (settings.MEDIA_ROOT + '/' + str(h.File1)).split('/')[-1]
                        fileName = file.split('.')[0]
                        dest = os.path.dirname(settings.MEDIA_ROOT + '/' + str(h.File1))

                        copyfile(settings.MEDIA_ROOT + '/' + str(h.File1), dest + '/' + fileName + '_1.xls')
                        os.rename(dest + '/' + fileName + '_1.xls', dest + '/' + fileName + '_1.xlsx')

                        wb = pd.ExcelFile(dest + '/' + fileName + '_1.xlsx')
                        removable_part = (dest + '/' + fileName + '_1.xlsx').split('/')[-1]
                        file_name = removable_part.split('.')[0]
                        path = (dest + '/' + fileName + '_1.xlsx').replace(removable_part, '')
                        total_sheets = wb.sheet_names
                        result = []
                        for sheet in total_sheets:
                            df = pd.read_excel(wb, sheet)
                            result.append(df)
                            html_data = df.to_html()
                            soup = BeautifulSoup(html_data, 'html.parser')
                            op = soup.prettify()
                            op = soup.get_text()
                            for data in op:
                                f = open(path + file_name + ".txt", "a+", encoding="utf-8")
                                f.write(str(data))
                                f.close()
                        if os.path.exists(dest + '/' + fileName + '_1.xlsx'):
                            os.remove(dest + '/' + fileName + '_1.xlsx')
                        else:
                            print("File doesn't exists")

                        data = open(dest + '/' + fileName + '_1.txt', 'r', encoding="ISO-8859-1").read()
                        data = data.lower()
                        searchfor = searchfor.lower()
                        if searchfor in data:
                            res_id.append(h.pk)
                    elif h.file_type == 'xlsx':

                        dest = os.path.dirname(settings.MEDIA_ROOT + '/' + str(h.File1))
                        wb = pd.ExcelFile(settings.MEDIA_ROOT + '/' + str(h.File1))
                        removable_part = str(settings.MEDIA_ROOT + '/' + str(h.File1)).split('/')[-1]
                        file_name = removable_part.split('.')[0]

                        path = str(settings.MEDIA_ROOT + '/' + str(h.File1)).replace(removable_part, '')
                        total_sheets = wb.sheet_names
                        result = []
                        for sheet in total_sheets:
                            df = pd.read_excel(wb, sheet)
                            result.append(df)
                            html_data = df.to_html()
                            soup = BeautifulSoup(html_data, 'html.parser')
                            op = soup.prettify()
                            op = soup.get_text()
                            for data in op:
                                f = open(path + file_name + ".txt", "a+", encoding="utf-8")
                                f.write(str(data))
                                f.close()
                        data = open(dest + '/' + file_name + '.txt', 'r', encoding="ISO-8859-1").read()
                        data = data.lower()
                        searchfor = searchfor.lower()
                        if searchfor in data:
                            res_id.append(h.pk)


                elif (h.file_type == 'docx' or h.file_type == 'doc'):

                    import fulltext
                    text = fulltext.get(settings.MEDIA_ROOT + '/' + str(h.File1), '< no content >')
                    text = text.lower()
                    searchfor = searchfor.lower()
                    if searchfor in text:
                        res_id.append(h.pk)




                else:
                    data = open(settings.MEDIA_ROOT + '/' + str(h.File1), 'r', encoding="ISO-8859-1").read()
                    data = data.lower()
                    searchfor = searchfor.lower()
                    if searchfor in data:
                        res_id.append(h.pk)

            if res_id != []:
                filter_res = FileUpload.objects.filter(id__in=res_id,deleted='0')
                qs_json = serializers.serialize('json', filter_res)
                qs_json = json.loads(qs_json)

                page = request.GET.get('page', 1)
                #paginator = Paginator(qs_json, 4)
                #page_records = paginator.count

                count = 0

                for i in qs_json:
                    count += 1
                    i['fields']['srNo'] = str(count)
                    i['fields']['File1'] = '/media/' + i['fields']['File1']
                    dateup = i['fields']['uploaded_at']
                    dt = datetime.strptime(dateup, "%Y-%m-%dT%H:%M:%S.%f")

                    i['fields']['uploaded_at'] = dt

                qs_json = paginate(request,qs_json,page)

            else:
                qs_json = None
            data = dict()
            args4 = { 'files': qs_json } 
            data['html_book_list'] = render_to_string('files/user_files_table.html', args4, request=request)

            data['page_div'] = render_to_string('files/pagination.html',args4, request=request)

            data['html_form'] = render_to_string('metasearch.html', request=request)
            return JsonResponse(data)
        return deletefilefunction(request, qs_json, type, 'user_files.html')     

    else:

        context = {'file': file, 'type': type}
        data['html_form'] = render_to_string('files/confirm_file_delete.html',
            context,
            request = request,
        )
        return JsonResponse(data)


def deletefilefunction(request, files, type, template_name):
    data = dict()
    if request.method == 'GET':
        data['form_is_valid'] = True
        args = {'files': files, 'type': type}
        data['html_book_list'] = render_to_string('files/user_files_table.html', args, request=request)
        args2 = { 'files': files}
        data['page_div'] = render_to_string('files/pagination.html', args2, request=request)

    context = {'type': type}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


@login_required
def send_to_share(request, id):
    doc = get_object_or_404(FileUpload, pk=id)
    if request.user.is_authenticated:
        try:
            if doc.is_shared:
                pass
            else:
                filename = doc.File1.name.split('/')[-1]
                data = HttpResponse(doc.File1, content_type='text/plain')
                data['Content-Disposition'] = 'attachment; filename=%s' % filename
                p = SharedDocument(user=request.user, status='drafted', file=doc, timestamp=datetime.now())
                p.save()
        except (KeyError, FileUpload.DoesNotExist):
            return JsonResponse({'success': False})
        else:
            return JsonResponse({'success': True})
    else:
        return JsonResponse({'success': False})


def stop_sharing(request, id):
    doc = get_object_or_404(SharedDocument, pk=id)
    if request.user.is_authenticated:
        try:
            if doc:
                doc.delete()
        except (KeyError, SharedDocument.DoesNotExist):
            return JsonResponse({'success': False})
        else:
            return JsonResponse({'success': True})
    else:
        return JsonResponse({'success': False})


@login_required
def share_document_to_user(request, file_id):
    doc = get_object_or_404(FileUpload, pk=file_id)
    if request.method == 'POST':
        p = SharedDocument(user=request.user, status='drafted', file=doc, timestamp=datetime.now())
        p.save()
        file = SharedDocument.objects.get(pk=p.pk)
        form = SharedDocumentRequestForm(request.POST, request.FILES, instance=file)
        if form.is_valid():
            form_set = form.save(commit=False)
            form_set.save()
            p = Notification.objects.create(actor=request.user, sender=file.user, receiver=file.requested_user,
                                            object_id=file.file.id, object_type=file.file, status=file,
                                            type='Share File', unread=True)
            p.save()
            return JsonResponse({'success': True})
    else:
        form = SharedDocumentRequestForm()
    argds = {'form': form, 'doc': doc}
    return render(request, 'files/shared_document_to_user.html', argds)


@login_required(login_url='/login/')
def shared_document(request):
    shared_files = SharedDocument.objects.filter(status='Approved', user__username=request.user,file__deleted='0')
    qs_json = serializers.serialize('json', shared_files)
    qs_json = json.loads(qs_json)

    page = request.GET.get('page', 1)
    #paginator = Paginator(qs_json, 5)
    #page_records = paginator.count

    count = 0
    for i in qs_json:
        count += 1
        i['fields']['srNo'] = str(count)
        user = User.objects.get(pk = i['fields']['user'])
        i['fields']['user'] = user
        file = FileUpload.objects.get(pk=i['fields']['file'])
        i['fields']['file'] = file
    
    qs_json = paginate(request,qs_json,page)
    context = {'files': qs_json}
    return render(request, 'files/shared_documents.html', context)


@login_required(login_url='/login/')
def shared_by_all(request):
    shared_files = SharedDocument.objects.filter(status='Approved',file__deleted='0')
    qs_json = serializers.serialize('json', shared_files)
    qs_json = json.loads(qs_json)

    page = request.GET.get('page', 1)
    #paginator = Paginator(qs_json, 5)
    #page_records = paginator.count

    count = 0
    for i in qs_json:
        count += 1
        i['fields']['srNo'] = str(count)
        user = User.objects.get(pk=i['fields']['user'])
        sharedWith = User.objects.get(pk=i['fields']['requested_user'])
        i['fields']['user'] = user
        i['fields']['requested_user'] = sharedWith
        file = FileUpload.objects.get(pk=i['fields']['file'])
        i['fields']['file'] = file

    qs_json = paginate(request,qs_json,page)

    args2 = {'files':qs_json }
    return render(request, 'files/all_shared_documents.html',args2)


@login_required(login_url='/login/')
def shared_by_other(request):
    shared_files = SharedDocument.objects.filter(status='Approved', requested_user__username=request.user,file__deleted='0')
    qs_json = serializers.serialize('json', shared_files)
    qs_json = json.loads(qs_json)

    page = request.GET.get('page', 1)
    #paginator = Paginator(qs_json, 5)
    #page_records = paginator.count

    count = 0
    for i in qs_json:
        count += 1
        i['fields']['srNo'] = str(count)
        user = User.objects.get(pk=i['fields']['user'])
        i['fields']['user'] = user
        file = FileUpload.objects.get(pk=i['fields']['file'])
        i['fields']['file'] = file

    qs_json = paginate(request,qs_json,page)
    opi = {'files': qs_json}
    return render(request, 'files/shared_by_other.html',opi)


@login_required(login_url='/login/')
def share_files_externally(request, file_id):
    file = get_object_or_404(FileUpload, pk=file_id)

    if request.method == 'POST':
        form = SharedDocumentExternally(request.POST, request.FILES)
        if form.is_valid():
            current_site = get_current_site(request)
            name = form.cleaned_data['name']
            email = form.cleaned_data['email']
            mail_subject = 'Document Recieved..!!'
            Filename = file.Filename + '.' + file.file_type
            From = request.user.email
            print('User mail id', From)
            args9 = { 'user': email, 'domain': current_site.domain, 'uid': urlsafe_base64_encode(force_bytes(file.id)).decode(), 'token': File_encrypt_token.make_token(file), 'protocol': 'https', 'name': name, 'first_name': request.user.first_name, 'last_name': request.user.last_name }
            message = render_to_string('files/shared_documents_email.html',args9) 
            try:
                #Change the From recepient here with From variable to let customer have DMS user's mail id.
                mail = EmailMessage(mail_subject, message, settings.EMAIL_HOST_USER, to=[email])
                print('Mail', mail.send())
            except:
                return "Attachment error"
            messages.success(request, 'Your mail with selected attachment has been sent')

    else:
        form = SharedDocumentExternally()
    argsdd = {'form': form, 'doc': file}
    return render(request, 'files/share_files_externally.html',argsdd )


def document_shared_externally(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        file = FileUpload.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, FileUpload.DoesNotExist):
        file = None
    if file is not None and File_encrypt_token.check_token(file, token):
        context4 = {'file':file}
        return render(request, 'other_files/document_shared_externally.html',context4)
    else:
        return HttpResponse('Activation link is invalid!')


'''Protecting Doc'''

@csrf_exempt
def document_protection(request, file_id):
    data = dict()
    file = get_object_or_404(FileUpload, pk=file_id)
    if request.method == 'POST':
        if request.user.is_authenticated and request.is_ajax:
            password = request.POST.get('password')
            if file.is_protected:
                if check_password(password, file.protected):
                    file.is_protected = False
                    file.protected = None
                    file.save()
                    data = {'is_not_protected': 'File is not protected now.!'}
                    return JsonResponse(data)
                else:
                    data = {'wrong_password': 'Wrong password Entered.!'}
                    return JsonResponse(data)

            else:
                file.is_protected = True
                encrypted_password = make_password(password)
                file.protected = encrypted_password

                file.save()
                data = {'protected': 'File is now protected.!'}
                return JsonResponse(data)

    else:
        context = {'file': file}
        data['html_form'] = render_to_string('files/protecting_doc.html', context, request=request)
        return JsonResponse(data)


'''Search'''

@login_required
@csrf_exempt
def search_files(request,pk):

    if request.GET.get('searchVal'):

        searchfor=request.GET.get('searchVal')

        if searchfor!='0' and searchfor!='' and searchfor!='NULL' and searchfor is not None:
            filter_res = FileUpload.objects.filter(Q(Filename__icontains = searchfor) | Q(Uploader__icontains = searchfor),user_id=pk).order_by("-pk")

        elif searchfor=='0':
            filter_res = FileUpload.objects.filter(user_id=pk).order_by("-pk")
        qs_json = serializers.serialize('json', filter_res)
        qs_json=json.loads(qs_json)

        page = request.GET.get('page', 1)
        paginator = Paginator(qs_json, 5)
        page_records=paginator.count

        count=0

        for i in qs_json:
            count+=1
            i['fields']['srNo']=str(count)
            size = get_file_size((settings.MEDIA_DIR + '/' + i['fields']['File1']))
            i['fields']['File_size']=str(size)
            i['fields']['File1']='/media/'+i['fields']['File1']
            dateup=i['fields']['uploaded_at']
            dt= datetime.strptime(dateup, "%Y-%m-%dT%H:%M:%S.%f")

            i['fields']['uploaded_at']=dt

        try:
            qs_json = paginator.page(page)

        except PageNotAnInteger:
            qs_json = paginator.page(1)
        except EmptyPage:
            qs_json = paginator.page(paginator.num_pages)
        return search_list(request, qs_json, 'files/search.html')

    else:

        if request.GET.get('searchFOR'):
            searchfor=request.GET.get('searchFOR')
            filter_res = FileUpload.objects.filter(Q(Filename__icontains = searchfor) | Q(Uploader__icontains = searchfor),user_id=pk).order_by("-pk")
            if filter_res:
                qs_json = serializers.serialize('json', filter_res)
                qs_json=json.loads(qs_json)

                page = request.GET.get('page')
                paginator = Paginator(qs_json, 5)
                page_records=paginator.count

                count=0

                for i in qs_json:
                    count+=1
                    i['fields']['srNo']=str(count)
                    size = get_file_size((settings.MEDIA_DIR + '/' + i['fields']['File1']))
                    i['fields']['File_size']=str(size)
                    i['fields']['File1']='/media/'+i['fields']['File1']
                    dateup=i['fields']['uploaded_at']
                    dt= datetime.strptime(dateup, "%Y-%m-%dT%H:%M:%S.%f")

                    i['fields']['uploaded_at']=dt

                try:
                    qs_json = paginator.page(page)

                except PageNotAnInteger:
                    qs_json = paginator.page(1)
                except EmptyPage:
                    qs_json = paginator.page(paginator.num_pages)
                return search_list(request, qs_json, 'files/search.html')

        else:
            searchfor=request.GET.get('searchMe')
            if searchfor:

                filter_res = FileUpload.objects.filter(Q(Filename__icontains = searchfor) | Q(Uploader__icontains = searchfor),user_id=pk).order_by("-pk")
                if filter_res:
                    qs_json = serializers.serialize('json', filter_res)
                    qs_json=json.loads(qs_json)

                    page = request.GET.get('page', 1)
                    paginator = Paginator(qs_json, 5)
                    page_records=paginator.count

                    count=0

                    for i in qs_json:
                        count+=1
                        i['fields']['srNo']=str(count)
                        size = get_file_size((settings.MEDIA_DIR + '/' + i['fields']['File1']))
                        i['fields']['File_size']=str(size)
                        i['fields']['File1']='/media/'+i['fields']['File1']
                        dateup=i['fields']['uploaded_at']
                        dt= datetime.strptime(dateup, "%Y-%m-%dT%H:%M:%S.%f")

                        i['fields']['uploaded_at']=dt

                    try:
                        qs_json = paginator.page(page)

                    except PageNotAnInteger:
                        qs_json = paginator.page(1)
                    except EmptyPage:
                        qs_json = paginator.page(paginator.num_pages)
                    args0 = {'files': qs_json,'searchfor':searchfor}
                    return render(request, 'files/search.html',args0)
                else:
                    args00 = {'files': None,'searchfor':searchfor}
                    return render(request, 'files/search.html',args00)
            else:

                filter_res = FileUpload.objects.filter(user_id=pk).order_by("-pk")
                qs_json = serializers.serialize('json', filter_res)
                qs_json=json.loads(qs_json)

                page = request.GET.get('page', 1)
                paginator = Paginator(qs_json, 5)
                page_records=paginator.count

                count=0

                for i in qs_json:
                    count+=1
                    i['fields']['srNo']=str(count)
                    size = get_file_size((settings.MEDIA_DIR + '/' + i['fields']['File1']))
                    i['fields']['File_size']=str(size)
                    i['fields']['File1']='/media/'+i['fields']['File1']
                    dateup=i['fields']['uploaded_at']
                    dt= datetime.strptime(dateup, "%Y-%m-%dT%H:%M:%S.%f")

                    i['fields']['uploaded_at']=dt

                try:
                    qs_json = paginator.page(page)

                except PageNotAnInteger:
                    qs_json = paginator.page(1)
                except EmptyPage:
                    qs_json = paginator.page(paginator.num_pages)
                context0 = {'files': qs_json}
                return render(request, 'files/search.html',context0)


def search_list(request, files, template_name):
    data = dict()
    if request.method == 'GET':

        data['form_is_valid'] = True
        context = { 'files': files}
        data['html_book_list'] = render_to_string('files/user_files_table.html',context, request=request)
        
        data['page_div'] = render_to_string('files/pagination.html', context, request=request)


    data['html_form'] = render_to_string(template_name, request=request)
    return JsonResponse(data)


'''Accesible History'''


@login_required(login_url='/login/')
def access_history_table(request):
    if request.user.is_superuser:
        hist = FileUpload.history.filter(Q(history_type='+') | Q(history_type='-'),deleted='0').order_by("-pk")
    else:
        hist = FileUpload.history.filter(Q(history_type='+') | Q(history_type='-'), user_id=request.user,deleted='0').order_by("-pk")

    if request.GET.get('searchVal'):
        searchfor=request.GET.get('searchVal')

        if searchfor!='0' and searchfor!='' and searchfor!='NULL' and searchfor is not None:
            if request.user.is_superuser:
                hist = FileUpload.history.filter(Q(Filename__icontains = searchfor) | Q(user__username__icontains = searchfor),
						Q(history_type='+') | Q(history_type='-'),deleted='0').order_by("-pk")
            else:
                hist = FileUpload.history.filter(Q(Filename__icontains = searchfor) | Q(user__username__icontains = searchfor),
						Q(history_type='+') | Q(history_type='-'), user_id=request.user,deleted='0').order_by("-pk")

        elif searchfor=='0':

            if request.user.is_superuser:
                hist = FileUpload.history.filter(deleted='0').order_by("-pk")
            else:
                hist = FileUpload.history.filter(user_id=request.user,deleted='0').order_by("-pk")
        qs_json = serializers.serialize('json', hist)
        qs_json=json.loads(qs_json)


        page = request.GET.get('page', 1)
        #paginator = Paginator(qs_json, 5)
        #page_records=paginator.count

        count=0
        
        for i in qs_json:

            count+=1
            i['fields']['srNo']=str(count)

            dateup=i['fields']['history_date']
            dt= datetime.strptime(dateup, "%Y-%m-%dT%H:%M:%S.%f")
            i['fields']['history_date']=dt
            hist = FileUpload.history.filter(id=i['fields']['id'])
            for h in hist:
                ans=h.history_object
            i['fields']['username']=ans
            if i['fields']['history_user']=='' or i['fields']['history_user']=='NULL' or i['fields']['history_user'] is None:
                i['fields']['history_user']=''

        qs_json = paginate(request,qs_json,page)
        return access_history_search_list(request, qs_json, 'history_table.html')

    elif request.GET.get('searchFOR'):
        searchfor=request.GET.get('searchFOR')
        if request.user.is_superuser:
            hist = FileUpload.history.filter(Q(Filename__icontains = searchfor) | Q(user__username__icontains = searchfor),
						Q(history_type='+') | Q(history_type='-'),deleted='0').order_by("-pk")
        else:
            hist = FileUpload.history.filter(Q(Filename__icontains = searchfor) | Q(user__username__icontains = searchfor),
						Q(history_type='+') | Q(history_type='-'), user_id=request.user,deleted='0').order_by("-pk")
        qs_json = serializers.serialize('json', hist)
        qs_json=json.loads(qs_json)


        page = request.GET.get('page')
        #paginator = Paginator(qs_json, 5)
        #page_records=paginator.count

        count=0
        
        for i in qs_json:

            count+=1
            i['fields']['srNo']=str(count)

            dateup=i['fields']['history_date']
            dt= datetime.strptime(dateup, "%Y-%m-%dT%H:%M:%S.%f")
            i['fields']['history_date']=dt
            hist = FileUpload.history.filter(id=i['fields']['id'])
            for h in hist:
                ans=h.history_object
            i['fields']['username']=ans
            if i['fields']['history_user']=='' or i['fields']['history_user']=='NULL' or i['fields']['history_user'] is None:
                i['fields']['history_user']=''

        qs_json = paginate(request,qs_json,page)
        return access_history_search_list(request, qs_json, 'history_table.html')


    qs_json = serializers.serialize('json', hist)
    qs_json=json.loads(qs_json)


    page = request.GET.get('page', 1)
    #paginator = Paginator(qs_json, 5)
    #page_records=paginator.count

    count=0
    
    for i in qs_json:

        count+=1
        i['fields']['srNo']=str(count)

        dateup=i['fields']['history_date']
        dt= datetime.strptime(dateup, "%Y-%m-%dT%H:%M:%S.%f")
        i['fields']['history_date']=dt
        hist = FileUpload.history.filter(id=i['fields']['id'])
        for h in hist:
            ans=h.history_object
        i['fields']['username']=ans
        if i['fields']['history_user']=='' or i['fields']['history_user']=='NULL' or i['fields']['history_user'] is None:
            i['fields']['history_user']=''

    qs_json = paginate(request,qs_json,page)
    context = {'files': qs_json}
    return render(request, 'history_table.html',context)


def access_history_search_list(request, files, template_name):
    data = dict()
    if request.method == 'GET':


        data['form_is_valid'] = True
        args = {'files': files}
        data['html_book_list'] = render_to_string('files/history_list.html',args, request=request)

        data['page_div'] = render_to_string('files/pagination.html',args, request=request)


    data['html_form'] = render_to_string(template_name, request=request)
    return JsonResponse(data)



'''For Archival'''


@login_required(login_url='/login/')
def archive_files(request):

    files = ArchiveFiles.objects.filter(user_id=request.user.pk,deleted='0').order_by('-deleted_at')
    qs_json = serializers.serialize('json', files)
    qs_json=json.loads(qs_json)
    page = request.GET.get('page', 1)
    #paginator = Paginator(qs_json, 4)
    form = NewChoiceForm()
    count=0
    for i in qs_json:
        count+=1
        i['fields']['srNo']=str(count)
        i['fields']['File1']='/media/'+i['fields']['File1']
        # dateup=i['fields']['uploaded_at']
        # dt= datetime.strptime(dateup, "%Y-%m-%dT%H:%M:%S.%f")
        #
        # i['fields']['uploaded_at']=dt
        datedel=i['fields']['deleted_at']
        dt2= datetime.strptime(datedel, "%Y-%m-%dT%H:%M:%S.%f")

        i['fields']['deleted_at']=dt2

    qs_json = paginate(request,qs_json,page)
    if request.GET.get('optionValue'):

        option = request.GET.get('optionValue')

        if option == '1':
            fDate = request.GET.get('fDate')
            tDate = request.GET.get('tDate')
            if fDate == '':
                fDate = tDate
            if tDate == '':
                tDate = fDate
            from_date = datetime.strptime(fDate, '%d-%m-%Y').strftime('%Y-%m-%d %H:%M:%S')

            to_date = datetime.strptime(tDate, '%d-%m-%Y')

            toDate = calendar.timegm(to_date.timetuple())

            advancedToDate = int(toDate) + 8640000

            finalToDate = datetime.utcfromtimestamp(advancedToDate).strftime('%Y-%m-%d %H:%M:%S')
            files = ArchiveFiles.objects.filter(user_id=request.user.pk, deleted_at__range=(from_date, finalToDate),deleted='0').order_by('-deleted_at')
            qs_json = serializers.serialize('json', files)
            qs_json=json.loads(qs_json)
            page = request.GET.get('page', 1)
            #paginator = Paginator(qs_json, 4)
            form = NewChoiceForm()
            count=0
            for i in qs_json:
                count+=1
                i['fields']['srNo']=str(count)
                i['fields']['File1']='/media/'+i['fields']['File1']
                # dateup=i['fields']['uploaded_at']
                # dt= datetime.strptime(dateup, "%Y-%m-%dT%H:%M:%S.%f")
                #
                # i['fields']['uploaded_at']=dt
                datedel=i['fields']['deleted_at']
                dt2= datetime.strptime(datedel, "%Y-%m-%dT%H:%M:%S.%f")

                i['fields']['deleted_at']=dt2

            qs_json = paginate(request,qs_json,page)
            return archive_list(request, form, qs_json, 'archive_files.html')

        elif option == '0':
            
            files = ArchiveFiles.objects.filter(user_id=request.user.pk,deleted='0').order_by('-deleted_at')

            if request.GET.get('fDate'):
                
                fDate = request.GET.get('fDate')
                tDate = request.GET.get('tDate')
            if request.GET.get('newfromDate'):
                fDate = request.GET.get('newfromDate')
                tDate = request.GET.get('newtoDate')

            if fDate=='0' or tDate=='0':
                qs_json = serializers.serialize('json', files)
                qs_json=json.loads(qs_json)
                page = request.GET.get('page', 1)
                #paginator = Paginator(qs_json, 4)
                form = NewChoiceForm()
                count=0
                for i in qs_json:
                    count+=1
                    i['fields']['srNo']=str(count)
                    i['fields']['File1']='/media/'+i['fields']['File1']
                    # dateup=i['fields']['uploaded_at']
                    # dt= datetime.strptime(dateup, "%Y-%m-%dT%H:%M:%S.%f")
                    #
                    # i['fields']['uploaded_at']=dt
                    datedel=i['fields']['deleted_at']
                    dt2= datetime.strptime(datedel, "%Y-%m-%dT%H:%M:%S.%f")

                    i['fields']['deleted_at']=dt2

                qs_json = paginate(request,qs_json,page)
                return archive_list(request, form, qs_json, 'archive_files.html')
    opi = {'files':qs_json,'NewChoiceForm':form}
    return render(request,'archive_files.html',opi)


@csrf_exempt
def archive_list(request,form,files,template_name):
    import json

    data = dict()
    if request.method == 'GET':
        print("JJJJJKGK")

        data['form_is_valid'] = True
        args = {'files': files}
        data['html_book_list'] = render_to_string('files/archve_files_table.html', args, request=request)

        data['page_div'] = render_to_string('files/pagination.html', args, request=request)

    context = {'NewChoiceForm': form}

    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)

def delete_archive_files(request, id, type):
    form = NewChoiceForm()
    file = get_object_or_404(ArchiveFiles, id=id)
    data = dict()
    if request.GET.get('delete'):


        # file.delete()
        ArchiveFiles.objects.filter(pk=file.id).update(deleted='1')
        files = ArchiveFiles.objects.filter(user_id=request.user.pk,deleted='0').order_by('-deleted_at')
        if request.GET.get('fDate') or request.GET.get('tDate'):
            fDate=request.GET.get('fDate')
            tDate=request.GET.get('tDate')
            if fDate == '':
                fDate = tDate
            if tDate == '':
                tDate = fDate
            from_date = datetime.strptime(fDate, '%d-%m-%Y').strftime('%Y-%m-%d %H:%M:%S')

            to_date = datetime.strptime(tDate, '%d-%m-%Y')

            toDate = calendar.timegm(to_date.timetuple())

            advancedToDate = int(toDate) + 86400

            finalToDate = datetime.utcfromtimestamp(advancedToDate).strftime('%Y-%m-%d %H:%M:%S')
            files = ArchiveFiles.objects.filter(user_id=request.user.pk, deleted_at__range=(from_date, finalToDate)).order_by('-deleted_at')


        qs_json = serializers.serialize('json', files)
        qs_json=json.loads(qs_json)
        page = request.GET.get('page', 1)
        #paginator = Paginator(qs_json, 5)
        count=0
        for i in qs_json:
            count+=1
            i['fields']['srNo']=str(count)
            i['fields']['File1']='/media/'+i['fields']['File1']
            #dateup=i['fields']['uploaded_at']
            #dt= datetime.strptime(dateup, "%Y-%m-%dT%H:%M:%S.%f")

            #i['fields']['uploaded_at']=dt
            datedel=i['fields']['deleted_at']
            dt2= datetime.strptime(datedel, "%Y-%m-%dT%H:%M:%S.%f")

            i['fields']['deleted_at']=dt2

        qs_json = paginate(request,qs_json,page)

        return deletearchivefilefunction(request, qs_json, 'archive_files.html',form)

    else:

        context = {'file': file,'type': type}
        data['html_form'] = render_to_string('files/confirm_archivefile_delete.html',
            context,
            request=request,
        )
        return JsonResponse(data)


def deletearchivefilefunction(request, files, template_name,form):
    data = dict()
    if request.method == 'GET':

        data['form_is_valid'] = True
        args = {'files': files}
        data['html_book_list'] = render_to_string('files/archve_files_table.html', args, request=request)

        data['page_div'] = render_to_string('files/pagination.html', args ,request=request)
    context={'NewChoiceForm':form}
    data['html_form'] = render_to_string(template_name, context, request=request)
    return JsonResponse(data)


'''For Notification'''


@login_required(login_url='/login/')
@user_passes_test(lambda u: u.is_superuser)
def files_for_approval(request):
    all_tasks = SharedDocument.objects.all().order_by('-pk')
    qs_json = serializers.serialize('json', all_tasks)
    qs_json = json.loads(qs_json)

    page = request.GET.get('page')
    #paginator = Paginator(qs_json, 5)

    count = 0

    for i in qs_json:
        count += 1
        i['fields']['srNo'] = str(count)
        username = User.objects.get(pk = i['fields']['user'])
        user = username.username
        i['fields']['user'] = user
        username1 = User.objects.get(pk=i['fields']['requested_user'])
        user1 = username1.username
        i['fields']['requested_user'] = user1
        file = FileUpload.objects.get(pk=i['fields']['file'])
        filename = file.Filename
        i['fields']['file'] = filename
        dateup = i['fields']['timestamp']
        dt = datetime.strptime(dateup, "%Y-%m-%dT%H:%M:%S.%f")
        i['fields']['timestamp'] = dt
    qs_json = paginate(request, qs_json, page)
    rt = {'files': qs_json}
    return render(request, 'files/all_files_for_approval.html',rt)


@user_passes_test(lambda u: u.is_superuser)
def for_approval(request, id):
    data = dict()
    task_id = SharedDocument.objects.get(id=id)
    form = SharedDocumentForm(request.POST, instance=task_id)
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            p = Notification.objects.create(actor=request.user, sender=task_id.user, receiver=task_id.requested_user,
                                            object_id=task_id.file.id, object_type=task_id.file, status=task_id,
                                            type='notification changed', unread=True)
            p.save()
            return HttpResponseRedirect('/all_files_for_sharing/')
    else:
        form = SharedDocumentForm()

    context = {'task': task_id, 'form': form}
    data['html_form'] = render_to_string('files/pending_tasks.html', context, request=request)
    return JsonResponse(data)


@login_required(login_url='/login/')
def share_notification_request(request):
    if request.user.is_superuser:
        notification = Notification.objects.filter(Q(type='Share File') |
                            Q(type='notification changed',receiver_id=request.user.id) |
                            Q(type='notification changed', sender_id=request.user.id)).order_by('-pk')
        qs_json = serializers.serialize('json', notification)
        qs_json = json.loads(qs_json)
        page = request.GET.get('page', 1)
        #paginator = Paginator(qs_json, 5)
        count = 0
        
        for i in qs_json:
            count += 1
            i['fields']['srNo'] = str(count)
            sender = User.objects.get(pk=i['fields']['sender'])
            receiver = User.objects.get(pk=i['fields']['receiver'])
            filename = FileUpload.objects.get(pk=i['fields']['object_type'])
            status = SharedDocument.objects.get(pk=i['fields']['status'])
            i['fields']['sender'] = sender
            i['fields']['receiver'] = receiver
            i['fields']['object_type'] = filename
            i['fields']['status'] = status
        qs_json = paginate(request,qs_json,page)
        args0 = {'files': qs_json}
        return render(request, 'files/notification_request_for_superuser.html', args0)
    else:
        notification = Notification.objects.filter(Q(receiver=request.user) | Q(sender=request.user), type='notification changed' ).order_by('-pk')
        qs_json = serializers.serialize('json', notification)
        qs_json = json.loads(qs_json)
        page = request.GET.get('page', 1)
        #paginator = Paginator(qs_json, 5)
        count = 0

        for i in qs_json:
            count += 1
            i['fields']['srNo'] = str(count)
            filename = FileUpload.objects.get(pk=i['fields']['object_type'])
            status = SharedDocument.objects.get(pk=i['fields']['status'])
            i['fields']['object_type'] = filename
            i['fields']['status'] = status

        qs_json = paginate(request,qs_json,page)
        context0 =  {'files': qs_json}
        return render(request, 'files/notification_request.html', context0 )



@login_required(login_url='/login/')
def notification_of_files(request):
    account_total = 0
    total = 0
    if request.user.is_superuser:
        count = Notification.objects.filter(unread=True, type='Share File').count()
        count2 = Notification.objects.filter(unread=True, type='notification changed', sender_id=request.user.id).count()
        count3 = Notification.objects.filter(unread=True, type='notification changed', receiver_id=request.user.id).count()

        drafted_count = NotificationForAccount.objects.filter(unread=True, type='Drafted',
                                                                receiver_id=request.user.id).count()

        approved_count = NotificationForAccount.objects.filter(unread=True, type='Approved',
                                                                receiver_id=request.user.id).count()
        pending_count = NotificationForAccount.objects.filter(unread=True, type='Pending',
                                                                receiver_id=request.user.id).count()
        rejected_count = NotificationForAccount.objects.filter(unread=True, type='Rejected',
                                                                receiver_id=request.user.id).count()

        total = count + count2 + count3

        account_total = approved_count + pending_count + rejected_count + drafted_count

    elif request.user.is_maker:

        # count = Notification.objects.filter(unread=True, type='Share File').count()
        count2 = Notification.objects.filter(unread=True, type='notification changed',
                                             sender_id=request.user.id).count()
        count3 = Notification.objects.filter(unread=True, type='notification changed',
                                             receiver_id=request.user.id).count()

        approved_count = NotificationForAccount.objects.filter(unread=True, type='Approved',
                                                                receiver_id=request.user.id).count()
        pending_count = NotificationForAccount.objects.filter(unread=True, type='Pending',
                                                                receiver_id=request.user.id).count()
        rejected_count = NotificationForAccount.objects.filter(unread=True, type='Rejected',
                                                                receiver_id=request.user.id).count()

        account_total = approved_count + pending_count + rejected_count
        total = count2 + count3

    elif request.user.is_checker:
        drafted_count = NotificationForAccount.objects.filter(unread=True, type='Drafted', receiver_id=request.user.id).count()
        account_total = drafted_count

    else:
        count5 = Notification.objects.filter(unread=True, type='notification changed', sender_id=request.user.id).count()
        count6 = Notification.objects.filter(unread=True, type='notification changed', receiver_id=request.user.id).count()
        total = count5 + count6

    context = {'total_count': total, 'account_total': account_total}
    return render(request, 'notification_page.html',context)


@login_required(login_url='/login/')
def mark_read(request, id):
    value = get_object_or_404(Notification, pk=id)
    if value.unread:
        value.unread = False
        value.save()
        data = {'success': True}
        return JsonResponse(data)
    else:
        value.unread = True
        value.save()
        data2 = {'success': True}
        return JsonResponse(data2)


@login_required(login_url='/login/')
def mark_read_account(request, id):
    value = get_object_or_404(NotificationForAccount, pk=id)
    print(value)
    if value.unread:
        value.unread = False
        value.save()
        return JsonResponse({'success': True})
    else:
        value.unread = True
        value.save()
        return JsonResponse({'success': True})


"""Reports"""


@login_required
def reports(request):
    total_docs = FileUpload.objects.filter(user__username=request.user,deleted='0').count()
    shared_docs = SharedDocument.objects.filter(user__username=request.user, status='Approved',file__deleted='0').count()
    fav_docs = FileUpload.objects.filter(user__username=request.user, is_favorite=True,deleted='0').count()
    proc_docs = FileUpload.objects.filter(user__username=request.user, is_protected=True,deleted='0').count()
    context = {'total_docs': total_docs, 'shared_docs': shared_docs, 'fav_docs': fav_docs, 'proc_docs': proc_docs}
    return render(request, 'reports.html',context)


@login_required(login_url='/login/')
@user_passes_test(lambda u: u.is_superuser)
def other_users_info(request):
    all_users = User.objects.all()
    context = {'all_users': all_users}
    return render(request, 'files/other_users_info.html', context)


def reports_of_user(request, user_id):
    if request.method == 'GET':
        total_docs = FileUpload.objects.filter(user__id=user_id).count()
        shared_docs = SharedDocument.objects.filter(user_id=user_id, status='Approved').count()
        fav_docs = FileUpload.objects.filter(user__id=user_id, is_favorite=True).count()
        proc_docs = FileUpload.objects.filter(user__id=user_id, is_protected=True).count()
        data = {'total_docs': total_docs, 'shared_docs': shared_docs, 'fav_docs': fav_docs, 'proc_docs': proc_docs}
        return JsonResponse(data)

    else:
        return JsonResponse({'success': False})


@login_required(login_url='/login/')
def discussions(request):
   return render(request, 'discussions.html')


def scan_document(request):
   return render(request, 'scan_document.html')


'''Sticky Notes'''
@login_required(login_url='/login/')
def notes(request, note='NULL'):
    notes = Notes.objects.filter(created_by=request.user)
    if note!='NULL':
        note = Notes.objects.get(id=note)
        if request.method == 'POST':
            form = NoteForm(request.POST, request.FILES, instance=note)
            if form.is_valid():
                form.save()
                return HttpResponseRedirect(reverse('DMS:notes'))
    else:
        if request.method == 'POST':
            form = NoteForm(request.POST)
            if form.is_valid():
                note = form.save(commit=False)
                note.created_by = request.user
                note.save()
                return HttpResponseRedirect(reverse('DMS:notes'))
        else:
            form = NoteForm()
        context = {'notes': notes, 'note_id':note, 'form': form}
        return render(request, 'notes.html',context)


'''Bulk Upload'''


@login_required(login_url='/login/')
def bulk_upload(request):
    context = {'type':'bulk'}
    return render(request, 'files/bulk_upload.html',context)


def bulk_upload_for_files(request):
    form = BulkFileUploadForm(request.POST, request.FILES)
    if form.is_valid():
        file = form.save(commit=False)
        file.user = request.user
        filename = form.cleaned_data['File1']
        file_name = str(filename).split('.')[0]
        file.Filename = str(filename)
        file.Uploader = request.user.first_name
        ext = str(filename).split('.')[-1]
        file.file_type = ext
        file.save()
        data = {'is_valid': True, 'name': file.Filename,'url': file.File1.url}
    else:
        data = {'is_valid': False,}
    return JsonResponse(data)


'''Encryption or Decryption'''



def encrypt_document(request, file_id):
    file = FileUpload.objects.get(id=file_id)
    filename = file.File1
    Filename = str(filename).split('/')[-1]

    if request.user.is_authenticated:
        try:
            if file.is_encrypted:
                b = bytes(file.File1.read())
                decoded = Cryptographer.decrypted(b)   #Cryptographer used to encrypt and decrypt the files
                file.File1.close()
                os.remove(os.path.join(settings.MEDIA_ROOT, str(filename)))
                file.File1.save(Filename, ContentFile(decoded), save=True)
                file.is_encrypted = False
                file.save()
                data = {'is_not_encrypted': 'Your file is Decrypted..!'}
                return JsonResponse(data)
            elif not file.is_encrypted:
                b = bytes(file.File1.read())
                encoded = Cryptographer.encrypted(b)
                file.File1.close()
                os.remove(os.path.join(settings.MEDIA_ROOT, str(filename)))
                file.File1.save(Filename, ContentFile(encoded), save=True)
                file.is_encrypted = True
                file.save()
                data = {'is_encrypted': 'Your file is now Encrypted..!'}
                return JsonResponse(data)
        except (KeyError, FileUpload.DoesNotExist):

            return JsonResponse({'success': False})
    else:
        data = {'not_authenticated': 'You are not logged In..!!'}
        return JsonResponse(data)


def account_open(request):
    if request.method == 'POST':
        form = AccountOpeningForm(request.POST, request.FILES)
        if form.is_valid():
            file = form.save(commit=False)
            receiver = form.cleaned_data['for_approval']
            file.user = request.user
            file.save()
            file.customer_id = file.pk
            file.save()
            q = AccountOpen.objects.get(id=file.pk)
            p = NotificationForAccount.objects.create(sender=request.user, receiver=receiver, type='Form Submitted',
                                                      unread=True, status=q)
            p.save()
            return redirect('/workflow_management/')
    else:
        form = AccountOpeningForm()
    context = {'form': form}
    return render(request, 'other_files/account_opening_form.html', {'form': form})


@login_required(login_url='/login/')
def account_opening_approval(request):
    get_notifications = NotificationForAccount.objects.filter(receiver = request.user).order_by("-pk")

    qs_json = serializers.serialize('json', get_notifications)
    qs_json = json.loads(qs_json)
    page = request.GET.get('page', 1)
    #paginator = Paginator(qs_json, 6)

    count = 0
    for i in qs_json:
        count += 1
        i['fields']['srNo'] = str(count)
        if i['fields']['status']:
            user_wf_data = UserDataWorkflowManagement.objects.get(pk = i['fields']['status'])
            i['fields']['customer_id'] = user_wf_data.application_number
            i['fields']['wf_name'] = user_wf_data.workflow_id.wf_name
            i['fields']['wf_id'] = user_wf_data.workflow_id.id
            i['fields']['wf_name'] = user_wf_data.workflow_id.wf_name
            sender = User.objects.get(pk=i['fields']['sender'])
            i['fields']['sender'] = sender
    qs_json = paginate(request,qs_json,page)
    context = {'files':qs_json}
    return render(request,'files/notifications_for_maker.html',context)


def approval_form_for_account_opening(request, task_id):
    data = dict()
    account = AccountOpen.objects.get(id=task_id)
    receiver_user = account.user
    form_approval = AccountOpeningApprovalForm(request.POST, instance=account)
    if request.method == 'POST':
        if form_approval.is_valid():
            form2 = form_approval.save(commit=False)
            p = NotificationForAccount.objects.create(sender=request.user, receiver=receiver_user,
                                                      type='Form Processed Further', unread=True, status=account)
            p.save()
            form2.save()
            return HttpResponseRedirect('/account_opening_approval')
    else:
        form_approval = AccountOpeningApprovalForm()
    context = {'account': account, 'form': form_approval}
    data['html_form'] = render_to_string('other_files/pending_approval_for_account.html', context, request=request)
    return JsonResponse(data)


def workflow_management(request):
    return render(request, 'other_files/workflow_management.html')


def approved_forms(request):
    account = AccountOpen.objects.filter(status='Approved')
    context = {'account': account}
    return render(request, 'other_files/approved_forms.html',context)


def rejected_forms(request):
    account = AccountOpen.objects.filter(status='Rejected')
    context = {'account': account}
    return render(request, 'other_files/rejected_forms.html',context)


def delete_notification_for_workflow(request, notif_id):
    data = {}
    notif = NotificationForAccount.objects.filter(pk=notif_id)

    if notif:
        notif.delete()
    get_notifications = NotificationForAccount.objects.filter(receiver=request.user).order_by("-pk")

    qs_json = serializers.serialize('json', get_notifications)
    qs_json = json.loads(qs_json)
    page = request.GET.get('page', 1)
    paginator = Paginator(qs_json, 6)
    print('page', page)
    count = 0
    for i in qs_json:
        count += 1
        i['fields']['srNo'] = str(count)
        for notification in get_notifications:
            i['fields']['wf_id'] = notification.status.workflow_id_id
            i['fields']['wf_name'] = notification.status.workflow_id.wf_name

    print(qs_json)
    try:
        qs_json = paginator.page(page)
    except PageNotAnInteger:
        qs_json = paginator.page(1)
    except EmptyPage:
        qs_json = paginator.page(paginator.num_pages)
    print(qs_json)
    context = {'files':qs_json}
    data['html_book_list'] = render_to_string('other_files/delete_notification.html',context ,request)
    data['page_div'] = render_to_string('files/pagination.html',context, request=request)
    return JsonResponse(data)


def user_directory_path(instance, filename):
    if filename:
        ans = filename.split('.')[-1]
    date=datetime.now()
    return 'Myfiles/{0}/{1}/{2}'.format(date.strftime('%d-%m-%Y'),
    instance.user.first_name+' '+instance.user.last_name, ans)

def restore_document(request,file_id):
    data= dict()
    file=get_object_or_404(ArchiveFiles,id=file_id)
    if file:
        id = file.file_id.id
        fileName = str(file.File1).split('/')[-1]
        coded_data_path = settings.MEDIA_ROOT + '/' + str(file.File1)
        path = user_directory_path(file, fileName)
        new_path = settings.MEDIA_ROOT + '/' + str(path)
        if not os.path.isdir(os.path.join(settings.MEDIA_ROOT + '/' + path + '/')):
            print('path not exists')
            try:
                os.makedirs(os.path.join(settings.MEDIA_ROOT + '/' + path + '/'))
            except OSError as exc:  # Guard against race condition
                print("path does not exists")
        if coded_data_path:
            move(coded_data_path, new_path)
        ArchiveFiles.objects.filter(id=file_id).update(deleted='1')
        FileUpload.objects.filter(id=id).update(deleted='0', File1=path + '/' + fileName, uploaded_at=datetime.now())
        # uploadedAt = file.uploaded_at
        # FileUpload.objects.create(file_type=file.file_type, Uploader=file.Uploader, Filename=file.Filename,
        #                           File1=file.File1, uploaded_at=file.uploaded_at, user=file.user,
        #                           is_favorite=file.is_favorite, is_shared=file.is_shared,
        #                           protected=file.protected,
        #                           is_protected=file.is_protected, is_encrypted=file.is_encrypted,
        #                           edited_at=file.edited_at, modified_at=file.modified_at,
        #                           category=file.category)
        # recent_id = FileUpload.objects.latest('id')
        # FileUpload.objects.filter(pk=recent_id.pk).update(uploaded_at=uploadedAt)
        # file.delete1()
        data['type']='success'
    else:
        data['type'] = 'error'
    return JsonResponse(data)


def check_valid_password(request):
    data = dict()
    if request.is_ajax and request.method == 'GET':
        original = request.GET.get('original_password')
        input = request.GET.get('inputVal')
        print(input)
        if input == 'false':

            data['is_matched'] = 'No data'
        else:

            if check_password(input, original):
                data['is_matched'] = True
            else:
                data['is_matched'] = False

        return JsonResponse(data)


@login_required(login_url='/login/')
def encryption(request):
    files = FileUpload.objects.filter(user_id=request.user,deleted='0').order_by("-pk")

    #If condition for Search Value....
    if request.GET.get('searchVal'):
        searchfor=request.GET.get('searchVal')

        if searchfor!='0' and searchfor!='' and searchfor!='NULL' and searchfor is not None:
            if request.user.is_superuser:
                files = FileUpload.objects.filter(Q(Filename__icontains = searchfor) | Q(Uploader__icontains = searchfor),deleted='0').order_by("-pk")
            else:
                files = FileUpload.objects.filter(Q(Filename__icontains = searchfor) | Q(Uploader__icontains = searchfor),user_id=request.user,deleted='0').order_by("-pk")

        elif searchfor=='0':

            if request.user.is_superuser:
                files = FileUpload.objects.filter(deleted='0').order_by("-pk")
            else:
                files = FileUpload.history.filter(user_id=request.user,deleted='0').order_by("-pk")
        qs_json = serializers.serialize('json', files)
        qs_json=json.loads(qs_json)
        page = request.GET.get('page', 1)
        #paginator = Paginator(qs_json, 5)

        count=0

        for i in qs_json:

            count+=1
            i['fields']['srNo']=str(count)

        qs_json = paginate(request,qs_json,page)
        return encrypt_search_list(request, qs_json, 'other_files/all_files_for_encryption.html')

    #If condition for SearchFOR....
    elif request.GET.get('searchFOR'):
        searchfor=request.GET.get('searchFOR')
        if request.user.is_superuser:
            files = FileUpload.objects.filter(Q(Filename__icontains = searchfor) | Q(Uploader__icontains = searchfor),deleted='0').order_by("-pk")
        else:
            files = FileUpload.objects.filter(Q(Filename__icontains = searchfor) | Q(Uploader__icontains = searchfor),user_id=request.user,deleted='0').order_by("-pk")
        qs_json = serializers.serialize('json', files)
        qs_json=json.loads(qs_json)

        page = request.GET.get('page')
        #paginator = Paginator(qs_json, 5)
        count=0

        for i in qs_json:
            count+=1
            i['fields']['srNo']=str(count)

        qs_json = paginate(request,qs_json,page)
        return encrypt_search_list(request, qs_json, 'other_files/all_files_for_encryption.html')

    #Else condition if there is no search value...
    else:
        qs_json = serializers.serialize('json', files)
        qs_json = json.loads(qs_json)
        page = request.GET.get('page', 1)
        #paginator = Paginator(qs_json, 5)

        count = 0
        for i in qs_json:
            count += 1
            i['fields']['srNo'] = str(count)
        qs_json = paginate(request,qs_json,page)

        context = {'files': qs_json}
        return render(request, 'other_files/all_files_for_encryption.html', context)


def encrypt_search_list(request, files, template_name):
    data = dict()
    if request.method == 'GET':
        data['form_is_valid'] = True
        context = {'files': files}
        data['html_book_list'] = render_to_string('files/encrypt_files.html',context, request=request)

        data['page_div'] = render_to_string('files/pagination.html',context, request=request)
    data['html_form'] = render_to_string(template_name, request=request)
    return JsonResponse(data)


def delete_sticky_notes(request, note_id):
    note = Notes.objects.get(id=note_id)
    if request.user.is_authenticated:
        try:
            note.delete()
            data = {'is_delete': 'Deleted'}
            notes = Notes.objects.filter(created_by = request.user)
            form = NoteForm()
            context = {'notes': notes,'note_id':'NULL','form':form}
            data['html_book_list'] = render_to_string('files/notes_data.html',context,request)
            return JsonResponse(data)
        except (KeyError, Notes.DoesNotExist):
            return JsonResponse({'success': False})
    else:
        data = {'not_authenticated': 'You are not logged In..!!'}
        return JsonResponse(data)


def show_delete_popup(request, file_id):
    data = dict()
    #file = get_object_or_404(FileUpload, pk=file_id)
    if request.is_ajax:
        type = request.GET.get('type')
        url_sub = request.GET.get('url_sub')
    if url_sub == 'archive_files':
        file = get_object_or_404(ArchiveFiles, pk=file_id)
    else:
        file = get_object_or_404(FileUpload, pk=file_id)

    context = {'file': file,'type':type,'url_sub':url_sub}
    data['html_form'] = render_to_string('files/document_prot.html', context, request=request)
    return JsonResponse(data)


@csrf_exempt
def check_protection(request, id, url_sub):
    data = dict()
    if url_sub == 'archive_files':
        requested_file = get_object_or_404(ArchiveFiles, pk=id)
    else:
        requested_file = get_object_or_404(FileUpload, pk=id)
    encrypt_pass = requested_file.protected
    if request.method == 'GET' and request.is_ajax:
        password = request.GET.get('pass')
        if check_password(password, encrypt_pass):
            data['is_matched'] = True
        else:
            data['is_matched'] = False
        print('data looking for ', data)
        return JsonResponse(data)


def ask_replace_file_password(request,file_id):
    data = dict()
    if request.method == 'POST' and request.is_ajax:
        formData = request.FILES
    context = {'file_id':file_id,'formData':formData}
    data['html_form'] = render_to_string('files/replace_file_password.html', context, request=request)
    return JsonResponse(data)


#API to share located files
def share_locked_file(request,file_id):
    data = dict()
    context =  {'file_id':file_id}
    data['html_form'] = render_to_string('files/share_locked_file.html', context, request=request)
    return JsonResponse(data)


@login_required(login_url='/login/')
def edit_sticky_notes(request, note_id):
    note = Notes.objects.get(id=note_id)
    form = NoteForm(instance=note)
    data = dict()
    if request.method == 'GET' and request.is_ajax:
        context = {'form': form, 'note_id': note_id}
        data['html_sticky_form'] = render_to_string('files/sticky_notes_form.html', context, request=request, )
        data['html_form'] = render_to_string('files/notes_data.html', context, request=request, )
        return JsonResponse(data)


@login_required(login_url='/login/')
def table_request_form(request):
    DB_EXCEPTIONS = (
        OperationalError,
        DatabaseError,
    )

    table_form = MisQueryForm()
    if request.method == 'POST':
        table_form = MisQueryForm(request.POST, request.FILES)
        if table_form.is_valid():
            query = table_form.cleaned_data['query']
            query = table_form.cleaned_data['query']
            cursor = connection.cursor()
            table_data = ''
            try:
                cursor.execute(query)
                columns = [col[0] for col in cursor.description]
                table_data = [dict(zip(columns, row)) for row in cursor.fetchall()]
                messages.success(request, 'All Done')
            except DB_EXCEPTIONS as exp:
                messages.error(request, exp)
            args = {'table_form': table_form, 'table_data': table_data}
            return render(request, 'other_files/table_request_form.html',args)
        else:
            return HttpResponse('Form is not valid')
    else:
        context0 = {'table_form': table_form}
        return render(request, 'other_files/table_request_form.html', context0)


@login_required(login_url='/login/')
def theme_management_selection(request):
    form = ThemeManagementForm()
    response = HttpResponseRedirect('theme_management_selection')
    if request.method == 'POST':
        form = ThemeManagementForm(request.POST)
        if form.is_valid():
            theme = form.cleaned_data['theme']
            user = UserSetting.objects.filter(user = request.user.pk)
            if user:
                UserSetting.objects.filter(user=request.user.pk).update(theme = theme)
            else:
                myuser = User.objects.get(id = request.user.pk)
                UserSetting.objects.create(user = myuser, theme = theme)

        return response
    context =  {'form': form}
    return render(request, 'other_files/theme_management.html', context)


@login_required(login_url='/login/')
@csrf_exempt
def meta_search(request):

    if request.method == 'POST' and request.is_ajax:

        res_id = []
        searchfor = request.POST.get('search_text')
        fromDate = request.POST.get('fromDate')
        toDate = request.POST.get('toDate')

        from_date = datetime.strptime(fromDate, '%d-%m-%Y').strftime('%Y-%m-%d %H:%M:%S')

        to_date = datetime.strptime(toDate, '%d-%m-%Y').strftime('%Y-%m-%d %H:%M:%S')

        filter_res = FileUpload.objects.filter(user_id=request.user.pk,uploaded_at__range=(from_date, to_date)).order_by("-pk")
        for h in filter_res:

            if h.file_type == 'pdf':
                # print(settings.MEDIA_ROOT + '/' + str(h.File1))
                pdf_pages = []
                object = PyPDF2.PdfFileReader(settings.MEDIA_ROOT + '/' + str(h.File1))
                NumPages = object.getNumPages()

                for i in range(0, NumPages):

                    PageObj = object.getPage(i)
                    Text = PageObj.extractText()
                    Text = Text.lower()
                    print(Text)
                    searchfor = searchfor.lower()
                    if (searchfor in Text) or (searchfor.replace(" ", "") in Text):
                        pdf_pages.append(str(i))


                if pdf_pages != []:
                    res_id.append(h.pk)


            # elif h.file_type == 'xlsx' or h.file_type == 'xls' or h.file_type == 'XLS' or h.file_type == 'csv':
            #     print(settings.MEDIA_ROOT + '/' + str(h.File1))
            #     if h.file_type == 'xls' or h.file_type == 'XLS':
            #         file = (settings.MEDIA_ROOT + '/' + str(h.File1)).split('/')[-1]
            #         fileName = file.split('.')[0]
            #         dest = os.path.dirname(settings.MEDIA_ROOT + '/' + str(h.File1))
            #
            #         copyfile(settings.MEDIA_ROOT + '/' + str(h.File1), dest+'/'+fileName+'_1.xls')
            #         os.rename(dest+'/'+fileName+'_1.xls', dest+'/'+fileName+'_1.xlsx')
            #
            #         wb = pd.ExcelFile(dest+'/'+fileName+'_1.xlsx')
            #         removable_part = (dest+'/'+fileName+'_1.xlsx').split('/')[-1]
            #         file_name = removable_part.split('.')[0]
            #         path = (dest+'/'+fileName+'_1.xlsx').replace(removable_part,'')
            #         total_sheets = wb.sheet_names
            #         result = []
            #         for sheet in total_sheets:
            #             df = pd.read_excel(wb, sheet)
            #             result.append(df)
            #             html_data = df.to_html()
            #             soup = BeautifulSoup(html_data, 'html.parser')
            #             op = soup.prettify()
            #             op = soup.get_text()
            #             print(result)
            #             for data in op:
            #                 f = open(path+file_name+".txt", "a+",encoding="utf-8")
            #                 f.write(str(data))
            #                 f.close()
            #         if os.path.exists(dest+'/'+fileName+'_1.xlsx'):
            #             os.remove(dest+'/'+fileName+'_1.xlsx')
            #         else:
            #             print("The file does not exist")
            #         data = open(dest+'/'+fileName+'_1.txt', 'r', encoding="ISO-8859-1").read()
            #         data = data.lower()
            #         searchfor = searchfor.lower()
            #         if searchfor in data:
            #             res_id.append(h.pk)
            #     elif h.file_type == 'xlsx':
            #
            #         dest = os.path.dirname(settings.MEDIA_ROOT + '/' + str(h.File1))
            #         wb = pd.ExcelFile(settings.MEDIA_ROOT + '/' + str(h.File1))
            #         removable_part = str(settings.MEDIA_ROOT + '/' + str(h.File1)).split('/')[-1]
            #         file_name = removable_part.split('.')[0]
            #
            #         path = str(settings.MEDIA_ROOT + '/' + str(h.File1)).replace(removable_part,'')
            #         total_sheets = wb.sheet_names
            #         result = []
            #         for sheet in total_sheets:
            #             df = pd.read_excel(wb, sheet)
            #             result.append(df)
            #             html_data = df.to_html()
            #             soup = BeautifulSoup(html_data, 'html.parser')
            #             op = soup.prettify()
            #             op = soup.get_text()
            #             print(result)
            #             for data in op:
            #                 f = open(path+file_name+".txt", "a+",encoding="utf-8")
            #                 f.write(str(data))
            #                 f.close()
            #         data = open(dest + '/' + file_name + '.txt', 'r', encoding="ISO-8859-1").read()
            #         data = data.lower()
            #         searchfor = searchfor.lower()
            #         if searchfor in data:
            #             res_id.append(h.pk)

            elif h.file_type == 'xlsx' or h.file_type == 'xls' or h.file_type == 'XLS' or h.file_type == 'csv':

                if h.file_type == 'xls' or h.file_type == 'XLS':
                    print("A",settings.MEDIA_ROOT + '/' + str(h.File1))
                    file = (settings.MEDIA_ROOT + '/' + str(h.File1)).split('/')[-1]
                    fileName = file.split('.')[0]
                    dest = os.path.dirname(settings.MEDIA_ROOT + '/' + str(h.File1))

                    copyfile(settings.MEDIA_ROOT + '/' + str(h.File1), dest+'/'+fileName+'_1.xls')
                    os.rename(dest+'/'+fileName+'_1.xls', dest+'/'+fileName+'_1.xlsx')

                    workbook = xlrd.open_workbook(dest+'/'+fileName+'_1.xlsx')
                    removable_part = (dest+'/'+fileName+'_1.xlsx').split('/')[-1]
                    file_name = removable_part.split('.')[0]
                    path = (dest+'/'+fileName+'_1.xlsx').replace(removable_part,'')
                    res = len(workbook.sheet_names())
                    file = open(path + file_name + ".txt", "w", encoding="utf-8")
                    for i in range(res):
                        sh = workbook.sheet_by_index(i)
                        n = 0
                        i = 0
                        for n in range(sh.nrows):
                            for i in range(sh.ncols):
                                data = str(sh.cell_value(n, i)) + " "

                                file.write(data + "\n")
                    file.close()



                    if os.path.exists(dest+'/'+fileName+'_1.xlsx'):
                        os.remove(dest+'/'+fileName+'_1.xlsx')
                    else:
                        print("The file does not exist")
                    data = open(dest+'/'+fileName+'_1.txt', 'r', encoding="ISO-8859-1").read()
                    data = data.lower()
                    searchfor = searchfor.lower()
                    if searchfor in data:
                        res_id.append(h.pk)
                    if os.path.exists(dest+'/'+fileName+'_1.txt'):
                        os.remove(dest+'/'+fileName+'_1.txt')
                    else:
                        print("The file does not exist")
                elif h.file_type == 'xlsx':
                    print("B", settings.MEDIA_ROOT + '/' + str(h.File1))
                    dest = os.path.dirname(settings.MEDIA_ROOT + '/' + str(h.File1))

                    workbook = xlrd.open_workbook(settings.MEDIA_ROOT + '/' + str(h.File1))
                    removable_part = str(settings.MEDIA_ROOT + '/' + str(h.File1)).split('/')[-1]
                    file_name = removable_part.split('.')[0]

                    path = str(settings.MEDIA_ROOT + '/' + str(h.File1)).replace(removable_part,'')


                    res = len(workbook.sheet_names())
                    file = open(path + file_name + ".txt", "w", encoding="utf-8")
                    for i in range(res):
                        sh = workbook.sheet_by_index(i)
                        n = 0
                        i = 0
                        for n in range(sh.nrows):
                            for i in range(sh.ncols):
                                data = str(sh.cell_value(n, i)) + " "

                                file.write(data + "\n")
                    file.close()


                    data1 = open(dest + '/' + file_name + '.txt', 'r', encoding="ISO-8859-1").read()

                    print('data1',data1)
                    data1 = data1.lower()
                    searchfor = searchfor.lower()
                    if searchfor in data1:
                        res_id.append(h.pk)
                    if os.path.exists(dest + '/' + file_name + '.txt'):
                        os.remove(dest + '/' + file_name + '.txt')
                    else:
                        print("The file does not exist")

            elif (h.file_type == 'docx' or h.file_type == 'doc'):
                # print("DOCX",settings.MEDIA_ROOT + '/' + str(h.File1))
                import fulltext
                text = fulltext.get(settings.MEDIA_ROOT + '/' + str(h.File1), '< no content >')
                text = text.lower()
                searchfor = searchfor.lower()
                if searchfor in text:
                    res_id.append(h.pk)




            else:
                data = open(settings.MEDIA_ROOT + '/' + str(h.File1), 'r', encoding="ISO-8859-1").read()
                data = str(data).lower()
                searchfor = searchfor.lower()
                if searchfor in data:
                    res_id.append(h.pk)


        if res_id!=[]:
            filter_res = FileUpload.objects.filter(id__in=res_id)
            qs_json = serializers.serialize('json', filter_res)
            qs_json = json.loads(qs_json)

            page = request.GET.get('page', 1)
            #paginator = Paginator(qs_json, 4)
            #page_records = paginator.count

            count = 0

            for i in qs_json:
                count += 1
                i['fields']['srNo'] = str(count)
                i['fields']['File1'] = '/media/' + i['fields']['File1']
                dateup = i['fields']['uploaded_at']
                dt = datetime.strptime(dateup, "%Y-%m-%dT%H:%M:%S.%f")

                i['fields']['uploaded_at'] = dt

            qs_json = paginate(request,qs_json,page)

        else:
            qs_json = None
        data = dict()


        argss = {'files': qs_json}
        data['html_book_list'] = render_to_string('files/user_files_table.html', argss, request=request)

        data['page_div'] = render_to_string('files/pagination.html', argss, request=request)

        data['html_form'] = render_to_string('metasearch.html', request=request)
        return JsonResponse(data)

    elif request.GET.get('search_text'):
        print("PETER")
        res_id = []
        searchfor = request.GET.get('search_text')
        fromDate = request.GET.get('fromDate')
        toDate = request.GET.get('toDate')

        from_date = datetime.strptime(fromDate, '%d-%m-%Y').strftime('%Y-%m-%d %H:%M:%S')

        to_date = datetime.strptime(toDate, '%d-%m-%Y').strftime('%Y-%m-%d %H:%M:%S')

        filter_res = FileUpload.objects.filter(user_id=request.user.pk,uploaded_at__range=(from_date, to_date)).order_by("-pk")
        for h in filter_res:

            if h.file_type == 'pdf':

                pdf_pages = []
                object = PyPDF2.PdfFileReader(settings.MEDIA_ROOT + '/' + str(h.File1))
                NumPages = object.getNumPages()

                for i in range(0, NumPages):

                    PageObj = object.getPage(i)
                    Text = PageObj.extractText()
                    Text = Text.lower()
                    searchfor = searchfor.lower()
                    if (searchfor in Text) or (searchfor.replace(" ", "") in Text):
                        pdf_pages.append(str(i))


                if pdf_pages != []:
                    res_id.append(h.pk)


            elif h.file_type == 'xlsx' or h.file_type == 'xls' or h.file_type == 'XLS' or h.file_type == 'csv':
                if h.file_type == 'xls' or h.file_type == 'XLS':

                    file = (settings.MEDIA_ROOT + '/' + str(h.File1)).split('/')[-1]
                    fileName = file.split('.')[0]
                    dest = os.path.dirname(settings.MEDIA_ROOT + '/' + str(h.File1))

                    copyfile(settings.MEDIA_ROOT + '/' + str(h.File1), dest + '/' + fileName + '_1.xls')
                    os.rename(dest + '/' + fileName + '_1.xls', dest + '/' + fileName + '_1.xlsx')

                    wb = pd.ExcelFile(dest + '/' + fileName + '_1.xlsx')
                    removable_part = (dest + '/' + fileName + '_1.xlsx').split('/')[-1]
                    file_name = removable_part.split('.')[0]
                    path = (dest + '/' + fileName + '_1.xlsx').replace(removable_part, '')
                    total_sheets = wb.sheet_names
                    result = []
                    for sheet in total_sheets:
                        df = pd.read_excel(wb, sheet)
                        result.append(df)
                        html_data = df.to_html()
                        soup = BeautifulSoup(html_data, 'html.parser')
                        op = soup.prettify()
                        op = soup.get_text()
                        for data in op:
                            f = open(path + file_name + ".txt", "a+", encoding="utf-8")
                            f.write(str(data))
                            f.close()
                    if os.path.exists(dest + '/' + fileName + '_1.xlsx'):
                        os.remove(dest + '/' + fileName + '_1.xlsx')
                    else:
                        pass
                    data = open(dest + '/' + fileName + '_1.txt', 'r', encoding="ISO-8859-1").read()
                    data = data.lower()
                    searchfor = searchfor.lower()
                    if searchfor in data:
                        res_id.append(h.pk)
                    if os.path.exists(dest+'/'+fileName+'_1.txt'):
                        os.remove(dest+'/'+fileName+'_1.txt')
                    else:
                        print("The file does not exist")
                elif h.file_type == 'xlsx' or h.file_type == 'csv':

                    dest = os.path.dirname(settings.MEDIA_ROOT + '/' + str(h.File1))
                    wb = pd.ExcelFile(settings.MEDIA_ROOT + '/' + str(h.File1))
                    removable_part = str(settings.MEDIA_ROOT + '/' + str(h.File1)).split('/')[-1]
                    file_name = removable_part.split('.')[0]

                    path = str(settings.MEDIA_ROOT + '/' + str(h.File1)).replace(removable_part, '')
                    total_sheets = wb.sheet_names
                    result = []
                    for sheet in total_sheets:
                        df = pd.read_excel(wb, sheet)
                        result.append(df)
                        html_data = df.to_html()
                        soup = BeautifulSoup(html_data, 'html.parser')
                        op = soup.prettify()
                        op = soup.get_text()
                        for data in op:
                            f = open(path + file_name + ".txt", "a+", encoding="utf-8")
                            f.write(str(data))
                            f.close()
                    data = open(dest + '/' + file_name + '.txt', 'r', encoding="ISO-8859-1").read()
                    data = data.lower()

                    searchfor = searchfor.lower()
                    if searchfor in data:
                        res_id.append(h.pk)
                    if os.path.exists(dest + '/' + file_name + '.txt'):
                        os.remove(dest + '/' + file_name + '.txt')
                    else:
                        print("The file does not exist")


            elif (h.file_type == 'docx' or h.file_type == 'doc'):

                import fulltext
                text = fulltext.get(settings.MEDIA_ROOT + '/' + str(h.File1), '< no content >')
                text = text.lower()
                searchfor = searchfor.lower()
                if searchfor in text:
                    res_id.append(h.pk)




            else:
                data = open(settings.MEDIA_ROOT + '/' + str(h.File1), 'r', encoding="ISO-8859-1").read()
                data = data.lower()
                searchfor = searchfor.lower()
                if searchfor in str(data):
                    res_id.append(h.pk)

        if res_id != []:
            filter_res = FileUpload.objects.filter(id__in=res_id)
            qs_json = serializers.serialize('json', filter_res)
            qs_json = json.loads(qs_json)

            page = request.GET.get('page', 1)
            #paginator = Paginator(qs_json, 4)
            #page_records = paginator.count

            count = 0

            for i in qs_json:
                count += 1
                i['fields']['srNo'] = str(count)
                i['fields']['File1'] = '/media/' + i['fields']['File1']
                dateup = i['fields']['uploaded_at']
                dt = datetime.strptime(dateup, "%Y-%m-%dT%H:%M:%S.%f")

                i['fields']['uploaded_at'] = dt

            qs_json = paginate(request,qs_json,page)

        else:
            qs_json = None
        data = dict()
        context1 = { 'files': qs_json}
        data['html_book_list'] = render_to_string('files/user_files_table.html', context1, request=request)

        data['page_div'] = render_to_string('files/pagination.html', context1, request=request)

        data['html_form'] = render_to_string('metasearch.html', request=request)
        return JsonResponse(data)

    else:
        return render(request,'metasearch.html')


#data in excel sheet

def data_to_excel(request):
    import pandas as pd
    from io import BytesIO
    if request.user.is_superuser:
        hist = FileUpload.history.filter(Q(history_type='+') | Q(history_type='-')).order_by("-pk")
    else:
        hist = FileUpload.history.filter(Q(history_type='+') | Q(history_type='-'),user_id=request.user).order_by("-pk")

    hist_data = serializers.serialize('json', hist)
    hist_data = json.loads(hist_data)

    super_dict = []
    for d in hist_data:
        date_up = d['fields']['uploaded_at']
        dt = datetime.strptime(date_up, "%Y-%m-%dT%H:%M:%S.%f")
        d['fields']['uploaded_at'] = dt
        history = d['fields']['history_type']
        if history == '+':
            d['fields']['history_type'] = 'Added'
        elif history == '-':
            d['fields']['history_type'] = 'Deleted'
        super_dict.append(d['fields'])

    posts_df = pd.DataFrame(super_dict, columns=['id', 'file_type', 'Uploader', 'Filename', 'uploaded_at', 'history_type'])
    print('Spreadsheet saved.')
    sio = BytesIO()
    PandasWriter = pd.ExcelWriter(sio, engine='xlsxwriter')
    posts_df.to_excel(PandasWriter, sheet_name='Sheet_1')
    PandasWriter.save()

    sio.seek(0)
    workbook = sio.getvalue()

    response = HttpResponse(workbook, content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')

    file_name = "excel.xlsx"
    response['Content-Disposition'] = 'attachment; filename=%s' % file_name
    return response


#data to pdf sheet


def data_to_pdf(request):
    import pandas as pd
    if request.user.is_superuser:
        hist = FileUpload.history.filter(Q(history_type='+') | Q(history_type='-')).order_by("-pk")
    else:
        hist = FileUpload.history.filter(Q(history_type='+') | Q(history_type='-'),user_id=request.user).order_by("-pk")

    hist_data = serializers.serialize('json', hist)
    hist_data = json.loads(hist_data)

    super_dict = []
    for d in hist_data:
        date_up = d['fields']['uploaded_at']
        dt = datetime.strptime(date_up, "%Y-%m-%dT%H:%M:%S.%f")
        d['fields']['uploaded_at'] = dt
        history = d['fields']['history_type']
        if history == '+':
            d['fields']['history_type'] = 'Added'
        elif history == '-':
            d['fields']['history_type'] = 'Deleted'
        super_dict.append(d['fields'])

    posts_df = pd.DataFrame(super_dict, columns=['id', 'file_type', 'Uploader', 'Filename', 'uploaded_at', 'history_type'])

    from jinja2 import Environment, FileSystemLoader

    env = Environment(loader=FileSystemLoader(os.path.join(settings.BASE_DIR, 'DMSapp/templates')))
    template = env.get_template("data_to_pdf_template.html")

    template_vars = {"title": "Access History",
                     "national_pivot_table": posts_df.to_html()}

    html_out = template.render(template_vars)
    print(html_out)

    from xhtml2pdf import pisa
    from io import BytesIO
    result = BytesIO()

    pdf = pisa.pisaDocument(html_out, dest=result)
    if not pdf.err:
        response = HttpResponse(result.getvalue(), content_type='application/pdf')
        file_name = "Access_History.pdf"
        response['Content-Disposition'] = 'attachment; filename=%s' % file_name
        return response

@login_required
def report_list(request):
    return render(request, 'reports_list.html')




