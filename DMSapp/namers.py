from django.http import JsonResponse, HttpResponse
from django.shortcuts import render_to_response, render
from django.template.loader import render_to_string
from django.views.decorators.csrf import csrf_exempt
from datetime import datetime
import calendar
from .models import FileUpload


def option_value(request, newOption, user_id, starred):
    if newOption == '11':
        print(newOption)
        if starred=='1':
            files = FileUpload.objects.filter(user_id=user_id, is_favorite='1',deleted='0')
        elif starred=='0':
            files = FileUpload.objects.filter(user_id=user_id,deleted='0')
        if files:
            data = []
            for f in files:
                fileName = f.File1

                if str(fileName).split('.')[-1] == 'pdf' or str(fileName).split('.')[-1] == 'PDF':
                    fDate = request.GET.get('newfromDate')
                    tDate = request.GET.get('newtoDate')
                    if fDate!='0' or tDate!='0':

                        if tDate == '':
                            tDate = fDate
                        elif fDate == '':
                            fDate = tDate
                        from_date = datetime.strptime(fDate, '%d-%m-%Y').strftime('%Y-%m-%d %H:%M:%S')
                        to_date = datetime.strptime(tDate, '%d-%m-%Y')
                        toDate = calendar.timegm(to_date.timetuple())
                        advancedToDate = int(toDate) + 86400
                        finalToDate = datetime.utcfromtimestamp(advancedToDate).strftime('%Y-%m-%d %H:%M:%S')
                        files = FileUpload.objects.filter(id=f.id, uploaded_at__range=(from_date, finalToDate),deleted='0')
                        if files:
                            data.append({'id': f.id, 'file_type': f.file_type, 'Uploader': f.Uploader,
                                         'Filename': f.Filename, 'File1': f.File1, 'uploaded_at': f.uploaded_at,
                                         'user__username': request.user,'category':f.category})
                    else:
                        data.append({'id': f.id, 'file_type': f.file_type, 'Uploader': f.Uploader,
                                     'Filename': f.Filename, 'File1': f.File1, 'uploaded_at': f.uploaded_at,
                                     'user__username': request.user,'category':f.category})

            return data
    elif newOption == '12':
        print("Excel")
        if starred=='1':
            files = FileUpload.objects.filter(user_id=user_id, is_favorite='1',deleted='0')
        elif starred=='0':
            files = FileUpload.objects.filter(user_id=user_id,deleted='0')
        if files:
            data = []
            for f in files:
                fileName = f.File1

                if str(fileName).split('.')[-1] == 'xlsx' or str(fileName).split('.')[-1] == 'xls' or \
                        str(fileName).split('.')[-1] == 'xlsb' or str(fileName).split('.')[-1] == 'xlsm':
                    fDate = request.GET.get('newfromDate')
                    tDate = request.GET.get('newtoDate')

                    if fDate!='0' or tDate!='0':
                        print("meenal")
                        if tDate == '':
                            tDate = fDate
                        elif fDate == '':
                            fDate = tDate
                        from_date = datetime.strptime(fDate, '%d-%m-%Y').strftime('%Y-%m-%d %H:%M:%S')
                        to_date = datetime.strptime(tDate, '%d-%m-%Y')
                        toDate = calendar.timegm(to_date.timetuple())
                        advancedToDate = int(toDate) + 86400
                        finalToDate = datetime.utcfromtimestamp(advancedToDate).strftime('%Y-%m-%d %H:%M:%S')
                        files = FileUpload.objects.filter(id=f.id, uploaded_at__range=(from_date, finalToDate),deleted='0')
                        if files:
                            data.append({'id': f.id, 'file_type': f.file_type, 'Uploader': f.Uploader,
                                         'Filename': f.Filename, 'File1': f.File1, 'uploaded_at': f.uploaded_at,
                                         'user__username': request.user,'category':f.category})
                    else:

                        data.append({'id': f.id, 'file_type': f.file_type, 'Uploader': f.Uploader,
                                     'Filename': f.Filename, 'File1': f.File1, 'uploaded_at': f.uploaded_at,
                                     'user__username': request.user,'category':f.category})

            return data
    elif newOption == '13':
        print("Word")
        if starred=='1':
            files = FileUpload.objects.filter(user_id=user_id, is_favorite='1',deleted='0')
        elif starred=='0':
            files = FileUpload.objects.filter(user_id=user_id,deleted='0')
        if files:
            data = []
            for f in files:
                fileName = f.File1

                if str(fileName).split('.')[-1] == 'docx' or str(fileName).split('.')[-1] == 'docm' or \
                        str(fileName).split('.')[-1] == 'doc':
                    fDate = request.GET.get('newfromDate')
                    tDate = request.GET.get('newtoDate')
                    if fDate!='0' or tDate!='0':

                        if tDate == '':
                            tDate = fDate
                        elif fDate == '':
                            fDate = tDate
                        from_date = datetime.strptime(fDate, '%d-%m-%Y').strftime('%Y-%m-%d %H:%M:%S')
                        to_date = datetime.strptime(tDate, '%d-%m-%Y')
                        toDate = calendar.timegm(to_date.timetuple())
                        advancedToDate = int(toDate) + 86400
                        finalToDate = datetime.utcfromtimestamp(advancedToDate).strftime('%Y-%m-%d %H:%M:%S')
                        files = FileUpload.objects.filter(id=f.id, uploaded_at__range=(from_date, finalToDate),deleted='0')
                        if files:
                            data.append({'id': f.id, 'file_type': f.file_type, 'Uploader': f.Uploader,
                                         'Filename': f.Filename, 'File1': f.File1, 'uploaded_at': f.uploaded_at,
                                         'user__username': request.user,'category':f.category})
                    else:
                        data.append({'id': f.id, 'file_type': f.file_type, 'Uploader': f.Uploader,
                                     'Filename': f.Filename, 'File1': f.File1, 'uploaded_at': f.uploaded_at,
                                     'user__username': request.user,'category':f.category})
            print(data)
            return data
    elif newOption == '14':
        if starred=='1':
            files = FileUpload.objects.filter(user_id=user_id, is_favorite='1',deleted='0')
        elif starred=='0':
            files = FileUpload.objects.filter(user_id=user_id,deleted='0')
        if files:
            data = []
            for f in files:
                fileName = f.File1

                if str(fileName).split('.')[-1] == 'txt':
                    fDate = request.GET.get('newfromDate')
                    tDate = request.GET.get('newtoDate')
                    if fDate!='0' or tDate!='0':

                        if tDate == '':
                            tDate = fDate
                        elif fDate == '':
                            fDate = tDate
                        from_date = datetime.strptime(fDate, '%d-%m-%Y').strftime('%Y-%m-%d %H:%M:%S')
                        to_date = datetime.strptime(tDate, '%d-%m-%Y')
                        toDate = calendar.timegm(to_date.timetuple())
                        advancedToDate = int(toDate) + 86400
                        finalToDate = datetime.utcfromtimestamp(advancedToDate).strftime('%Y-%m-%d %H:%M:%S')
                        files = FileUpload.objects.filter(id=f.id, uploaded_at__range=(from_date, finalToDate),deleted='0')
                        if files:
                            data.append({'id': f.id, 'file_type': f.file_type, 'Uploader': f.Uploader,
                                         'Filename': f.Filename, 'File1': f.File1, 'uploaded_at': f.uploaded_at,
                                         'user__username': request.user,'category':f.category})
                    else:
                        data.append({'id': f.id, 'file_type': f.file_type, 'Uploader': f.Uploader,
                                     'Filename': f.Filename, 'File1': f.File1, 'uploaded_at': f.uploaded_at,
                                     'user__username': request.user,'category':f.category})

            return data
    elif newOption == '15':
        print("IMAGES")
        if starred=='1':
            files = FileUpload.objects.filter(user_id=user_id, is_favorite='1',deleted='0')
        elif starred=='0':
            files = FileUpload.objects.filter(user_id=user_id,deleted='0')
        if files:
            data = []
            for f in files:
                fileName = f.File1

                if str(fileName).split('.')[-1] == 'gif' or str(fileName).split('.')[-1] == 'png' or \
                        str(fileName).split('.')[-1] == 'jpg' or str(fileName).split('.')[-1] == 'jpeg':
                    fDate = request.GET.get('newfromDate')
                    tDate = request.GET.get('newtoDate')
                    if fDate!='0' or tDate!='0':

                        if tDate == '':
                            tDate = fDate
                        elif fDate == '':
                            fDate = tDate
                        from_date = datetime.strptime(fDate, '%d-%m-%Y').strftime('%Y-%m-%d %H:%M:%S')
                        to_date = datetime.strptime(tDate, '%d-%m-%Y')
                        toDate = calendar.timegm(to_date.timetuple())
                        advancedToDate = int(toDate) + 86400
                        finalToDate = datetime.utcfromtimestamp(advancedToDate).strftime('%Y-%m-%d %H:%M:%S')
                        print(from_date)
                        print(finalToDate)
                        files = FileUpload.objects.filter(id=f.id, uploaded_at__range=(from_date, finalToDate),deleted='0')
                        if files:
                            data.append({'id': f.id, 'file_type': f.file_type, 'Uploader': f.Uploader,
                                         'Filename': f.Filename, 'File1': f.File1, 'uploaded_at': f.uploaded_at,
                                         'user__username': request.user,'category':f.category})
                    else:
                        data.append({'id': f.id, 'file_type': f.file_type, 'Uploader': f.Uploader,
                                     'Filename': f.Filename, 'File1': f.File1, 'uploaded_at': f.uploaded_at,
                                     'user__username': request.user,'category':f.category})

            return data

    else:

        fDate = request.GET.get('newfromDate')
        tDate = request.GET.get('newtoDate')
        if fDate!='0' or tDate!='0':

            if tDate == '':
                tDate = fDate
            elif fDate == '':
                fDate = tDate
            from_date = datetime.strptime(fDate, '%d-%m-%Y').strftime('%Y-%m-%d %H:%M:%S')
            to_date = datetime.strptime(tDate, '%d-%m-%Y')
            toDate = calendar.timegm(to_date.timetuple())
            advancedToDate = int(toDate) + 86400
            finalToDate = datetime.utcfromtimestamp(advancedToDate).strftime('%Y-%m-%d %H:%M:%S')

            if starred=='1':
                data = FileUpload.objects.filter(user_id=user_id, is_favorite='1', uploaded_at__range=(from_date, finalToDate),deleted='0')

            elif starred=='0':
                data = FileUpload.objects.filter(user_id=user_id, uploaded_at__range=(from_date, finalToDate),deleted='0')

            return data
        else:
            if starred=='1':
                data = FileUpload.objects.filter(user_id=user_id, is_favorite='1',deleted='0')
            elif starred=='0':
                data = FileUpload.objects.filter(user_id=user_id,deleted='0')
            return data
