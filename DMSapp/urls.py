from django.conf import settings
from django.contrib import admin
from django.conf.urls.static import static
from django.conf.urls import url, include
from django.urls import path, re_path
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from django.contrib.auth.decorators import login_required
from . import views

urlpatterns = [

	path('show_delete_popup/<int:file_id>/', views.show_delete_popup, name='show_delete_popup'),
    path('download/file/<int:id>/<type>/', views.download_protection, name='download'),
    url('fileupload/bulk_upload', views.bulk_upload, name='bulk_upload'),
    url(r'^$', views.home, name='home'),
    url(r'^fileupload/', views.fileupload, name='fileupload'),
    path('delete_archive_files/<int:id>/<type>/', views.delete_archive_files, name='delete_archive_files'),
    path('dashboard/', views.dashboard_view, name='dashboard'),

    path('user_files/', views.user_files, name='user_files'),
    url(r'document_upload', views.document_upload, name='document_upload'),

    path('delete_files/<int:id>/<type>/', views.delete_files_files, name='delete_files_files'),
	
	#user files
    path('root_files/<int:pk>/<type>/<username>', views.root_files, name='root_files'),
    path('user_files/<int:id>/<type>', views.user_files, name='user_files'),
    path('user_files/favorite/<int:file_id>', views.is_favorite, name='is_favorite'),
    path('starred_documents', views.starred_docs, name='starred_documents'),
    path('download/requested_file/<int:id>', views.download_history, name='download_file_request'),
    url(r'downloaded_files', views.download_files, name='downloaded_files'),
    path('total-document', views.total_document, name='total_document'),
    url(r'^send_to_share', views.send_to_share, name='send_to_share'),
    path('approval_sent_for_share/<int:id>', views.send_to_share, name='share_document'),

    path('approval_for_sharing/<int:id>', views.for_approval, name='for_approval'),
    path('all_files_for_sharing/', views.files_for_approval, name='files_for_approval'),
    path('all_files_for_sharing/<int:id>', views.files_for_approval, name='files_for_approval'),
    path('shared_documents/', views.shared_document, name='shared_document'),
    path('all_shared_documents', views.shared_by_all, name='all_shared_document'),
    path('stop_sharing/<int:id>/', views.stop_sharing, name='stop_sharing'),

    path('searchresults/<int:pk>/', views.search_files, name='search_files'),
    path('meta_search/', views.meta_search, name='meta_search'),
    path('document_view/<int:file_id>', views.document_protection, name='protecting_doc'),
    path('archive_files/', views.archive_files, name='archive_files'),
    url(r'access_history_table', views.access_history_table, name='access_history_table'),

    path('share_document_to_user/<int:file_id>', views.share_document_to_user, name='share_document_to_user'),
    path('shared_by_other', views.shared_by_other, name='shared_by_other'),
    path('notifications', views.notification_of_files, name='notification_files'),

    path('reports', views.reports, name='reports'),
    path('reports/other_users_info', views.other_users_info, name='other_users_info'),
    path('reports/reports_of_user/<int:user_id>', views.reports_of_user, name='reports_of_user'),

    path('notifications/notification_request', views.share_notification_request, name='share_notification_request'),
    path('notification/mark_read/<int:id>', views.mark_read, name='mark_read'),

    #account
    path('approval_form_for_account_opening/<int:task_id>/', views.approval_form_for_account_opening,
         name='approval_form_for_account_opening'),

    #Notes
    path('notification/notes', views.notes, name='notes'),
    path('notification/notes/<note>', views.notes, name='notes'),
    path('discussions/', views.discussions, name='discussions'),
    path('scan_document/', views.scan_document, name='scan_document'),


    url('bulk_upload_for_files', views.bulk_upload_for_files, name='bulk_upload_for_files'),

    #Encryption
    path('encrypt_document/<int:file_id>', views.encrypt_document, name='encrypt_document'),
    path('documents/encryption', views.encryption, name='encryption'),

    #Share Externally
    path('share_document_to_user/externally/<int:file_id>', views.share_files_externally, name='share_files_externally'),
    url(r'^document_shared_externally/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        views.document_shared_externally, name='document_shared_externally'),
    path('download_externally/requested_file/<int:id>', views.download_externally, name='download_externally'),

        #Account open
    path('workflow_management/', views.workflow_management, name='workflow_management'),
    path('account_opening_approval', views.account_opening_approval, name='account_opening_approval'),
    url(r'account_open', views.account_open, name='account_open'),
    path('notification/mark_read_account/<int:id>', views.mark_read_account, name='mark_read_account'),
    url(r'workflow_management/approved_forms', views.approved_forms, name='approved_forms_for_AO'),
    url(r'workflow_management/rejected_forms', views.rejected_forms, name='rejected_forms_for_AO'),

    path('delete_notification_for_workflow/<int:notif_id>', views.delete_notification_for_workflow, name='delete_notification_for_workflow'),
    path('validate_filename/', views.validate_filename, name='validate_filename'),
    path('replace_file/', views.replace_file, name='replace_file'),
    path('restore_document/<int:file_id>', views.restore_document, name='restore_document'),
    path('check_valid_password/', views.check_valid_password, name='check_valid_password'),
    path('delete_sticky_notes/<int:note_id>', views.delete_sticky_notes, name='delete_sticky_notes'),
    path('edit_sticky_notes/<int:note_id>', views.edit_sticky_notes, name='edit_sticky_notes'),
    path('check_protection/<int:id>/<url_sub>/', views.check_protection, name='check_protection'),
    
    path('ask_replace_file_password/<int:file_id>/', views.ask_replace_file_password, name='ask_replace_file_password'),
    path('share_locked_file/<int:file_id>/', views.share_locked_file, name='share_locked_file'),
    
    path('table_request_form', views.table_request_form, name='table_request_form'),
    path('theme_management_selection', views.theme_management_selection, name='theme_management_selection'),
	
	#data to pdf
	path('data_to_excel', views.data_to_excel, name='data_to_excel'),
    path('data_to_pdf', views.data_to_pdf, name='data_to_pdf'),
    path('report_list', views.report_list, name='report_list'),

]

urlpatterns += staticfiles_urlpatterns()


if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
