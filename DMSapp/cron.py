#Install crontab using pip install crontab
from .models import  ArchiveFiles

import datetime
from datetime import date, timedelta
import os
import shutil
from datetime import datetime
from django.conf import settings
from WorkflowManagement.models import UserDataWorkflowManagement, ScannedImagesUserData


def my_scheduled_job():
    now=datetime.datetime.now()+timedelta(days=10)
    ArchiveFiles.objects.filter(deleted_at__lt=now).delete()

def create_dir(value):
    if not os.path.isdir(value):
        print('path not exists')
        try:
            os.makedirs(value)
        except OSError as exc:  # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise


def filter_and_copy(fdate, tdate, dpath):
    scanned_data = ScannedImagesUserData.objects.filter(workflow_number__workstep_name='Exit',
                                                        workflow_number__modified_date_time__range=(fdate, tdate))

    for i in scanned_data:
        wf_data = i.user_scanned_data
        acc_number = i.account_number
        for z in wf_data:
            for x, y in z.items():
                destination = os.path.join(dpath + '/' + acc_number + '/')
                create_dir(destination)
                shutil.copy(os.path.join(settings.BASE_DIR + '/' + 'DMSapp/' + y), destination)

def workflow_scheduled_job():
    print("in cron tab")
    path_for_cron = os.path.join(settings.MEDIA_ROOT + '/' + 'Folder_For_CKYC/')
    create_dir(path_for_cron)
    today = date.today()

    # Day 1
    date_path = os.path.join(settings.MEDIA_ROOT + '/' + 'Folder_For_CKYC/' + str(today))
    create_dir(date_path)
    now = datetime.now()

    fdate = datetime.strptime(str(today), '%Y-%m-%d').strftime('%Y-%m-%d %H:%M:%S')
    tdate = datetime.strptime(str(now), '%Y-%m-%d %H:%M:%S.%f').strftime('%Y-%m-%d %H:%M:%S')

    filter_and_copy(fdate, tdate, date_path)

    # Day 2
    today_2 = date.today()-timedelta(days=2)
    date_path = os.path.join(settings.MEDIA_ROOT + '/' + 'Folder_For_CKYC/' + str(today_2))
    create_dir(date_path)

    fdate = datetime.strptime(str(today_2), '%Y-%m-%d').strftime('%Y-%m-%d %H:%M:%S')
    tdate = datetime.strptime(str(today), '%Y-%m-%d').strftime('%Y-%m-%d %H:%M:%S')

    filter_and_copy(fdate, tdate, date_path)

    # Day 3
    today_3 = date.today()-timedelta(days=3)
    date_path = os.path.join(settings.MEDIA_ROOT + '/' + 'Folder_For_CKYC/' + str(today_3))
    create_dir(date_path)

    fdate = datetime.strptime(str(today_3), '%Y-%m-%d').strftime('%Y-%m-%d %H:%M:%S')
    tdate = datetime.strptime(str(today_2), '%Y-%m-%d').strftime('%Y-%m-%d %H:%M:%S')

    filter_and_copy(fdate, tdate, date_path)

    # Day 4
    today_4 = date.today()-timedelta(days=4)
    date_path = os.path.join(settings.MEDIA_ROOT + '/' + 'Folder_For_CKYC/' + str(today_4))
    create_dir(date_path)

    fdate = datetime.strptime(str(today_4), '%Y-%m-%d').strftime('%Y-%m-%d %H:%M:%S')
    tdate = datetime.strptime(str(today_3), '%Y-%m-%d').strftime('%Y-%m-%d %H:%M:%S')

    filter_and_copy(fdate, tdate, date_path)

    # Day 5
    today_5 = date.today()-timedelta(days=5)
    date_path = os.path.join(settings.MEDIA_ROOT + '/' + 'Folder_For_CKYC/' + str(today_5))
    create_dir(date_path)

    fdate = datetime.strptime(str(today_4), '%Y-%m-%d').strftime('%Y-%m-%d %H:%M:%S')
    tdate = datetime.strptime(str(today_3), '%Y-%m-%d').strftime('%Y-%m-%d %H:%M:%S')

    filter_and_copy(fdate, tdate, date_path)

    # Day 4
    today_6 = date.today()-timedelta(days=6)
    date_path = os.path.join(settings.MEDIA_ROOT + '/' + 'Folder_For_CKYC/' + str(today_6))
    create_dir(date_path)

    fdate = datetime.strptime(str(today_4), '%Y-%m-%d').strftime('%Y-%m-%d %H:%M:%S')
    tdate = datetime.strptime(str(today_3), '%Y-%m-%d').strftime('%Y-%m-%d %H:%M:%S')

    filter_and_copy(fdate, tdate, date_path)
