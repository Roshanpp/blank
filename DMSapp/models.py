from django.db import models
from django.db.models.signals import post_save,pre_delete
from django.dispatch import receiver
from django.core.validators import FileExtensionValidator
import datetime
import calendar
from datetime import datetime,timedelta
import time
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from .validators import validate_file_extension, pdf_file_validation
from simple_history.models import HistoricalRecords
from django.contrib.auth import get_user_model
from .user_directory import user_directory_identity, user_directory_pan, \
                            user_directory_photo, user_directory_signature

from WorkflowManagement.models import UserDataWorkflowManagement

#custom user model
User = get_user_model()


def user_directory_path(instance, filename):
    if filename:
        ans = filename.split('.')[-1]
    date=datetime.now()
    return 'Myfiles/{0}/{1}/{2}/{3}'.format(instance.user.first_name+' '+instance.user.last_name,
                                            date.strftime('%d-%m-%Y'), ans, filename)
     


class FileUpload(models.Model):
    CHOICES = (
        ('Documents', _('Documents')),
        ('Invoice', _('Invoice')),
        ('HR Documents', _('HR Documents'))
    )

    file_type = models.CharField(max_length=10)
    Uploader = models.CharField(max_length=20, blank=False, null=True)
    Filename = models.CharField(max_length=20, blank=False, null=True)
    File1 = models.FileField(upload_to=user_directory_path, validators=[validate_file_extension])
    uploaded_at = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE)
    is_favorite = models.BooleanField(default=False)
    is_shared = models.BooleanField(default=False)
    protected = models.CharField(max_length=15, blank=True, null=True)
    is_protected = models.BooleanField(default=False)
    is_encrypted = models.BooleanField(default=False)
    edited_at = models.DateTimeField(null=True, blank=True)
    modified_at = models.DateTimeField(null=True, blank=True)
    category = models.CharField(max_length=40, choices=CHOICES, null=True, blank=True, default='Documents')
    deleted = models.CharField(max_length=1, null=False, default='0')
    history = HistoricalRecords()

    def __str__(self):
        return str(self.user)

    def delete(self, *args, **kwargs):

        super().delete(*args, **kwargs)
    

class ArchiveFiles(models.Model):
    file_id = models.ForeignKey(FileUpload, null=True, blank=True, on_delete=models.CASCADE)
    file_type = models.CharField(max_length=10)
    Uploader = models.CharField(max_length=20, blank=False, null=True)
    Filename = models.CharField(max_length=20, blank=False, null=True)
    File1 = models.CharField(max_length=256, blank=False, null=True)
    # uploaded_at = models.DateTimeField()
    deleted_at = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE)
    is_favorite = models.BooleanField(default=False)
    is_shared = models.BooleanField(default=False)
    protected = models.CharField(max_length=15, blank=True, null=True)
    is_protected = models.BooleanField(default=False)
    is_encrypted = models.BooleanField(default=False)
    deleted = models.CharField(max_length=1, null=False, default='0')
    # edited_at = models.DateTimeField(null=True, blank=True)
    # modified_at = models.DateTimeField(null=True, blank=True)
    category = models.CharField(max_length=40, null=True, blank=True, default='Documents')



    # def delete(self, *args, **kwargs):
    #     self.File1.delete()
    #     super().delete(*args, **kwargs)
    #
    # def delete1(self, *args, **kwargs):
    #     super().delete(*args, **kwargs)


# def make_file_log(sender, instance, **kwargs):
#
#     ArchiveFiles.objects.create(file_id=instance.pk,file_type=instance.file_type,
#     Uploader=instance.Uploader,Filename=instance.Filename,File1=instance.File1,
#     uploaded_at=instance.uploaded_at,user=instance.user,is_favorite=instance.is_favorite,
#     is_shared=instance.is_shared,protected=instance.protected,is_protected=instance.is_protected,
#     is_encrypted=instance.is_encrypted,edited_at=instance.edited_at,modified_at=instance.modified_at,category=instance.category)


# pre_delete.connect(make_file_log, sender=FileUpload)

def restore_document(sender, instance, **kwargs):
    print(instance.File1)
    FileUpload.objects.create(file_type=instance.file_type,Uploader=instance.Uploader,Filename=instance.Filename,
                              File1=instance.File1,uploaded_at=instance.uploaded_at,user=instance.user,
                              is_favorite=instance.is_favorite,is_shared=instance.is_shared,protected=instance.protected,
                              is_protected=instance.is_protected,is_encrypted=instance.is_encrypted,
                              edited_at=instance.edited_at,modified_at=instance.modified_at,category=instance.category)


#pre_delete.connect(restore_document, sender=ArchiveFiles)


class Project(models.Model):
    name = models.CharField(max_length=50, verbose_name="Name")
    #created_by = models.ForeignKey(User, related_name='Project_created_by', on_delete=models.CASCADE)
    created_date = models.DateTimeField(auto_now_add=True)
    modified_by = models.ForeignKey(User, related_name='Project_modified_by', on_delete=models.CASCADE)
    modified_date = models.DateTimeField(auto_now=True)
    assigned_to = models.ForeignKey(User, related_name='Project_assigned_to', default=1, on_delete=models.CASCADE)
    #status = models.ForeignKey('Status', related_name='Project_status', default=1, on_delete=models.CASCADE)

    '''def save(self):
        if self.id:
            self.modified_date = datetime.datetime.now()
        else:
            self.created_date = datetime.datetime.now()
        super(Project, self).save()'''

    def __str__(self):
        return self.name


#history of dowmload files(models)
class Download(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True,)
    download = models.CharField(max_length=50, null=True, blank=True)
    download_at = models.DateTimeField(auto_now_add=True)
    ip_address = models.CharField(max_length=20, null=True, blank=True)

    def __str__(self):
        return self.download


class SharedDocument(models.Model):
    request_status = (
        ('select', 'Select'),
        ('Approved', 'Approved'),
        ('Rejected', 'Rejected'),
        ('drafted', 'Drafted'),
        ('Added_comment', 'Added_comment'),
        )
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    file = models.ForeignKey(FileUpload, on_delete=models.CASCADE, null=True, blank=True)
    timestamp = models.DateTimeField(auto_now_add=True, auto_now=False)
    status = models.CharField(max_length=20, choices=request_status, default='select', blank=True)
    Comment = models.TextField(max_length=100, null=True, blank=True)
    requested_user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True, related_name='requested_user')

    @property
    def _field_history_user(self):
        return self.user

    def __str__(self):
        return self.status


class Notification(models.Model):
    actor = models.ForeignKey(User, related_name='actor', on_delete=models.CASCADE)
    receiver = models.ForeignKey(User, related_name='notifications', on_delete=models.CASCADE)
    sender = models.ForeignKey(User, related_name='sent_notifications', blank=True, null=True, on_delete=models.CASCADE)

    object_type = models.ForeignKey(FileUpload, blank=True, null=True, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField(blank=True, null=True)
    # object = GenericForeignKey('object_type', 'object_id')
    status = models.ForeignKey(SharedDocument, blank=True, null=True, on_delete=models.CASCADE)

    type = models.CharField(max_length=20)
    unread = models.BooleanField(default=True)

    slug = models.SlugField(max_length=255, unique=False, default='notification')

    # Other
    date_created = models.DateTimeField(auto_now=False, auto_now_add=True, blank=True)
    date_modified = models.DateTimeField(auto_now=True, auto_now_add=False, blank=True)


class Notes(models.Model):
    title = models.CharField(max_length=20, null=True, blank=False)
    note = models.CharField(max_length=100, null=True, blank=False)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return str(self.note)


class NotificationForAccount(models.Model):
    # actor = models.ForeignKey(Profile, related_name='actor', on_delete=models.CASCADE)
    receiver = models.ForeignKey(User, related_name='reciever_notifications', on_delete=models.CASCADE)
    sender = models.ForeignKey(User, related_name='sender_notifications', blank=True, null=True, on_delete=models.CASCADE)

    type = models.CharField(max_length=20)
    unread = models.BooleanField(default=True)

    slug = models.SlugField(max_length=255, unique=False, default='notification')

    # Other
    date_created = models.DateTimeField(auto_now=False, auto_now_add=True, blank=True)
    date_modified = models.DateTimeField(auto_now=True, auto_now_add=False, blank=True)
    status = models.ForeignKey(UserDataWorkflowManagement, blank=True, null=True, on_delete=models.CASCADE)
