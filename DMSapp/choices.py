THEME_CHOICES = (
    ('', '--Select--'),
    ('Default', 'Default'),
    ('Theme_1', 'Theme_1'),
    ('Theme_2', 'Theme_2'),
    ('Theme_3', 'Theme_3'),
    ('Theme_4', 'Theme_4'),
)


Theme_1 = {'title': 'Theme 1', 'bg_color': 'aliceblue', 'profile_title': '#1d7e8a',
           'profile_content': '#33a1af', 'header_color': 'powderblue', 'footer_color': 'black'}

Theme_2 = {'title': 'Theme 2', 'bg_color': 'honeydew', 'profile_title': '#efb242',
           'profile_content': '#ab5151', 'header_color': 'gainsboro', 'footer_color': 'black'}

Theme_3 = {'title': 'Theme 3', 'bg_color': 'green', 'profile_title': 'blue',
           'profile_content': 'grey', 'header_color': 'yellow', 'footer_color': 'yellow'}

Theme_4 = {'title': 'Theme 4', 'bg_color': 'yellow', 'profile_title': 'black',
           'profile_content': 'pink', 'header_color': 'green', 'footer_color': 'orange'}

Default = {'title': 'Default_Theme', 'bg_color': 'white', 'profile_title': '#616184',
           'profile_content': '#9898bf', 'header_color': '#e9e9e9', 'footer_color': '#1a1a1a'}
           
           
PAGE_RECORD_CHOICES = (
    ('', '--Select--'),
	('5', '5'),
    ('10', '10'),
    ('15', '15'),
    ('20', '20'),
    ('25', '25'),
    ('30', '30'),
)



