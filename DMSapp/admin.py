from django.contrib import admin
from .models import FileUpload, SharedDocument
from import_export import resources
from import_export.admin import ImportExportModelAdmin


class FileAdmin(ImportExportModelAdmin,admin.ModelAdmin):
    list_display =('Uploader','Filename','uploaded_at','file_type')
    search_fields = ['file_type','Uploader','Filename']
    list_filter =('file_type','Uploader','Filename',)
    list_per_page = 6

class SharedAdmin(ImportExportModelAdmin,admin.ModelAdmin):
    list_display = ('user', 'file', 'timestamp')
    search_fields = ['user', 'file']
    list_filter = ( 'user', 'file',)
    list_per_page = 6

admin.site.register(FileUpload,FileAdmin)
admin.site.register(SharedDocument,SharedAdmin)
