

#Validate file extension 
def validate_file_extension(value):
    import os
    from django.core.exceptions import ValidationError
    ext = os.path.splitext(value.name)[1]
    valid_extensions = ['.pdf', '.doc', '.docx', '.jpg', '.png', '.xlsx', '.xls', '.jpeg', '.zip', '.txt', '.tiff']
    if not ext.lower() in valid_extensions:
        raise ValidationError('Unsupported file extension.')


def pdf_file_validation(value):
    import os
    from django.core.exceptions import ValidationError
    ext = os.path.splitext(value.name)[1]
    valid_extensions = ['.pdf']
    if not ext.lower() in valid_extensions:
        raise ValidationError('Only support .pdf extension')
