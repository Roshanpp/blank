from django.core.paginator import Paginator,PageNotAnInteger, EmptyPage
from DMSaccount.models import UserSetting


def paginate(request,data,page):

    user = UserSetting.objects.filter(user_id = request.user.pk)
    if user:
        for u in user:
            page_per_records = u.page_records
    else:
        page_per_records = 5
    paginator = Paginator(data, page_per_records)
    try:
        qs_json = paginator.page(page)

    except PageNotAnInteger:
        qs_json = paginator.page(1)
    except EmptyPage:
        qs_json = paginator.page(paginator.num_pages)

    return qs_json