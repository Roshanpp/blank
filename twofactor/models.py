from django.db import models
from django.contrib.auth import get_user_model

User = get_user_model()



class TwoFactor(models.Model):
    number = models.CharField(max_length=16)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
