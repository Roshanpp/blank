from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import url
from django.urls import path, re_path

from . import views

urlpatterns = [
    url(r'^files/([^/]+)$', views.load, name='load_file'),
    # url(r'^files/', views.gall, name='gallary'),
    url(r'^canvas/', views.gall, name='load_file'),

]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
