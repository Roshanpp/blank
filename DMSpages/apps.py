from django.apps import AppConfig


class DmspagesConfig(AppConfig):
    name = 'DMSpages'
