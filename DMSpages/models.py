from django.db import models

from ckeditor_uploader.fields import RichTextUploadingField
# from ckeditor.fields import RichTextField
from ckeditor.widgets import CKEditorWidget


class Post(models.Model):
    content = RichTextUploadingField(null=True, blank=True)
    widget1 = CKEditorWidget()

# Create your models here.
