from django.shortcuts import render, HttpResponse
from django.views.decorators.csrf import csrf_exempt

from DMSapp.models import FileUpload
from DMSpages.forms import PostForm


def home(request):
    files = FileUpload.objects.all()
    context = {'files': files}
    return render(request, 'load_pages.html', context)


@csrf_exempt
def save(request):
    name = request.POST.get('Filename')
    File = request.POST.get('data')
    data = FileUpload(Filename=name, File1=File)
    data.save()
    return render(request, 'load_pages.html')


def gall(request):
    posts = [dict(id=file.id, title=file.Filename) for file in FileUpload.objects.order_by('id')]
    context = {'posts': posts}
    return render(request, 'canvas.html',)


def load_file(request, name):
    data = FileUpload.objects.filter(Filename=name)
    # posts = [dict(id=file.id, title=file.Filename, File=file.File1) for file in FileUpload.objects.filter(Filename=name)]
    return render(request, 'load_file.html', {'posts': data})


def load(request, name):
    data = FileUpload.objects.filter(Filename=name)
    if request.method == 'POST':
        form = PostForm(request.POST)
        if form.is_valid:
            form.save()
            context = {'form': form}
            return render(request, 'ck.html',context)
        else:
            return HttpResponse('Not reached')
    else:
        form = PostForm
        context = {'form': form}
        return render(request, 'ck.html', context)
