from django.db import models
from django.contrib.auth import get_user_model
import jsonfield


# Create your models here.


User = get_user_model()


default_field = [
              {"label":"First Name","element_type":"TextField","validation":"only_alphabets","required":False},
              {"label":"Last Name","element_type":"TextField","validation":"only_alphabets","required":False},
              {"label":"Gender","element_type":"RadioButton","element_names":[["male","Male"],["female","Female"]],"validation":"No Validation","required":False},
              {"label":"Email","element_type":"TextField","validation":"email","required":False},
              {"label":"Mobile Number","element_type":"TextField","validation":"checkmobile","required":False},
              {"label":"Application Form","element_type":"TextField","validation":"only numbers","required":True}
]


class NewWorkflowManagement(models.Model):
    wf_id = models.IntegerField(blank=True, null=True)
    wf_step = models.CharField(max_length=30, blank=True, null=True)
    wf_name = models.CharField(max_length=20, blank=True, null=True, unique=True)
    wf_definition = jsonfield.JSONField(default=default_field)
    status = models.BooleanField(default=False)
    new_wf_definition = jsonfield.JSONField()

    def __str__(self):
        return self.wf_name


class UserDataWorkflowManagement(models.Model):
    status = (
        ('Drafted', 'Drafted'),
        ('Pending', 'Pending'),
        ('Approved', 'Approved'),
        ('Rejected', 'Rejected'),
    )

    workflow_id = models.ForeignKey(NewWorkflowManagement, on_delete=models.CASCADE, null=True, blank=True)
    workflow_number = models.CharField(unique=True, max_length=250, default='null')
    user = models.ForeignKey(User, related_name='Changes_Made_By', on_delete=models.SET_NULL, null=True, blank=True)
    user_data_definition = jsonfield.JSONField()
    status = models.CharField(max_length=20, choices=status, null=True, blank=True)
    remarks = models.TextField(max_length=100, null=True, blank=True)
    maker = models.ForeignKey(User, related_name='Maker', on_delete=models.SET_NULL, null=True, blank=True)
    checker = models.ForeignKey(User, related_name='Checker', on_delete=models.SET_NULL, null=True, blank=True)
    application_number = models.CharField(max_length=25, blank=True, null=True)
    documents = models.BooleanField(default=False)

    # Change for the New Workflow Management Process
    account_number = models.CharField(max_length=30, null=True, blank=True)
    new_user_data_definition = jsonfield.JSONField()
    f_entry = models.CharField(max_length=30, null=True, blank=True)

    workstep_name = models.CharField(max_length=250, null=True)
    previous_workstep = models.CharField(max_length=250, null=True)
    entry_date_time = models.DateTimeField(null=True, blank=True)
    modified_date_time = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return self.status


class ModelForTaggingFiles(models.Model):
    customer_data_field = jsonfield.JSONField()
    remarks = models.CharField(max_length=25, blank=True, null=True)


class ModelForCustomerData(models.Model):
    customer_data = jsonfield.JSONField()
    customer_number = models.CharField(max_length=25, null=True, blank=True)
    remarks = models.CharField(max_length=25, null=True, blank=True)


class UserDataForScannedImages(models.Model):
    tagging_format = jsonfield.JSONField()
    account_number = models.CharField(max_length=25, blank=True, null=True)
    status = models.BooleanField(default=False, null=True, blank=True)


class ScannedImagesUserData(models.Model):
    workflow_id = models.ForeignKey(NewWorkflowManagement, on_delete=models.SET_NULL, null=True, blank=True)
    user_scanned_data = jsonfield.JSONField()
    application_number = models.CharField(max_length=25, blank=True, null=True)
    is_encrypted = models.BooleanField(default=False, null=True, blank=True)
    workflow_number = models.ForeignKey(UserDataWorkflowManagement, on_delete=models.SET_NULL, null=True, blank=True)

    account_number = models.CharField(max_length=30, null=True, blank=True)
    versions_field = jsonfield.JSONField(blank=True, null=True)
    account_type = models.CharField(max_length=300, blank=True, null=True)


    def __str__(self):
        return self.account_number

#################### Reports ########################

class ReportForWorkflow(models.Model):
    report_name = models.CharField(max_length=50, null=False, default='')
    creation_date = models.DateTimeField(auto_now=False, auto_now_add=True, blank=True)
    query = models.TextField(max_length=300, null=False, default='')
    description = models.TextField(max_length=200, null=True, blank=True)

    def __str__(self):
        return self.report_name
