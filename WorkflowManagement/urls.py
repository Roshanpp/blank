from django.conf.urls import url, include
from django.urls import path, re_path
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

from . import views

urlpatterns = [

    path('workflow_form_selection', views.workflow_form_selection, name='workflow_form_selection'),
    path('workflow_management_selection/<int:queryset_id>', views.workflow_management_selection, name='workflow_management_selection'),
    path('workflow_management_form_view/<int:queryset_id>', views.workflow_form_view, name='workflow_form_view'),
    path('removeField/<int:queryset_id>/<label>', views.remove_field, name='removeField'),
    path('validate_label_name_workflow/<int:queryset_id>', views.validate_label, name='validate_label'),
    path('validate_mobile_number_workflow/', views.checkmobile, name='validate_mobile_number_workflow'),
    path('validate_pan_number_workflow/', views.validate_pan_number, name='validate_pan_number'),
    path('validate_aadhar_number/', views.validate_aadhar_number, name='validate_aadhar_number'),
    path('validateEmail/', views.validateEmail, name='validateEmail'),
    path('confirm_workflow/<int:queryset_id>', views.confirm_workflow, name='confirm_workflow'),
    path('toggle_button/<int:wf_id>', views.toggle_button, name='toggle_button'),
    path('validateOnlyAlphabets/', views.validateOnlyAlphabets, name='validateOnlyAlphabets'),
    path('workflow_management', views.workflow_management, name='workflow_management'),
    path('delete_workflow/<int:id>', views.delete_workflow, name='delete_workflow'),
    path('validateAlphaNumeric/', views.validateAlphaNumeric, name='validateAlphaNumeric'),
    path('workflow_selection_for_department', views.workflow_selection_for_department, name='workflow_selection_for_department'),
    path('fill_workflow_form/<int:queryset_id>', views.fill_workflow_form, name='fill_workflow_form'),
    path('all_wf_forms/<int:queryset_id>/<status>', views.all_wf_forms, name='all_wf_forms'),
    path('all_wf_forms/<int:queryset_id>/<status>/<int:read>', views.all_wf_forms, name='all_wf_forms'),
    path('all_workflow_for_maker', views.all_workflow_for_maker, name='all_workflow_for_maker'),
    path('all_user_forms_workflow/<int:forms_pk>', views.all_user_forms_workflow, name='all_user_forms_workflow'),
    path('all_user_forms_workflow/<int:forms_pk>/<int:read>', views.all_user_forms_workflow, name='all_user_forms_workflow'),
    path('edit_workflow_maker/<int:user_data_pk>', views.edit_workflow_maker, name='edit_workflow_maker'),
    path('submit_to_checker/<int:user_data_pk>', views.submit_to_checker, name='submit_to_checker'),
    path('all_workflow_for_checker', views.all_workflow_for_checker, name='all_workflow_for_checker'),
    path('all_wf_for_approval/<int:wf_id>', views.all_wf_for_approval, name='all_wf_for_approval'),
    path('customer_data_for_approval/<int:data_id>', views.customer_data_for_approval, name='customer_data_for_approval'),
    path('customer_data_for_approval/<int:data_id>/<int:read>', views.customer_data_for_approval, name='customer_data_for_approval'),
    path('checker_for_approval/<int:data_id>', views.checker_for_approval, name='checker_for_approval'),

    path('all_rejected_forms/<int:form_id>', views.all_rejected_forms, name='all_rejected_forms'),
    path('all_rejected_forms/<int:form_id>/<int:read>', views.all_rejected_forms, name='all_rejected_forms'),
    path('delete_form/<int:form_id>', views.delete_form, name='delete_form'),

    url('image_ocr', views.image_ocr ,name='image_ocr'),

    path('scan_documents', views.scan_documents_sanelib, name='scan_documents_sanelib'),
    path('document_scan_parameter', views.document_scan_parameter, name='document_scan_parameter'),

    path('scanned_images', views.scanned_images, name='scanned_images'),
    path('scanned_user_data', views.scanned_user_data, name='scanned_user_data'),
    path('submit_tag_option', views.submit_tag_option, name='submit_tag_option'),
    path('crop_popup/', views.crop_popup, name='crop_popup'),
    path('submit_cropped_image', views.submit_cropped_image, name='submit_cropped_image'),
    path('save_rotated_image', views.save_rotated_image, name='save_rotated_image'),

    path('ajax_scanned_document', views.ajax_scanned_document, name='ajax_scanned_document'),
    path('ajax_login_call', views.ajax_login_call, name='ajax_login_call'),

    # New Changes in Workflow

    #path('workflowProcess', views.workflowProcess, name='workflowProcess'),

    path('workflow_process/<int:id>',views.workflow_process,name='workflow_process'),
    path('workflow_process/workflowLogin', views.workflowLogin, name='workflowLogin'),
    path("workflowDocsLogin", views.workflowDocsLogin, name='workflowDocsLogin'),
    path('fetch_data_from_finacle', views.fetch_data_from_finacle, name='fetch_data_from_finacle'),
    path('confirm_finacle_entry/<int:user_data_pk>', views.confirm_finacle_entry, name='confirm_finacle_entry'),

    path('workflow_docs', views.workflow_docs, name='workflow_docs'),
    path('workflow_docs_search', views.workflow_docs_search, name='workflow_docs_search'),

    path('download_workflow_docs/<int:value>/<str_value>', views.download_workflow_docs, name="download_workflow_docs"),
    path('download_workflow_docs/<int:value>', views.download_workflow_docs, name="download_workflow_docs"),
    path('confirm_workflow_docs', views.confirm_workflow_docs, name="confirm_workflow_docs"),

    path('del_workflow_docs/<int:value>/<str_value>', views.del_workflow_docs, name="del_workflow_docs"),
    path('del_workflow_docs/<int:value>', views.del_workflow_docs, name="del_workflow_docs"),

    path('view_mis_report', views.view_mis_report, name='view_mis_report'),
    path('save_image_workflow_process', views.save_image_workflow_process, name="save_image_workflow_process"),
    path('workflow_docs_for_version', views.workflow_docs_for_version, name='workflow_docs_for_version'),
    url(r'^download_version_file/(?P<path>.*)$', views.download_version_file, name='download_version_file'),


    path('ajax_call_for_files', views.ajax_call_for_files, name='ajax_call_for_files'),
    path('workflow_docs_list', views.workflow_docs_list, name="workflow_docs_list"),
    path('view_approved_document/<doc>/<doc_id>', views.view_approved_document, name="view_approved_document"),
    
    path('extract_account_numner_nodata/<value>', views.extract_account_numner_nodata, name='extract_account_numner_nodata'),
    path('delete_workflow_scanned_data/<int:value>', views.delete_workflow_scanned_data, name='delete_workflow_scanned_data'),

    path('download_workflow_document_for_ckyc/', views.download_workflow_document_for_ckyc, name='download_workflow_document_for_ckyc'),
    path('workflow_reports', views.workflow_reports, name='workflow_reports'),
]


urlpatterns += staticfiles_urlpatterns()


if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
