try:
    from PIL import Image
except ImportError:
    import Image
#import pytesseract
import cv2
import numpy as np
import tempfile
# alternate approach using pyocr
from PIL import Image
import sys
import os
from django.conf import settings

try:
    import pyocr
    import pyocr.builders
except ImportError:
    print("Pyocr isn't available")

from django.conf import settings
import codecs
import collections as co
import pandas as pd

try:
    no_of_tool = pyocr.get_available_tools()
    tool = no_of_tool[0]
    print('tool',tool)
    builder = pyocr.builders.TextBuilder()
except:
    print("No installer of Tesseract found")


def ocr_core(filename,lang1):
    """
    This function will handle the core OCR processing of images.pu

    """
    print('static path', settings.STATIC_ROOT) 
    file_path = os.path.join(settings.STATIC_ROOT, 'xlsx/lang.xlsx') 
    print(file_path)
    df = pd.read_excel(file_path)
    df = df.where(pd.notnull(df), None)
    od = co.OrderedDict((k.strip(), v.strip())
                        for (v, k) in df.values)
    if lang1 == "None":
        lang1 = "eng"
    else:
        lang_code = od[lang1]
        lang1 = "eng+" + str(lang_code)

    print(lang1)
    txt = tool.image_to_string(Image.open( filename),lang=lang1,builder=builder)
    print(txt)
    return txt
