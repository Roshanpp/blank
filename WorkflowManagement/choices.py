
from django import forms
import re

Element_Type = (
    ('', '--Select--'),
    ('CheckBox', 'CheckBox'),
    ('ChoiceField', 'ChoiceField'),
    ('FileField', 'FileField'),
    ('RadioButton', 'RadioButton'),
    ('TextArea', 'TextArea'),
    ('TextField', 'TextField'),
)


# alphanumeric = validators.RegexValidator(r'^[0-9a-zA-Z]*$', 'Only alphanumeric characters are allowed.')
# email = validators.EmailValidator('Please enter correct email ID.')


def checkmobile(value):
    Pattern = re.compile("(0/91/022)?[7-9][0-9]{9}")

    if not Pattern.match(value):
        raise forms.ValidationError("Invalid Mobile Number")


validations = (
    ('', '--Select--'),
    ('alphanumeric', 'alphanumeric'),
    ('checkmobile', 'checkmobile'),
    ('email', 'email'),
    ('No Validation', 'No Validation'),
    ('only_alphabets', 'Only Alphabets'),
    ('aadhar_validation','Aadhar Validation'),
    ('pancard_validation','Pancard Validation'),
    ('only numbers','Only Numbers')
)

time = 0

AO_fields = [{1:'AO_Introduction'}, {2:'AO_Scanning'}, {3:'Finacle Data Entry'}, {4:'AO_Acceptance'},
             {5:'Download Workflow'}, {6:'Exit'}]
