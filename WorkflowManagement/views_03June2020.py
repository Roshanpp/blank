from django.shortcuts import render
from django.contrib.auth import get_user_model
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse, Http404
from django.template.loader import render_to_string
from django.contrib import messages
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from datetime import datetime
import os
from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.decorators import login_required
from django.views.decorators.cache import never_cache
from django.core.files.base import ContentFile
from PIL import Image
from django.core.files.storage import default_storage
from django.db.utils import OperationalError, DatabaseError
from django.db import connection

from .ocr_core import ocr_core
from DMSapp.crypt import Cryptographer
from django.shortcuts import redirect, render, HttpResponseRedirect, reverse, get_object_or_404, HttpResponse
from .forms import NewWorkflowManagementForm, WorkflowManagementFormset, WorkflowFormView, MakerWorkflowFormView, \
					MakerWorkflowFormView2, UserDataWorkflowManagementForm, SelectTaggingInfo, \
					MisQueryFormWorkflow
from .models import NewWorkflowManagement, UserDataWorkflowManagement, ModelForTaggingFiles, ModelForCustomerData, \
					UserDataForScannedImages, ScannedImagesUserData
import re
import random
from dateutil import parser
from DMSapp.models import NotificationForAccount
from django.core import serializers
import json
from django.core.paginator import Paginator,PageNotAnInteger, EmptyPage
from django.db.models import Q
from .models import default_field
from DMSapp.pagination import paginate
import base64
import mimetypes
import zipfile
User = get_user_model()


fetch_data = {'First Name': 'JIO','Last Name':'JO','Gender':'Male','Email':'jo@gmail.com',
			   'Mobile Number':  '9089090890','Customer Name': 'JIO UO','Customer ID': '124'}


@login_required(login_url='/login/')
def workflow_management(request):
	return render(request, 'workflow_management.html')


@login_required(login_url='/login/')
def workflow_form(request):
	if request.method == 'POST':
		form = NewWorkflowManagementForm(request.POST, request.FILES)
		if form.is_valid():
			form.save()
			request.session.cycle_key()
			return redirect('home')
	else:
		form = NewWorkflowManagementForm()
	context = {'form': form}
	return render(request, 'workflow_management_new.html', context)


@login_required(login_url='/login/')
@user_passes_test(lambda u: u.is_manager)
def workflow_form_selection(request):
	all_wf = NewWorkflowManagement.objects.all().order_by('-pk')

	if request.GET.get('searchVal'):
		searchfor=request.GET.get('searchVal')

		if searchfor!='0' and searchfor!='' and searchfor!='NULL' and searchfor is not None:
			all_wf = NewWorkflowManagement.objects.filter(Q(wf_name__icontains = searchfor)).order_by("-pk")

		elif searchfor=='0':
			all_wf = NewWorkflowManagement.objects.all().order_by('-pk')

		qs_json = serializers.serialize('json', all_wf)
		qs_json=json.loads(qs_json)
		page = request.GET.get('page', 1)
		#paginator = Paginator(qs_json, 5)

		count=0

		for i in qs_json:
			count+=1

			i['fields']['srNo']=str(count)
			a = json.loads(i['fields']['wf_definition'])
			if a == default_field:
				i['fields']['default'] = True
			else:
				i['fields']['default'] = False

		qs_json = paginate(request,qs_json,page)
		return workflow_search_list(request, qs_json, 'workflow_selection.html')

	elif request.GET.get('searchFOR'):
		searchfor=request.GET.get('searchFOR')
		all_wf = NewWorkflowManagement.objects.filter(Q(wf_name__icontains=searchfor)).order_by("-pk")
		qs_json = serializers.serialize('json', all_wf)
		qs_json=json.loads(qs_json)
		page = request.GET.get('page')
		#paginator = Paginator(qs_json, 5)

		count=0

		for i in qs_json:
			count+=1
			i['fields']['srNo']=str(count)
			a = json.loads(i['fields']['wf_definition'])
			if a == default_field:
				i['fields']['default'] = True
			else:
				i['fields']['default'] = False

		qs_json = paginate(request,qs_json,page)
		return workflow_search_list(request, qs_json, 'workflow_selection.html')
	else:
		qs_json = serializers.serialize('json', all_wf)
		qs_json = json.loads(qs_json)

		page = request.GET.get('page', 1)
		#paginator = Paginator(qs_json, 5)
		count = 0
		for i in qs_json:
			count += 1
			i['fields']['srNo'] = str(count)
			print(i)
			a = json.loads(json.dumps(i['fields']['wf_definition']))
			if a == default_field:
				i['fields']['default'] = True
			else:
				i['fields']['default'] = False

		qs_json = paginate(request,qs_json,page)
	if request.method == 'POST':
		form = NewWorkflowManagementForm(request.POST, request.FILES)
		if form.is_valid():
			name = form.cleaned_data['name']
			if name:
				queryset = NewWorkflowManagement.objects.filter(wf_name=name)
				if queryset:
					messages.error(request, 'Workflow with same name already exists')
				else:
					NewWorkflowManagement(wf_name=name).save()
					queryset = NewWorkflowManagement.objects.get(wf_name=name)

					messages.success(request, 'Workflow with name ' + queryset.wf_name + ' created successfully')
					return HttpResponseRedirect(reverse('WorkflowManagement:workflow_form_selection'))
	else:
		form = NewWorkflowManagementForm()
	context0 = {'form': form, 'files': qs_json}
	return render(request, 'workflow_selection.html', context0)


def workflow_search_list(request, files, template_name):
	data = dict()
	if request.method == 'GET':

		data['form_is_valid'] = True
		context01 = {'files': files}
		data['html_book_list'] = render_to_string('workflow_files/workflow_names.html',context01, request=request)

		data['page_div'] = render_to_string('files/pagination.html',context01, request=request)
	context = {'files':files}
	data['html_form'] = render_to_string(template_name, context, request=request)
	return JsonResponse(data)


@csrf_exempt
@login_required(login_url='/login/')
def workflow_management_selection(request, queryset_id):
	queryset = NewWorkflowManagement.objects.get(pk=queryset_id)
	qs_json = queryset.wf_definition

	page = request.GET.get('page', 1)
	#paginator = Paginator(qs_json, 5)
	count = 0

	for k in qs_json:
		count += 1
		k['srNo'] = str(count)

	qs_json = paginate(request,qs_json,page)
	if request.method == 'POST':
		if request.POST.get('checked'):
			formset = WorkflowManagementFormset()
			is_checked = request.POST.get('checked')
			if is_checked == "true":
				is_checked = True
			else:
				is_checked = False

			queryset_id = request.POST.get('queryset_id')
			element = request.POST.get('element')

			rec = NewWorkflowManagement.objects.get(pk=queryset_id)
			val_data = rec.wf_definition
			for v in val_data:
				if v['label'] == element:
					v['required'] = is_checked
			rec.save()

			return HttpResponseRedirect(reverse('WorkflowManagement:workflow_management_selection',
												args=[queryset_id]))
		else:

			formset = WorkflowManagementFormset(request.POST, request.FILES)
			if formset.is_valid():
				jsonData = []
				for form in formset:
					label = form.cleaned_data['label']
					element_type = form.cleaned_data['element_type']
					validation = form.cleaned_data['validation']
					count = 1
					if element_type == 'RadioButton' or element_type == 'CheckBox' or element_type == 'ChoiceField':
						radioVal=[]
						total_chq = request.POST.get('total_chq')
						while str(count) != str(int(total_chq) + 1):
							radio_option = request.POST.get('option_name_' + str(count))
							innerlist = [radio_option, radio_option]
							radioVal.append(innerlist)
							count += 1

					if element_type == 'RadioButton' or element_type == 'CheckBox' or element_type == 'ChoiceField':
						data = {'label': label, 'element_type': element_type,'element_names':radioVal, 'validation': validation, 'required': False}
					else:
						data = {'label': label, 'element_type': element_type,'validation': validation, 'required': False}

					q = NewWorkflowManagement.objects.filter(pk=queryset_id)
					if q:
						for k in q:
							definition = k.wf_definition
							for d in definition:
								jsonData.append(d)

					p = NewWorkflowManagement.objects.get(pk=queryset_id)
					jsonData.append(data)
					p.wf_definition = jsonData

					p.save()

					return HttpResponseRedirect(reverse('WorkflowManagement:workflow_management_selection',
													args=[queryset_id]))
	else:
		formset = WorkflowManagementFormset()
	context00 = {'files': qs_json,'queryset': queryset, 'formset': formset}
	return render(request, 'workflow_management_selection.html', context00)


@login_required(login_url='/login/')
@csrf_exempt
def workflow_form_view(request, queryset_id):
	queryset = NewWorkflowManagement.objects.get(pk=queryset_id)
	if request.method == 'POST':
		form = WorkflowFormView(request.POST, queryset_id=queryset_id)
	else:
		form = WorkflowFormView(queryset_id=queryset_id)
	context = {'queryset': queryset, 'form': form}
	return render(request, 'workflow_form_view.html', context)


@login_required(login_url='/login/')
def remove_field(request, queryset_id, label):
	formset = WorkflowManagementFormset()
	data = dict()
	if queryset_id:
		query_set = NewWorkflowManagement.objects.get(pk=queryset_id)
		wf_def = query_set.wf_definition

		for i in range(len(wf_def)):
			if wf_def[i]["label"] == label:
				wf_def.pop(i)
				query_set.save()
				break

		query_set2 = NewWorkflowManagement.objects.get(pk=queryset_id)
		qs_json = query_set2.wf_definition

		page = request.GET.get('page', 1)
		paginator = Paginator(qs_json, 5)
		count = 0
		for k in qs_json:
			count += 1
			k['srNo'] = str(count)

		try:
			qs_json = paginator.page(page)

		except PageNotAnInteger:
			qs_json = paginator.page(1)
		except EmptyPage:
			qs_json = paginator.page(paginator.num_pages)

		context0 = {'queryset':query_set2,'files': qs_json,'formset': formset}
		data['html_book_list'] = render_to_string('workflow_files/workflow_fields.html',context0, request=request)

		context1 = {'files': qs_json}
		data['page_div'] = render_to_string('files/pagination.html', context1, request=request)
	context={'files': qs_json,'queryset': query_set,'formset': formset}
	data['html_form'] = render_to_string('workflow_management_selection.html', context , request=request)
	return JsonResponse(data)


@login_required(login_url='/login/')
def validate_label(request, queryset_id):
	label = request.GET.get('label', None)
	query_set = NewWorkflowManagement.objects.get(pk=queryset_id)
	wf_def = query_set.wf_definition
	data = dict()
	for i in range(len(wf_def)):
		if wf_def[i]["label"] == label:
			data = {'is_taken': True}
			break
		else:
			data = {'is_taken': False}
	data['match'] = all(x.isalpha() or x.isspace() for x in label)  #label.isalnum()
	return JsonResponse(data)


@login_required(login_url='/login/')
def checkmobile(request):
	if request.GET.get('mobile'):
		mobile_no = request.GET.get('mobile')
		print(mobile_no)
		Pattern = re.compile("(0/91/022)?[7-9][0-9]{9}")
		if not Pattern.match(mobile_no):
			print("FDHSDFHSDG")
			data = {'invalid': True}
		else:
			data = {'invalid': False}
		return JsonResponse(data)


@login_required(login_url='/login/')
def confirm_workflow(request, queryset_id):
	queryset = NewWorkflowManagement.objects.get(pk=queryset_id)
	if queryset.status is False:
		queryset.status = True
		queryset.wf_step = "Before F.Entry"  # Added to distinguish between BRANCH and FINACLE CUSTOMER NAME
		queryset.save()
		messages.success(request, 'Your Form has been successfully registered with ' + queryset.wf_name + ' name')
		return HttpResponseRedirect(reverse('WorkflowManagement:workflow_form_selection'))
	else:
		pass
	form = WorkflowFormView(queryset_id=queryset_id)
	context = {'queryset': queryset, 'form': form}
	return render(request, 'workflow_form_view.html', context)


@login_required(login_url='/login/')
def toggle_button(request, wf_id):
	workflow = NewWorkflowManagement.objects.get(pk=wf_id)
	try:
		if workflow.status:
			workflow.status = False
			workflow.save()
			return JsonResponse({'success': False})
		else:
			workflow.status = True
			workflow.save()
			return JsonResponse({'success': True})
	except (KeyError, NewWorkflowManagement.DoesNotExist):
		return JsonResponse({'success': False})


def delete_workflow(request, id):
	work_flow = get_object_or_404(NewWorkflowManagement, id=id)
	data = dict()
	if request.GET.get('delete'):
		work_flow.delete()
		data['form_is_valid'] = True
		all_wf = NewWorkflowManagement.objects.all().order_by('-pk')

		if request.GET.get('searchFOR'):

			searchfor = request.GET.get('searchFOR')
			filter_res = ''
			if searchfor != '':
				all_wf = NewWorkflowManagement.objects.filter(Q(wf_name__icontains=searchfor)).order_by("-pk")

			else:
				all_wf = NewWorkflowManagement.objects.all().order_by('-pk')

			qs_json = serializers.serialize('json', all_wf)
			qs_json=json.loads(qs_json)

			page = request.GET.get('page', 1)
			paginator = Paginator(qs_json, 5)
			count=0

			for i in qs_json:
				count += 1
				i['fields']['srNo']=str(count)
				a = json.loads(i['fields']['wf_definition'])
				if a == default_field:
					i['fields']['default'] = True
				else:
					i['fields']['default'] = False

			try:
				qs_json = paginator.page(page)

			except PageNotAnInteger:
				qs_json = paginator.page(1)
			except EmptyPage:
				qs_json = paginator.page(paginator.num_pages)

			return workflow_search_list(request, qs_json, 'workflow_selection.html')
		else:
			qs_json = serializers.serialize('json', all_wf)
			qs_json = json.loads(qs_json)

			page = request.GET.get('page', 1)
			paginator = Paginator(qs_json, 5)

			count = 0
			for i in qs_json:
				count += 1
				i['fields']['srNo'] = str(count)
				a = json.loads(json.dumps(i['fields']['wf_definition']))
				if a == default_field:
					i['fields']['default'] = True
				else:
					i['fields']['default'] = False

			try:
				qs_json = paginator.page(page)

			except PageNotAnInteger:
				qs_json = paginator.page(1)
			except EmptyPage:
				qs_json = paginator.page(paginator.num_pages)
			context0 = {'files': qs_json}
			data['html_book_list'] = render_to_string('workflow_files/workflow_names.html', context0, request=request)
			data['page_div'] = render_to_string('files/pagination.html', context0, request=request)
			context = {'files':qs_json}
			data['html_form'] = render_to_string('workflow_selection.html', request=request)
			return JsonResponse(data)

	else:

		context = {'work_flow': work_flow}
		data['html_form'] = render_to_string('workflow_files/confirm_workflow_delete.html',context,request=request)
		return JsonResponse(data)


@login_required(login_url='/login/')
def workflow_selection_for_department(request):
	wf = NewWorkflowManagement.objects.filter(status=True).order_by('-pk')
	qs_json = serializers.serialize('json', wf)
	qs_json = json.loads(qs_json)

	page = request.GET.get('page', 1)
	paginator = Paginator(qs_json, 5)

	count = 0
	for i in qs_json:
		count += 1
		i['fields']['srNo'] = str(count)

	try:
		qs_json = paginator.page(page)

	except PageNotAnInteger:
		qs_json = paginator.page(1)
	except EmptyPage:
		qs_json = paginator.page(paginator.num_pages)
	context2 = {'files': qs_json}
	return render(request, 'all_workflow.html', context2)


def user_directory(wf_name, user, workflow_number):
	date = datetime.now()
	location = os.path.join(settings.FILE_STORAGE, wf_name, str(user), date.strftime('%d-%m-%Y'), workflow_number)
	return location


@login_required(login_url='/login/')
def fill_workflow_form(request, queryset_id):
	queryset = NewWorkflowManagement.objects.get(pk=queryset_id)
	json_data = queryset.wf_definition
	if request.method == 'POST':
		form2 = list(WorkflowFormView(request.POST, request.FILES, queryset_id=queryset_id))
		half_list = len(form2) / 2
		form = WorkflowFormView(request.POST, request.FILES, queryset_id=queryset_id)
		if form.is_valid():
			workflow_no = ''
			try:
				latest_id = UserDataWorkflowManagement.objects.latest('id')
				old_workflow_no = latest_id.workflow_number
				if old_workflow_no:
					letter_WF, new_workflow_no = old_workflow_no.split('F')
					trailing_value = str(int(new_workflow_no) + 1).zfill(10)
					print(trailing_value, 'trailing_value')
					workflow_no = 'WF' + trailing_value

			except UserDataWorkflowManagement.DoesNotExist:
				workflow_no = 'WF0000000001'
			# customer_id = 'ACS_' + str(random.randint(9, 100000))
			user_workflow = UserDataWorkflowManagement(workflow_id=queryset, workflow_number=workflow_no, user=request.user,
													workstep_name='Account Introduction', entry_date_time=datetime.now())
			user_workflow.save()
			wf_id = user_workflow.pk

			json_array = {}
			json_object = []

			for json in json_data:
				if not json['element_type'] == 'FileField':
					key = json['label']
					value = form.cleaned_data[key]
					json_array[json['label']] = value
					print(json_array)

				if json['element_type'] == 'FileField':
					key = json['label']
					file = request.FILES[key]
					location = user_directory(queryset.wf_name, request.user, workflow_no)
					storage = FileSystemStorage(location=location)
					filename =storage.save(file.name, file)
					print(filename)
					date = datetime.now()
					file_url = '/media/Workflow_Management' + '/' + str(queryset.wf_name) + '/' + str(request.user) \
							   + '/' + date.strftime('%d-%m-%Y') + '/' + workflow_no + '/' + filename
					print(file_url)
					json_array[json['label']] = file_url


			json_object.append(json_array)

			account_number = ''
			for object in json_object:
				for i, j in object.items():
					if i == 'Application Form':
						account_number = j

			UserDataWorkflowManagement.objects.filter(pk=wf_id).update(user_data_definition=json_object, status='Pending',
																	   account_number=account_number)

			user_data_wf = UserDataWorkflowManagement.objects.get(pk=wf_id)
			wf_number = user_data_wf.workflow_number

			# user_workflow.save()
			request.session.cycle_key()
			get_maker = User.objects.filter(is_maker=True)
			print('get_maker',get_maker)
			for maker in get_maker:
				p = NotificationForAccount.objects.create(sender=request.user, receiver=maker, type='Pending',
														  unread=True, status=user_workflow)
				p.save()
			messages.success(request, 'Your Form has been submitted successfully.\n Workflow Number: ' + wf_number)
			return HttpResponseRedirect(reverse('WorkflowManagement:fill_workflow_form', args=[queryset_id]))
		else:
			context3 = {'form': form, 'queryset': queryset, 'half_list': half_list}
			return render(request, 'workflow_form_for_user.html', context3)
	else:
		form = list(WorkflowFormView(queryset_id=queryset_id))
		half_list = len(form)/2
		context4 = {'form': form, 'queryset': queryset, 'half_list': half_list}
		return render(request, 'workflow_form_for_user.html', context4)



@csrf_exempt
@login_required(login_url='/login/')
@user_passes_test(lambda u: u.is_maker)
def all_wf_forms(request, queryset_id, status, read=None):
	if read:
		value = get_object_or_404(NotificationForAccount, pk=read)
		value.unread = False
		value.save()
	wf_forms = UserDataWorkflowManagement.objects.filter(workflow_id=queryset_id, status=status).order_by('-pk')

	# Post method for search utility
	if request.method == 'POST':
		requested_number = request.POST.get('search_text')
		try:
			trailing_value = str(int(requested_number)).zfill(10)
			workflow_no = 'WF' + trailing_value
			try:
				wf_forms = UserDataWorkflowManagement.objects.filter(Q(workflow_id=queryset_id, status=status,
																	account_number=requested_number)
																	| Q(workflow_id=queryset_id, status=status,
																	workflow_number=workflow_no))
			except UserDataWorkflowManagement.DoesNotExist:
				print('Data does not exist')
		except ValueError:
			messages.error(request, 'Please enter valid search detail')
			print('Caught ValueError Exception')

	qs_json = serializers.serialize('json', wf_forms)
	qs_json = json.loads(qs_json)

	page = request.GET.get('page', 1)
	paginator = Paginator(qs_json, 6)
	count = 0

	for i in qs_json:
		count += 1
		i['fields']['srNo'] = str(count)
		wf = NewWorkflowManagement.objects.get(pk = i['fields']['workflow_id'])
		wf_name = wf.wf_name
		i['fields']['wf_name'] = wf_name
		if i['fields']['checker']:
			checker = User.objects.get(pk = i['fields']['checker'])
			checker_name = checker.username
			i['fields']['checker'] = checker_name
		else:
			i['fields']['checker'] = ''

	print(qs_json)
	try:
		qs_json = paginator.page(page)

	except PageNotAnInteger:
		qs_json = paginator.page(1)
	except EmptyPage:
		qs_json = paginator.page(paginator.num_pages)
	context5 = {'files': qs_json, 'queryset_id': queryset_id, 'status': status}
	return render(request, 'all_wf_forms.html',context5)


@login_required(login_url='/login/')
@user_passes_test(lambda u: u.is_maker)
def all_workflow_for_maker(request):
	workflow = NewWorkflowManagement.objects.filter(status=True).order_by('-pk')
	qs_json = serializers.serialize('json', workflow)
	qs_json = json.loads(qs_json)

	page = request.GET.get('page', 1)
	paginator = Paginator(qs_json, 5)

	count = 0
	for i in qs_json:
		count += 1
		i['fields']['srNo'] = str(count)

	try:
		qs_json = paginator.page(page)

	except PageNotAnInteger:
		qs_json = paginator.page(1)
	except EmptyPage:
		qs_json = paginator.page(paginator.num_pages)
	context = {'files': qs_json}
	return render(request, 'all_workflow_for_maker.html',context)


@login_required(login_url='/login/')
@user_passes_test(lambda u: u.is_maker)
def all_user_forms_workflow(request, forms_pk, read=None):
	if read:
		value = get_object_or_404(NotificationForAccount, pk=read)
		value.unread = False
		value.save()

	user_data = UserDataWorkflowManagement.objects.get(pk=forms_pk)
	if user_data.f_entry == "Y":
		json_data = user_data.new_user_data_definition
	else:
		json_data = user_data.user_data_definition
	documents_available = user_data.documents

	if not os.path.isdir(settings.MEDIA_ROOT + '/' + 'Scanned_images_temp'):
		print('path not exists')
		try:
			os.makedirs(settings.MEDIA_ROOT + '/' + 'Scanned_images_temp')
		except OSError as exc:  # Guard against race condition
			if exc.errno != errno.EEXIST:
				raise

	path_for_jpeg = os.path.join(settings.MEDIA_ROOT + '/' + 'Scanned_images_temp')
	import shutil
	for the_file in os.listdir(path_for_jpeg):
		file_path = os.path.join(path_for_jpeg, the_file)
		try:
			if os.path.isfile(file_path):
				os.unlink(file_path)
			elif os.path.isdir(file_path):
				shutil.rmtree(file_path)

		except Exception as e:
			print(e)

	onlyfiles = []
	if documents_available:
		try:
			tiff_data = ScannedImagesUserData.objects.get(workflow_number=user_data)
			print('tiff_data', tiff_data)

			tiff_json_data = tiff_data.user_scanned_data
			is_encrypt = tiff_data.is_encrypted
			for tag in tiff_json_data:
				for i, j in tag.items():
					print('j', j)
					path_j = os.path.join(settings.BASE_DIR + '/DMSapp' + j)
					print('encrypted file path', path_j)
					if is_encrypt:
						# Decrypting the file to open it in canvas
						file2 = open(path_j, 'rb')
						b2 = bytes(file2.read())
						decoded = Cryptographer.decrypted(b2)  # Cryptographer used to encrypt and decrypt the files
						decoded_file = ContentFile(decoded)

						default_storage.save(os.path.join(path_for_jpeg + '/' + i + '.tiff'), decoded_file)
						print('File is decrypted')
					else:
						shutil.copy2(os.path.join(settings.BASE_DIR + '/DMSapp' + j), os.path.join(path_for_jpeg + '/' +  i + '.tiff'))

			if os.path.exists(path_for_jpeg):
				onlyfiles = [f for f in os.listdir(path_for_jpeg) if os.path.isfile(os.path.join(path_for_jpeg, f))]
				print("Files are:", onlyfiles)
				
				#Checking if the files are in tiff format or not
				for i in onlyfiles:
					img = Image.open(os.path.join(path_for_jpeg + '/' + i))
					if img.format == "TIFF":
						pass
					else:
						print("Changing the converted file into tiff")
						im2 = img.convert('1')
						im2.save(os.path.join(path_for_jpeg + '/'+i), 'TIFF')

		except ScannedImagesUserData.DoesNotExist:
			print('No Such Data in ScannedImagesUserData')

	# Get image data from database after scanning
	image_list = []
	print('image_list', onlyfiles)
	len_of_file = len(onlyfiles)
	for i in range(len_of_file):
		print(i)
		image_json_array = {onlyfiles[i]: '/media/Scanned_images_temp/' + onlyfiles[i]}
		image_list.append(image_json_array)

	print('image_list', image_list)

	context1 = {'json_data': json_data, 'user_data': user_data, 'image': image_list,'documents_available': documents_available}
	return render(request, 'all_user_forms_workflow.html',context1)


@login_required(login_url='/login/')
@user_passes_test(lambda u: u.is_maker)
def edit_workflow_maker(request, user_data_pk):
	user_data = UserDataWorkflowManagement.objects.get(pk=user_data_pk)
	if user_data.f_entry == "Y":
		print('here in Y')
		json_data = user_data.new_user_data_definition
	else:
		json_data = user_data.user_data_definition
	for p in json_data:
		for l, b in p.copy().items():
			if '/media/Workflow_Management/' in b:
				p.pop(l, None)

	json2 = {}
	for i in json_data:
		json2 = i

	print('json2', json2)
	for i, j in json2.items():
		if '[' and ']' in j:
			import ast
			list1 = ast.literal_eval(j)
			json2[i] = list1

	formset = MakerWorkflowFormView(queryset_id=user_data_pk, data=json2)
	formset2 = MakerWorkflowFormView2(queryset_id=user_data_pk, data=json2)
	if request.method == 'POST':
		formset = MakerWorkflowFormView(request.POST, request.FILES, queryset_id=user_data_pk)
		formset2 = MakerWorkflowFormView(request.POST, request.FILES, queryset_id=user_data_pk)
		if formset.is_valid():
			json_array = {}
			json_object = []
			for key in json2:
				print(key)
				value = formset.cleaned_data[key]
				json_array[key] = value
			json_object.append(json_array)

			fr_user_data = UserDataWorkflowManagement.objects.get(pk=user_data_pk)
			if fr_user_data.f_entry == "Y":
				json_data2 = fr_user_data.new_user_data_definition
			else:
				json_data2 = fr_user_data.user_data_definition
			for p in json_data2:
				for i, j in p.items():
					old_key = i
					old_value = j
					for t in json_object:
						for h, k in t.items():
							if old_key == h:
								if old_value != k:
									p[i] = k

			if fr_user_data.f_entry == "Y":
				UserDataWorkflowManagement.objects.filter(pk=user_data_pk).update(new_user_data_definition=json_data2)
			else:
				UserDataWorkflowManagement.objects.filter(pk=user_data_pk).update(user_data_definition=json_data2)
			request.session.cycle_key()
			messages.success(request, 'Changes has been successfully Updated')
			return HttpResponseRedirect(reverse('WorkflowManagement:edit_workflow_maker', args=[user_data_pk]))
		else:
			user_data = UserDataWorkflowManagement.objects.get(pk=user_data_pk)
			if user_data.f_entry == "Y":
				json_data = user_data.new_user_data_definition
				wf_json = user_data.workflow_id.new_wf_definition
			else:
				json_data = user_data.user_data_definition
				wf_json = user_data.workflow_id.wf_definition
			# wf_id = user_data.workflow_id.id
			# workflow_json = NewWorkflowManagement.objects.get(pk=wf_id)

			documents_available = user_data.documents
			imgs = []
			queryset1 = []
			for j in json_data:
				for k, v in j.items():
					for w in wf_json:
						if w['label'] == k:
							queryset1.append({str(k): w['required']})
					if '/media/Workflow_Management/' in v:
						imgs.append({k: v})

			onlyfiles = []
			path_for_jpeg = os.path.join(settings.MEDIA_ROOT + '/' + 'Scanned_images_temp')

			import shutil
			for the_file in os.listdir(path_for_jpeg):
				file_path = os.path.join(path_for_jpeg, the_file)
				try:
					if os.path.isfile(file_path):
						os.unlink(file_path)
					elif os.path.isdir(file_path):
						shutil.rmtree(file_path)

				except Exception as e:
					print(e)

			if documents_available:
				try:
					tiff_data = ScannedImagesUserData.objects.get(workflow_number=user_data)
					print('tiff_data', tiff_data)

					tiff_json_data = tiff_data.user_scanned_data
					is_encrypt = tiff_data.is_encrypted
					for tag in tiff_json_data:
						for i, j in tag.items():
							print('j', j)
							path_j = os.path.join(settings.BASE_DIR + '/DMSapp' + j)
							print('encrypted file path', path_j)
							if is_encrypt:
								# Decrypting the file to open it in canvas
								file2 = open(path_j, 'rb')
								b2 = bytes(file2.read())
								decoded = Cryptographer.decrypted(b2)  # Cryptographer used to encrypt and decrypt the files
								decoded_file = ContentFile(decoded)

								default_storage.save(os.path.join(path_for_jpeg + '/' + i + '.tiff'), decoded_file)
								print('File is decrypted')
							else:
								shutil.copy2(os.path.join(settings.BASE_DIR + '/DMSapp' + j),
											 os.path.join(path_for_jpeg + '/' +  i + '.tiff'))

					if os.path.exists(path_for_jpeg):
						onlyfiles = [f for f in os.listdir(path_for_jpeg) if os.path.isfile(os.path.join(path_for_jpeg, f))]
						print("Files are:", onlyfiles)
						
						#Checking if the files are in tiff format or not
						for i in onlyfiles:
							img = Image.open(os.path.join(path_for_jpeg + '/' + i))
							if img.format == "TIFF":
								pass
							else:
								print("Changing the converted file into tiff")
								im2 = img.convert('1')
								im2.save(os.path.join(path_for_jpeg + '/'+i), 'TIFF')

				except ScannedImagesUserData.DoesNotExist:
					print('No Such Data in ScannedImagesUserData')
			# Get image data to show in edit workflow
			# temp_list = []
			# path_for_png = os.path.join(settings.MEDIA_ROOT + '/' + 'Scanned_images_temp')
			# for file in os.listdir(path_for_png):
			# 	if file.endswith(".png"):
			# 		print(file)
			# 		temp_list.append(file)

			image_list2 = []
			print('image_list', onlyfiles)
			len_of_file = len(onlyfiles)
			for i in range(len_of_file):
				print(i)
				image_json_array = {onlyfiles[i]: '/media/Scanned_images_temp/' + onlyfiles[i]}
				image_list2.append(image_json_array)

			print(image_list2)

			context4 =  {'formset': formset, 'user_data': user_data, 'imgs': image_list2,'queryset':queryset1,'documents_available': documents_available}
			return render(request, 'edit_workflow_maker.html',context4)
	elif request.is_ajax():
		print('In ajax call')

		formset = MakerWorkflowFormView(queryset_id=user_data_pk, data=json2)

		user_data = UserDataWorkflowManagement.objects.get(pk=user_data_pk)
		if user_data.f_entry == "Y":
			json_data = user_data.new_user_data_definition
		else:
			json_data = user_data.user_data_definition
		wf_id = user_data.workflow_id.id
		f_entry_status = user_data.f_entry
		q = NewWorkflowManagement.objects.get(pk=wf_id)

		jsonData = []
		if q:
			if f_entry_status == "Y":
				definition = q.new_wf_definition
			else:
				definition = q.wf_definition
			print('definition', definition)
			for d in definition:
				if d['label'] == 'Customer Name':
					data = {'label': "Branch Customer Name", 'element_type': d["element_type"],
							'validation': d["validation"], 'required': d["required"]}

					data2 = {'label': "Finacle Customer Name", 'element_type': d["element_type"],
							'validation': d["validation"], 'required': d["required"]}
					jsonData.append(data)
					jsonData.append(data2)
				else:
					jsonData.append(d)

		workflow_json = NewWorkflowManagement.objects.get(pk=wf_id)
		# jsonData.append(data)
		workflow_json.new_wf_definition = jsonData
		workflow_json.save()

		user_data2 = UserDataWorkflowManagement.objects.get(pk=user_data_pk)
		if user_data2.f_entry == "Y":
			json_data2 = user_data2.new_user_data_definition
		else:
			json_data2 = user_data2.user_data_definition
		jsonData2 = []
		jsonDict2 = {}
		for t in json_data2:
			for i, j in t.items():
				if i == 'Customer Name':
					jsonDict2.update({"Branch Customer Name": j})
					jsonDict2.update({"Finacle Customer Name": fetch_data['Customer Name']})
				else:
					jsonDict2.update({i: j})

		# Check if Branch Customer Name and Finacle Customer Name are same
		data = dict()
		try:
			if jsonDict2["Branch Customer Name"] == jsonDict2["Finacle Customer Name"]:
				data["Matched"] = "Matched"
			else:
				data["Matched"] = "Not Matched"
		except:
			print("No Such keys like Branch Customer Name or Finacle Customer Name")

		jsonData2.append(jsonDict2)

		user_data2.new_user_data_definition = jsonData2
		user_data2.f_entry = "Y"

		user_data2.save()

		workflow_json3 = NewWorkflowManagement.objects.get(pk=wf_id)
		if f_entry_status == "Y":
			wf_json = workflow_json3.new_wf_definition
		else:
			wf_json = workflow_json3.wf_definition
		documents_available = user_data2.documents

		imgs = []
		queryset1 = []
		for j in json_data:
			for k, v in j.items():
				for w in wf_json:
					if w['label'] == k:
						queryset1.append({str(k):w['required']})
				if '/media/Workflow_Management/' in v:
					imgs.append({k: v})

		# Get image data to show in edit workflow
		temp_list = []
		path_for_png = os.path.join(settings.MEDIA_ROOT + '/' + 'Scanned_images_temp')
		for file in os.listdir(path_for_png):
			if file.endswith(".tiff"):
				print(file)
				temp_list.append(file)

		image_list2 = []
		print('image_list', temp_list)
		len_of_file = len(temp_list)
		for i in range(len_of_file):
			print(i)
			image_json_array = {temp_list[i]: '/media/Scanned_images_temp/' + temp_list[i]}
			image_list2.append(image_json_array)

		context89 = {'formset': formset, 'user_data': user_data,'formset2': formset2, 'imgs': image_list2,
					  'queryset':queryset1,'documents_available': documents_available}
		data['html_form'] = render_to_string('workflow_files/include_edit_workflow_maker.html', context89, request=request)
		return JsonResponse(data)
		# return render(request, 'edit_workflow_maker.html', context89)
	else:
		print("In else statement")
		user_data = UserDataWorkflowManagement.objects.get(pk=user_data_pk)
		if user_data.f_entry == "Y":
			json_data = user_data.new_user_data_definition
			wf_json = user_data.workflow_id.new_wf_definition
		else:
			json_data = user_data.user_data_definition
			wf_json = user_data.workflow_id.wf_definition
		# wf_id = user_data.workflow_id.id
		# workflow_json = NewWorkflowManagement.objects.get(pk = wf_id)

		documents_available = user_data.documents
		imgs = []
		queryset1 = []
		for j in json_data:
			for k, v in j.items():
				for w in wf_json:
					if w['label'] == k:
						queryset1.append({str(k):w['required']})
				if '/media/Workflow_Management/' in v:
					imgs.append({k: v})

		onlyfiles = []
		path_for_jpeg = os.path.join(settings.MEDIA_ROOT + '/' + 'Scanned_images_temp')
		import shutil
		for the_file in os.listdir(path_for_jpeg):
			file_path = os.path.join(path_for_jpeg, the_file)
			try:
				if os.path.isfile(file_path):
					os.unlink(file_path)
				elif os.path.isdir(file_path):
					shutil.rmtree(file_path)
			except Exception as e:
				print(e)
		if documents_available:
			try:
				tiff_data = ScannedImagesUserData.objects.get(workflow_number=user_data)
				tiff_json_data = tiff_data.user_scanned_data
				is_encrypt = tiff_data.is_encrypted
				for tag in tiff_json_data:
					for i, j in tag.items():
						path_j = os.path.join(settings.BASE_DIR + '/DMSapp' + j)
						if is_encrypt:
							# Decrypting the file to open it in canvas
							file2 = open(path_j, 'rb')
							b2 = bytes(file2.read())
							decoded = Cryptographer.decrypted(b2)  # Cryptographer used to encrypt and decrypt the files
							decoded_file = ContentFile(decoded)
							default_storage.save(os.path.join(path_for_jpeg + '/' + i + '.tiff'), decoded_file)
						else:
							shutil.copy2(os.path.join(settings.BASE_DIR + '/DMSapp' + j),
										 os.path.join(path_for_jpeg + '/' + i + '.tiff'))
				if os.path.exists(path_for_jpeg):
					onlyfiles = [f for f in os.listdir(path_for_jpeg) if os.path.isfile(os.path.join(path_for_jpeg, f))]
					# Checking if the files are in tiff format or not
					for i in onlyfiles:
						img = Image.open(os.path.join(path_for_jpeg + '/' + i))
						if img.format == "TIFF":
							pass
						else:
							im2 = img.convert('1')
							im2.save(os.path.join(path_for_jpeg + '/' + i), 'TIFF')
			except ScannedImagesUserData.DoesNotExist:
				print('No Such Data in ScannedImagesUserData')
		image_list2 = []
		len_of_file = len(onlyfiles)
		for i in range(len_of_file):
			image_json_array = {onlyfiles[i]: '/media/Scanned_images_temp/' + onlyfiles[i]}
			image_list2.append(image_json_array)

		context88 = {'formset': formset, 'user_data': user_data,'formset2': formset2, 'imgs': image_list2,'queryset':queryset1,'documents_available': documents_available}
		return render(request, 'edit_workflow_maker.html',context88)


def confirm_finacle_entry(request, user_data_pk):
	user_data = UserDataWorkflowManagement.objects.get(pk=user_data_pk)
	user_data.f_entry = "N"
	user_data.save()
	return JsonResponse({'success': True})


@login_required(login_url='/login/')
def submit_to_checker(request, user_data_pk):
	UserDataWorkflowManagement.objects.filter(pk=user_data_pk).update(status='Drafted', maker=request.user)
	workflow_id = UserDataWorkflowManagement.objects.get(pk=user_data_pk)
	UserDataWorkflowManagement.objects.filter(pk=user_data_pk).update(workstep_name='AOC Acceptance',
																	  previous_workstep='Finacle Data Entry',
																	  modified_date_time=datetime.now())
	get_checker = User.objects.filter(is_checker=True)
	for checker in get_checker:
		p = NotificationForAccount.objects.create(sender=request.user, receiver=checker, type='Drafted',
												  unread=True, status=workflow_id)
		p.save()
	messages.success(request, 'Request has been submitted for approval')
	return HttpResponseRedirect(reverse('WorkflowManagement:all_wf_forms', args=[workflow_id.workflow_id_id, 'Drafted']))


@login_required(login_url='/login/')
@user_passes_test(lambda u: u.is_checker)
def all_workflow_for_checker(request):
	wf_forms = NewWorkflowManagement.objects.filter(status=True).order_by('-pk')
	qs_json = serializers.serialize('json', wf_forms)
	qs_json = json.loads(qs_json)

	page = request.GET.get('page', 1)
	paginator = Paginator(qs_json, 5)

	count = 0
	for i in qs_json:
		count += 1
		i['fields']['srNo'] = str(count)

	try:
		qs_json = paginator.page(page)

	except PageNotAnInteger:
		qs_json = paginator.page(1)
	except EmptyPage:
		qs_json = paginator.page(paginator.num_pages)

	context = {'files':qs_json}
	return render(request, 'all_workflow_for_checker.html', context)


@login_required(login_url='/login/')
@user_passes_test(lambda u: u.is_checker)
def all_wf_for_approval(request, wf_id):
	customer_data = UserDataWorkflowManagement.objects.filter(workflow_id=wf_id, status='Drafted').order_by('-pk')
	qs_json = serializers.serialize('json', customer_data)
	qs_json = json.loads(qs_json)

	page = request.GET.get('page', 1)
	paginator = Paginator(qs_json, 6)
	count = 0

	for i in qs_json:
		count += 1
		i['fields']['srNo'] = str(count)
		wf = NewWorkflowManagement.objects.get(pk=i['fields']['workflow_id'])
		wf_name = wf.wf_name
		i['fields']['wf_name'] = wf_name
		if i['fields']['maker']:
			maker = User.objects.get(pk=i['fields']['maker'])
			maker_name = maker.username
			i['fields']['maker'] = maker_name
		else:
			i['fields']['maker'] = ''

	try:
		qs_json = paginator.page(page)

	except PageNotAnInteger:
		qs_json = paginator.page(1)
	except EmptyPage:
		qs_json = paginator.page(paginator.num_pages)
	context = {'files': qs_json}
	return render(request, 'all_wf_for_approval.html', context)


@login_required(login_url='/login/')
@user_passes_test(lambda u: u.is_checker)
def customer_data_for_approval(request, data_id, read=None):
	if read:
		value = get_object_or_404(NotificationForAccount, pk=read)
		value.unread = False
		value.save()
	user_data = UserDataWorkflowManagement.objects.get(pk=data_id)
	if user_data.f_entry == "Y":
		json_data = user_data.new_user_data_definition
	else:
		json_data = user_data.user_data_definition

	documents_available = user_data.documents

	path_for_jpeg = os.path.join(settings.MEDIA_ROOT + '/' + 'Scanned_images_temp')
	import shutil
	for the_file in os.listdir(path_for_jpeg):
		file_path = os.path.join(path_for_jpeg, the_file)
		try:
			if os.path.isfile(file_path):
				os.unlink(file_path)
			elif os.path.isdir(file_path):
				shutil.rmtree(file_path)

		except Exception as e:
			print(e)

	onlyfiles = []
	if documents_available:
		try:
			tiff_data = ScannedImagesUserData.objects.get(workflow_number=user_data)
			print('tiff_data', tiff_data)

			tiff_json_data = tiff_data.user_scanned_data
			is_encrypt = tiff_data.is_encrypted
			for tag in tiff_json_data:
				for i, j in tag.items():
					print('j', j)
					path_j = os.path.join(settings.BASE_DIR + '/DMSapp' + j)
					print('encrypted file path', path_j)
					if is_encrypt:
						# Decrypting the file to open it in canvas
						file2 = open(path_j, 'rb')
						b2 = bytes(file2.read())
						decoded = Cryptographer.decrypted(b2)  # Cryptographer used to encrypt and decrypt the files
						decoded_file = ContentFile(decoded)

						default_storage.save(os.path.join(path_for_jpeg + '/' + i + '.tiff'), decoded_file)
						print('File is decrypted')
					else:
						shutil.copy2(os.path.join(settings.BASE_DIR + '/DMSapp' + j),
									 os.path.join(path_for_jpeg + '/' +  i + '.tiff'))

			if os.path.exists(path_for_jpeg):
				onlyfiles = [f for f in os.listdir(path_for_jpeg) if os.path.isfile(os.path.join(path_for_jpeg, f))]
				print("Files are:", onlyfiles)
				
				#Checking if the files are in tiff format or not
				for i in onlyfiles:
					img = Image.open(os.path.join(path_for_jpeg + '/' + i))
					if img.format == "TIFF":
						pass
					else:
						print("Changing the converted file into tiff")
						im2 = img.convert('1')
						im2.save(os.path.join(path_for_jpeg + '/'+i), 'TIFF')

		except ScannedImagesUserData.DoesNotExist:
			print('No Such Data in ScannedImagesUserData')

	image_list2 = []
	print('image_list', onlyfiles)
	len_of_file = len(onlyfiles)
	for i in range(len_of_file):
		print(i)
		image_json_array = {onlyfiles[i]: '/media/Scanned_images_temp/' + onlyfiles[i]}
		image_list2.append(image_json_array)

	print(image_list2)

	context = {'json_data': json_data, 'data': user_data,'image': image_list2,'documents_available': documents_available}
	return render(request, 'customer_data_for_approval.html',context)


@login_required(login_url='/login/')
@user_passes_test(lambda u: u.is_checker)
@csrf_exempt
def checker_for_approval(request, data_id):
	data = dict()
	task_id = UserDataWorkflowManagement.objects.get(id=data_id)
	form = UserDataWorkflowManagementForm(request.POST, instance=task_id)
	if request.method == 'POST':
		if form.is_valid():
			form2 = form.save(commit=False)
			# status = form.cleaned_data['status']
			status = request.POST.get("TITLE")
			if status == "Approve":
				form2.status = "Approved"
				status2 = "Approved"

				# Changes for New Workflow Process
				task_id.workstep_name = 'Work Exit'
				task_id.previous_workstep = 'AOC Acceptance'
				task_id.modified_date_time = datetime.now()
				task_id.save()
			elif status == "Reject":
				form2.status = "Rejected"
				status2 = "Rejected"

				# Changes for New Workflow Process
				task_id.workstep_name = 'Finacle Data Entry'
				task_id.previous_workstep = 'AOC Acceptance'
				task_id.modified_date_time = datetime.now()
				task_id.save()

			form2.checker = request.user
			form2.save()
			get_maker = User.objects.filter(is_maker=True)
			for maker in get_maker:
				p = NotificationForAccount.objects.create(sender=request.user, receiver=maker, type=status2,
														  unread=True, status=task_id)
				p.save()
			messages.success(request, 'Request has been processed further')
			return HttpResponseRedirect(reverse('WorkflowManagement:all_wf_for_approval',  args=[task_id.workflow_id.id]))
	else:
		form = UserDataWorkflowManagementForm()
		title = request.GET.get("title")
		context = {'task': task_id, 'form': form,"title":title}
		data['html_form'] = render_to_string('workflow_files/pending_tasks_for_checker.html', context, request=request)
		return JsonResponse(data)


@login_required(login_url='/login/')
def all_rejected_forms(request, form_id, read=None):
	if read:
		value = get_object_or_404(NotificationForAccount, pk=read)
		value.unread = False
		value.save()
	print(form_id)
	data = UserDataWorkflowManagement.objects.get(pk=form_id)
	if data.f_entry == "Y":
		json_data = data.new_user_data_definition
	else:
		json_data = data.user_data_definition
	print('asdfasdfasfd', json_data)
	context4 = {'json_data': json_data, 'data':data}
	return render(request, 'workflow_files/rejected_forms.html', context4)


@login_required(login_url='/login/')
def delete_form(request, form_id):
	user_form = get_object_or_404(UserDataWorkflowManagement, pk=form_id)
	data = dict()
	wf_id=user_form.workflow_id_id
	print(wf_id,'wf_id')
	if request.GET.get('delete'):
		user_form.delete()

		data['form_is_valid'] = True
		data['wf_id'] = wf_id
		messages.success(request, 'UserForm deleted Successfully')
		return JsonResponse(data)


	else:
		context = {'user_form': user_form}
		data['html_form'] = render_to_string('workflow_files/delete_user_form.html', context, request=request)
		return JsonResponse(data)

def validate_pan_number(request):
	if request.GET.get('pan'):
		pan = request.GET.get('pan')
		if re.match(r'^[A-Z]{5}[0-9]{4}[A-Z]$', pan):
			data = {'invalid': False}
		else:
			data = {'invalid': True}
		return JsonResponse(data)

def validate_aadhar_number(request):

	if request.GET.get('aadhar'):
		aadhar = request.GET.get('aadhar')
		if re.match(r'^\d{12}$', aadhar):
			data = {'invalid': False}
		else:
			data = {'invalid': True}
		print(data)
		return JsonResponse(data)


def validateOnlyAlphabets(request):

	if request.GET.get('value'):
		value = request.GET.get('value')
		if re.match(r'^[a-zA-Z]*$', value):
			data = {'invalid': False}
		else:
			data = {'invalid': True}

		return JsonResponse(data)

def validateAlphaNumeric(request):

	if request.GET.get('value'):
		value = request.GET.get('value')
		if re.match(r'^[0-9a-zA-Z]*$', value):
			data = {'invalid': False}
		else:
			data = {'invalid': True}

		return JsonResponse(data)


def validateEmail(request):
	if request.GET.get('email'):
		email = request.GET.get('email')
		match = re.match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$', email)
		if match == None:
			data = {'invalid': True}
		else:
			data = {'invalid': False}

		print(data)
		return JsonResponse(data)


def image_ocr(request):
	if request.method == 'POST':
		data = {}
		imgUrl = request.POST['imgUrl'];
		lang= request.POST['lang']
		print('lang', lang)
		imgUrl= settings.MEDIA_ROOT + imgUrl[imgUrl.find('media')+5:]
		txt = ocr_core(imgUrl,lang).splitlines()
		print(txt)
		txt = list(filter(str.strip, txt))

		data = {1:txt}
		print(data)
		return JsonResponse(data)

	else:
		return HttpResponse('post did not reach')


def scan_documents_sanelib(request):
	import sane
	if request.POST:
		device = request.POST['device']
		device = device.split("'")
		device = device[1]
		print('device', device)
		context = {'success': True,'device_name': device}
		return render(request, 'scan_documents_sanelib.html', context)

	ver = sane.init()
	devices = sane.get_devices()
	context1 = {'devices': devices}
	return render(request, 'scan_documents_sanelib.html', context1)


def document_scan_parameter(request):
	import sane
	device = ""
	if request.POST:
		source = request.POST['source']
		mode = request.POST['mode']
		device = request.POST['device']
		print(source, device, mode)
		depth = 8
		dev = sane.open(device)

		params = dev.get_parameters()
		dev.depth = depth
		dev.mode = mode
		# dev.br_x = 500
		# dev.br_y = 400
		params = dev.get_parameters()
		print('Device parameters:', params)

		# if not os.path.isdir(settings.MEDIA_ROOT + '/' + 'Scanned_images' + '/' + cust_number):
		# 	print('path not exists')
		# 	try:
		# 		os.makedirs(settings.MEDIA_ROOT + '/' + 'Scanned_images' + '/' + cust_number)
		# 		path = os.path.join(settings.MEDIA_ROOT + '/' + 'Scanned_images' + '/' + cust_number)
		# 		print('path created')
		if not os.path.isdir(settings.MEDIA_ROOT + '/' + 'Scanned_images_temp'):
			print('path not exists')
			try:
				os.makedirs(settings.MEDIA_ROOT + '/' + 'Scanned_images_temp')
			except OSError as exc: # Guard against race condition
				if exc.errno != errno.EEXIST:
					raise

		path = os.path.join(settings.MEDIA_ROOT + '/' + 'Scanned_images_temp')
		import shutil
		for the_file in os.listdir(path):
			file_path = os.path.join(path, the_file)
			try:
				if os.path.isfile(file_path):
					os.unlink(file_path)
				elif os.path.isdir(file_path):
					shutil.rmtree(file_path)

			except Exception as e:
				print(e)

		json_data_table =  []
		json_data = ''
		if source == 'ADF':
			dev.source = source
			print('source selected', source)
			name_of_file = []
			for i, pageim in enumerate(dev.multi_scan()):
				print('pageim', pageim)
				pageim.save(os.path.join(path, 'A_' + str(i) + '.jpeg'), subsampling=0, quality=100)
				name_of_file.append('A_' + str(i) + '.jpeg')

				# Making json_data for appending data in Table
				json_data = {'A_' + str(i) : '/media/Scanned_images_temp/' + 'A_' + str(i) + '.jpeg'}

				json_data_table.append(json_data)

			# Deleting all previous records and creating a new one.
			# ModelForTaggingFiles.objects.all().delete()
			# ModelForTaggingFiles.objects.create(customer_data_field=json_data_table)

			# from PIL import Image
			# from PIL import TiffImagePlugin
			#
			# with TiffImagePlugin.AppendingTiffWriter(path + '/' + 'NEW.tiff',True) as tf:
			# 	for tiff_in in name_of_file:
			# 		try:
			# 			im= Image.open(path +'/'+ tiff_in)
			# 			im.save(tf)
			# 			tf.newFrame()
			# 			im.close()
			# 			print('converted all tiff files')
			# 		except FileDoesNotExist:
			# 			print('Error and Error')

		elif source == 'Flatbed':
			dev.start()
			im = dev.snap()
			im.save(os.path.join(path, 'test_scanner.jpeg'), subsampling=0, quality=100)

			# except OSError as exc: # Guard against race condition
			# 	if exc.errno != errno.EEXIST:
			# 		raise

		dev.close()

		#Cropping Images for Photo and Signature
		if json_data_table:

			im = Image.open(os.path.join(settings.BASE_DIR + '/DMSapp/media/Scanned_images_temp/A_0.jpeg'))

			# Area for Photo
			left = 500
			top = 65
			right = 650
			bottom = 230
			im1 = im.crop((left, top, right, bottom))
			im1.save(os.path.join(path + '/A_P.jpeg'), subsampling=0, quality=100)
			json_data_table.append({'A_P': '/media/Scanned_images_temp/A_P.jpeg'})

			# Area for Signature
			left1 = 500
			top1 = 700
			right1 = 800
			bottom1 = 800
			im2 = im.crop((left1, top1, right1, bottom1))
			im2.save(os.path.join(path + '/A_S.jpeg'), subsampling=0, quality=100)
			json_data_table.append({'A_S': '/media/Scanned_images_temp/A_S.jpeg'})

		# Tagging Image Data with Customer Forms
		customer_data_json = []
		cust_data = ''
		if json_data_table:
			for j_data in json_data_table:
				for i, j in j_data.items():
					if i == 'A_0':
						cust_data = {'Application Form': j}
					elif i == 'A_1':
						cust_data = {'Application Form': j}
					elif i == 'A_2':
						cust_data = {'Application Form': j}
					elif i == 'A_3':
						cust_data = {'Aadhar Card': j}
					elif i == 'A_4':
						cust_data = {'Pan Card': j}
					elif i == 'A_5':
						cust_data = {'Driving License': j}
					elif i == 'A_S':
						cust_data = {'Signature': j}
					elif i == 'A_P':
						cust_data = {'Photo': j}

					customer_data_json.append(cust_data)

		ModelForCustomerData.objects.all().delete()
		ModelForCustomerData.objects.create(customer_data=customer_data_json)
		messages.success(request, 'Scan Completed successfully')

	context3 = {'device_name': device, 'success': True, 'view_data': True}
	return render(request, 'scan_documents_sanelib.html',context3)


def scanned_images(request):
	model_data = ModelForCustomerData.objects.all().order_by('id')[0]
	json_data = model_data.customer_data
	print(json_data)

	image_list = []
	for img in json_data:
		for i, j in img.items():
			image_list.append(j)
	print(image_list)
	list_count = len(image_list)

	form = SelectTaggingInfo()
	if request.method == 'POST':
		form = SelectTaggingInfo(request.POST, request.FILES)
		account_data = request.POST.get('account_number_text')
		context = {'image': json_data, 'list_count': list_count, 'form': form}
		return render(request, 'workflow_files/scanned_images.html',context)

	context1 = {'image': json_data, 'list_count': list_count, 'form': form}
	return render(request, 'workflow_files/scanned_images.html', context1)


def scanned_user_data(request):
	if request.method == 'POST':
		customer_data = request.POST.get('account_number_text')
		print(customer_data)
		UserDataForScannedImages.objects.create(account_number=customer_data)
		return JsonResponse({'success': True, 'data': 'Submitted'})
	return JsonResponse({'success': False})


@csrf_exempt
def submit_tag_option(request):
	if request.method == 'POST':
		account_data = request.POST['account_data']
		data_values = request.POST['json_data_values']

		# Create the field with account number
		user_data = UserDataForScannedImages.objects.filter(account_number=account_data)
		if user_data:
			pass
		else:
			UserDataForScannedImages.objects.create(account_number=account_data)

		# Loading data in json and tagging them in table with same account number
		json_data_values = json.loads(data_values)
		account_data_info = []

		for data in json_data_values:
			for k, l in data.items():
				filename = l.split('/')[-1]
				print(filename)
				source_path = os.path.join(settings.BASE_DIR + '/DMSapp' + l)
				destination_dir = os.path.join(settings.BASE_DIR + '/DMSapp/media/WorkflowData/' + account_data)
				destination_path = os.path.join(settings.BASE_DIR + '/DMSapp/media/WorkflowData/' + account_data + '/' +filename)
				print(source_path, destination_path)
				if not os.path.isdir(destination_dir):
					print('inside')
					try:
						os.makedirs(destination_dir)
					except OSError as exc:  # Guard against race condition
						if exc.errno != errno.EEXIST:
							raise
				import shutil
				print('moving')
				shutil.copy2(source_path, destination_path)
				file_path = os.path.join('/media/WorkflowData/' + account_data + '/' + filename)
				account_info = {k: file_path}

				account_data_info.append(account_info)

				# Compressing files in bz2 format
				import bz2

				filename_tarbz2 = filename.split('.')[0]
				print(filename_tarbz2)

				with open(destination_path, 'rb') as data:
					tarbz2contents = bz2.compress(data.read(), 8)

				destination_path_tarbz2 = os.path.join(settings.BASE_DIR + '/DMSapp/media/WorkflowData/' + account_data + '/' + filename_tarbz2 + '.bz2')
				fh = open(destination_path_tarbz2, "wb")
				fh.write(tarbz2contents)
				fh.close()

				# Deleting non-compressed file
				os.remove(destination_path)

		user_data = UserDataForScannedImages.objects.filter(account_number=account_data)
		if user_data:
			user_data.update(tagging_format=account_data_info, status=True)
			return JsonResponse({'account': 'Exist'})
		else:
			return JsonResponse({'account': 'Not Exist'})
	return JsonResponse({'success': False})


@csrf_exempt
def crop_popup(request):
	path = request.GET.get('path')
	image_loaded = request.GET.get('image_loaded')
	context = {'img': path,'image_loaded': image_loaded}
	return render(request, 'workflow_files/crop_popup.html', context)


def decoder_for_base64_file(data, filename):
	import six
	import base64

	if isinstance(data, six.string_types):
		try:
			decoded_file = base64.b64decode(data)
		except TypeError:
			TypeError('invalid_image')

		# Generate file name:
		file_name = filename
		complete_file_name = "%s.%s" % (file_name, "jpeg", )
		raw_content =  ContentFile(decoded_file, name=file_name)
		return raw_content


@csrf_exempt
def submit_cropped_image(request):
	if request.method == 'POST':
		image_data = request.POST['image_data']
		image_loaded = request.POST.get('image_loaded')

		filename = image_loaded.split('/')[-1]
		image_data = image_data.replace('data:image/png;base64,','')

		file_content = decoder_for_base64_file(image_data, filename)
		path = os.path.join(settings.MEDIA_ROOT + '/Scanned_images_temp/test.png')
		default_storage.save(path, file_content)

		# Converting the .png to .jpeg
		im = Image.open(settings.MEDIA_ROOT + '/Scanned_images_temp/test.png')
		rgb_im = im.convert('RGB')
		rgb_im.save(settings.BASE_DIR + '/DMSapp' + image_loaded, subsampling=0, quality=100)

		# Remove the testing file
		os.remove(settings.MEDIA_ROOT + '/Scanned_images_temp/test.png')
		return JsonResponse({'success': True})

	else:
		return HttpResponse('post did not reach')


@csrf_exempt
def save_rotated_image(request):
	canvas_data = request.POST.get('canvas_data')
	image_loaded = request.POST.get('image_loaded')
	print(image_loaded)

	filename = image_loaded.split('/')[-1]

	image_data = canvas_data.replace('data:image/png;base64,', '')

	file_content = decoder_for_base64_file(image_data, filename)
	path = os.path.join(settings.MEDIA_ROOT + '/Scanned_images_temp/test.png')
	default_storage.save(path, file_content)

	# Converting the .png to .jpeg
	im = Image.open(settings.MEDIA_ROOT + '/Scanned_images_temp/test.png')
	rgb_im = im.convert('RGB')
	rgb_im.save(settings.BASE_DIR + '/DMSapp' + image_loaded, subsampling=0, quality=100)

	# Remove the testing file
	os.remove(settings.MEDIA_ROOT + '/Scanned_images_temp/test.png')

	return JsonResponse({'success': True})


@csrf_exempt
def ajax_scanned_document(request):
	if request.method == "POST":
		import shutil
		from xml.dom import minidom
		xml_string = request.body.decode('utf-8')
		json_load_var = json.loads(xml_string)

		requested_account_number = ''
		workflow_number = ''
		coded_data = ''

		if 'account_number' in json_load_var:
			requested_account_number = json_load_var['account_number']
		if 'workflow_number' in json_load_var:
			workflow_number = str(json_load_var['workflow_number'])
		if 'data' in json_load_var:
			coded_data = json_load_var['data']
		print('data',coded_data)

		try:
			if requested_account_number:
				existing_data = UserDataWorkflowManagement.objects.filter(
					workflow_number=workflow_number)
				print('existing data', existing_data)
				wf_number = ''
				acc_number = ''
				if existing_data:
					for data in existing_data:
						wf_number = data.workflow_number
						acc_number = data.account_number

					image_object = []
					print(wf_number, workflow_number)
					print(acc_number, requested_account_number)
					if (wf_number == workflow_number) and (acc_number == requested_account_number):
						print('data matched')

						s = io.BytesIO(base64.b64decode(coded_data))

						print(s.getvalue(), 's')
						s.seek(0)
						with zipfile.ZipFile(s, 'r') as zip_ref:
							zip_ref.extractall(os.path.join(settings.MEDIA_ROOT + '/Scanned_Data/' + wf_number + '/'))

						#path_new = os.path.join(settings.MEDIA_ROOT + '/Scanned_Data/' + wf_number + '/' + acc_number )
						path_new = os.path.join(settings.MEDIA_ROOT + '/Scanned_Data/' + wf_number + '/' )

						if not os.path.isdir(path_new):
							print('path not exists')
							try:
								os.makedirs(path_new)
							except OSError as exc:  # Guard against race condition
								print("Not able to make directory", path_new)

						if os.path.exists(path_new):
							print("Path exists, export successfully done")
							onlyfiles = [f for f in os.listdir(path_new) if os.path.isfile(os.path.join(path_new, f))]
							print("Files are:", onlyfiles)

							for i in onlyfiles:
								path_new_1 = os.path.join(path_new + '/' + i)
								file1 = open(path_new_1, 'rb')
								b = bytes(file1.read())
								encoded = Cryptographer.encrypted(b)  # Cryptographer used to encrypt and decrypt the files
								file1.close()
								if os.path.exists(path_new_1):
									os.remove(path_new_1)
								encoded_file = ContentFile(encoded)

								path_new_2 = os.path.join(settings.MEDIA_ROOT + '/Scanned_Data/' + wf_number + '/' + i)
								if os.path.exists(path_new_2):
									os.remove(path_new_2)

								default_storage.save(path_new_2, encoded_file)
								print('File:', i, ' is encrypted')

								image_path = os.path.join('/media/Scanned_Data/' + wf_number + '/'+ i)

								i_name = i.split('.')[0]
								image_json = {i_name: image_path}
								image_object.append(image_json)

							print(image_object)

						else:
							print("Data is not exported successfully")

						# Check if any entry for same application number is present or not in database
						workflow_number_queryset = ScannedImagesUserData.objects.filter(account_number=requested_account_number)
						print('workflow_number_queryset', workflow_number_queryset)

						user_existing_data = UserDataWorkflowManagement.objects.get(workflow_number=workflow_number)

						if workflow_number_queryset:
							print('Already have data')
							for i in workflow_number_queryset:
								i.user_scanned_data = image_object
								i.account_number = requested_account_number
								i.save()
						else:
							print('in else of ScannedImagesUserData')
							ScannedImagesUserData.objects.create(account_number=requested_account_number,
																 workflow_number=user_existing_data,
																 user_scanned_data=image_object, account_type="COSMOS")

						# Making the data available in table
						existing_data = UserDataWorkflowManagement.objects.filter(
							workflow_number=workflow_number)
						for e_data in existing_data:
							e_data.documents = True
							e_data.workstep_name = 'Finacle Data Entry'
							e_data.previous_workstep = 'Scanning'
							e_data.modified_date_time = datetime.now()
							e_data.save()

						scanned_created_data = ScannedImagesUserData.objects.filter(workflow_number__workflow_number=workflow_number)
						for f_data in scanned_created_data:
							f_data.is_encrypted = True
							f_data.save()

						return JsonResponse({'data': 'Export Successfully Completed'})
					else:
						return JsonResponse({'data': 'No Such Data'})
				else:
					return JsonResponse({'data': 'No Such Data'})
			else:
				return JsonResponse({'data': 'Application Number is null'})

		except UserDataWorkflowManagement.DoesNotExist:
			return JsonResponse({'data': 'No Such Data'})

	return JsonResponse({'data': False})


# @csrf_exempt
# def ajax_scanned_document(request):
# 	from django.contrib.sessions.models import Session
#
# 	if request.method == "POST":
# 		print('outside')
# 		print(request.user)
# 		session_key = request.POST.get('session_key')
# 		print('session_key', session_key)
# 		try:
# 			session = Session.objects.get(session_key=session_key)
# 			session_data = session.get_decoded()
# 			print('session data', session_data)
# 			try:
# 				uid = session_data.get('_auth_user_id')
# 				user = User.objects.get(id=uid)
# 				print('user', user)
# 				if user:
# 					print('Inside user if else condition')
# 					coded_data = request.POST.get('data')
# 					requested_application_number = request.POST.get('account_number')
# 					workflow_number = request.POST.get('workflow_number')
# 					try:
# 						existing_data = UserDataWorkflowManagement.objects.filter(
# 							application_number=requested_application_number)
# 						print('existing data', existing_data)
# 						wf_number = ''
# 						app_number = ''
# 						if existing_data:
# 							for data in existing_data:
# 								wf_number = data.workflow_number
# 								app_number = data.application_number
#
# 							print(wf_number, workflow_number)
# 							print(app_number, requested_application_number)
# 							if (wf_number == workflow_number) and (app_number == requested_application_number):
# 								print('data matched')
#
# 								image_data = coded_data.replace('data:image/tiff;base64,', '')  # get the image_data
# 								file_content = decoder_for_base64_file(image_data, "test_image")  # decode the image
#
# 								path_new = os.path.join(
# 									settings.MEDIA_ROOT + '/Scanned_Data/' + wf_number +
# 									'/' + wf_number + '_' + app_number + '.tiff')  # Path to save the customer data in back folder
#
# 								# Removing the if the same file exists
# 								if os.path.exists(path_new):
# 									os.remove(path_new)
# 								default_storage.save(path_new, file_content)  # storing the image using default storage
#
# 								# Encrypting the file side by side
# 								file1 = open(path_new, 'rb')
# 								b = bytes(file1.read())
# 								encoded = Cryptographer.encrypted(b)  # Cryptographer used to encrypt and decrypt the files
#
# 								print('path_new', path_new)
# 								if os.path.exists(path_new):
# 									os.remove(path_new)
#
# 								encoded_file = ContentFile(encoded)
# 								default_storage.save(path_new, encoded_file)
# 								print('File is encrypted')
#
# 								# Check if any entry for same application number is present or not in database
# 								application_number = ScannedImagesUserData.objects.filter(application_number=requested_application_number)
# 								print('application_number', application_number)
#
# 								user_existing_data = UserDataWorkflowManagement.objects.get(application_number=app_number)
#
# 								image_path = os.path.join('/media/Scanned_Data/' + wf_number + '/' + wf_number + '_'
# 														  + app_number + '.tiff')
#
# 								image_json = {'Application_Form': image_path}
#
# 								if application_number:
# 									print('Already have data')
# 								else:
# 									print('in else of ScannedImagesUserData')
# 									ScannedImagesUserData.objects.create(application_number=requested_application_number,
# 																		 workflow_number=user_existing_data,
# 																		 user_scanned_data = image_json)
#
# 								# Making the data available in table
# 								existing_data = UserDataWorkflowManagement.objects.filter(
# 									application_number=requested_application_number)
# 								for e_data in existing_data:
# 									e_data.documents = True
# 									e_data.save()
#
# 								scanned_created_data = ScannedImagesUserData.objects.filter(application_number=requested_application_number)
# 								for f_data in scanned_created_data:
# 									f_data.is_encrypted = True
# 									f_data.save()
#
# 								return JsonResponse({'data': 'File Saved'})
# 							else:
# 								return JsonResponse({'data': 'No Such Data'})
#
# 					except UserDataWorkflowManagement.DoesNotExist:
# 						return JsonResponse({'data': 'No Such Data'})
#
# 			except User.DoesNotExist:
# 				# Deleting the session key
# 				Session.objects.filter(session_key=session_key).delete()
# 				data = {'data': 'No Such Data'}
# 				return JsonResponse(data)
#
# 		except Session.DoesNotExist:
# 			Session.objects.filter(session_key=session_key).delete()
# 			data = {'data': 'Not Authenticated'}
# 			return JsonResponse(data)
#
# 	return JsonResponse({'data': False})
#

@csrf_exempt
def ajax_login_call(request):
	if request.method == 'POST':
		print('in post request')
		from django.contrib.auth import login, authenticate, logout
		from django.contrib.sessions.backends.db import SessionStore

		username = request.POST.get('username')
		password = request.POST.get('password')
		print(password)
		user = authenticate(username=username, password=password)
		print(user)
		if user is not None:
			if user.is_active:
				print('user is active')
				login(request, user)
				session = request.session.session_key

				data = {'data': 'Authenticated', 'Session': session}
				return JsonResponse(data)

			return JsonResponse({'data': 'User Not Active'})
		else:
			return JsonResponse({'data': 'Data Mismatch'})
	else:
		print('Not a post request')
		JsonResponse({'data': False})


# New Changes for Workflow dated 20-jan-2020

from django.contrib.auth import login, authenticate, logout
from DMSaccount.security import admin_security, captcha_enbales, security_error
from DMSaccount.forms import CaptchaTestForm


def workflowProcess(request):
	if request.user.is_authenticated:
		print("user is authenticated")
	else:
		return HttpResponseRedirect(reverse('workflowLogin'))


def workflow_process(request):
	if request.user.is_authenticated:
		if request.method == 'GET':
			data = UserDataWorkflowManagement.objects.all().order_by('-pk')
			new_data = serializers.serialize('json', data)
			json_data = json.loads(new_data)
			if request.GET.get('selected_menu'):
				selected_menu = request.GET.get('selected_menu')
				if selected_menu:
					if selected_menu == 'AO_Scanning':
						scan_data = []
						data1 = dict()
						current_datetime = datetime.now()
						count = 0
						for i in json_data:

							entry = i['fields']['entry_date_time']

							username_id = i['fields']['user']
							user_name = User.objects.get(pk=username_id)
							i['fields']['user_name'] = user_name.username

							maker_id = i['fields']['maker']
							if maker_id:
								user_name = User.objects.get(pk=maker_id)
								maker_user_name = user_name.username
								i['fields']['maker_user_name'] = maker_user_name
							else:
								i['fields']['maker_user_name'] = ""

							checker_id = i['fields']['checker']
							if checker_id:
								user_name = User.objects.get(pk=checker_id)
								checker_user_name = user_name.username
								i['fields']['checker_user_name'] = checker_user_name
							else:
								i['fields']['checker_user_name'] = ""

							entry_time = parser.parse(i['fields']['entry_date_time'])
							elapsedTime = current_datetime - entry_time
							total_diff = list(divmod(elapsedTime.total_seconds(), 60))
							minutes = total_diff[0]

							if minutes >= 1440 and i['fields']['documents']== False:
								count += 1
								i['fields']['srNo'] = str(count)
								# i['fields']['workstep_name'] = 'Scanning'

								# Changing step from Account Intro To Scanning ######################
								user_data = UserDataWorkflowManagement.objects.get(id=i['pk'])
								if user_data:
									if user_data.workstep_name != 'Scanning':
										UserDataWorkflowManagement.objects.filter(pk=i['pk']).update(
											workstep_name='Scanning',previous_workstep='Account Introduction',
											modified_date_time=datetime.now())

								dt = datetime.strftime(entry_time, "%b. %d,%Y %I:%M %p")
								i['fields']['entry_date_time'] = str(dt)
								if i['fields']['modified_date_time']:
									turn_around = i['fields']['modified_date_time']
									ta_time = parser.parse(i['fields']['modified_date_time'])
									dt1 = datetime.strftime(ta_time, "%b. %d,%Y %I:%M %p")
									i['fields']['modified_date_time'] = str(dt1)
									print(dt1)
								else:
									i['fields']['modified_date_time'] = ''

								if i['fields']['previous_workstep'] is None:
									i['fields']['previous_workstep'] = ''

								wf = NewWorkflowManagement.objects.get(id=i['fields']['workflow_id'])
								i['fields']['wf_name'] = wf.wf_name

								scan_data.append(i)
						print(scan_data)
						dumped_data = json.dumps(scan_data)

						return HttpResponse(dumped_data,content_type="application/json")

					elif selected_menu == 'My Queue':

						current_datetime = datetime.now()
						count = 0
						for i in json_data:
							count += 1
							i['fields']['srNo'] = str(count)

							entry = i['fields']['entry_date_time']

							username_id = i['fields']['user']
							user_name = User.objects.get(pk=username_id)
							i['fields']['user_name'] = user_name.username

							maker_id = i['fields']['maker']
							if maker_id:
								user_name = User.objects.get(pk=maker_id)
								maker_user_name = user_name.username
								i['fields']['maker_user_name'] = maker_user_name
							else:
								i['fields']['maker_user_name'] = ""

							checker_id = i['fields']['checker']
							if checker_id:
								user_name = User.objects.get(pk=checker_id)
								checker_user_name = user_name.username
								i['fields']['checker_user_name'] = checker_user_name
							else:
								i['fields']['checker_user_name'] = ""

							entry_time = parser.parse(i['fields']['entry_date_time'])
							elapsedTime = current_datetime - entry_time
							total_diff = list(divmod(elapsedTime.total_seconds(), 60))
							minutes = total_diff[0]

							if minutes >= 1440 and i['fields']['documents']==False:
								# i['fields']['workstep_name'] = 'Scanning'

								# Changing step from Account Intro To Scanning ######################
								user_data = UserDataWorkflowManagement.objects.get(id=i['pk'])
								if user_data:
									if user_data.workstep_name != 'Scanning':
										UserDataWorkflowManagement.objects.filter(pk=i['pk']).update(
											workstep_name='Scanning',previous_workstep='Account Introduction',modified_date_time=datetime.now())

							dt = datetime.strftime(entry_time, "%b. %d,%Y %I:%M %p")
							i['fields']['entry_date_time'] = dt
							if i['fields']['modified_date_time']:
								turn_around = i['fields']['modified_date_time']
								ta_time = parser.parse(i['fields']['modified_date_time'])
								dt1 = datetime.strftime(ta_time, "%b. %d,%Y %I:%M %p")
								i['fields']['modified_date_time'] = dt1

							else:
								i['fields']['modified_date_time'] = ''

							if i['fields']['previous_workstep'] is None:
								i['fields']['previous_workstep'] = ''

							wf = NewWorkflowManagement.objects.get(id=i['fields']['workflow_id'])
							i['fields']['wf_name'] = wf.wf_name

						dumped_data = json.dumps(json_data)
						print(dumped_data)
						return HttpResponse(dumped_data, content_type="application/json")

					elif selected_menu == 'Finacle Data Entry':
						user_data = UserDataWorkflowManagement.objects.filter(workstep_name = 'Finacle Data Entry')
						new_data = serializers.serialize('json', user_data)
						json_data = json.loads(new_data)
						count = 0
						for i in json_data:
							count += 1
							i['fields']['srNo'] = str(count)

							username_id = i['fields']['user']
							user_name = User.objects.get(pk=username_id)
							i['fields']['user_name'] = user_name.username

							entry = i['fields']['entry_date_time']
							maker_id = i['fields']['maker']
							if maker_id:
								user_name = User.objects.get(pk=maker_id)
								maker_user_name = user_name.username
								i['fields']['maker_user_name'] = maker_user_name
							else:
								i['fields']['maker_user_name'] = ""

							checker_id = i['fields']['checker']
							if checker_id:
								user_name = User.objects.get(pk=checker_id)
								checker_user_name = user_name.username
								i['fields']['checker_user_name'] = checker_user_name
							else:
								i['fields']['checker_user_name'] = ""

							entry_time = parser.parse(i['fields']['entry_date_time'])
							dt = datetime.strftime(entry_time, "%b. %d,%Y %I:%M %p")
							i['fields']['entry_date_time'] = dt

							if i['fields']['modified_date_time']:
								turn_around = i['fields']['modified_date_time']
								ta_time = parser.parse(i['fields']['modified_date_time'])
								dt1 = datetime.strftime(ta_time, "%b. %d,%Y %I:%M %p")
								i['fields']['modified_date_time'] = dt1

							else:
								i['fields']['modified_date_time'] = ''

							if i['fields']['previous_workstep'] is None:
								i['fields']['previous_workstep'] = ''

							wf = NewWorkflowManagement.objects.get(id=i['fields']['workflow_id'])
							i['fields']['wf_name'] = wf.wf_name

						dumped_data = json.dumps(json_data)

						return HttpResponse(dumped_data, content_type="application/json")

					elif selected_menu == 'AO_AOC Acceptance':
						user_data = UserDataWorkflowManagement.objects.filter(workstep_name = 'AOC Acceptance')
						new_data = serializers.serialize('json', user_data)
						json_data = json.loads(new_data)
						count = 0
						for i in json_data:
							count += 1
							i['fields']['srNo'] = str(count)

							username_id = i['fields']['user']
							user_name = User.objects.get(pk=username_id)
							i['fields']['user_name'] = user_name.username

							entry = i['fields']['entry_date_time']
							maker_id = i['fields']['maker']
							if maker_id:
								user_name = User.objects.get(pk=maker_id)
								maker_user_name = user_name.username
								i['fields']['maker_user_name'] = maker_user_name
							else:
								i['fields']['maker_user_name'] = ""

							checker_id = i['fields']['checker']
							if checker_id:
								user_name = User.objects.get(pk=checker_id)
								checker_user_name = user_name.username
								i['fields']['checker_user_name'] = checker_user_name
							else:
								i['fields']['checker_user_name'] = ""

							entry_time = parser.parse(i['fields']['entry_date_time'])
							dt = datetime.strftime(entry_time, "%b. %d,%Y %I:%M %p")
							i['fields']['entry_date_time'] = dt

							if i['fields']['modified_date_time']:
								turn_around = i['fields']['modified_date_time']
								ta_time = parser.parse(i['fields']['modified_date_time'])
								dt1 = datetime.strftime(ta_time, "%b. %d,%Y %I:%M %p")
								i['fields']['modified_date_time'] = dt1

							else:
								i['fields']['modified_date_time'] = ''

							if i['fields']['previous_workstep'] is None:
								i['fields']['previous_workstep'] = ''

							wf = NewWorkflowManagement.objects.get(id=i['fields']['workflow_id'])
							i['fields']['wf_name'] = wf.wf_name

						dumped_data = json.dumps(json_data)

						return HttpResponse(dumped_data, content_type="application/json")

					elif selected_menu == 'AO_Account Introduction':
						user_data = UserDataWorkflowManagement.objects.filter(workstep_name = 'Account Introduction')
						new_data = serializers.serialize('json', user_data)
						json_data = json.loads(new_data)
						count = 0
						for i in json_data:
							count += 1
							i['fields']['srNo'] = str(count)

							username_id = i['fields']['user']
							user_name = User.objects.get(pk=username_id)
							i['fields']['user_name'] = user_name.username

							entry = i['fields']['entry_date_time']
							maker_id = i['fields']['maker']
							if maker_id:
								user_name = User.objects.get(pk=maker_id)
								maker_user_name = user_name.username
								i['fields']['maker_user_name'] = maker_user_name
							else:
								i['fields']['maker_user_name'] = ""

							checker_id = i['fields']['checker']
							if checker_id:
								user_name = User.objects.get(pk=checker_id)
								checker_user_name = user_name.username
								i['fields']['checker_user_name'] = checker_user_name
							else:
								i['fields']['checker_user_name'] = ""

							entry_time = parser.parse(i['fields']['entry_date_time'])
							dt = datetime.strftime(entry_time, "%b. %d,%Y %I:%M %p")
							i['fields']['entry_date_time'] = dt

							if i['fields']['modified_date_time']:
								turn_around = i['fields']['modified_date_time']
								ta_time = parser.parse(i['fields']['modified_date_time'])
								dt1 = datetime.strftime(ta_time, "%b. %d,%Y %I:%M %p")
								i['fields']['modified_date_time'] = dt1
							else:
								i['fields']['modified_date_time'] = ''

							if i['fields']['previous_workstep'] is None:
								i['fields']['previous_workstep'] = ''

							wf = NewWorkflowManagement.objects.get(id=i['fields']['workflow_id'])
							i['fields']['wf_name'] = wf.wf_name
						dumped_data = json.dumps(json_data)

						return HttpResponse(dumped_data, content_type="application/json")

			else:
				current_datetime = datetime.now()
				count = 0
				for i in json_data:
					count += 1
					i['fields']['srNo'] = str(count)

					entry = i['fields']['entry_date_time']

					username_id = i['fields']['user']
					user_name = User.objects.get(pk=username_id)
					i['fields']['user_name'] = user_name.username

					maker_id = i['fields']['maker']
					if maker_id:
						user_name = User.objects.get(pk=maker_id)
						maker_user_name=user_name.username
						i['fields']['maker_user_name']=maker_user_name
					else:
						i['fields']['maker_user_name'] = ""


					checker_id = i['fields']['checker']
					if checker_id:
						user_name = User.objects.get(pk=checker_id)
						checker_user_name = user_name.username
						i['fields']['checker_user_name'] = checker_user_name
					else:
						i['fields']['checker_user_name'] = ""

					entry_time = parser.parse(i['fields']['entry_date_time'])
					elapsedTime = current_datetime - entry_time
					total_diff = list(divmod(elapsedTime.total_seconds(), 60))
					minutes = total_diff[0]
					if minutes >= 1440 and i['fields']['documents']== False:
						user_data = UserDataWorkflowManagement.objects.get(id=i['pk'])
						if user_data:
							if user_data.workstep_name != 'Scanning' and user_data.workstep_name == 'Account Introduction':
								UserDataWorkflowManagement.objects.filter(pk=i['pk']).update(workstep_name='Scanning',
																							 previous_workstep='Account Introduction',
																							 modified_date_time=datetime.now())
					dt = datetime.strftime(entry_time, "%b. %d,%Y %I:%M %p")
					i['fields']['entry_date_time'] = dt
					if i['fields']['modified_date_time']:
						turn_around = i['fields']['modified_date_time']
						ta_time = parser.parse(i['fields']['modified_date_time'])
						dt1 = datetime.strftime(ta_time, "%b. %d,%Y %I:%M %p")
						i['fields']['modified_date_time'] = dt1

					else:
						i['fields']['modified_date_time'] = ''

					if i['fields']['previous_workstep'] is None:
						i['fields']['previous_workstep'] = ''

					wf = NewWorkflowManagement.objects.get(id=i['fields']['workflow_id'])
					i['fields']['wf_name'] = wf.wf_name

				context = {'data': json_data}
				return render(request,'workflow_process.html', context)
	else:
		return HttpResponseRedirect(reverse('WorkflowManagement:workflowLogin'))


#Workflow Process Login


def workflowLogin(request):
	login_security = admin_security()
	aes_pass = settings.AES_PASSWORD
	print(aes_pass, 'aes_pass', login_security)

	if request.user.is_authenticated:
		return HttpResponseRedirect(reverse('WorkflowManagement:workflow_process'))
	if request.method == 'POST':
		print('inside POST request')
		username = request.POST.get('auth')
		enc_password = request.POST.get('auth_key')
		password = security_error(enc_password, settings.AES_PASSWORD)
		user = authenticate(username=username, password=password)
		if user is not None:
			if user.is_active:
				if login_security == 1:
					login(request, user)
					print('here in user logged in')
					print('user logged in')
				elif login_security == 2:
					form = CaptchaTestForm(request.POST)
					if form.is_valid():
						human = True
						login(request, user)
					else:
						messages.error(request, 'Invalid reCAPTCHA. Please try again.')
						return redirect('workflowLogin')

				elif login_security == 3:
					var = captcha_enbales(request)
					if var is True:
						login(request, user)
					else:
						return redirect('workflowLogin')

				elif login_security == 4:
					userId = user.pk
					return HttpResponseRedirect(reverse('send_otp', args=[userId]))

				elif login_security == 5:
					var = request.POST.get('success')
					var1 = request.POST.get('sum')
					if var == var1:
						login(request, user)
					else:
						messages.error(request, '"Invalid Captcha token" . Please try again.')
						return redirect('workflowLogin')
				# elif login_security == 6:
				# 	faceRegistered = FaceImages.objects.filter(username=username)
				# 	if faceRegistered:
				# 		recognized = faceRecognizer(request, username)
				# 		print('recognized', recognized)
				# 		if recognized:
				# 			login(request, user)
				# 		else:
				# 			return redirect('user_login')
				# 	else:
				# 		messages.info(request, mark_safe(
				# 			"Face Not Registered. Click <a href='#' id='mylink' class='" + username + "'>here</a> to register your face."))
				# 		return redirect('user_login')
				if request.POST.get('next'):
					if request.POST.get('next') == '/':
						return HttpResponseRedirect(reverse('WorkflowManagement:workflow_process'))
					return HttpResponseRedirect(request.POST.get('next', reverse('WorkflowManagement:workflowProcess')))
				else:
					print('in workflowProcess')
					return HttpResponseRedirect(reverse('WorkflowManagement:workflow_process'))
			else:
				return HttpResponse("Your account was inactive.")
		else:
			messages.error(request, 'Invalid Login Credentials Given. Please try again.')
			return redirect('workflowLogin')
	else:
		form = CaptchaTestForm()
		context = {'login_security': login_security, 'aes_pass': aes_pass, 'login_action': "workflowLogin"}
		return render(request, 'login.html', locals())


def workflowDocsLogin(request):
	login_security = admin_security()
	aes_pass = settings.AES_PASSWORD
	print(aes_pass, 'aes_pass', login_security)

	if request.user.is_authenticated:
		return HttpResponseRedirect(reverse('WorkflowManagement:workflow_docs'))
	if request.method == 'POST':
		print('inside POST request')
		username = request.POST.get('auth')
		enc_password = request.POST.get('auth_key')
		password = security_error(enc_password, settings.AES_PASSWORD)
		user = authenticate(username=username, password=password)
		if user is not None:
			if user.is_active:
				if login_security == 1:
					login(request, user)
					print('here in user logged in')
					print('user logged in')
				elif login_security == 2:
					form = CaptchaTestForm(request.POST)
					if form.is_valid():
						human = True
						login(request, user)
					else:
						messages.error(request, 'Invalid reCAPTCHA. Please try again.')
						return redirect('WorkflowManagement:workflowDocsLogin')

				elif login_security == 3:
					var = captcha_enbales(request)
					if var is True:
						login(request, user)
					else:
						return redirect('WorkflowManagement:workflowDocsLogin')

				elif login_security == 4:
					userId = user.pk
					return HttpResponseRedirect(reverse('send_otp', args=[userId]))

				elif login_security == 5:
					var = request.POST.get('success')
					var1 = request.POST.get('sum')
					if var == var1:
						login(request, user)
					else:
						messages.error(request, '"Invalid Captcha token" . Please try again.')
						return redirect('WorkflowManagement:workflowDocsLogin')
				# elif login_security == 6:
				# 	faceRegistered = FaceImages.objects.filter(username=username)
				# 	if faceRegistered:
				# 		recognized = faceRecognizer(request, username)
				# 		print('recognized', recognized)
				# 		if recognized:
				# 			login(request, user)
				# 		else:
				# 			return redirect('user_login')
				# 	else:
				# 		messages.info(request, mark_safe(
				# 			"Face Not Registered. Click <a href='#' id='mylink' class='" + username + "'>here</a> to register your face."))
				# 		return redirect('user_login')
				if request.POST.get('next'):
					if request.POST.get('next') == '/':
						return HttpResponseRedirect(reverse('WorkflowManagement:workflow_docs'))
					return HttpResponseRedirect(request.POST.get('next', reverse('WorkflowManagement:workflow_docs')))
				else:
					print('in workflowDocs')
					return HttpResponseRedirect(reverse('WorkflowManagement:workflow_docs'))
			else:
				return HttpResponse("Your account was inactive.")
		else:
			messages.error(request, 'Invalid Login Credentials Given. Please try again.')
			return redirect('WorkflowManagement:workflowLogin')
	else:
		form = CaptchaTestForm()
		context = {'login_security': login_security, 'aes_pass': aes_pass, 'login_action': "workflowDocLogin"}
		return render(request, 'login.html', locals())


@login_required(login_url='/workflowDocsLogin')
def workflow_docs(request):
	return render(request, 'workflow_docs.html')


@login_required(login_url='/workflowDocsLogin')
def workflow_docs_search(request):
	context = {'search_data':""}
	if request.method == 'POST':
		search_data = request.POST.get('search_data')
		scanned_data = ScannedImagesUserData.objects.filter(Q(account_number=search_data) |
															Q(workflow_number__workflow_number=search_data))
		docs = ''
		docs_id = ''
		if scanned_data:
			for i in scanned_data:
				docs = i.user_scanned_data
				docs_id = i.pk
		context = {"data": docs, 'docs_id': docs_id, 'search_data': search_data}
		return render(request, 'workflow_files/workflow_docs_search.html', context=context)
	else:
		return render(request, 'workflow_files/workflow_docs_search.html', context=context)


def confirm_workflow_docs(request):
	if request.user.is_authenticated:
		return JsonResponse({'success': True})
	else:
		return JsonResponse({'success': False})


def download_workflow_docs(request, value, str_value=None):
	import io, zipfile
	import PIL
	import reportlab
	import reportlab.lib.pagesizes as pdf_sizes

	download_file = get_object_or_404(ScannedImagesUserData, pk=value)
	files = download_file.user_scanned_data
	is_encrypt = download_file.is_encrypted
	files_list = []

	print(is_encrypt, "File_encrypted")

	selected_option = request.GET.get('selectedOption')
	print('selected_option', selected_option)

	if str_value:
		if selected_option == "Tiff":
			for tag in files:
				for i,j in tag.items():
					if j:
						if str_value == i:
							s = j.split('/media')
							files_list.append(s[1])

			if files_list:
				full_path = ''
				fname = ''
				for fpath in files_list:
					full_path = os.path.join(settings.MEDIA_ROOT + '/' + fpath)
					fdir, fname = os.path.split(full_path)

					path_for_jpeg = os.path.join(settings.MEDIA_ROOT + '/' + 'Scanned_images_temp')
					import shutil
					for the_file in os.listdir(path_for_jpeg):
						file_path = os.path.join(path_for_jpeg, the_file)
						try:
							if os.path.isfile(file_path):
								os.unlink(file_path)
							elif os.path.isdir(file_path):
								shutil.rmtree(file_path)

						except Exception as e:
							print(e)

					if is_encrypt:
						file2 = open(full_path, 'rb')
						b2 = bytes(file2.read())
						decoded = Cryptographer.decrypted(b2)  # Cryptographer used to encrypt and decrypt the files
						decoded_file = ContentFile(decoded)
						print(decoded_file)
						default_storage.save(os.path.join(path_for_jpeg + '/' + 'test.tiff'), decoded_file)
						print('File is decrypted')
					else:
						default_storage.save(os.path.join(path_for_jpeg + '/' + 'test.tiff'), full_path)

					with open(os.path.join(path_for_jpeg + '/' + 'test.tiff'), 'rb') as fh:
						data = HttpResponse(fh.read(), content_type='text/plain')
						data['Content-Disposition'] = 'attachment; filename=%s' % fname
						return data
			else:
				raise Http404

		elif selected_option == 'JPEG':
			for tag in files:
				for i,j in tag.items():
					if j:
						if str_value == i:
							s = j.split('/media')
							files_list.append(s[1])
			temp_list = []
			file_name = ''
			if files_list:
				path_j = os.path.join(settings.MEDIA_ROOT + files_list[0])

				path_for_jpeg = os.path.join(settings.MEDIA_ROOT + '/' + 'Scanned_images_temp')
				import shutil
				for the_file in os.listdir(path_for_jpeg):
					file_path = os.path.join(path_for_jpeg, the_file)
					try:
						if os.path.isfile(file_path):
							os.unlink(file_path)
						elif os.path.isdir(file_path):
							shutil.rmtree(file_path)

					except Exception as e:
						print(e)

				if is_encrypt:
					file2 = open(path_j, 'rb')
					b2 = bytes(file2.read())
					decoded = Cryptographer.decrypted(b2)  # Cryptographer used to encrypt and decrypt the files
					decoded_file = ContentFile(decoded)
					print(decoded_file)
					default_storage.save(os.path.join(path_for_jpeg + '/' + 'test.tiff'), decoded_file)
					print('File is decrypted')
				else:
					default_storage.save(os.path.join(path_for_jpeg + '/' + 'test.tiff'), path_j)

				img = Image.open(os.path.join(path_for_jpeg + '/' + 'test.tiff'))


				if hasattr(img, 'filename'):
					file1 = img.filename
					fdir, fname = os.path.split(file1)
					print('fname', fname)
					file2 = fname.split('.')[0]
					file_name = str(file2) + '.zip'

				else:
					file_name = 'File.zip'

				no = img.n_frames
				print('number of images in tiff', no)

				for i in range(no):
					try:
						img.seek(i)
						img.save(os.path.join(path_for_jpeg + '/' + 'page_%s.png' % (i,)))
						temp_list.append('page_%s.png' % (i,))
					except EOFError:
						break

			s = io.BytesIO()
			zf = zipfile.ZipFile(s, "w")
			zip_subdir = os.path.join(settings.MEDIA_ROOT + "/zipped_file")

			if temp_list:
				len_of_file = len(temp_list)
				for i in range(len_of_file):
					image_path = os.path.join(settings.MEDIA_ROOT + '/Scanned_images_temp/' + temp_list[i])
					fdir, f_name = os.path.split(image_path)
					zip_path = os.path.join(zip_subdir, f_name)
					zf.write(image_path, zip_path)

				zf.close()

				data = HttpResponse(s.getvalue(), content_type="plain/text")
				data['Content-Disposition'] = 'attachment; filename=%s' % file_name
				return data
			else:
				raise Http404

		elif selected_option == "PDF":
			for tag in files:
				for i,j in tag.items():
					if j:
						if str_value == i:
							s = j.split('/media')
							files_list.append(s[1])

			if files_list:
				path_j = os.path.join(settings.MEDIA_ROOT + files_list[0])

				path_for_jpeg = os.path.join(settings.MEDIA_ROOT + '/' + 'Scanned_images_temp')
				import shutil
				for the_file in os.listdir(path_for_jpeg):
					file_path = os.path.join(path_for_jpeg, the_file)
					try:
						if os.path.isfile(file_path):
							os.unlink(file_path)
						elif os.path.isdir(file_path):
							shutil.rmtree(file_path)

					except Exception as e:
						print(e)

				if is_encrypt:
					file2 = open(path_j, 'rb')
					b2 = bytes(file2.read())
					decoded = Cryptographer.decrypted(b2)  # Cryptographer used to encrypt and decrypt the files
					decoded_file = ContentFile(decoded)
					print(decoded_file)
					default_storage.save(os.path.join(path_for_jpeg + '/' + 'test.tiff'), decoded_file)
					print('File is decrypted')
				else:
					default_storage.save(os.path.join(path_for_jpeg + '/' + 'test.tiff'), path_j)

				y = os.path.join(path_for_jpeg + '/' + 'test.tiff')
				tiff_img = PIL.Image.open(y)

				height, width = tiff_img.tag[0x101][0], tiff_img.tag[0x100][0]

				out_pdf_io = io.BytesIO()
				c = reportlab.pdfgen.canvas.Canvas(out_pdf_io, pagesize=pdf_sizes.letter)

				pdf_width, pdf_height = pdf_sizes.letter

				page = 0
				while True:
					try:
						tiff_img.seek(page)
					except EOFError:
						break

					if pdf_width * height / width <= pdf_height:
						# Stretch wide
						c.drawInlineImage(tiff_img, 0, 0, pdf_width, pdf_width * height / width)
					else:
						# Stretch long
						c.drawInlineImage(tiff_img, 0, 0, pdf_height * width / height, pdf_height)

					c.showPage()
					max_pages = 200
					if max_pages and page > max_pages:
						print("Too many pages, breaking early")
						break
					page += 1

				c.save()
				file_name = 'file.pdf'
				data = HttpResponse(out_pdf_io.getvalue(), content_type="plain/text")
				data['Content-Disposition'] = 'attachment; filename=%s' % file_name
				return data

			else:
				raise Http404

	else:
		for tag in files:
			for i, j in tag.items():
				if j:
					s = j.split('/media')
					files_list.append(s[1])

		zip_subdir = os.path.join(settings.MEDIA_ROOT + "/zipped_file")
		zip_filename = "File.zip"

		s = io.BytesIO()
		zf = zipfile.ZipFile(s, "w")
		
		for fpath in files_list:
			path_for_jpeg = os.path.join(settings.MEDIA_ROOT + '/' + 'Scanned_images_temp')
			import shutil
			for the_file in os.listdir(path_for_jpeg):
				file_path = os.path.join(path_for_jpeg, the_file)
				try:
					if os.path.isfile(file_path):
						os.unlink(file_path)
					elif os.path.isdir(file_path):
						shutil.rmtree(file_path)

				except Exception as e:
					print(e)
		
			full_path = os.path.join(settings.MEDIA_ROOT + '/' + fpath)
			fdir, fname = os.path.split(full_path)
			if is_encrypt:
				file2 = open(full_path, 'rb')
				b2 = bytes(file2.read())
				decoded = Cryptographer.decrypted(b2)  # Cryptographer used to encrypt and decrypt the files
				decoded_file = ContentFile(decoded)
				default_storage.save(os.path.join(path_for_jpeg + '/' + fname), decoded_file)
				print('File is decrypted')
			else:
				default_storage.save(os.path.join(path_for_jpeg + '/' + fname), full_path)
			# Calculate path for file in zip
			
			zip_path = os.path.join(zip_subdir, fname)
			zf.write(os.path.join(path_for_jpeg + '/' + fname), fname, zipfile.ZIP_DEFLATED)


		zf.close()
		data = HttpResponse(s.getvalue(),  content_type="application/zip")
		data['Content-Disposition'] = 'attachment; filename=%s' % zip_filename
		return data


def del_workflow_docs(request, value, str_value=None):

	download_file = get_object_or_404(ScannedImagesUserData, pk=value)
	files = download_file.user_scanned_data

	if str_value:
		for tag in files:
			for i,j in tag.items():
				if j:
					if str_value == i:
						print(i)
						files.update({i: ""})

		ScannedImagesUserData.objects.filter(pk=value).update(user_scanned_data=files)
		return JsonResponse(files)


def fetch_data_from_finacle(request):
	if request.method == 'GET':
		user_data_pk = request.GET.get('user_data')
		print(user_data_pk)
		user_data = UserDataWorkflowManagement.objects.get(pk=user_data_pk)
		if user_data.f_entry == "Y":
			json_data = user_data.new_user_data_definition
		else:
			json_data = user_data.user_data_definition
		for p in json_data:
			for l, b in p.copy().items():
				if '/media/Workflow_Management/' in b:
					p.pop(l, None)

		print('JsonData', json_data)
		json2 = {}
		for i in json_data:
			json2 = i

		for i, j in json2.items():
			if '[' and ']' in j:
				import ast
				list1 = ast.literal_eval(j)
				json2[i] = list1

		print(json2)
		formset = MakerWorkflowFormView(queryset_id=user_data_pk, data=json2)
		formset2 = MakerWorkflowFormView2(queryset_id=user_data_pk, data=json2)

		return JsonResponse({'success': True})


try:
    import xlwt
    import pytz
except ImportError:
   print("lwt and pytz is not available")

#from weasyprint import HTML
import io
#from weasyprint.fonts import FontConfiguration


@login_required(login_url='/login/')
def view_mis_report(request):
	table_form = MisQueryFormWorkflow()
	DB_EXCEPTIONS = (
		OperationalError,
		DatabaseError,
	)
	if request.method == 'POST':
		table_form = MisQueryFormWorkflow(request.POST, request.FILES)
		if table_form.is_valid():
			query = table_form.cleaned_data['query']
			export_to = table_form.cleaned_data['choice_field']
			try:
				cursor = connection.cursor()
				cursor.execute(query)
				columns1 = [col[0] for col in cursor.description]
				messages.success(request, 'All Done')
				table_data = [dict(zip(columns1, row)) for row in cursor.fetchall()]

				if table_data:
					if export_to == 'Excel':
						response = HttpResponse(content_type='application/ms-excel')
						response['Content-Disposition'] = 'attachment; filename="ExcelReport-{date}.xls"'.format(
							date=datetime.now().strftime('%Y%m%d-%H%M%S'), )

						wb = xlwt.Workbook(encoding='utf-8')
						ws = wb.add_sheet('Sheet1')
						row_num = 0

						font_style = xlwt.XFStyle()
						font_style.font.bold = True

						for col_num in range(len(columns1)):
							ws.write(row_num, col_num, columns1[col_num], font_style)

						font_style = xlwt.XFStyle()

						for row in table_data:
							row_num += 1
							for col_num in range(len(row)):
								label = columns1[col_num]
								if label == 'entry_date_time' and row[label] is not None:
									row_value = datetime.strftime(row[label], "%b. %d,%Y %I:%M %p")
								elif label == 'modified_date_time' and row[label] is not None:
									row_value = datetime.strftime(row[label], "%b. %d,%Y %I:%M %p")
								else:
									row_value = row[label]
								ws.write(row_num, col_num, row_value, font_style)

						wb.save(response)
						messages.success(request, 'Data Exported Successfully')
						return response

					elif export_to == 'PDF':
						first_index = table_data[0]
						columns_new = []
						for f,v in first_index.items():
							columns_new.append(f)
						html_string = render_to_string('workflow_files/pdf_template.html',
													   {'columns': columns_new, 'table_data': table_data})
						html = HTML(string=html_string)

						font_config = FontConfiguration()
						s = io.BytesIO()

						html.write_pdf(s, font_config=font_config,
									   stylesheets=[settings.STATIC_ROOT + '/css/bootstrap.min.css'])

						response = HttpResponse(s.getvalue(), content_type='application/pdf')
						response['Content-Disposition'] = 'attachment; filename="PDF_Report-{date}.pdf"'.format(
							date=datetime.now().strftime('%Y%m%d-%H%M%S'), )
						return response
				else:
					messages.error(request, 'No Data Found')

			except DB_EXCEPTIONS as exp:
				messages.error(request, exp)

			return render(request, 'view_mis_reports.html', {'table_form': table_form})
		else:
			return render(request, 'view_mis_reports.html', {'table_form': table_form})

	else:
		context0 = {'table_form': table_form}
		return render(request, 'view_mis_reports.html', context0)
		
		
# Saving Image after Editing in Workflow Process

@csrf_exempt
def save_image_workflow_process(request):
	if request.method == 'POST':
		selected_option = request.POST['selected_value']
		image_data = request.POST['data_image']
		wf_number = request.POST['wf_number']
		image_value = request.POST['image_value']

		wf_data = ScannedImagesUserData.objects.get(workflow_number__workflow_number=wf_number)

		user_scanned_images = wf_data.user_scanned_data
		print(selected_option)
		dd_value = selected_option.split('/')[-1].split('.')[0]
		req_image = ''
		for img in user_scanned_images:
			for k, l in img.items():
				if k == dd_value:
					req_image = l

		print(req_image)
		path_for_save_file = os.path.join(settings.MEDIA_ROOT + '/' + 'save_workflow_image_temp')

		if not os.path.isdir(path_for_save_file):
			print('path not exists')
			try:
				os.makedirs(path_for_save_file)
			except OSError as exc:  # Guard against race condition
				if exc.errno != errno.EEXIST:
					raise

		import shutil
		for the_file in os.listdir(path_for_save_file):
			file_path = os.path.join(path_for_save_file, the_file)
			try:
				if os.path.isfile(file_path):
					os.unlink(file_path)
				elif os.path.isdir(file_path):
					shutil.rmtree(file_path)

			except Exception as e:
				print(e)

		if req_image:
			print(settings.BASE_DIR + '/DMSapp' + req_image)
			req_path = settings.BASE_DIR + '/DMSapp' + req_image

			temp_tiff_path = path_for_save_file + '/' + dd_value + ".tiff"
			if wf_data.is_encrypted:
				# Decrypting the file to open it in canvas
				file2 = open(req_path, 'rb')
				b2 = bytes(file2.read())
				decoded = Cryptographer.decrypted(b2)  # Cryptographer used to encrypt and decrypt the files
				decoded_file = ContentFile(decoded)

				default_storage.save(os.path.join(temp_tiff_path), decoded_file)
			else:
				shutil.copy2(os.path.join(req_path),
							 os.path.join(temp_tiff_path))

			name_of_file = []
			if os.path.exists(temp_tiff_path):
				img = Image.open(temp_tiff_path)
				no = img.n_frames

				for i in range(no):
					try:
						img.seek(i)
						print(image_value, 'here')
						if int(image_value) == i:
							image_data2 = image_data.replace('data:image/png;base64,', '')
							imgdata = base64.b64decode(image_data2)
							with open(os.path.join(path_for_save_file + '/' + 'page_%s.png' % (i,)), "wb") as fh:
								fh.write(imgdata)
							name_of_file.append('page_%s.png' % (i,))
							# img.save(os.path.join(path_for_save_file + '/' + 'page_%s.png' % (i,)))
						else:
							img.save(os.path.join(path_for_save_file + '/' + 'page_%s.png' % (i,)))
							name_of_file.append('page_%s.png' % (i,))
						print(type(image_value), type(i))
					except EOFError:
						break
				img.close()

			os.remove(temp_tiff_path)

			from PIL import TiffImagePlugin
			with TiffImagePlugin.AppendingTiffWriter(temp_tiff_path, True) as tf:
				for tiff_in in name_of_file:
					try:
						im = Image.open(path_for_save_file +'/'+ tiff_in)
						im.save(tf)
						tf.newFrame()
						im.close()
						print('converted all tiff files')
					except FileNotFoundError:
						print('Error and Error')

			file1 = open(temp_tiff_path, 'rb')
			b = bytes(file1.read())
			encoded = Cryptographer.encrypted(b)
			encoded_file = ContentFile(encoded)
			file1.close()

			os.remove(temp_tiff_path)
			default_storage.save(os.path.join(temp_tiff_path), encoded_file)

			req_image2 = req_image.split('.')[0]

			d1 = datetime.now().strftime("%d-%m-%Y-%H-%M")
			d2 = datetime.now().strftime("%d-%m-%Y-%H:%M")

			shutil.copy2(os.path.join(req_path), settings.BASE_DIR + '/DMSapp' + req_image2 + "_" + d1 + ".tiff")
			shutil.copy2(temp_tiff_path, os.path.join(req_path))

			last_json_field = wf_data.versions_field

			json_array_version = []
			json_dict_version = {}

			if last_json_field:
				for ko in last_json_field:
					list_keys = list(ko.keys())
					print(list_keys)
					if dd_value in list_keys:
						ko_value = ko[dd_value]
						g = []
						for ko_item in ko_value:
							for k, v in ko_item.items():
								if k.startswith("V-"):
									version_value = k
									g.append(version_value)
						print(g, "ssadsadfsadfsdf")
						last_value = max(g)
						number = int(last_value.split('-')[-1]) + 1

						ko_value.append({"V-" + str(number): req_image2 + "_" + d1 + ".tiff", 'modified_time': d2})
						ko[dd_value] = ko_value

					else:
						inner_list = []
						inner_list.append({"V-1": req_image2 + "_" + d1 + ".tiff", 'modified_time': d2})
						ko[dd_value] = inner_list
				json_array_version = last_json_field
			else:
				inner_list = []
				inner_list.append({"V-1": req_image2 + "_" + d1 + ".tiff",'modified_time':d2})
				json_dict_version[dd_value] = inner_list
				json_array_version.append(json_dict_version)

			wf_data.versions_field = json_array_version
			wf_data.save()

		return JsonResponse({'success': True})
	else:
		return JsonResponse({'success': False})


def workflow_docs_for_version(request):
	data = []
	primary_number = request.GET.get("workflow_number")
	selected_doc = request.GET.get("message")
	workflow_number = ScannedImagesUserData.objects.get(pk=primary_number)
	all_versions = workflow_number.versions_field
	if all_versions:
		req_value = ''

		for value in all_versions:
			for i, j in value.items():
				if selected_doc == i:
					req_value = j

		for r in req_value:
			mydict = {}
			for k,v in r.items():
				if k == 'modified_time':
					mydict['modified_time'] = datetime.strftime(parser.parse(v), "%b. %d, %Y %I:%M %p")
				else:
					mydict['version'] = k
					mydict['path'] = v

			data.append(mydict)


	return render(request, "workflow_docs_for_version.html", {'req_value':data,'selected_doc':selected_doc})


def download_version_file(request,path):

	print('path',path)
	file_path = path.replace('media','')
	filename = file_path.split('/')[-1]
	only_filename = filename.split('.')[0]
	full_path = settings.MEDIA_ROOT + '/' + file_path
	path_only = os.path.dirname(full_path)
	print('full_path',full_path)
	fl = open(full_path, 'rb')
	b2 = bytes(fl.read())
	decoded = Cryptographer.decrypted(b2)
	decoded_file = ContentFile(decoded)
	# if os.path.isfile(os.path.join(path_only + '/' + only_filename + '_decrypted.tiff')):
	# 	os.unlink(os.path.join(path_only + '/' + only_filename + '_decrypted.tiff'))
	s = io.BytesIO(decoded)
	# default_storage.save(os.path.join(path_only + '/' + only_filename + '_decrypted.tiff'), decoded_file)
	# f2 = open(os.path.join(path_only + '/' + only_filename + '_decrypted.tiff'), 'rb')
	mime_type, _ = mimetypes.guess_type(filename)
	response = HttpResponse(s.getvalue(), content_type=mime_type)
	response['Content-Disposition'] = "attachment; filename=%s" % filename

	return response

#def workflow_docs_for_version(request):
	#primary_number = request.GET.get("workflow_number")
	#selected_doc = request.GET.get("message")
	#workflow_number = ScannedImagesUserData.objects.get(pk=primary_number)
	#all_versions = workflow_number.versions_field
	#req_value = ''
	#for value in all_versions:
	#	for i, j in value.items():
	#		if selected_doc == i:
	#			req_value = j
	#return render(request, "workflow_docs_for_version.html", {'req_value':req_value})




# API for data integration in MIgration -- Added for COSMMOS Bank


import shutil, os, glob
 
def moveAllFilesinDir(srcDir, dstDir):
    # Check if both the are directories
    if os.path.isdir(srcDir) and os.path.isdir(dstDir) :
        # Iterate over all the files in source directory
        for filePath in glob.glob(srcDir + '\*'):
            # Move each file to destination Directory
            filenam2 = os.path.basename(filePath)
            print(filenam2, "filenam2")
            if os.path.isfile(os.path.join(dstDir, filenam2)):
                os.remove(os.path.join(dstDir, filenam2))
            shutil.move(filePath, dstDir)
    else:
        print("srcDir & dstDir should be Directories")    


@csrf_exempt
def ajax_call_for_files(request):

    from zipfile import ZipFile
    import time
    import shutil
    if request.method == "POST":
        l_acc_no = request.POST.get('l_acc_no')
        l_acc_type = request.POST.get('l_acc_type')
        #data = request.POST.get('data')
        files_1 = request.FILES.get('files')
        
        print('files', files_1, l_acc_type, l_acc_no)
        
        temp_dir = os.path.join(settings.MEDIA_ROOT +  '/Migration_Media')
        default_storage.save(os.path.join(temp_dir + '/'+ files_1.name), files_1)
        
        files_1_name, files_1_extension = os.path.splitext(files_1.name)
        print(files_1_name, files_1_extension)
        
        # Temporary File Folder
        o_temp_dir = os.path.join(settings.MEDIA_ROOT + '/Migration_Media/' +  files_1.name)
        
        print(o_temp_dir)
        
        if files_1_extension == '.zip':
            with ZipFile(o_temp_dir, 'r') as zipObj:
                for member in zipObj.infolist():
                    if member.filename[-1] == '/':
                        continue
                    member.filename = os.path.basename(member.filename)
                    
                    zipObj.extract(member, os.path.join(settings.MEDIA_ROOT + '/Migration_Media/' + l_acc_no + '/' +  l_acc_type ))
                # zipObj.extractall(os.path.join(settings.MEDIA_ROOT + '/Migration_Media/' + l_acc_no + '/' +  l_acc_type ))
                time.sleep(1)
        else:
            target_dir = os.path.join(settings.MEDIA_ROOT + '/Migration_Media/' + l_acc_no + '/' +  l_acc_type )
            print(target_dir)
            if not os.path.isdir(target_dir):
                try:
                    os.makedirs(target_dir)
                except OSError as exc:
                    print('Not able to make the directory')   
            shutil.copy(o_temp_dir, target_dir)
        
        #removing the File after extraction
        try:
            if o_temp_dir:
                os.remove(o_temp_dir)
        except OSError as exp:
            print('No Temp File present')

        queryset = NewWorkflowManagement.objects.get(pk='1')
        user_data = User.objects.get(id=1)
        
        workflow_no = ''
        try:
            latest_id = UserDataWorkflowManagement.objects.latest('id')
            old_workflow_no = latest_id.workflow_number
            if old_workflow_no:
                letter_WF, new_workflow_no = old_workflow_no.split('F')
                trailing_value = str(int(new_workflow_no) + 1).zfill(10)
                # # print(trailing_value, 'trailing_value')
                workflow_no = 'WF' + trailing_value

        except UserDataWorkflowManagement.DoesNotExist:
            workflow_no = 'WF0000000001'
        # customer_id = 'ACS_' + str(random.randint(9, 100000))
        check_user_workflow = UserDataWorkflowManagement.objects.filter(account_number=l_acc_no)
        if check_user_workflow:
            print("Already have data in UserDataWorkflowManagement")
            for i in check_user_workflow:
                wf_id = i.pk
        else:
            date2 = datetime.now()
            user_workflow = UserDataWorkflowManagement(workflow_id=queryset,workflow_number=workflow_no, entry_date_time=date2, user=user_data, account_number=l_acc_no)
            user_workflow.save()
            wf_id = user_workflow.pk
        
        path_new_1 = os.path.join( settings.MEDIA_ROOT + '/Scanned_Data/' + l_acc_no + '/' + l_acc_type + '/')  # Path to save the customer data in back folder

        if not os.path.isdir(path_new_1):
            print('path not exists')
            try:
                os.makedirs(path_new_1)
            except OSError as exc:  # Guard against race condition
                if exc.errno != errno.EEXIST:
                    raise
        
        old_migrated_path = os.path.join(settings.MEDIA_ROOT + '/Migration_Media/' + l_acc_no + '/' +  l_acc_type + '/')

        new_migrated_path = path_new_1

        moveAllFilesinDir(old_migrated_path, new_migrated_path)
        time.sleep(2)
        
        # default_storage.save(path_new, file_content)  # storing the image using default storage
        # file_name = default_storage.save(os.path.join(path_new_1 + files_1.name), files_1)

        image_object = []        
        image_json = {}
        for f in os.listdir(new_migrated_path):
            print('DONE', f)

            # Encrypting the file side by side
            file1 = open(os.path.join(new_migrated_path, f), 'rb')
            b = bytes(file1.read())
            encoded = Cryptographer.encrypted(b)  # Cryptographer used to encrypt and decrypt the files
            file1.close()
            
            if os.path.isfile(os.path.join(new_migrated_path, f)):
                os.remove(os.path.join(new_migrated_path, f))
            encoded_file = ContentFile(encoded)
            default_storage.save(os.path.join(new_migrated_path, f), encoded_file)
            print('File is encrypted')

            if os.path.isfile(os.path.join(new_migrated_path, f)):
                print('Inside isfile', f)

                filenam2 = f.replace("\\" , "")
                print(filenam2, "filenam2")
                media_path = os.path.join('/media/Scanned_Data/' + l_acc_no + '/' + l_acc_type)
                image_json.update( {str(l_acc_type + '-' + filenam2): os.path.join(media_path, f)}) 


        image_object.append(image_json)

        # image_json = {files_1.name: os.path.join(path_new_1 + files_1.name)}
        print('new_migrated_path', image_object)
        
        workflow_number_queryset = ScannedImagesUserData.objects.filter(account_number=l_acc_no, account_type=l_acc_type)
        print('workflow_number_queryset', workflow_number_queryset)

        user_existing_data = UserDataWorkflowManagement.objects.get(pk=wf_id)

        if workflow_number_queryset:
            print('Already have data')
            for set in workflow_number_queryset:
                user_data = set.user_scanned_data
            for k,v in image_json.items():
                for d in user_data:
                    d[k] = v
            workflow_number_queryset.update(user_scanned_data = user_data)
        else:
            print('in else of ScannedImagesUserData')
            ScannedImagesUserData.objects.create(workflow_number=user_existing_data, user_scanned_data = image_object, is_encrypted=True, account_type=l_acc_type, account_number=l_acc_no)
        
        # Making the data available in table
        existing_data = UserDataWorkflowManagement.objects.filter(account_number=l_acc_no)
        for e_data in existing_data:
            e_data.documents = True
            e_data.save()


        return JsonResponse({'data': "Data Matched"})
    else:
        return JsonResponse({'data': "Not A Post request"})




