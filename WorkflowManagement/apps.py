from django.apps import AppConfig


class WorkflowmanagementConfig(AppConfig):
    name = 'WorkflowManagement'
