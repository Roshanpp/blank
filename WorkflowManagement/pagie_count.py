from django import template

register = template.Library()

import os
from django.conf import settings
from PIL import Image
from DMSapp.crypt import Cryptographer
from django.core.files.base import ContentFile



def pagie_count(path):
    ext = path.split('.')[-1]
    if ext == 'tiff' or ext == 'tif' or ext == 'TIF' or ext == 'TIFF':
        tiff_path = os.path.join(settings.BASE_DIR + "/DMSapp" + path)
        if os.path.exists(tiff_path):
            
            file2 = open(tiff_path, 'rb')
            b2 = bytes(file2.read())
            decoded = Cryptographer.decrypted(b2)
            decoded_file = ContentFile(decoded)
            
            file4 = decoded_file.open('rb')

            img = Image.open(file4)
            no = img.n_frames
            return no
    if ext == 'jpeg' or ext == 'jpg' or ext == 'png':
        return 1
    else:
        return 0

register.filter('pagie_count', pagie_count)
