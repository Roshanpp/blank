from django.shortcuts import render
from django.core.mail import send_mail

from .forms import ContactForm
from django.conf import settings


def contact(request):
    form = ContactForm(request.POST or None)
    title = 'Contact'
    context = {'title': title, 'form': form}
    if form.is_valid():
        name = form.cleaned_data['name']
        comment = form.cleaned_data['comment']
        subject = 'Email from user'
        message = '%s %s' % (comment, name)
        emailTo = [settings.EMAIL_HOST_USER]
        emailFrom = form.cleaned_data['email']
        send_mail(subject, message, emailFrom, emailTo, fail_silently=False)
        title = 'Thanks'
        confirm_message = 'Thanks for your mail'
        form = None

        context = {'title': title, 'form': form, 'confirm_message':confirm_message}
    return render(request, 'contact.html', context)

    '''return render(request, 'contact.html', context)
else:
form = ContactForm()
return render(request, 'contact.html', context)'''








