from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import url, include
from django.urls import path

from . import views

urlpatterns = [
    
    path('toggle_security/<int:id>', views.toggle_security, name='toggle_security'),
    url(r'^logout/', views.user_logout, name='logout'),
    url(r'^profile/$', views.user_profile, name='user_profile'),
    path('profile/<username>/', views.user_profile, name='user_profile'),
    path('profile/edit/<int:pk>', views.edit_profile, name='edit_profile'),

    path('all_users', views.all_users, name='all_users'),
    path('all_users/<username>/', views.delete_users, name='delete_users'),
    path('userprofile_del/<int:pk>', views.delete_users, name='deactivate_user_view'),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', views.activate,
        name='activate'),

    path('security/<int:id>/', views.select_security, name='select_security'),
    url(r'security/', views.security, name='select_security'),
    #path('toggle_security/<int:id>', views.toggle_security, name='toggle_security'),
    path('signup_complete', views.signup_complete, name='signup_complete'),
    path('signup_completed', views.signup_completed, name='signup_completed'),

    url(r'^reset_password/', views.reset_password, name='reset'),
    url(r'^password_reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', views.reset_token,
        name='reset_token'),
    url(r'password_reset_complete', views.password_reset_complete, name='password_reset_complete'),


    path('administration', views.administration, name='administration'),
    path('administration/user_roles', views.user_roles, name='user_roles'),
    path('settings', views.dms_settings, name='dms_settings'),

    #validations
    url(r'^validate_username/$', views.validate_username, name='validate_username'),
    url(r'^validate_email/$', views.validate_email, name='validate_email'),
    url(r'^validate_pass/$', views.validate_pass, name='validate_pass'),
    path('snataken/', views.snataken, name='snataken'),
    path('snatakenforlogin/', views.snatakenforlogin, name='snatakenforlogin'),
    path('camera/<user>/<forType>/', views.camera, name='camera'),
    path('camera2/<user>/', views.camera2, name='camera2'),
    path('page_records_setting', views.page_records_setting, name='page_records_setting'),


    path('administration/user_roles/change_role_for_user/<int:user_id>', views.change_role_for_user, name='change_role_for_user'),
    path('change_password',views.change_password,name='change_password'),
    path('check_user_password/',views.check_user_password,name='check_user_password'),

    path('confirm_login_if_stored_session/<session>', views.check_stored_session, name='check_stored_session'),
    path('user_logout_on_confirm', views.user_logout_on_confirm, name='user_logout_on_confirm'),


]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

urlpatterns += [url(r'^captcha/', include('captcha.urls')), ]
