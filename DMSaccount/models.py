from django.db import models
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.db.models.signals import post_save
from django.contrib.auth.models import AbstractUser
from django.utils import timezone
from django.dispatch import receiver
import uuid
from django.core.exceptions import ValidationError
import re



class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, username, email, password, is_staff, is_admin, is_active, is_superuser, **extra_fields):
        now = timezone.now()
        if not username:
            raise ValueError('Username must be set')
        email = self.normalize_email(email)
        user = self.model(username=username, email=email, is_staff=is_staff, is_admin=is_admin, is_active=is_active,
                          is_superuser=is_superuser, date_joined=now, **extra_fields)

        user.set_password(password)
        user.save(self._db)
        return user

    def create_user(self, username, email, password, **extra_fields):
        return self._create_user(username, email, password, False, False, True, False, **extra_fields)

    def create_superuser(self, username, email, password, **extra_fields):
        return self._create_user(username, email, password, True, True, True, True, **extra_fields)


class User(AbstractUser):
    is_manager = models.BooleanField(default=False, blank=True, null=True)
    is_checker = models.BooleanField(default=False, blank=True, null=True)
    is_maker = models.BooleanField(default=False, blank=True, null=True)
    

    def __str__(self):
        return self.username
        
        
class UserSetting(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    theme = models.CharField(max_length=60, default="Default")
    page_records = models.IntegerField(default=5)

    def __str__(self):
        return self.user.username


def checkmobile(value):
    Pattern = re.compile("(0/91/022)?[7-9][0-9]{9}")

    if not Pattern.match(value):
        raise ValidationError("Invalid Mobile Number")


class Profile(models.Model):
    username = models.OneToOneField(User, on_delete=models.CASCADE)
    description = models.CharField(max_length=100, default='', blank=True, null=True)
    city = models.CharField(max_length=100, default='', blank=True, null=True)
    website = models.URLField(default='', blank=True, null=True)
    phone = models.CharField(max_length=10, validators=[checkmobile], blank=True, null=True)
    image = models.ImageField(upload_to='profile_image', blank=True)
    birth_date = models.DateField(null=True, blank=True)
    email = models.EmailField(null=True, blank=True, unique=True)
    Designation = models.CharField(max_length=20, blank=True, null=True)
    Employee_Code = models.CharField(max_length=20, blank=True, null=True)
    Department = models.CharField(max_length=20, blank=True, null=True)


    # Default Permission
    is_staff = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)

    date_joined = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.username.username

#def create_user_profile(sender, instance, created, **kwargs):
#    if created:
#        Profile.objects.create(username=instance)

#post_save.connect(create_user_profile, sender=User)



class Comment(models.Model):
    text = models.TextField(max_length=1000)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = 'comment'
        verbose_name_plural = 'comments'


class Security(models.Model):
    security_option = models.CharField(max_length=20)
    is_activate = models.BooleanField(default=False)
    description = models.CharField(max_length=200, default=False)

    def __str__(self):
        return str(self.id)


class FaceImages(models.Model):
    username = models.CharField(unique=True,null=False,max_length=100)
    Image1 = models.FileField(upload_to='MyImages')

    def __str__(self):
        return self.username


class LoginImages(models.Model):
    username = models.CharField(unique=False,null=False,max_length=100)
    Image1 = models.FileField(upload_to='LogImages')

    def __str__(self):
        return self.username

    def delete(self, *args, **kwargs):
        self.Image1.delete()
        super().delete(*args, **kwargs)


# Model to store the list of logged in users
class LoggedInUser(models.Model):
    user = models.OneToOneField(User, related_name='logged_in_user', on_delete=models.CASCADE)
    # Session keys are 32 characters long
    session_key = models.CharField(max_length=32, null=True, blank=True)

    def __str__(self):
        return str(self.user)
