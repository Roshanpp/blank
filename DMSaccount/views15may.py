from django.shortcuts import render,  redirect, HttpResponseRedirect, HttpResponse, get_object_or_404
from django.contrib.auth.decorators import user_passes_test, login_required
from django.urls import reverse
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.models import User
from django.contrib import messages
from django.http import JsonResponse
from django.core.exceptions import PermissionDenied
from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes, force_text
from django.core.mail import EmailMessage, send_mail
from django.forms.models import inlineformset_factory
from django.conf import settings
from django.views.decorators.cache import never_cache
from django.core import serializers
from django.core.paginator import Paginator,PageNotAnInteger
from django.views.decorators.csrf import csrf_exempt
from django.utils.translation import gettext as _

import json
import urllib
import http.client

from .tokens import account_activation_token, reset_activation_token
from .forms import PhoneForm, SignUpForm, CaptchaTestForm, DeactivateUserForm, SecurityForm, EditProfileForm, ProfileInlineForm, ResetPasswordForm
from .models import Profile, Security
from .security import admin_security, captcha_enbales
import cv2
import numpy
import PIL
import gzip
from pathlib import Path



@never_cache
def signup(request):
	if request.method == 'POST':
		form = SignUpForm(request.POST)
		profile_form = PhoneForm(request.POST)
		if form.is_valid() and profile_form.is_valid():
			user = form.save(commit=False)
			user.is_active = False  #Make it False if you dont want user to activate his account at first
			user.email = profile_form.cleaned_data.get('email')

			profile=profile_form.save(commit=False)
			user.save()
			current_site = get_current_site(request)
			mail_subject = 'Activate your blog account.'
			message = render_to_string('acc_active_email.html', {
				'user': user,
				'domain': current_site.domain,
				'uid': urlsafe_base64_encode(force_bytes(user.pk)).decode(),
				'token': account_activation_token.make_token(user),
			})
			to_email = profile_form.cleaned_data.get('email')
			send_mail(mail_subject, message, settings.EMAIL_HOST_USER, [to_email],
					  fail_silently=False)

			profile.username=user
			if 'image' in request.FILES:
				profile.image = request.FILES['image']
			profile.save()

			messages.success(request, 'Your profile has been created successfully.!!')
			return HttpResponseRedirect(reverse('signup'))
			# return JsonResponse({'success': True})
			# return HttpResponseRedirect(reverse('DMSaccount:signup_complete'))

	else:
		form = SignUpForm()

		x, y = form.fields["password1"], form.fields["password2"]
		x.label, y.label = _("Password"), _("Confirm Password")
		profile_form = PhoneForm()

		z = profile_form.fields["image"]
		z.label = _("Profile Pic")

	return render(request, 'signup.html', {'form': form,'profile_form':profile_form})


def signup_complete(request):
	return render(request, 'signup_complete.html')

def signup_completed(request):
	return render(request, 'signup_completed.html')


def activate(request, uidb64, token):
	try:
		uid = force_text(urlsafe_base64_decode(uidb64))
		user = User.objects.get(pk=uid)
		print(user)
	except(TypeError, ValueError, OverflowError, User.DoesNotExist):
		user = None
	if user is not None and account_activation_token.check_token(user, token):
		user.is_active = True
		user.save()
		return HttpResponseRedirect(reverse('DMSaccount:signup_completed'))
		# return HttpResponse('Thank you for your email confirmation. Now you can login your account.')
	else:
		print(force_text(urlsafe_base64_decode(uidb64)))
		return HttpResponse('Activation link is invalid!')


@never_cache
def login1(request):
	login_security = admin_security()
	if request.user.is_authenticated:
		return HttpResponseRedirect(reverse('home'))
	if request.method == 'POST':
		username = request.POST.get('username')
		password = request.POST.get('password')
		user = authenticate(username=username, password=password)
		if user is not None:
			if user.is_active:
				if login_security == 1:
					login(request, user)

				elif login_security == 2:
					form = CaptchaTestForm(request.POST)
					if form.is_valid():
						human = True
						login(request, user)
					else:
						messages.error(request, 'Invalid reCAPTCHA. Please try again.')
						return redirect('user_login')

				elif login_security == 3:
					var = captcha_enbales(request)
					if var is True:
						login(request, user)
					else:
						return redirect('user_login')

				elif login_security == 4:
					userId = user.pk
					return HttpResponseRedirect(reverse('send_otp', args=[userId]))

				elif login_security == 5:
					var = request.POST.get('success')
					var1=request.POST.get('sum')
					if var == var1:
						login(request, user)
					else:
						messages.error(request, '"Invalid Captcha token" . Please try again.')
						return redirect('user_login')

				return HttpResponseRedirect(reverse('home'))
			else:
				return HttpResponse("Your account was inactive.")
		else:
			messages.error(request, 'Invalid Login Credentials Given. Please try again.')
			return redirect('user_login')
	else:
		form = CaptchaTestForm()
		return render(request, 'login.html', locals(), {' login_security': login_security})


def enter_otp(request, id, mobile_no):
	return render(request, 'users/otppage.html', {'userid': id, 'mobileNo': mobile_no})


def verify_otp(request, id, mobile_no):
	auth = settings.OTP_AUTH_KEY

	if request.method == "POST":
		enteredOtp = request.POST.get('otp')
		conn = http.client.HTTPSConnection("control.msg91.com")
		payload = ""
		headers = {'content-type': "application/x-www-form-urlencoded"}
		conn.request("POST", "/api/verifyRequestOTP.php?authkey="+auth+"&mobile="+mobile_no+"&otp=" + enteredOtp+"",
					 payload, headers)
		res = conn.getresponse()
		data = res.read()
		ans = json.loads(data.decode("utf-8"))

		if ans['type'] == 'success':
			print(ans['type'])
			for e in Profile.objects.filter(username_id=id):
				username = e.username
				print(username)
				login(request, username)
				return HttpResponseRedirect(reverse('home'))

		elif ans['type'] == 'error':
			return render(request, 'users/invalidotp.html', {'userid': id, 'mobileNo': mobile_no})
	else:
		return render(request, 'users/otppage.html', {})


def send_otp(request, id):
	auth = settings.OTP_AUTH_KEY
	otpexp = 5
	email_id = ''
	user = Profile.objects.get(username_id=id)
	for v in User.objects.filter(username=user):
		email_id = v.email


	for m in Profile.objects.filter(username_id=id):
		mobile_no=m.phone

	conn = http.client.HTTPConnection("control.msg91.com")
	payload = ""
	print(email_id)
	print(user)
	conn.request("POST", "/api/sendotp.php?template=&otp_length=&authkey="+auth+"&message=&sender=&mobile=" + str(mobile_no) +
				 "&otp=&otp_expiry="+str(otpexp)+"&email="+email_id+"", payload)
	res = conn.getresponse()
	data = res.read()
	return HttpResponseRedirect(reverse('enterOTP', args=[id, mobile_no]))


def resend_otp(request, id, mobile_no):
	auth = '264199AaxLWEo00P5c6ecde4'
	otpexp = 5
	email_id = ''
	user = Profile.objects.get(username_id=id)
	for v in User.objects.filter(username=user):
		email_id = v.email


	conn = http.client.HTTPConnection("control.msg91.com")
	payload = ""
	conn.request("POST", "/api/sendotp.php?template=&otp_length=&authkey="+auth+"&message=&sender=&mobile=" +
				 str(mobile_no)+"&otp=&otp_expiry="+str(otpexp)+"&email="+email_id+"", payload)
	res = conn.getresponse()
	data = res.read()
	return HttpResponseRedirect(reverse('enterOTP', args=[id, mobile_no]))


def user_profile(request, username=None):
	print(username)
	if username:
		profile = Profile.objects.get(username__username=username)
		user = User.objects.get(username=username)
		user_last_login = user.last_login
		date_joined = user.date_joined
	else:
		user = request.user
		profile = Profile.objects.get(username=user)
		user_last_login = user.last_login
		date_joined = user.date_joined
	args = {'last_login': user_last_login, 'date_joined': date_joined, 'profile': profile, 'user': user}
	return render(request, 'users/user_profile.html', args)


def all_users(request):
	users = User.objects.all()
	qs_json = serializers.serialize('json', users)
	qs_json=json.loads(qs_json)

	page = request.GET.get('page',1)
	paginator = Paginator(qs_json, 10)
	page_records=paginator.count

	count=0

	for i in qs_json:
		count+=1
		i['fields']['srNo']=str(count)

	print(qs_json)
	try:
		qs_json = paginator.page(page)

	except PageNotAnInteger:
		qs_json = paginator.page(1)
	except EmptyPage:
		qs_json = paginator.page(paginator.num_pages)
	print(page_records)
	print(qs_json)
	return render(request, 'users/All_users.html', {'files': qs_json})


def change_password(request):
	if request.method == 'POST':
		form = PasswordChangeForm(request.user, request.POST)
		if form.is_valid():
			user = form.save()
			update_session_auth_hash(request, user)  # Important!
			messages.success(request, 'Your password was successfully updated!')
			return redirect('home')
		else:
			messages.error(request, 'Please correct the error below.')
	else:
		form = PasswordChangeForm(request.user)
	return render(request, 'password.html', {
		'form': form
	})


def user_logout(request):
	logout(request)
	return HttpResponseRedirect(reverse('user_login'))


def admin(request):
	if request.user.is_superuser:
		return render(request, 'admin.html')
	else:
		HttpResponse("Please login")


@login_required
def edit_profile(request, pk):
	user = User.objects.get(pk=pk)
	profile = Profile.objects.get(username__username=user)
	form = EditProfileForm(instance=user)

	form_set = ProfileInlineForm(instance=profile)

	if request.user.is_authenticated:
		if request.method == "POST":
			form = EditProfileForm(request.POST, request.FILES, instance=user)
			form_set = ProfileInlineForm(request.POST, request.FILES, instance=profile)

			if form.is_valid():
				created_user = form.save(commit=False)

				if form_set.is_valid():
					created_user.save()
					form_set.save()
				messages.success(request, 'Profile Updated Successfully..!!')

		return render(request, 'users/edit_profile.html', {'id': pk, 'form': form, 'form_set': form_set})

	else:
		HttpResponse('You have been logged out. Please login again')


@user_passes_test(lambda u: u.is_superuser)
def delete_users(request,  pk):
	try:
		u = User.objects.get(id=pk)
		u.delete()
		print('user deleted')

	except User.DoesNotExist:
		messages.error(request, "User doesnot exist")
		return render(request, 'users/All_users.html')

	except Exception as e:
		return render(request, 'users/All_users.html', {'err': e.message})

	return HttpResponseRedirect(reverse('DMSaccount:all_users'))


@login_required(login_url='login/')
def deactivate_user_view(request, pk):
	user = User.objects.get(pk=pk)
	user_form = DeactivateUserForm(instance=user)
	if request.user.is_superuser:
		if request.method == "POST":
			user_form = DeactivateUserForm(request.POST, instance=user)
			if user_form.is_valid():
				deactivate_user = user_form.save(commit=False)
				user.is_active = False
				deactivate_user.save()
		return render(request, "userprofile_del.html", {"user_form": user_form})
	else:
		raise PermissionDenied

####################################################################################################


def security(request):
	all_security = Security.objects.all()
	return render(request, 'security.html', {'security': all_security})


def select_security(request, id):
	select = Security.objects.get(id=id)
	form = SecurityForm(request.POST, instance=select)
	if request.method == 'POST':
		if form.is_valid():
			form.save()
			return redirect('home')
	else:
		form = SecurityForm()
	return render(request, 'security_change.html', {'form': form})



'''Forget Password'''


def reset_password(request):
	data = []
	if request.method == 'POST':
		email = request.POST.get('email')
		email_ids = Profile.objects.all()
		for e in email_ids:
			value = e.email
			data.append(value)
		if email in data:
			current_site = get_current_site(request)
			mail_subject = 'Reset Your Password'
			message = render_to_string('registrations/password_reset_email.html', {
				'user': email,
				'domain': current_site.domain,
				'uid': urlsafe_base64_encode(force_bytes(email)).decode(),
				'token': reset_activation_token.make_token(email),
				'protocol': 'https'
			})

			send_mail(mail_subject, message, settings.EMAIL_HOST_USER, [email],
					  fail_silently=False)

			return render(request, 'registrations/password_reset_done.html')

		messages.error(request, 'There is no profile with this email ID')
		return HttpResponseRedirect(reverse('DMSaccount:reset'))
	else:
		return render(request, 'registrations/password_email_confirm.html')


def reset_token(request, uidb64, token):
	try:
		uid = force_text(urlsafe_base64_decode(uidb64))
		value = Profile.objects.filter(email=uid)
		for v in value:
			email = v.email

	except(TypeError, ValueError, OverflowError, Profile.DoesNotExist):
		email = None
	if email is not None and reset_activation_token.check_token(email, token):
		validlink = True
		uid = force_text(urlsafe_base64_decode(uidb64))
		email_id = User.objects.get(email=uid)
		if request.method == 'POST':
			form = ResetPasswordForm(request.POST, instance=email_id)
			if form.is_valid():
				pasword = form.cleaned_data.get('password1')
				form.save()
				messages.success(request, 'Your password has been changed successfully. You can')
				# return HttpResponseRedirect(reverse('DMSaccount:password_reset_complete'))
		else:
			form = ResetPasswordForm(instance=email_id)
		return render(request, 'registrations/password_reset_confirm.html', {'validlink': validlink, 'form': form})
	else:
		return HttpResponse('Activation link is invalid!')


def password_reset_complete(request):
	return render(request, 'registrations/password_reset_complete.html')


'''Validations in Signup'''


def validate_username(request):
    username = request.GET.get('username', None)
    data = {
        'is_taken': User.objects.filter(username__iexact=username).exists()
    }
    return JsonResponse(data)
def validate_email(request):
    email = request.GET.get('email', None)
    data = {
        'is_taken': User.objects.filter(email__iexact=email).exists()
    }
    return JsonResponse(data)

DEFAULT_PASSWORD_LIST_PATH = Path(__file__).resolve().parent.parent.parent / 'common-passwords1.txt.gz'


@csrf_exempt
def validate_pass(request, password_list_path=DEFAULT_PASSWORD_LIST_PATH):
    password=request.POST.get('password1',None)
    try:
        with gzip.open(str(password_list_path)) as f:
            common_passwords_lines = f.read().decode().splitlines()
    except IOError:
        with open(str(password_list_path)) as f:
            common_passwords_lines = f.readlines()

    passwords_list = {p.strip() for p in common_passwords_lines}

    data = {
        'is_common': False,
         'is_number':False,
         'is_upper_lower':False
          }
    if password.lower().strip() in passwords_list:
        data['is_common']=True
    if  any(letter.islower() for letter in password) and any(letter.isupper() for letter in password) :
        data['is_upper_lower']=True
    if any(char.isdigit() for char in password):
        data['is_number'] =True
    return JsonResponse(data)
