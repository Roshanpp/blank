from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.core import validators
from .models import Profile, Security
import re
from captcha.fields import CaptchaField

import re
class SignUpForm(UserCreationForm):
    username = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Username'}))
    password1 = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control', 'placeholder':'Password'}))
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control', 'placeholder':' Confirm Password'}))
    first_name = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'First Name'}))
    last_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':'Last Name'}))

    class Meta:
        model = User
        fields = ('username', 'password1', 'password2', 'first_name', 'last_name')



def checkmobile(value):
    Pattern = re.compile("(0/91/022)?[7-9][0-9]{9}")

    if not Pattern.match(value):
        raise forms.ValidationError("Invalid Mobile Number")


class PhoneForm(forms.ModelForm):
    birth_date = forms.DateField(input_formats=['%d-%m-%Y'],widget=forms.DateTimeInput(attrs={
            'class': 'datetimepicker-input form-control',
            'data-target': '#datetimepicker1',
            'id':"yz",
            'placeholder':'Birth Date'
        }))

    email = forms.CharField(widget=forms.EmailInput(attrs={'class':'form-control', 'placeholder':'Email'}))
    # image = forms.CharField(widget=forms.FileInput(attrs={'class':'form-control lbl_image', 'placeholder':'Image'}))

    phone = forms.CharField(required=True, help_text='Required',validators=[checkmobile],
                            widget=forms.NumberInput(attrs={'class': 'form-control','placeholder':'Mobile Phone'}))
    class Meta:
        model = Profile
        fields = ('phone', 'birth_date', 'email','image')


class EditProfileForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('email', 'first_name', 'last_name',)


class CaptchaTestForm(forms.Form):
    captcha = CaptchaField()


class DeactivateUserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['is_active']


class SecurityForm(forms.ModelForm):
    class Meta:
        model = Security
        fields = ('is_activate', )
