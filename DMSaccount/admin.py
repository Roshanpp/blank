from django.contrib import admin
from django import forms
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from ajax_select import register, LookupChannel
from ajax_select.fields import AutoCompleteSelectField, AutoCompleteSelectMultipleField
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from .models import Profile, Security, FaceImages, LoginImages, User, LoggedInUser

class UserCreationForm(forms.ModelForm):
    """A form for creating new users. Includes all the required
    fields, plus a repeated password."""
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ('is_manager', 'is_checker', 'is_maker')

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField(label= ("Password"),
      help_text= ("Raw passwords are not stored, so there is no way to see "
                  "this user's password, but you can change the password "
                  "using <a href=\"../password/\">this form</a>."))

    class Meta:
        model = User
        fields = ('email', 'password', 'is_active', 'is_manager', 'is_checker', 'is_maker')

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]


class UserAdmin(ImportExportModelAdmin,admin.ModelAdmin):
    # The forms to add and change user instances
    form = UserChangeForm
    add_form = UserCreationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('username','first_name','last_name','email','date_joined','last_login')
    list_filter = ('is_active', 'is_staff', 'is_manager')
    list_per_page = 6
    fieldsets = (
        (None, {'fields': ('username',  'password')}),
        ('Personal info', {'fields': ('first_name', 'last_name', 'email',)}),
        ('Login Details', {'fields': ('last_login', 'date_joined')}),
        ('Permissions', {'fields': ('is_superuser', 'is_active', 'is_staff', 'is_manager', 'is_checker', 'is_maker')}),

    )
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'password1', 'password2')}
        ),
    )
    search_fields = ('username',)
    ordering = ('username',)
    filter_horizontal = ()

class ProfileAdmin(ImportExportModelAdmin,admin.ModelAdmin):
    list_display =('username','description','city','website','phone','image','birth_date','email','Designation','Employee_Code','Department')
    search_fields = ['description','city','phone','email','Designation','Employee_Code']
    list_filter =('username','description','city','email','Designation',)
    #list_editable =('city',)
    list_per_page =6




class FaceImageAdmin(ImportExportModelAdmin,admin.ModelAdmin):
    list_display =('username','Image1')
    search_fields =('username',)
    list_per_page = 6
    list_filter=('username','Image1')

class SecurityAdmin(ImportExportModelAdmin,admin.ModelAdmin):
    list_display = ('security_option','is_activate','description')
    search_fields = ('security_option',)
    list_per_page = 6
    list_filter = ('security_option','is_activate','description')

class LoginImageAdmin(ImportExportModelAdmin,admin.ModelAdmin):
    list_display = ('username','Image1')
    search_fields = ('username',)
    list_per_page = 6
    list_filter = ('username','Image1')

class LoggedInUserAdmin(ImportExportModelAdmin,admin.ModelAdmin):
    list_display = ('user',)
    search_fields = ('user',)
    list_per_page = 6
    list_filter = ('user',)


admin.site.register(User, UserAdmin)
admin.site.register(Profile,ProfileAdmin)
admin.site.register(Security,SecurityAdmin)
admin.site.register(FaceImages,FaceImageAdmin)
admin.site.register(LoginImages,LoginImageAdmin)
admin.site.register(LoggedInUser,LoggedInUserAdmin)
