from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.core.exceptions import ValidationError
from django.core import validators
from .models import Profile, Security
from captcha.fields import CaptchaField
from django.utils.translation import ugettext_lazy
from django.contrib.auth import get_user_model
import os
from DMSapp.choices import PAGE_RECORD_CHOICES

User = get_user_model()

import re


alphanumeric = validators.RegexValidator(r'^[0-9a-zA-Z]*$', 'Only alphanumeric characters are allowed.')


class SignUpForm(UserCreationForm):
    username = forms.CharField(label=ugettext_lazy('Username'),validators=[alphanumeric], widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Username','autocomplete':'off'}))
    password1 = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control', 'placeholder':'Password','autocomplete':'off'}))
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control', 'placeholder':' Confirm Password','autocomplete':'off'}))
    first_name = forms.CharField(label=ugettext_lazy('First Name'),widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'First Name'}))
    last_name = forms.CharField(label=ugettext_lazy('Last Name'),widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder':'Last Name'}))

    class Meta:
        model = User
        fields = ('username', 'password1', 'password2', 'first_name', 'last_name')



def checkmobile(value):
    Pattern = re.compile("(0/91/022)?[7-9][0-9]{9}")

    if not Pattern.match(value):
        raise forms.ValidationError("Invalid Mobile Number")


class PhoneForm(forms.ModelForm):
    birth_date = forms.DateField(label=ugettext_lazy('Birth Date'),input_formats=['%d-%m-%Y'],widget=forms.DateTimeInput(attrs={
            'class': 'datetimepicker-input form-control',
            'data-target': '#datetimepicker1',
            'id':"yz",
            'placeholder':'Birth Date'
        }))

    email = forms.CharField(label=ugettext_lazy('Email'),widget=forms.EmailInput(attrs={'class':'form-control', 'placeholder':'Email'}))
    phone = forms.CharField(label=ugettext_lazy('Mobile No.'),required=True, help_text='Required',validators=[checkmobile],
                            widget=forms.TextInput(attrs={'class': 'form-control numeric','placeholder':'Mobile Phone',
                                                            'id':'phone'}))
    class Meta:
        model = Profile
        fields = ('phone','birth_date','email', 'image')
        widgets={
             'image': forms.FileInput(attrs={'class':'form-control lbl_image'})}


class EditProfileForm(forms.ModelForm):
    first_name = forms.CharField(label=ugettext_lazy('First Name'),widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'first_name'}))
    last_name = forms.CharField(label=ugettext_lazy('Last Name'),widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'last_name'}))

    class Meta:
        model = User
        fields = ('first_name', 'last_name',)


class ProfileInlineForm(forms.ModelForm):
    email = forms.CharField(label=ugettext_lazy('Email'),widget=forms.EmailInput(attrs={'class': 'form-control'}))
    phone = forms.CharField(label=ugettext_lazy('Mobile No.'),required=True, validators=[checkmobile],
                            widget=forms.TextInput(attrs={'class': 'form-control numeric', 'placeholder': 'Mobile Phone',
                                                            'id': 'phone'}))
    remove_photo = forms.BooleanField(label=ugettext_lazy('Remove Photo'),required=False)
    Employee_Code = forms.CharField(label=ugettext_lazy('Employee Code'),widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Employee_Code'}))
    Department = forms.CharField(label=ugettext_lazy('Department'),widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Department'}))
    Designation = forms.CharField(label=ugettext_lazy('Designation'),widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Designation'}))
    image = forms.FileField(label=ugettext_lazy('Image'), required=False, widget=forms.FileInput(attrs={'class':  'lbl_img_border'}))

    class Meta:
        model = Profile
        fields = ('email', 'phone', 'image', "Employee_Code", 'Department', 'Designation',)

    def save(self, commit=True):
        instance = super(ProfileInlineForm, self).save(commit=False)
        if self.cleaned_data.get('remove_photo') and self.cleaned_data.get("image"):
            try:
                os.unlink(instance.image.path)
            except OSError:
                pass
            instance.image = None
        if commit:
            instance.save()
        return instance

class CaptchaTestForm(forms.Form):
    captcha = CaptchaField()


class DeactivateUserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['is_active']


class SecurityForm(forms.ModelForm):
    class Meta:
        model = Security
        fields = ('is_activate', )


class ResetPasswordForm(UserCreationForm):
    password1 = forms.CharField(label=ugettext_lazy('Password'), widget=forms.PasswordInput(attrs={'class':'form-control', 'placeholder':'Password'}))
    password2 = forms.CharField(label=ugettext_lazy('Confirm Password'), widget=forms.PasswordInput(attrs={'class':'form-control', 'placeholder':'Confirm Password'}))

    class Meta:
        model = User
        fields = ('password1', 'password2',)


class UserRolesPermissionForm(forms.ModelForm):
    choices = (
        ('True', 'Yes'),
        ('False', 'No')
    )

    is_superuser = forms.BooleanField(label=ugettext_lazy('Super User'), required=False)
    is_active = forms.BooleanField(label=ugettext_lazy('Active'), required=False)
    is_manager = forms.BooleanField(label=ugettext_lazy('Manager'), required=False)
    is_checker = forms.BooleanField(label=ugettext_lazy('Checker'), required=False)
    is_maker = forms.BooleanField(label=ugettext_lazy('Maker'), required=False)

    class Meta:
        model = User
        fields = ('is_superuser', 'is_active', 'is_manager', 'is_checker', 'is_maker')
        
        
class PageRecordForm(forms.Form):
	records = forms.ChoiceField(choices=PAGE_RECORD_CHOICES, label=ugettext_lazy('Records Per Page'), widget=forms.Select(), required=False)
