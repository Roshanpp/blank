document.getElementById("yz").onchange = function () {
    // console.log("HELLO IMAGE UPLAOD");
    var reader = new FileReader();
    var canvas = document.getElementById('imageCanvas');
    var ctx = canvas.getContext('2d');
    var thumbSize = 33;
    var filePath = $(this)[0].value;
    var extn = filePath.substring(filePath.lastIndexOf('.') + 1).toLowerCase();
    console.log(filePath);
    if (extn == "gif" || extn == "png" || extn == "jpg" || extn == "jpeg"){
      reader.onload = function (e) {
        console.log("Image");
        var img = new Image();
        img.onload = function(){
            // console.log(filePath);
            canvas.width = thumbSize;
            canvas.height = thumbSize;
            ctx.drawImage(img,0,0,thumbSize,thumbSize);
        }
        img.src = event.target.result;
      }

    }else if (extn == 'pdf' || extn == 'PDF' ){
      reader.onload = function (e) {

        var img = new Image();

        img.onload = function(){
            canvas.width = thumbSize;
            canvas.height = thumbSize;
            ctx.drawImage(img,0,0,thumbSize,thumbSize);
        }
        img.src = pdfurl
      }
    }else if (extn == 'docx' || extn == 'docm' || extn == 'doc' ){
      reader.onload = function (e) {
        console.log("WORD");
        var img = new Image();

        img.onload = function(){
            canvas.width = thumbSize;
            canvas.height = thumbSize;
            ctx.drawImage(img,0,0,thumbSize,thumbSize);
        }
        img.src = wordurl
      }
    }else if (extn == 'xls' || extn == 'xlsb' || extn == 'xlsm' || extn == 'xlsx'){
      reader.onload = function (e) {
        console.log("EXCEL");
        var img = new Image();

        img.onload = function(){
            canvas.width = thumbSize;
            canvas.height = thumbSize;
            ctx.drawImage(img,0,0,thumbSize,thumbSize);
        }
        img.src = excelurl
      }
    }else if (extn == 'txt'){
      reader.onload = function (e) {
        console.log("TEXT");
        var img = new Image();

        img.onload = function(){
            canvas.width = thumbSize;
            canvas.height = thumbSize;
            ctx.drawImage(img,0,0,thumbSize,thumbSize);
        }
        img.src = texturl
      }
    }else if (extn == 'csv'){
      reader.onload = function (e) {
        console.log("TEXT");
        var img = new Image();

        img.onload = function(){
            canvas.width = thumbSize;
            canvas.height = thumbSize;
            ctx.drawImage(img,0,0,thumbSize,thumbSize);
        }
        img.src = csvurl
      }
    }
    reader.readAsDataURL(this.files[0]);

}
