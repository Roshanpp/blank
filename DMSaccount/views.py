from django.shortcuts import render,  redirect, HttpResponseRedirect, HttpResponse, get_object_or_404
from django.contrib.auth.decorators import user_passes_test, login_required
from django.urls import reverse
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth import update_session_auth_hash
from django.contrib import messages
from django.http import JsonResponse
from django.core.exceptions import PermissionDenied
from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes, force_text
from django.core.mail import EmailMessage, send_mail
from django.forms.models import inlineformset_factory
from django.conf import settings
from django.views.decorators.cache import never_cache
from django.core import serializers
from django.core.paginator import Paginator,PageNotAnInteger
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from django.utils.translation import gettext as _
from django.contrib.auth import get_user_model
from django.utils.safestring import mark_safe
from django.contrib.sessions.models import Session
from django.contrib.auth.decorators import login_required

import os
import json
import urllib
import http.client

from .tokens import account_activation_token, reset_activation_token
from .forms import PhoneForm, SignUpForm, CaptchaTestForm, DeactivateUserForm, SecurityForm, EditProfileForm, ProfileInlineForm, ResetPasswordForm, UserRolesPermissionForm, PageRecordForm
from .models import Profile, Security, FaceImages , LoginImages, UserSetting
from .security import admin_security, captcha_enbales, security_error
import numpy as np
import cv2
import pickle
#import face_recognition
from PIL import Image
import gzip
from pathlib import Path
import base64
from django.core.files.base import ContentFile
import re
from datetime import datetime
import time
from DMSapp.pagination import paginate
from django.contrib.auth.hashers import check_password
from django.contrib.auth.hashers import make_password
User = get_user_model()


@never_cache
def signup(request):  #Signup Modul
	login_security = admin_security()
	if request.method == 'POST':
		form = SignUpForm(request.POST)
		profile_form = PhoneForm(request.POST)
		if form.is_valid() and profile_form.is_valid():
			user = form.save(commit=False)
			user.is_active = True  #Make it False if you dont want user to activate his account at first
			user.email = profile_form.cleaned_data.get('email')

			profile=profile_form.save(commit=False)
			user.save()
			current_site = get_current_site(request)
			mail_subject = 'Activate your blog account.'
			context = { 'user': user, 'domain': current_site.domain, 'uid': urlsafe_base64_encode(force_bytes(user.pk)), 'token': account_activation_token.make_token(user)}
			message = render_to_string('acc_active_email.html',context) 
			to_email = profile_form.cleaned_data.get('email')
			# send_mail(mail_subject, message, settings.EMAIL_HOST_USER, [to_email],
			# 		  fail_silently=False)
			profile.username=user
			if 'image' in request.FILES:
				profile.image = request.FILES['image']
			profile.save()
			messages.success(request, 'Your profile has been created successfully.!!')
			#print(profile)
			return HttpResponseRedirect(reverse('signup'))
			# return JsonResponse({'success': True})
			# return HttpResponseRedirect(reverse('DMSaccount:signup_complete'))

	else:
		form = SignUpForm()
		#Changing the Field name in fRontend from backend.
		x, y = form.fields["password1"], form.fields["password2"]
		x.label, y.label = _("Password"), _("Confirm Password")
		profile_form = PhoneForm()

		z = profile_form.fields["image"]
		z.label = _("Profile Pic")
	context = {'form': form,'profile_form':profile_form,'login_security': login_security}
	return render(request, 'signup.html', context)


def is_empty(any_structure):
	if len(any_structure) != 0:
		print('Structure is not empty.')
		return False
	else:
		print('Structure is empty.')
		return True


@csrf_exempt
def snataken(request): #Module used for taking the snap if face recognition is enabled.
	import os
	if request.method == 'POST':
		ans = request.POST.get('data')

		username=request.POST.get('userName')
		print('usernamedffdfdfdffdf',username)
		header, data = ans.split(';base64,')
		ext=header.split('/')[-1]

		media_dir = settings.DATASET_PATH

		new_dir_path = os.path.join(media_dir , username)


		try:
			decoded_file = base64.b64decode(data)
		except TypeError:
			TypeError('invalid_image')
		img = ContentFile(decoded_file, name=username+'.'+ext)

		p = FaceImages.objects.filter(username=username)
		if p:
			os.remove(settings.MEDIA_ROOT + '/MyImages/' + username +'.'+ ext)
			for value in p:
				value_id = value.id

			p = FaceImages(pk=value_id, username=username, Image1=img)
			p.save()
		else:
			print('in else statement')
			p = FaceImages(username=username, Image1=img)
			p.save()
			oldmask = os.umask(000)
			os.mkdir(new_dir_path,0o777)
			os.umask(oldmask)

		face_cascade = cv2.CascadeClassifier(settings.STATIC_ROOT +'/cascades/data/haarcascade_frontalface_alt2.xml')
		print('face_cascade completed..!!')
		pil_image=Image.open(settings.MEDIA_ROOT+'/MyImages/'+ username + '.' + ext).convert('L')
		frame = np.array(pil_image, 'uint8')
		pil_image.close()
		#frame = cv2.imread(settings.MEDIA_ROOT+'/MyImages/'+username+'.'+ext)
		sample = 0
		print('Image to numpy completed..!')
		while (True):

			#gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
			print('In while loop for sample', sample)
			faces = face_cascade.detectMultiScale(
				frame,
				scaleFactor=1.3,
				minNeighbors=3,
				minSize=(30, 30)
			)
			if not is_empty(faces):
				if faces.any():
					print('faces', faces)
					print('detection of faces for sample', sample )
					for (x, y, w, h) in faces:
						sample += 1
						print("sample", sample)
						roi_gray = frame[y:y + h, x:x + w]

						cv2.imwrite(new_dir_path +'/'+ username+"." + str(sample) + ".jpg", roi_gray)

						color = (255, 0, 0)  # BGR
						stroke = 2
						end_cord_x = x + w
						end_cord_y = y + h
						cv2.rectangle(frame, (x, y), (end_cord_x, end_cord_y), color, stroke)
						print('sample', sample, 'completed..!')
				else:
					# break;
					return JsonResponse({'Face_Captured': False})
			else:
				return JsonResponse({'Face_Captured': False})
			if (sample == 5):
				print('five samples printed..!')
				break
		# cap.release()
		cv2.destroyAllWindows()

		return JsonResponse({'Face_Captured':True})
	else:
		return JsonResponse({'Face_Captured': False})

# @csrf_exempt
# def snataken(request):
# 	import datetime
# 	import time
# 	from datetime import timedelta
# 	import calendar
# 	import os
# 	if request.method == 'POST':
# 		ans = request.POST.get('data')
# 		print('data snaptaken',ans)
# 		username=request.POST.get('userName')
# 		print('username',username)
# 		header, data = ans.split(';base64,')
# 		ext=header.split('/')[-1]
# 		try:
# 			decoded_file = base64.b64decode(data)
# 		except TypeError:
# 			TypeError('invalid_image')
# 		img = ContentFile(decoded_file, name=username+'.'+ext)
# 		print(img)
#
# 		p = FaceImages(username=username, Image1=img)
# 		p.save()
#
# 		face_cascade = cv2.CascadeClassifier(settings.BASE_DIR+'/cascades/data/haarcascade_frontalface_alt2.xml')
#
# 		recognizer = cv2.face.LBPHFaceRecognizer_create()
# 		# cap = cv2.VideoCapture(settings.MEDIA_ROOT+'/MyImages/'+username+'.'+ext)
# 		sample = 0
#
# 		frame = cv2.imread(settings.MEDIA_ROOT+'/MyImages/'+username+'.'+ext)
# 		# import numpy as np
# 		# frame = np.array(img1, dtype=np.uint8)
# 		# print(frame)
# 		while (True):
# 			# Capture frame by frame
# 			# ret,frame=cap.read()
#
# 			print("GROP")
# 			gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
#
# 			# face_cascade=face_cascade.load('cascades/data/haarcascade_frontalface_alt2.xml')
# 			# print(face_cascade)
# 			faces = face_cascade.detectMultiScale(
# 			gray,
# 			scaleFactor=1.3,
# 			minNeighbors=3,
# 			minSize=(30,30))
#
# 			print(faces)
# 			for (x, y, w, h) in faces:
# 				print("KKLL")
# 				sample += 1
# 				roi_gray = gray[y:y + h, x:x + w]
# 				print(roi_gray)
# 				# roi_color = frame[y:y + h, x:x + w]
# 				dataSet = settings.MEDIA_ROOT+'/dataSet/'
# 				file_name = username+'_'+str(sample)+'_' + ".png"
# 				# file_name_jpeg = time.strftime("%Y_%m_%d-%H:%M:%S") + ".jpeg"
# 				print(file_name)
# 				cv2.imwrite(os.path.join(dataSet, file_name), roi_gray)
# 				# cv2.imwrite(os.path.join(dataSet, file_name_jpeg), roi_gray)
#
# 				color = (255, 0, 0)  # BGR
# 				stroke = 2
# 				end_cord_x = x + w
# 				end_cord_y = y + h
# 				cv2.rectangle(frame, (x, y), (end_cord_x, end_cord_y), color, stroke)
# 				# cv2.waitKey(100)
# 			# cv2.imshow('frame', frame)
# 			# cv2.waitKey(1)
# 			if sample == 20:
# 				break
#
# 		# cap.release()
# 		cv2.destroyAllWindows()
#
#
# 		# return HttpResponse('Please confirm your email address to complete the registration')
# 		return JsonResponse({'success':True})
# 	else:
# 		return JsonResponse({'success': False})


@csrf_exempt
def snatakenforlogin(request): #Taking smap at the time of login if FR is enabled
	import cv2
	import numpy as np
	if request.method == 'POST':
		ans = request.POST.get('data')
		username=request.POST.get('userName')
		print('username',username)
		header, data = ans.split(';base64,')
		ext=header.split('/')[-1]
		try:
			decoded_file = base64.b64decode(data)
		except TypeError:
			TypeError('invalid_image')
		img = ContentFile(decoded_file, name=username+'.jpg')


		try:
			images = LoginImages.objects.get(username=username)
			if images:
				images.delete()
		except:
			pass
		p = LoginImages(username=username, Image1=img)
		p.save()
		print('successfully Read...')

		# img = os.path.join(settings.MEDIA_ROOT + '/LogImages/' + username + '.jpg')
		# print(settings.MEDIA_ROOT + '/LogImages/' + username + '.jpg')
		# print('got image',img)
		# pil_image=Image.open(img)
		# img = np.array(pil_image, 'uint8')
		# pil_image.close()
		# #img = cv2.imread(img)
		# print('successfuly read......', img)
		#
		# alpha = 2
		# beta = 25
		#
		# result = cv2.addWeighted(img, alpha, np.zeros(img.shape, img.dtype), 0, beta)
		# print('successfully weighted.....')
		#
		# cv2.imwrite(settings.MEDIA_ROOT + '/LogImages/' + username + '.jpg', result)
		# print('successfully written')
		# cv2.destroyAllWindows()

		return JsonResponse({'success':True})
	else:
		return JsonResponse({'success': False})


# def faceRecognizer(request,username):
#     print('Start recognizing faces...')
#     face_cascade = cv2.CascadeClassifier(
#         settings.STATIC_ROOT + '/cascades/data/haarcascade_frontalface_alt2.xml')
#     cv2.destroyAllWindows()
#     print('face_cascade', face_cascade)
#     print('path', settings.MEDIA_ROOT + '/LogImages/')
#     print(username)
#     image = Image.open(settings.MEDIA_ROOT + '/LogImages/' + username + '.jpg').convert('L')
#     frame = np.array(image, 'uint8')
#     image.close()
#     #frame = cv2.imread(settings.MEDIA_ROOT + '/LogImages/' + username + '.jpg',cv2.IMREAD_COLOR)
#     print('Read frame....')
#
#     print(frame)
#     #gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
#     faces = face_cascade.detectMultiScale(
#         frame,
#         scaleFactor=1.3,
#         minNeighbors=3,
#         minSize=(30, 30)
#     )
#     # faces = list(faces)
#     print("Face", faces)
#     if not is_empty(faces):
#
#         if faces.any():
#
#             print("Faces are here",faces)
#             for (x, y, w, h) in faces:
#                 print("KKLL")
#
#                 roi_gray = frame[y:y + h, x:x + w]
#
#
#                 cv2.imwrite(settings.MEDIA_ROOT + "/Logdataset/" + username + ".jpg", roi_gray)
#                 print("In Recognizer", username)
#
#                 color = (255, 0, 0)  # BGR
#                 stroke = 2
#                 end_cord_x = x + w
#                 end_cord_y = y + h
#                 cv2.rectangle(frame, (x, y), (end_cord_x, end_cord_y), color, stroke)
#
#             proname = username + '.5.jpg'
#             print('proname',proname)
#             frameNew = cv2.imread(settings.MEDIA_ROOT + "/Logdataset/" + username + '.jpg')
#             print('frameNew',frameNew)
#
#             face_1_image = face_recognition.load_image_file(settings.MEDIA_ROOT + "/dataSet/" + username + '/' + str(proname))
#             print(settings.MEDIA_ROOT + "/dataSet/" + username + '/' + str(proname))
#
#             print('face_1_image',face_1_image)
#             print('face_encodings for recognition',face_recognition.face_encodings(face_1_image))
#             face_1_face_encoding = face_recognition.face_encodings(face_1_image)
#             print('face_1_face_encoding',face_1_face_encoding)
#             print('length', len(face_1_face_encoding))
#             if len(face_1_face_encoding) > 0:
#                 face_1_face_encoding = face_1_face_encoding[0]
#
#                 print('face_1_face_encoding',face_1_face_encoding)
#                 rgb_small_frame = frameNew[:, :, ::-1]
#                 print('rgb_small_frame', rgb_small_frame)
#                 face_locations = face_recognition.face_locations(rgb_small_frame, number_of_times_to_upsample=2)
#                 print('face_locations',face_locations)
#                 if face_locations:
#                     face_encodings = face_recognition.face_encodings(rgb_small_frame, face_locations )
#                     print('face_encodings', face_encodings)
#                 else:
#                     print("No Face locations found..!!")
#                     messages.error(request, 'Image is blur or not bright enough!')
#                     return False
#
#                 check = face_recognition.compare_faces(face_1_face_encoding, face_encodings)
#                 if check[0]:
#                     return True
#                 else:
#                     messages.error(request, 'Invalid User!')
#                     return False
#             else:
#                 messages.error(request, 'Image is blur or not bright enough or Face is too Far from Camera!')
#
#                 return False
#             cv2.destroyAllWindows()
#         else:
#             return False
#         cv2.destroyAllWindows()
#     else:
#         messages.error(request, 'Face Not Found.')
#         return False


def faceRecognizer(request,username):
	print('Start recognizing faces...')
	face_cascade = cv2.CascadeClassifier(
		settings.STATIC_ROOT + '/cascades/data/haarcascade_frontalface_alt2.xml')
	cv2.destroyAllWindows()
	print('face_cascade', face_cascade)
	print('path', settings.MEDIA_ROOT + '/LogImages/')
	print(username)
	image = Image.open(settings.MEDIA_ROOT + '/LogImages/' + username + '.jpg').convert('L')
	frame = np.array(image, 'uint8')
	image.close()
	#frame = cv2.imread(settings.MEDIA_ROOT + '/LogImages/' + username + '.jpg',cv2.IMREAD_COLOR)
	print('Read frame....')

	print(frame)
	#gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

	faces = face_cascade.detectMultiScale(
		frame,
		scaleFactor=1.3,
		minNeighbors=3,
		minSize=(30, 30)
	)

	# faces = list(faces)
	print("Face", faces)
	if not is_empty(faces):

		if faces.any():

			print("Faces are here",faces)
			for (x, y, w, h) in faces:
				print("KKLL")

				roi_gray = frame[y:y + h, x:x + w]


				cv2.imwrite(settings.MEDIA_ROOT + "/Logdataset/" + username + ".jpg", roi_gray)
				print("In Recognizer", username)

				color = (255, 0, 0)  # BGR
				stroke = 2
				end_cord_x = x + w
				end_cord_y = y + h
				cv2.rectangle(frame, (x, y), (end_cord_x, end_cord_y), color, stroke)
			count = 0
			list_of_faces = []

			while(count != 5):
				count += 1
				proname = username + '.' + str(count) + '.jpg'
				print('proname', proname)
				frameNew = cv2.imread(settings.MEDIA_ROOT + "/Logdataset/" + username + '.jpg')
				print('frameNew', frameNew)

				face_1_image = face_recognition.load_image_file(
					settings.MEDIA_ROOT + "/dataSet/" + username + '/' + str(proname))
				print(settings.MEDIA_ROOT + "/dataSet/" + username + '/' + str(proname))

				print('face_1_image', face_1_image)
				print('face_encodings for recognition', face_recognition.face_encodings(face_1_image))
				face_1_face_encoding = face_recognition.face_encodings(face_1_image)
				print('face_1_face_encoding', face_1_face_encoding)
				print('length', len(face_1_face_encoding))
				if len(face_1_face_encoding) > 0:
					face_1_face_encoding = face_1_face_encoding[0]

					print('face_1_face_encoding', face_1_face_encoding)
					# small_frame = cv2.resize(frameNew, (0, 0), fx=0.25, fy=0.25)
					# print(small_frame)
					rgb_small_frame = frameNew[:, :, ::-1]
					print('rgb_small_frame', rgb_small_frame)
					face_locations = face_recognition.face_locations(rgb_small_frame, number_of_times_to_upsample=2)
					print('face_locations', face_locations)
					face_encodings = face_recognition.face_encodings(rgb_small_frame, face_locations)
					print('face_encodings', face_encodings)
					if face_locations:
						check = face_recognition.compare_faces(face_1_face_encoding, face_encodings)
						print('check', check)
					else:
						check = [False]
					if check[0]:
						list_of_faces.append(0)
					else:
						list_of_faces.append(1)
				else:
					list_of_faces.append(2)

				total_count = max(set(list_of_faces), key=list_of_faces.count)
				print('list of faces', list_of_faces)

			cv2.destroyAllWindows()
			if total_count == 0:
				return True
			elif total_count == 1:
				messages.error(request, 'Invalid User!')
				return False
			elif total_count == 2:
				messages.error(request, 'Image is blur or not bright enough or Face is too Far from Camera!')
				return False
		else:
			return False
		cv2.destroyAllWindows()
	else:
		messages.error(request, 'Face Not Found.')
		return False


def camera(request,user, forType):
	print(user)
	context = {'user':user, 'forType': forType}
	return render(request, 'camera.html',context)

def camera2(request,user):
	print(user)
	context = {'user':user}
	return render(request, 'camera2.html',context)

def signup_complete(request):
	return render(request, 'signup_complete.html')

def signup_completed(request):
	return render(request, 'signup_completed.html')


def activate(request, uidb64, token):
	try:
		uid = force_text(urlsafe_base64_decode(uidb64))
		user = User.objects.get(pk=uid)
		print(user)
	except(TypeError, ValueError, OverflowError, User.DoesNotExist):
		user = None
	if user is not None and account_activation_token.check_token(user, token):
		user.is_active = True
		user.save()
		return HttpResponseRedirect(reverse('DMSaccount:signup_completed'))
		# return HttpResponse('Thank you for your email confirmation. Now you can login your account.')
	else:
		print(force_text(urlsafe_base64_decode(uidb64)))
		return HttpResponse('Activation link is invalid!')

		
def check_stored_session(request, session):
	Sessions = Session.objects.filter(session_key=session)
	if Sessions:
		for row in Sessions:
			row.delete()
		return JsonResponse({'success': True})
	else:
		return JsonResponse({'success':True})


def user_logout_on_confirm(request):
	logout(request)
	return JsonResponse({'success':True})
	

@never_cache   #decorator for not caching this API's data in browser
@csrf_protect
def login1(request):
	login_security = admin_security()
	aes_pass = settings.AES_PASSWORD
	#for Speaker
	DMS = 'DMS'
	#Remove above line if speaker is not required

	if request.user.is_authenticated:
		return HttpResponseRedirect(reverse('home'))
	if request.method == 'POST':
		username = request.POST.get('auth')
		enc_password = request.POST.get('auth_key')
		password = security_error(enc_password, settings.AES_PASSWORD)
		user = authenticate(username=username, password=password)
		print('user', user, username, password)
#		if user:
#			User.objects.filter(username=user).update(is_active=True)  # Added for Cosmos
#			user = User.objects.get(username=user)
#			a = Profile.objects.filter(username=user)
#			if not a:
#				Profile.objects.create(username=user)
		
		stored_session = ''
		if user is not None:
		

#			Sessions = Session.objects.all()
#			now = datetime.now()
#			for row in Sessions:
#				print(row.get_decoded().get('_auth_user_id'))
#				print(user.id)
#				if str(row.get_decoded().get("_auth_user_id")) == str(user.id):
#					print('Same sessions')
#					print(row)
#					seconds_left = (row.expire_date - now).total_seconds()
#					if seconds_left <= 0:
#						row.delete()
#					else:
#						stored_session = row
#					#row.delete()

#			request.session['logged'] = user.id
			
			if user.is_active:
				if login_security == 1:
					login(request, user, backend='django.contrib.auth.backends.ModelBackend')
				elif login_security == 2:
					form = CaptchaTestForm(request.POST)
					if form.is_valid():
						human = True
						login(request, user, backend='django.contrib.auth.backends.ModelBackend')
					else:
						messages.error(request, 'Invalid reCAPTCHA. Please try again.')
						return redirect('user_login')

				elif login_security == 3:
					var = captcha_enbales(request)
					if var is True:
						login(request, user, backend='django.contrib.auth.backends.ModelBackend')
					else:
						return redirect('user_login')

				elif login_security == 4:
					userId = user.pk
					return HttpResponseRedirect(reverse('send_otp', args=[userId]))

				elif login_security == 5:
					var = request.POST.get('success')
					var1=request.POST.get('sum')
					if var == var1:
						login(request, user, backend='django.contrib.auth.backends.ModelBackend')
					else:
						messages.error(request, '"Invalid Captcha token" . Please try again.')
						return redirect('user_login')
				elif login_security == 6:
					faceRegistered = FaceImages.objects.filter(username = username)
					if faceRegistered:
						recognized=faceRecognizer(request,username)
						print('recognized',recognized)
						if recognized:
							login(request, user, backend='django.contrib.auth.backends.ModelBackend')
						else:
							return redirect('user_login')
					else:
						messages.info(request, mark_safe("Face Not Registered. Click <a href='#' id='mylink' class='"+username+"'>here</a> to register your face."))
						return redirect('user_login')
				if request.POST.get('next'):
					if request.POST.get('next') == '/':
						if stored_session:
							messages.error(request, stored_session)
							return HttpResponseRedirect(reverse('home'))
						return HttpResponseRedirect(reverse('home', args=[DMS]))
					if stored_session:
						print(stored_session ,'stored')
						messages.error(request, stored_session)
						return HttpResponseRedirect(request.POST.get('next', reverse('home')))
					return HttpResponseRedirect(request.POST.get('next', reverse('home')))
				else:
					if stored_session:
						messages.error(request, stored_session)
						return HttpResponseRedirect(reverse('home'))
					return HttpResponseRedirect(reverse('home', args=[DMS]))
			else:
				return HttpResponse("Your account was inactive.")
		else:
			messages.error(request, 'Invalid Login Credentials Given. Please try again.')
			return redirect('user_login')
	else:
		form = CaptchaTestForm()
		context =  {'login_security': login_security, 'aes_pass': aes_pass}
		return render(request, 'login.html', locals())


def enter_otp(request, id, mobile_no):
	context = {'userid': id, 'mobileNo': mobile_no}
	return render(request, 'users/otppage.html', context)


def verify_otp(request, id, mobile_no):
	auth = settings.OTP_AUTH_KEY

	if request.method == "POST":
		enteredOtp = request.POST.get('otp')
		conn = http.client.HTTPSConnection("control.msg91.com")
		payload = ""
		headers = {'content-type': "application/x-www-form-urlencoded"}
		conn.request("POST", "/api/verifyRequestOTP.php?authkey="+auth+"&mobile="+mobile_no+"&otp=" + enteredOtp+"",
					 payload, headers)
		res = conn.getresponse()
		data = res.read()
		ans = json.loads(data.decode("utf-8"))

		if ans['type'] == 'success':
			print(ans['type'])
			for e in Profile.objects.filter(username_id=id):
				username = e.username
				print(username)
				login(request, username, backend='django.contrib.auth.backends.ModelBackend')
				return HttpResponseRedirect(reverse('home'))

		elif ans['type'] == 'error':
			context = {'userid': id, 'mobileNo': mobile_no}
			return render(request, 'users/invalidotp.html',context)
	else:
		return render(request, 'users/otppage.html')


def send_otp(request, id):
	auth = settings.OTP_AUTH_KEY
	otpexp = 5
	email_id = ''
	user = Profile.objects.get(username_id=id)
	for v in User.objects.filter(username=user):
		email_id = v.email


	for m in Profile.objects.filter(username_id=id):
		mobile_no=m.phone

	conn = http.client.HTTPConnection("control.msg91.com")
	payload = ""
	print(email_id)
	print(user)
	conn.request("POST", "/api/sendotp.php?template=&otp_length=&authkey="+auth+"&message=&sender=&mobile=" + str(mobile_no) +
				 "&otp=&otp_expiry="+str(otpexp)+"&email="+email_id+"", payload)
	res = conn.getresponse()
	data = res.read()
	return HttpResponseRedirect(reverse('enterOTP', args=[id, mobile_no]))


def resend_otp(request, id, mobile_no):
	auth = '264199AaxLWEo00P5c6ecde4'
	otpexp = 5
	email_id = ''
	user = Profile.objects.get(username_id=id)
	for v in User.objects.filter(username=user):
		email_id = v.email


	conn = http.client.HTTPConnection("control.msg91.com")
	payload = ""
	conn.request("POST", "/api/sendotp.php?template=&otp_length=&authkey="+auth+"&message=&sender=&mobile=" +
				 str(mobile_no)+"&otp=&otp_expiry="+str(otpexp)+"&email="+email_id+"", payload)
	res = conn.getresponse()
	data = res.read()
	return HttpResponseRedirect(reverse('enterOTP', args=[id, mobile_no]))


@login_required(login_url='/login/')
def user_profile(request, username=None):
	if username:
		profile = Profile.objects.get(username__username=username)
		user = User.objects.get(username=username)
		user_last_login = user.last_login
		date_joined = user.date_joined
		splited_date = str(date_joined).split(' ')[1]
		if '.' in splited_date:
			date_joined = datetime.strptime(str(date_joined), '%Y-%m-%d %H:%M:%S.%f').strftime('%b %d, %Y %I:%M %p')
		else:
			date_joined = datetime.strptime(str(date_joined), '%Y-%m-%d %H:%M:%S').strftime('%b %d, %Y %I:%M %p')
	else:
		user = request.user
		profile = Profile.objects.get(username=user)
		user_last_login = user.last_login
		date_joined = user.date_joined
		splited_date = str(date_joined).split(' ')[1]
		if '.' in splited_date:
			date_joined = datetime.strptime(str(date_joined), '%Y-%m-%d %H:%M:%S.%f').strftime('%b %d, %Y %I:%M %p')
		else:
			date_joined = datetime.strptime(str(date_joined), '%Y-%m-%d %H:%M:%S').strftime('%b %d, %Y %I:%M %p')
	args = {'last_login': user_last_login, 'date_joined': date_joined, 'profile': profile, 'user': user}
	return render(request, 'users/user_profile.html', args)


def all_users(request):
	users = User.objects.filter(is_active=True, is_superuser=False)
	qs_json = serializers.serialize('json', users)
	qs_json=json.loads(qs_json)

	page = request.GET.get('page',1)
	#paginator = Paginator(qs_json, 10)
	#page_records=paginator.count

	count=0

	for i in qs_json:
		count+=1
		i['fields']['srNo']=str(count)

	qs_json = paginate(request,qs_json,page)
	args = {'files': qs_json}
	return render(request, 'users/All_users.html', args)


def change_password(request):
	if request.method == 'POST':
		form = PasswordChangeForm(request.user, request.POST)
		if form.is_valid():
			user = form.save()
			update_session_auth_hash(request, user)  # Important!
			messages.success(request, 'Your password was successfully updated!')
			return redirect('home')
		else:
			messages.error(request, 'Please correct the error below.')
	else:
		form = PasswordChangeForm(request.user)
	args = {'form':form}
	return render(request, 'password.html', args) 
	

def user_logout(request):
	request.session.cycle_key()
	logout(request)
	return HttpResponseRedirect(reverse('user_login'))


def admin(request):
	if request.user.is_superuser:
		return render(request, 'admin.html')
	else:
		HttpResponse("Please login")


@login_required(login_url='/login/')
def edit_profile(request, pk):
	user = User.objects.get(pk=pk)
	profile = Profile.objects.get(username__username=user)
	form = EditProfileForm(instance=user)

	form_set = ProfileInlineForm(instance=profile)

	if request.user.is_authenticated:
		profile = Profile.objects.get(pk=profile.pk)
		print("here is profile:",profile)
		if request.method == "POST":
			form = EditProfileForm(request.POST, request.FILES, instance=user)
			form_set = ProfileInlineForm(request.POST, request.FILES, instance=profile)
			if form.is_valid():
				created_user = form.save(commit=False)

				if form_set.is_valid():
					email = form_set.cleaned_data['email']
					user.email = email
					created_user.save()
					form_set.save()
					request.session.cycle_key()
					messages.success(request, 'Profile Updated Successfully..!!')
				else:
					messages.error(request, 'Please correct below errors.')
			else:
				messages.error(request, 'Please correct below errors.')
		args = {'id': pk, 'form': form, 'form_set': form_set, 'profile': profile}
		return render(request, 'users/edit_profile.html', args)

	else:
		HttpResponse('You have been logged out. Please login again')


@user_passes_test(lambda u: u.is_superuser)
def delete_users(request,  pk):
	user = User.objects.get(pk=pk)
	data = dict()
	try:
		user.is_active = False
		user.save()
		print('User is deactivated')
		users = User.objects.filter(is_active=True, is_superuser=False)

		qs_json = serializers.serialize('json', users)
		qs_json = json.loads(qs_json)

		page = request.GET.get('page', 1)
		paginator = Paginator(qs_json, 10)

		count = 0
		for i in qs_json:
			count += 1
			i['fields']['srNo'] = str(count)
		try:
			qs_json = paginator.page(page)

		except PageNotAnInteger:
			qs_json = paginator.page(1)
		except EmptyPage:
			qs_json = paginator.page(paginator.num_pages)
		args = {'files': qs_json}
		data['html_book_list'] = render_to_string('users/all_users_data.html', args, request)
		return JsonResponse(data)

	except User.DoesNotExist:
		messages.error(request, "User doesnot exist")
		args = {'success': False}
		return JsonResponse(args)


@login_required(login_url='login/')
def deactivate_user_view(request, pk):
	user = User.objects.get(pk=pk)
	user_form = DeactivateUserForm(instance=user)
	if request.user.is_superuser:
		if request.method == "POST":
			user_form = DeactivateUserForm(request.POST, instance=user)
			if user_form.is_valid():
				deactivate_user = user_form.save(commit=False)
				user.is_active = False
				deactivate_user.save()
		args =  {"user_form": user_form}
		return render(request, "userprofile_del.html", args)
	else:
		raise PermissionDenied

####################################################################################################


def dms_settings(request):
	return render(request, 'settings.html')

@login_required(login_url='login/')
#All security measures deployed on DMS
def security(request):
	all_security = Security.objects.all()
	args = {'security': all_security}
	return render(request, 'security.html',args)

@login_required(login_url='login/')
def select_security(request, id):
	select = Security.objects.get(id=id)
	form = SecurityForm(request.POST, instance=select)
	if request.method == 'POST':
		if form.is_valid():
			form.save()
			return redirect('home')
	else:
		form = SecurityForm()
	args = {'form': form,'select':select}
	return render(request, 'security_change.html',args )


def toggle_security(request, id):
	security = Security.objects.get(pk=id)
	print(security.is_activate)
	try:
		if security.is_activate:
			print('DOne')
			security.is_activate = False
			security.save()
			return JsonResponse({'success': False})
		else:
			security.is_activate = True
			security.save()
			return JsonResponse({'success': True})
	except (KeyError, Security.DoesNotExist):
		return JsonResponse({'success': False})


'''Forget Password'''



#Rest password using email token functionality
def reset_password(request):
	data = []
	if request.method == 'POST':
		email = request.POST.get('email')
		email_ids = Profile.objects.all()
		match = re.match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$', email)
		if match == None:
			messages.error(request, 'Invalid Email Id.')

		else:

			for e in email_ids:
				value = e.email
				data.append(value)
			if email in data:
				current_site = get_current_site(request)
				mail_subject = 'Reset Your Password'
				context = { 'user': email, 'domain': current_site.domain, 'uid': urlsafe_base64_encode(force_bytes(email)), 'token': reset_activation_token.make_token(email), 'protocol': 'https' }
				message = render_to_string('registrations/password_reset_email.html',context) 

				send_mail(mail_subject, message, settings.EMAIL_HOST_USER, [email],
						  fail_silently=False)

				return render(request, 'registrations/password_reset_done.html')

			messages.error(request, 'There is no profile with this email ID')
		return HttpResponseRedirect(reverse('DMSaccount:reset'))
	else:
		return render(request, 'registrations/password_email_confirm.html')



def reset_token(request, uidb64, token):
	try:
		uid = force_text(urlsafe_base64_decode(uidb64))
		value = Profile.objects.filter(email=uid)
		for v in value:
			email = v.email

	except(TypeError, ValueError, OverflowError, Profile.DoesNotExist):
		email = None
	if email is not None and reset_activation_token.check_token(email, token):
		validlink = True
		uid = force_text(urlsafe_base64_decode(uidb64))
		email_id = User.objects.get(email=uid)
		if request.method == 'POST':
			form = ResetPasswordForm(request.POST, instance=email_id)
			if form.is_valid():
				pasword = form.cleaned_data.get('password1')
				form.save()
				messages.success(request, 'Your password has been changed successfully. You can')
				# return HttpResponseRedirect(reverse('DMSaccount:password_reset_complete'))
		else:
			form = ResetPasswordForm(instance=email_id)
		context = {'validlink': validlink, 'form': form}
		return render(request, 'registrations/password_reset_confirm.html', context)
	else:
		return HttpResponse('Activation link is invalid!')


def password_reset_complete(request):
	return render(request, 'registrations/password_reset_complete.html')


'''Validations in Signup'''


def validate_username(request):
	username = request.GET.get('username', None)
	data = {
		'is_taken': User.objects.filter(username__iexact=username).exists()
	}
	return JsonResponse(data)
def validate_email(request):
	email = request.GET.get('email', None)
	data = {
		'is_taken': User.objects.filter(email__iexact=email).exists()
	}
	return JsonResponse(data)


DEFAULT_PASSWORD_LIST_PATH = Path(__file__).resolve().parent.parent.parent / 'common-passwords1.txt.gz'


@csrf_exempt
def validate_pass(request, password_list_path=DEFAULT_PASSWORD_LIST_PATH):
	password=request.POST.get('password1',None)
	try:
		with gzip.open(str(password_list_path)) as f:
			common_passwords_lines = f.read().decode().splitlines()
	except IOError:
		with open(str(password_list_path)) as f:
			common_passwords_lines = f.readlines()

	passwords_list = {p.strip() for p in common_passwords_lines}

	data = {
		'is_common': False,
		 'is_number':False,
		 'is_upper_lower':False
		  }
	if password.lower().strip() in passwords_list:
		data['is_common']=True
	if  any(letter.islower() for letter in password) and any(letter.isupper() for letter in password) :
		data['is_upper_lower']=True
	if any(char.isdigit() for char in password):
		data['is_number'] =True
	return JsonResponse(data)


@login_required
def administration(request):
	return render(request, 'administration/administration.html')


@login_required
def user_roles(request):
	users = User.objects.all()

	profiles = Profile.objects.all() 
	context =  {'users': users,'profiles':profiles}
	return render(request, 'administration/user_roles.html', context)


@login_required
def change_role_for_user(request, user_id):
	requested_user = User.objects.get(id=user_id)
	form = UserRolesPermissionForm(instance=requested_user)
	if request.method == 'POST':
		form = UserRolesPermissionForm(request.POST, request.FILES, instance=requested_user)
		if form.is_valid():
			form.save()
			messages.success(request, 'Your request have been successfully saved.')
			return HttpResponseRedirect(reverse('DMSaccount:user_roles'))
	context = {'requested_user': requested_user, 'form': form}
	return render(request, 'administration/change_role_for_user.html', context )
   
   
#checkout changes for git made by supriya....

@login_required
def page_records_setting(request):

	if request.method == 'POST':
		form = PageRecordForm(data = request.POST)
		if form.is_valid():
			records = form.cleaned_data['records']
			print(records)
			user = UserSetting.objects.filter(user=request.user.pk)
			if user:
				UserSetting.objects.filter(user=request.user.pk).update(page_records=records)
			else:
				myuser = User.objects.get(id=request.user.pk)
				UserSetting.objects.create(user=myuser, page_records=records)
			messages.success(request, 'Settings Saved Successfully.!!')
			return HttpResponseRedirect(reverse('DMSaccount:page_records_setting'))
	else:
		form = PageRecordForm()
		context = {'form':form}
		return render(request,'page_records.html',context)
		
		
@login_required		
def change_password(request):
	if request.method == 'POST':
		new_pwd = request.POST.get('new_pwd')

		encrypted_password = make_password(new_pwd)

		user_ob = User.objects.filter(id = request.user.pk)
		user_ob.update(password = encrypted_password)
		messages.success(request, 'Password Updated Successfully')
	return render(request,'change_user_password.html')



def check_user_password(request):
	data = dict()
	if request.method == 'GET':
		input = request.GET.get('original_password')

		database_pwd = User.objects.get(id = request.user.pk)
		original = database_pwd.password

		if check_password(input, original):
			data['is_matched'] = True

		else:
			data['is_matched'] = False

		return JsonResponse(data)

