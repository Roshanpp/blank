from django.shortcuts import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth.models import User

from DMSaccount.models import Security, Profile
from django.contrib import messages
from django.conf import settings
from Crypto.Cipher import AES
import requests
import base64
import http.client
import json
import os

#Returns which security is enabled for login
def admin_security():
    list = [sec.id for sec in Security.objects.all() if sec.is_activate is not False]
    if list != []:
        print(list)
        value = max(list)
        return value
    else:
        print(list)
        return 1


#Returns the response of google captcha when it is enabled
def captcha_enbales(request):
    request.recaptcha_is_valid = None
    if request.method == 'POST':
        recaptcha_response = request.POST.get('g-recaptcha-response')
        data = {
            'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
            'response': recaptcha_response
        }
        r = requests.post('https://www.google.com/recaptcha/api/siteverify', data=data)
        result = r.json()
        print(result)
        if result['success']:
            request.recaptcha_is_valid = True
        else:
            request.recaptcha_is_valid = False
            messages.error(request, 'Invalid reCAPTCHA. Please try again.')
    return request.recaptcha_is_valid




def unpad(data):
    return data[:-ord(data[len(data)-1:])]


def security_error(value, key):
    print(value, key)
    encrypted = base64.b64decode(value)
    BLOCK_SIZE = 16
    IV = encrypted[:BLOCK_SIZE]
    print('length',len(IV))
    cipher = AES.new(key.encode('utf-8'), AES.MODE_CBC, IV)
    password2 = unpad(cipher.decrypt(encrypted[BLOCK_SIZE:])).decode('utf-8')
    return password2
