from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils import six


class TokenGenerator(PasswordResetTokenGenerator):
    def _make_hash_value(self, user, timestamp):
        return (
            six.text_type(user.pk) + six.text_type(timestamp) +
            six.text_type(user.is_active)
        )


account_activation_token = TokenGenerator()


class ResetPasswordGenerator(PasswordResetTokenGenerator):
    def _make_hash_value(self, email, timestamp):
        return (
            six.text_type(email) + six.text_type(timestamp)
        )


reset_activation_token = ResetPasswordGenerator()


class ExternallySharedToken(PasswordResetTokenGenerator):
    def _make_hash_value(self, file, timestamp):
        return (
            six.text_type(file.File1) + six.text_type(timestamp) +
            six.text_type(file.is_protected)
        )


File_encrypt_token = ExternallySharedToken()
