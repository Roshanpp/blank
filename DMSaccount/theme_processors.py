from django.contrib.auth import get_user_model
from DMSapp.choices import Theme_1, Theme_2, Theme_3, Theme_4, Default
from DMSaccount.models import UserSetting

User = get_user_model()


def categories_processor(request):
    if request.user.is_authenticated:
        #user = User.objects.get(id=request.user.id)
        user = UserSetting.objects.filter(user_id=request.user.pk)
        if user:
            for i in user:

                theme_color = i.theme
            selected_theme = Default
            if theme_color == 'Theme_1':
                selected_theme = Theme_1
            if theme_color == 'Theme_2':
                selected_theme = Theme_2
            if theme_color == 'Theme_3':
                selected_theme = Theme_3
            if theme_color == 'Theme_4':
                selected_theme = Theme_4
        else:
            selected_theme = Default

        # theme_color = ast.literal_eval(theme_color1)

        bg_color = selected_theme['bg_color']
        profile_title = selected_theme['profile_title']
        profile_content = selected_theme['profile_content']
        header_color = selected_theme['header_color']
        footer_color = selected_theme['footer_color']
        context = {'color': bg_color, 'header_color': header_color, 'footer_color': footer_color}
        return context

    else:
        context2 = {'None': None}
        return context2


